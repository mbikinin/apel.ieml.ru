/*
SQLyog Enterprise - MySQL GUI v8.12 
MySQL - 5.0.24a-standard : Database - ecu
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `title` varchar(250) default NULL,
  `preview` text,
  `text` text,
  `cdate` datetime default NULL,
  `show` tinyint(4) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `articles` */

/*Table structure for table `galleries` */

DROP TABLE IF EXISTS `galleries`;

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(250) default NULL,
  `show` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `galleries` */

insert  into `galleries`(`id`,`title`,`show`) values (1,'На главной',1),(2,'студенты',1);

/*Table structure for table `gallery_photos` */

DROP TABLE IF EXISTS `gallery_photos`;

CREATE TABLE `gallery_photos` (
  `id` int(11) NOT NULL auto_increment,
  `gallery_id` int(11) default NULL,
  `title` varchar(250) default NULL,
  `url` varchar(250) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_gallery_photos_gallery_id` (`gallery_id`),
  CONSTRAINT `FK_gallery_photos_gallery_id` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gallery_photos` */

insert  into `gallery_photos`(`id`,`gallery_id`,`title`,`url`) values (1,1,'Первая','1.jpg'),(2,1,'Вторая','2.jpg'),(3,1,'Третья','3.jpg'),(4,1,'Четвертая','4.jpg'),(5,2,'1','5.jpg');

/*Table structure for table `html_modules` */

DROP TABLE IF EXISTS `html_modules`;

CREATE TABLE `html_modules` (
  `id` int(11) NOT NULL auto_increment,
  `content` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `html_modules` */

insert  into `html_modules`(`id`,`content`) values (1,'<div class=\"language-center\">\r\n	<div><a href=\"http://lang.ec-univer.ru/\">\r\n		<img src=\"/design/images/cu/language_center.jpg\" alt=\"Центр языка\">\r\n	</a></div>\r\n\r\n</div>\r\n<div class=\"language-center\">\r\n	<div><a href=\"http://ec-univer.ru/bank/\">\r\n		<img src=\"/design/images/cu/bank.png\" alt=\"Банк ЭКУ\">\r\n	</a></div>\r\n</div>\r\n<div class=\"activ\">\r\n	<div><a href=\"/activ\">\r\n		<img src=\"/design/images/cu/sit_activist.gif\" alt=\"Клуб активистов\">\r\n	</a></div>\r\n</div>\r\n'),(2,'<div style=\"text-align:center;\">\r\n	<a href = \"/young/\"><img src=\"/design/images/cu/young.gif\" alt=\"\"></a><br/>\r\n</div>\r\n\r\n<div class=\"agni_spravka\" id=\"agni_spravka\">\r\n	<p>В группе компаний «Татнефть» в настоящее время более 27 тысяч молодых работников – это примерно треть трудового коллектива. Из них руководителей 1650, специалистов 5992, служащих 566, рабочих 19615 человек. Молодых специалистов, окончивших вузы и устроившихся на работу в компанию в течение трех лет после получения высшего образования, – 3706 человек. Высшее образование среди молодых имеют 9288 работников, средне-специальное – 7436, продолжают обучение в вузах 2243 человека.</p>\r\n	<p>В 2010 году в «Татнефть» было трудоустроено более 350 молодых специалистов различных специальностей. С каждым годом численность молодежи возрастает на 2-3%, что является результатом продуманной и последовательной корпоративной молодежной политики.</p>\r\n	<div></div>\r\n</div>'),(3,'<div class=\"rb-holder\">\r\n        <div><a href=\"http://ec-univer.ru/uprav/\">\r\n          <img src=\"/design/images/cu/uprav.png\" alt=\"Органы управления\">\r\n        </a></div>\r\n	\r\n        <div><a href=\"/plan/\">\r\n          <img src=\"/design/images/cu/uchplan.gif\" alt=\"Учебный план\">\r\n        </a></div>\r\n      \r\n        <div><a href=\"http://www.tatneft.ru/wps/wcm/connect/tatneft/progymnasium\">\r\n          <img src=\"/design/images/cu/ngdu_gymnasium.jpg\" alt=\"Гимназия НГДУ\">\r\n        </a></div>\r\n    <div><a href=\"/club/\">\r\n          <img src=\"/design/images/cu/agni.png\" alt=\"Клуб АГНИ\">\r\n        </a></div>\r\n        <div><a href=\"/m/upo\">\r\n          <img src=\"/design/images/cu/project.png\" alt=\"Управляй проектом\">\r\n        </a></div>\r\n	<div><a href=\"/looking_for_you/\">\r\n          <img src=\"/design/images/cu/look_for_you.gif\" alt=\"On-line защита\">\r\n        </a></div>\r\n        <div><a href=\"/on-line/\">\r\n          <img src=\"/design/images/cu/on-line.gif\" alt=\"On-line защита\">\r\n        </a></div>\r\n        <div><a href=\"http://elibrary.agni-rt.ru:8000/\">\r\n          <img src=\"/design/images/cu/library.gif\" alt=\"On-line защита\">\r\n        </a></div>\r\n        <div><a href=\"/cpk/\">\r\n          <img src=\"/design/images/cu/cpk.jpg\" alt=\"On-line защита\">\r\n        </a></div>\r\n        <div><a href=\"/safety/\">\r\n          <img src=\"/design/images/cu/sit_online.gif\" alt=\"Сит\">\r\n        </a></div>\r\n        <div><a nohref=\"\"> <br /><br /> <br /><br />\r\n\r\n          <img src=\"/design/images/cu/aforism.jpg\" alt=\"Афоризм\">\r\n        </a></div>\r\n\r\n      </div>'),(4,'<table>\r\n      <tr>\r\n      <td><a href=\"/lider.htm\"><img class=\"no_margins\" src=\"/design/images/ban_agni.png\" alt=\"Уроки лидерства\" border=\"0\" /></a></td>\r\n      <td><a href=\"http://lavushenko.ru/psychology/\"><img class=\"no_margins\" src=\"/design/images/cu/ps_pr.png\" alt=\"Психологический практикум\" border=\"0\" /></a></td>\r\n      </tr>\r\n      <tr>\r\n      <td><a href=\"http://lavushenko.ru/gracian/\"><img class=\"no_margins\" src=\"/design/images/cu/post_grac.png\" alt=\"Постулаты Грасина\" border=\"0\" /></a></td>\r\n      <td><a href=\"/jim_collins/\"><img class=\"no_margins\" src=\"/design/images/cu/jim.png\" alt=\"Постулаты Джима Коллинза\" border=\"0\" /></a></td>\r\n      </tr>\r\n      </table>'),(5,'<div class=\"f-lvp\">\r\n\r\n        <a href=\"http://lavushenko.ru/m/stock\"><img src=\"/design/images/cu/b1.jpg\" alt=\"\"></a>\r\n        <a href=\"http://lavushenko.ru/m/cyb\"><img src=\"/design/images/cu/b2.jpg\" alt=\"\"></a>\r\n        <a href=\"http://lavushenko.ru/m/brain_ring\"><img src=\"/design/images/cu/b3.jpg\" alt=\"\"></a>\r\n      </div>');

/*Table structure for table `html_pages` */

DROP TABLE IF EXISTS `html_pages`;

CREATE TABLE `html_pages` (
  `id` int(11) NOT NULL auto_increment,
  `text` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `html_pages` */

insert  into `html_pages`(`id`,`text`) values (1,'<p>\r\n	213213123213132</p>'),(2,'<div class=\"base\">\r\n	<img alt=\"\" src=\"/design/images/cu/base.jpg\" /></div>\r\n<h2>\r\n	ЭК-Универ &ndash; новые возможности</h2>\r\n<p>\r\n	Электронный Корпоративный Университет &ndash; это инновационный образовательный ресурс, открывающий новые возможности для обучения и профессионального развития специалистов. Преимуществами ЭК-Универа являются доступность обучения в любое время и в любом месте, использование интерактивных форм и методов обучения, применение современных ИТ-технологий для управления учебным процессом. <a href=\"#\" onclick=\"show_extra()\">Подробнее</a><br />\r\n	&nbsp;</p>'),(3,''),(4,'парампарарам'),(5,''),(6,''),(7,''),(8,''),(9,''),(10,''),(11,''),(12,''),(13,''),(14,''),(15,''),(16,''),(17,'<div align=\"center\"><strong>ОТКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО</strong><br />\r\n<strong> &laquo;ТАТНЕФТЬ&raquo; </strong><br />\r\n<strong>имени В.Д. Шашина</strong><br />\r\n</div>\r\n<br />\r\n<div align=\"right\"><strong>УТВЕРЖДЕНО<br />\r\nГенеральным директором <br />\r\n</strong></div>\r\n<br />\r\n<div align=\"center\"><strong>П Р О Т О К О Л</strong><br />\r\n</div>\r\n16 июня 2011 г.<br />\r\nАльметьевск<br />\r\n<br />\r\nзаседания Большого жюри и экзаменационной комиссии (согласно &laquo;Регламенту защиты дипломных проектов в рамках Корпоративного университета&raquo;) по определению победителя в конкурсе на получение рабочего места студентами специальностей &laquo;Экономика и управление на предприятиях нефтяной и газовой промышленности&raquo; и &laquo;Экономика труда&raquo; АГНИ<br />\r\n<br />\r\n<strong>Присутствовали:</strong><br />\r\nзаместитель генерального директора по экономике                           - В.П.Лавущенко <br />\r\nначальник экономического управления                                               - Р.С. Зиен <br />\r\nначальник отдела кадров                                                                       - Р.Р. Хамадьяров <br />\r\nначальника ООТиЗП                                                                              - В.Ю. Макаров <br />\r\nруководители экономических служб структурных подразделений и предприятий внешнего сервиса ОАО &laquo;Татнефть&raquo;<br />\r\n<strong><br />\r\n</strong>\r\n<div align=\"center\"><strong>ПОВЕСТКА ДНЯ:</strong><br />\r\n</div>\r\n<br />\r\nПодведение итогов конкурса на рабочее место по выпускникам АГНИ 2011 года специальностей &laquo;Экономика и управление на предприятиях нефтяной и газовой промышленности&raquo; и &laquo;Экономика труда&raquo;<br />\r\n<br />\r\n<div align=\"center\"><strong>ПОСТАНОВИЛИ:</strong><br />\r\n</div>\r\n<br />\r\n1.	Считать победителями конкурса:<br />\r\nПо специальности &laquo;Экономика и управление на предприятиях нефтяной и газовой промышленности&raquo;<br />\r\n1)	Давлетбаева Диляра Рафисовна<br />\r\n2)	Брендюрева Ирина Владимировна<br />\r\n3)	Козырева Алена Федоровна<br />\r\n4)	Галкаева Альбина Наилевна<br />\r\n5)	Курбатов Алексей Сергеевич<br />\r\n6)	Петрова Анжела Германовна<br />\r\n7)	Галимова Алина Анисовна<br />\r\n8)	Корнева Светлана Николаевна<br />\r\n9)	Ахмадиева Наиля Шамилевна<br />\r\n10)	Минхаирова Гульнур Илфатовна <br />\r\n11)	Муртазина Эльмира Ришатовна<br />\r\n12)	Майорова Любовь Владимировна<br />\r\n13)	Ситдикова Лена Альфритовна<br />\r\n14)	Фролова Екатерина Николаевна<br />\r\n15)	Валиева Гульназ Рифкатовна<br />\r\n16)	Абрамов Антон Николаевич<br />\r\n17)	Гилязова Алина Халиловна<br />\r\n18)	Гафурова Альбина Ильдусовна<br />\r\n19)	Хайртдинова Дина Ирековна<br />\r\n20)	Митковская Марина Викторовна<br />\r\n21)	Тихонова Мария Васильевна<br />\r\n22)	Хазиева Регина Ринатовна<br />\r\n23)	Ягафарова Регина Айдаровна<br />\r\n24)	Синякаева Миляуша Римовна<br />\r\n25)	Дунаева Дарья Александровна<br />\r\n<br />\r\nПо специальности &laquo;Экономика труда&raquo;<em><br />\r\n</em><br />\r\n26)	Мусина Ляйсан Ильдусовна<br />\r\n27)	Исламова Эльмира Ильнуровна<br />\r\n28)	Гараева Айгуль Нафисовна<br />\r\n29)	Латыпова Алия Рамилевна<br />\r\n30)	Богданова Диана Ильинична<br />\r\n31)	Костяева Екатерина Викторовна<br />\r\n32)	Низамиева Гульнур Тахировна<br />\r\n33)	Хабипова Алина Азатовна<br />\r\n34)	Хабипова (Каюмова) Алия Азатовна<br />\r\n35)	Сабирова Гулия  Робертовна<br />\r\n36)	Сабирова Лилия Робертовна<br />\r\n37)	Урманов Фарид Рустамович<br />\r\n38)	Мансурова Алия Ринатовна<br />\r\n39)	Шайхутдинова Гузель Гелюсовна<br />\r\n40)	Ахметханова Эльвира Альбертовна<br />\r\n<br />\r\n2.	Разрешается принять на работу в структурные подразделения и предприятия внешнего сервиса ОАО &laquo;Татнефть&raquo; следующих выпускников:<br />\r\n<br />\r\n1)	Ягафарову Регину Айдаровну в НГДУ &laquo;Альметьевнефть&raquo;<br />\r\n2)	Костяеву Екатерину Викторовну в НГДУ &laquo;Альметьевнефть&raquo;<br />\r\n3)	Ахмадиеву Наилю Шамилевну в НГДУ &laquo;Азнакаевскнефть&raquo;<br />\r\n4)	Фролову Екатерину Николаевну в НГДУ &laquo;Джалильнефть&raquo;<br />\r\n5)	Козыреву Алену Федоровну в НГДУ &laquo;Елховнефть&raquo;<br />\r\n6)	Гафурову Альбину Ильдусовну в НГДУ &laquo;Елховнефть<br />\r\n7)	Ситдикову Лену Альфритовну в НГДУ &laquo;Лениногорскнефть&raquo;<br />\r\n8)	Богданову Диану Ильиничну в НГДУ &laquo;Лениногорскнефть&raquo;<br />\r\n9)	Курбатова Алексея Сергеевича в НГДУ &laquo;Ямашнефть&raquo;<br />\r\n10)	Синякаеву Миляушу Римовну в НГДУ &laquo;Ямашнефть&raquo;<br />\r\n11)	Дунаеву Дарью Александровну в НГДУ &laquo;Ямашнефть&raquo;<br />\r\n12)	Абрамова Антона Николаевича в НИС ОАО &laquo;Татнефть&raquo;<br />\r\n13)	Гилязову Алину Халиловну в НИС ОАО &laquo;Татнефть&raquo;<br />\r\n14)	Минхаирову Гульнур Илфатовну в управление &laquo;Татнефтеснаб&raquo;<br />\r\n15)	Давлетбаеву Диляру Рафисовну в ООО УК &laquo;Система &ndash;Сервис&raquo;<br />\r\n16)	Корневу Светлану Николаевну в ООО УК &laquo;Система &ndash;Сервис&raquo;<br />\r\n17)	Низамиеву Гульнур Тахировну  в ООО УК &laquo;Система &ndash;Сервис&raquo;<br />\r\n18)	Хазиеву Регину Ринатовну в УК ООО &laquo;ТМС Групп&raquo;<br />\r\n19)	Сабирову Гулию  Робертовну  в УК ООО &laquo;Татбурнефть&raquo;<br />\r\n20)	Сабирову Лилию Робертовну  в УК ООО &laquo;Татбурнефть&raquo;<br />\r\n21)	Мусину Ляйсан Ильдусовну в ЗАО &laquo;ЭнерджиКонсалитнг/Татнефть&raquo;<br />\r\n22)	Мансурову Алию Ринатовну в ЗАО &laquo;ЭнерджиКонсалитнг/Татнефть&raquo;<br />\r\n23)	Урманова Фарида Рустамовича в ООО УК &laquo;Татспецтранспорт&raquo;<br />\r\n24)	Ахметханову Эльвиру Альбертовну в ООО УК &laquo;Татнефть-Энергосервис&raquo;<br />\r\n<br />\r\n3.	Организовать прием на работу выпускников в соответствии с п.2. и привести численность персонала в соответствие с установленным лимитом на 31 декабря 2011 года.<br />\r\n<strong>Отв.: Р.Р. Хамадьяров  </strong><br />\r\n<br />\r\n4.	Контроль за исполнением настоящего протокола возложить на  заместителя генерального директора по экономике  В.П. Лавущенко'),(18,'213123123'),(19,''),(20,''),(21,''),(22,''),(23,'<div align=\"center\"><strong>ОТКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО</strong><br />\r\n<strong> &laquo;ТАТНЕФТЬ&raquo; </strong><br />\r\n<strong>имени В.Д. Шашина</strong><br />\r\n</div>\r\n<br />\r\n<div align=\"right\"><strong>УТВЕРЖДЕНО<br />\r\nГенеральным директором <br />\r\n</strong></div>\r\n<br />\r\n<div align=\"center\"><strong>П Р О Т О К О Л</strong><br />\r\n</div>\r\n16 июня 2011 г.<br />\r\nАльметьевск<br />\r\n<br />\r\nзаседания Большого жюри и экзаменационной комиссии (согласно &laquo;Регламенту защиты дипломных проектов в рамках Корпоративного университета&raquo;) по определению победителя в конкурсе на получение рабочего места студентами специальностей &laquo;Экономика и управление на предприятиях нефтяной и газовой промышленности&raquo; и &laquo;Экономика труда&raquo; АГНИ<br />\r\n<br />\r\n<strong>Присутствовали:</strong><br />\r\nзаместитель генерального директора по экономике                           - В.П.Лавущенко <br />\r\nначальник экономического управления                                               - Р.С. Зиен <br />\r\nначальник отдела кадров                                                                       - Р.Р. Хамадьяров <br />\r\nначальника ООТиЗП                                                                              - В.Ю. Макаров <br />\r\nруководители экономических служб структурных подразделений и предприятий внешнего сервиса ОАО &laquo;Татнефть&raquo;<br />\r\n<strong><br />\r\n</strong>\r\n<div align=\"center\"><strong>ПОВЕСТКА ДНЯ:</strong><br />\r\n</div>\r\n<br />\r\nПодведение итогов конкурса на рабочее место по выпускникам АГНИ 2011 года специальностей &laquo;Экономика и управление на предприятиях нефтяной и газовой промышленности&raquo; и &laquo;Экономика труда&raquo;<br />\r\n<br />\r\n<div align=\"center\"><strong>ПОСТАНОВИЛИ:</strong><br />\r\n</div>\r\n<br />\r\n1.	Считать победителями конкурса:<br />\r\nПо специальности &laquo;Экономика и управление на предприятиях нефтяной и газовой промышленности&raquo;<br />\r\n1)	Давлетбаева Диляра Рафисовна<br />\r\n2)	Брендюрева Ирина Владимировна<br />\r\n3)	Козырева Алена Федоровна<br />\r\n4)	Галкаева Альбина Наилевна<br />\r\n5)	Курбатов Алексей Сергеевич<br />\r\n6)	Петрова Анжела Германовна<br />\r\n7)	Галимова Алина Анисовна<br />\r\n8)	Корнева Светлана Николаевна<br />\r\n9)	Ахмадиева Наиля Шамилевна<br />\r\n10)	Минхаирова Гульнур Илфатовна <br />\r\n11)	Муртазина Эльмира Ришатовна<br />\r\n12)	Майорова Любовь Владимировна<br />\r\n13)	Ситдикова Лена Альфритовна<br />\r\n14)	Фролова Екатерина Николаевна<br />\r\n15)	Валиева Гульназ Рифкатовна<br />\r\n16)	Абрамов Антон Николаевич<br />\r\n17)	Гилязова Алина Халиловна<br />\r\n18)	Гафурова Альбина Ильдусовна<br />\r\n19)	Хайртдинова Дина Ирековна<br />\r\n20)	Митковская Марина Викторовна<br />\r\n21)	Тихонова Мария Васильевна<br />\r\n22)	Хазиева Регина Ринатовна<br />\r\n23)	Ягафарова Регина Айдаровна<br />\r\n24)	Синякаева Миляуша Римовна<br />\r\n25)	Дунаева Дарья Александровна<br />\r\n<br />\r\nПо специальности &laquo;Экономика труда&raquo;<em><br />\r\n</em><br />\r\n26)	Мусина Ляйсан Ильдусовна<br />\r\n27)	Исламова Эльмира Ильнуровна<br />\r\n28)	Гараева Айгуль Нафисовна<br />\r\n29)	Латыпова Алия Рамилевна<br />\r\n30)	Богданова Диана Ильинична<br />\r\n31)	Костяева Екатерина Викторовна<br />\r\n32)	Низамиева Гульнур Тахировна<br />\r\n33)	Хабипова Алина Азатовна<br />\r\n34)	Хабипова (Каюмова) Алия Азатовна<br />\r\n35)	Сабирова Гулия  Робертовна<br />\r\n36)	Сабирова Лилия Робертовна<br />\r\n37)	Урманов Фарид Рустамович<br />\r\n38)	Мансурова Алия Ринатовна<br />\r\n39)	Шайхутдинова Гузель Гелюсовна<br />\r\n40)	Ахметханова Эльвира Альбертовна<br />\r\n<br />\r\n2.	Разрешается принять на работу в структурные подразделения и предприятия внешнего сервиса ОАО &laquo;Татнефть&raquo; следующих выпускников:<br />\r\n<br />\r\n1)	Ягафарову Регину Айдаровну в НГДУ &laquo;Альметьевнефть&raquo;<br />\r\n2)	Костяеву Екатерину Викторовну в НГДУ &laquo;Альметьевнефть&raquo;<br />\r\n3)	Ахмадиеву Наилю Шамилевну в НГДУ &laquo;Азнакаевскнефть&raquo;<br />\r\n4)	Фролову Екатерину Николаевну в НГДУ &laquo;Джалильнефть&raquo;<br />\r\n5)	Козыреву Алену Федоровну в НГДУ &laquo;Елховнефть&raquo;<br />\r\n6)	Гафурову Альбину Ильдусовну в НГДУ &laquo;Елховнефть<br />\r\n7)	Ситдикову Лену Альфритовну в НГДУ &laquo;Лениногорскнефть&raquo;<br />\r\n8)	Богданову Диану Ильиничну в НГДУ &laquo;Лениногорскнефть&raquo;<br />\r\n9)	Курбатова Алексея Сергеевича в НГДУ &laquo;Ямашнефть&raquo;<br />\r\n10)	Синякаеву Миляушу Римовну в НГДУ &laquo;Ямашнефть&raquo;<br />\r\n11)	Дунаеву Дарью Александровну в НГДУ &laquo;Ямашнефть&raquo;<br />\r\n12)	Абрамова Антона Николаевича в НИС ОАО &laquo;Татнефть&raquo;<br />\r\n13)	Гилязову Алину Халиловну в НИС ОАО &laquo;Татнефть&raquo;<br />\r\n14)	Минхаирову Гульнур Илфатовну в управление &laquo;Татнефтеснаб&raquo;<br />\r\n15)	Давлетбаеву Диляру Рафисовну в ООО УК &laquo;Система &ndash;Сервис&raquo;<br />\r\n16)	Корневу Светлану Николаевну в ООО УК &laquo;Система &ndash;Сервис&raquo;<br />\r\n17)	Низамиеву Гульнур Тахировну  в ООО УК &laquo;Система &ndash;Сервис&raquo;<br />\r\n18)	Хазиеву Регину Ринатовну в УК ООО &laquo;ТМС Групп&raquo;<br />\r\n19)	Сабирову Гулию  Робертовну  в УК ООО &laquo;Татбурнефть&raquo;<br />\r\n20)	Сабирову Лилию Робертовну  в УК ООО &laquo;Татбурнефть&raquo;<br />\r\n21)	Мусину Ляйсан Ильдусовну в ЗАО &laquo;ЭнерджиКонсалитнг/Татнефть&raquo;<br />\r\n22)	Мансурову Алию Ринатовну в ЗАО &laquo;ЭнерджиКонсалитнг/Татнефть&raquo;<br />\r\n23)	Урманова Фарида Рустамовича в ООО УК &laquo;Татспецтранспорт&raquo;<br />\r\n24)	Ахметханову Эльвиру Альбертовну в ООО УК &laquo;Татнефть-Энергосервис&raquo;<br />\r\n<br />\r\n3.	Организовать прием на работу выпускников в соответствии с п.2. и привести численность персонала в соответствие с установленным лимитом на 31 декабря 2011 года.<br />\r\n<strong>Отв.: Р.Р. Хамадьяров  </strong><br />\r\n<br />\r\n4.	Контроль за исполнением настоящего протокола возложить на  заместителя генерального директора по экономике  В.П. Лавущенко'),(24,'12121'),(25,'<p>\r\n	<a href=\"/jim_collins\"><img src=\"/design/images/lider/jim.png\" /></a> <a href=\"http://lavushenko.ru/gracian/\"><img src=\"/design/images/lider/post_grac.png\" /></a></p>\r\n<ul class=\"modernList activeControl\">\r\n	<li>\r\n		<a href=\"../lider/10_rules\">Десять правил подготовки и проведения совещания</a></li>\r\n	<li>\r\n		<a href=\"../lider/2.htm\">Планирование и составление презентации</a></li>\r\n	<li>\r\n		<a href=\"../lider/3.htm\">Правила написания делового письма</a></li>\r\n	<li>\r\n		<a href=\"../lider/4.htm\">Как получить от подчиненных максимальную отдачу</a></li>\r\n	<li>\r\n		<a href=\"../lider/5.htm\">Правильно ли вы выбрали работу?</a></li>\r\n	<li>\r\n		<a href=\"../lider/6.htm\">Признаки сильного руководителя</a></li>\r\n	<li>\r\n		<a href=\"../lider/7.htm\">Признаки слабого руководителя</a></li>\r\n	<li>\r\n		<a href=\"../lider/8.htm\">Семь типичных управленческих ошибок</a></li>\r\n	<li>\r\n		<a href=\"../lider/9.htm\">Правила успешного выступления</a></li>\r\n	<li>\r\n		<a href=\"../lider/10.htm\">Как справиться со стрессом</a></li>\r\n	<li>\r\n		<a href=\"../lider/11.htm\">Одиннадцать факторов, влияющих на управленческую деятельность</a></li>\r\n	<li>\r\n		<a href=\"../lider/12.htm\">Принципы убеждающей речи</a></li>\r\n	<li>\r\n		<a href=\"../lider/13.htm\">Система правил убеждающего воздействия Д. Карнеги</a></li>\r\n	<li>\r\n		<a href=\"../lider/14.htm\">Создайте позитивное отношение к работе</a></li>\r\n	<li>\r\n		<a href=\"../lider/15.htm\">Учитесь убеждать</a></li>\r\n	<li>\r\n		<a href=\"../lider/16.htm\">Шесть правил ведения переговоров</a></li>\r\n	<li>\r\n		<a href=\"../lider/17.htm\">Полезные цитаты</a></li>\r\n	<li>\r\n		<a href=\"../lider/18.htm\">ВОСЕМЬ ПРИНЦИПОВ УПРАВЛЕНИЯ</a></li>\r\n</ul>'),(26,'<div class=\"clear\">\r\n	clear floats</div>\r\n<p>\r\n	<img alt=\"\" class=\"floatRight\" src=\"/design/images/lider_1.jpg\" /></p>\r\n<ol class=\"modernList activeControl\">\r\n	<li>\r\n		Проясните для себя цель совещания.</li>\r\n	<li>\r\n		Тщательно спланируйте совещание &mdash; определите состав присутствующих, заранее разошлите повестку дня и все необходимые материалы.</li>\r\n	<li>\r\n		Убедитесь, что присутствуют все &mdash; и только &mdash; нужные люди.</li>\r\n	<li>\r\n		Заранее договоритесь о лимите времени и начинайте совещание вовремя. Постарайтесь проводить совещание в помещении, где есть часы.</li>\r\n	<li>\r\n		Тщательно спланируйте повестку дня, выделив определенное время на каждый из пунктов. Учтите время, необходимое для объявления цели совещания, обеспечения эффективной дискуссии, достижения соглашения и принятия программы необходимых действий.</li>\r\n	<li>\r\n		Убедитесь, что протоколы отличаются краткостью и точностью, что в них содержится информация о том, кто, что и когда делал.</li>\r\n	<li>\r\n		Заканчивайте совещания на позитивной ноте, обобщая принятые решения и согласованные действия.</li>\r\n	<li>\r\n		Помните, что в качестве председателя вы должны совмещать две роли &mdash; рефери и лидера.</li>\r\n	<li>\r\n		Регулярно анализируйте свои действия как председателя и будьте готовы обратиться за обратной связью, чтобы развивать свои навыки.</li>\r\n	<li>\r\n		Время от времени проводите обзор регулярных совещаний. Убедитесь, что они необходимы и что на них присутствуют нужные люди.</li>\r\n</ol>\r\n<h3>\r\n	И еще, чтобы успешно провести совещание, вы должны:</h3>\r\n<ul class=\"modernList\">\r\n	<li>\r\n		мыслить четко и быстро;</li>\r\n	<li>\r\n		внимательно слушать;</li>\r\n	<li>\r\n		уметь ясно и кратко выражать свои мысли;</li>\r\n	<li>\r\n		быть готовым пояснить плохо сформулированную точку зрения;</li>\r\n	<li>\r\n		быть справедливым и беспристрастным;</li>\r\n	<li>\r\n		предотвращать лишние вмешательства;</li>\r\n	<li>\r\n		быть терпеливым, терпимым и доброжелательным;</li>\r\n	<li>\r\n		быть дружелюбным, но в то же время энергичным и деловым.</li>\r\n</ul>\r\n<br />\r\n<p>\r\n	<em>Из книги Джона Адаира &laquo;Эффективные коммуникации&raquo;</em><br />\r\n	<br />\r\n	<em>Фото с сайта <a href=\"http://kadastr.ru\">Kadastr.ru</a></em></p>'),(27,''),(28,''),(29,''),(30,'<p>\r\n	<strong><img align=\"baseline\" alt=\"\" border=\"1\" height=\"213\" hspace=\"1\" src=\"/files/tspk(1).jpg\" vspace=\"1\" width=\"694\" /><br />\r\n	<img align=\"right\" alt=\"\" border=\"1\" height=\"148\" hspace=\"1\" src=\"/files/subportal_img_1.jpg\" vspace=\"1\" width=\"198\" />&laquo;Центр подготовки кадров &ndash; Татнефть&raquo;</strong> - негосударственное образовательное учреждение в сфере дополнительного образования и профессиональной подготовки.<br />\r\n	<br />\r\n	&laquo;Центр подготовки кадров &ndash; Татнефть&raquo; (НОУ &laquo;ЦПК-Татнефть&raquo;) осуществляет подготовку, переподготовку и повышение квалификации рабочих кадров нефтедобывающей и перерабатывающей отрасли, строительства, транспорта и других отраслей народного хозяйства; подготовку руководителей и специалистов организаций по промышленной, энергетической, радиационной безопасности, охране труда, гигиене труда и промышленной санитарии. Обучение в НОУ &laquo;ЦПК-Татнефть&raquo; осуществляется по <strong>235 направлениям</strong>.<br />\r\n	<br />\r\n	Высокое качество предоставляемых образовательных услуг является одним из способов достижения лидирующих позиций на <img align=\"right\" alt=\"\" border=\"1\" height=\"148\" hspace=\"1\" src=\"/files/subportal_img_2(1).jpg\" vspace=\"1\" width=\"198\" />рынке и обеспечения устойчивого роста и развития НОУ &laquo;ЦПК-Татнефть&raquo;.<br />\r\n	<br />\r\n	Сегодня в ЦПК создана современная материально-техническая база для проведения производственного обучения. Для отработки практических навыков обучающихся оборудованы учебные мастерские и учебные полигоны. В НОУ &laquo;ЦПК-Татнефть&raquo; есть технические библиотеки, книжный фонд которых составляет более 30,2 тысяч экземпляров.<br />\r\n	<br />\r\n	За годы деятельности<strong> обучено и выпущено более 677 тысяч квалифицированных рабочих и специалистов</strong>. Среди обучающихся были рабочие и специалисты нефтяных компаний Кубы, Вьетнама, Ирака, Казахстана, Туркменистана.<br />\r\n	<br />\r\n	Обучение в НОУ &laquo;ЦПК-Татнефть&raquo; ведется по дневной и вечерней форме обучения, с отрывом и частичным отрывом от <img align=\"right\" alt=\"\" border=\"1\" height=\"148\" hspace=\"1\" src=\"/files/subportal_img_3(1).jpg\" vspace=\"1\" width=\"198\" />производства. В группы зачисляются как по направлениям от предприятий, так и по личным заявлениям граждан.<br />\r\n	<br />\r\n	<strong>Узнать всю информацию о НОУ &laquo;ЦПК-Татнефть&raquo; можно <a href=\"http://www.tatneft.ru/wps/wcm/connect/tatneft/tspk/ob_uchrezhdenii/o_nou_tspk-tatneft\">далее</a></strong><br />\r\n	<br />\r\n	<strong>Ознакомится с доступными курсами обучения можно здесь</strong><br />\r\n	<a href=\"http://www.tatneft.ru/wps/wcm/connect/tatneft/tspk/vidi_deyatelnosti/obuchenie/\"><img align=\"left\" alt=\"\" height=\"75\" src=\"/files/сит2.jpg\" width=\"214\" /></a><br />\r\n	<br />\r\n	<br />\r\n	<br />\r\n	<br />\r\n	<br />\r\n	<br />\r\n	<br />\r\n	<strong>Для кадровых служб Компаний доступна функция &laquo;Реестр прошедших обучение в НОУ ЦПК Татнефть&raquo; с которым можно ознакомиться далее</strong><br />\r\n	<a href=\"http://www.tatneft.ru/wps/wcm/connect/tatneft/tspk/reestr\"><img align=\"baseline\" alt=\"\" height=\"75\" hspace=\"1\" src=\"/files/реестр.jpg\" vspace=\"1\" width=\"214\" /></a></p>');

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id` int(11) NOT NULL auto_increment,
  `item_id` int(11) default NULL,
  `title` varchar(250) default NULL,
  `type` int(11) default NULL,
  `area` int(11) default NULL,
  `ordi` int(11) default NULL,
  `show` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `modules` */

insert  into `modules`(`id`,`item_id`,`title`,`type`,`area`,`ordi`,`show`) values (1,1,'Баннер 1',1,2,2,1),(2,NULL,'Левое меню',2,2,1,1),(3,NULL,'Форма авторизации',3,2,3,1),(4,2,'Баннер 2',1,2,4,1),(5,3,'Главная баннеры справа',1,3,1,1),(6,4,'Главная баннеры центр 1',1,5,1,1),(7,1,'Галлерея на главной',4,5,2,1),(8,5,'Главная баннеры центр 2',1,5,3,1);

/*Table structure for table `modules_pages` */

DROP TABLE IF EXISTS `modules_pages`;

CREATE TABLE `modules_pages` (
  `id` int(11) NOT NULL auto_increment,
  `module_id` int(11) default NULL,
  `page_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `modules_pages` */

insert  into `modules_pages`(`id`,`module_id`,`page_id`) values (1,1,19),(2,2,NULL),(3,1,10),(4,3,NULL),(5,4,10),(6,5,10),(7,6,10),(8,7,10),(9,8,10);

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL auto_increment,
  `item_id` int(11) default NULL,
  `parent_id` int(11) default NULL,
  `lft` int(11) default NULL,
  `rgt` int(11) default NULL,
  `name` varchar(250) default NULL,
  `type` int(11) default NULL,
  `url` varchar(250) default NULL,
  `show` tinyint(1) default NULL,
  `attributes` text,
  `show_name` tinyint(1) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pages` */

insert  into `pages`(`id`,`item_id`,`parent_id`,`lft`,`rgt`,`name`,`type`,`url`,`show`,`attributes`,`show_name`) values (1,NULL,0,1,34,'Корень',1,'1',1,NULL,1),(10,2,1,2,17,'Главная',2,'/main',1,'a:1:{s:15:\"parent_li_class\";s:3:\"red\";}',0),(11,1,1,18,27,'Школьникам',2,'/pupil',1,'a:1:{s:15:\"parent_li_class\";s:6:\"yellow\";}',1),(12,27,1,28,29,'Студентам',2,'/ec-student',1,'a:1:{s:15:\"parent_li_class\";s:5:\"green\";}',1),(13,28,1,30,31,'Специалистам',2,'/ec-specialist',1,'a:1:{s:15:\"parent_li_class\";s:4:\"blue\";}',1),(14,29,1,32,33,'Студенческая биржа труда',2,'/stock',1,'a:1:{s:15:\"parent_li_class\";s:6:\"yellow\";}',1),(15,24,10,3,4,'Публикации',2,'/publications',1,NULL,1),(16,25,10,5,8,'Уроки лидерства',2,'/lider',1,NULL,1),(17,NULL,10,9,10,'Психологический практикум',2,'/psychology',1,NULL,1),(18,NULL,11,19,20,'Знакомство с ОАО \"Татнефть\"',2,'/pupil/tour',1,NULL,1),(19,NULL,11,21,22,'Обучающие Фильмы',2,'/pupil/video',1,NULL,1),(20,NULL,11,23,24,'Профориентация',2,'/pupil/prof',1,NULL,1),(21,NULL,11,25,26,'Викторины',2,'/pupil/quiz',1,NULL,1),(24,23,10,13,14,'Официальные документы',2,'/documents',1,NULL,1),(25,26,16,6,7,'Десять правил подготовки и проведения совещания',2,'/lider/10_rules',1,'a:2:{s:2:\"id\";s:8:\"10_rules\";s:15:\"parend_li_class\";s:3:\"red\";}',1),(26,30,10,15,16,'ЦПК',2,'/nou_cpk',1,NULL,1);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `int` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`int`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `permissions` */

/*Table structure for table `photos` */

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `title` varchar(250) default NULL,
  `file_name` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `photos` */

/*Table structure for table `tree_admins` */

DROP TABLE IF EXISTS `tree_admins`;

CREATE TABLE `tree_admins` (
  `id` int(11) NOT NULL auto_increment,
  `table_name` varchar(250) default NULL,
  `table_id` int(11) default NULL,
  `user_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tree_admins` */

insert  into `tree_admins`(`id`,`table_name`,`table_id`,`user_id`) values (1,'pages',1,1);

/*Table structure for table `user_permissions` */

DROP TABLE IF EXISTS `user_permissions`;

CREATE TABLE `user_permissions` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `permission_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_permissions` */

insert  into `user_permissions`(`id`,`user_id`,`permission_id`) values (1,1,1),(2,2,2);

/*Table structure for table `user_profiles` */

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `surname` varchar(250) default NULL,
  `name` varchar(250) default NULL,
  `patronymic` varchar(250) default NULL,
  `birth_date` date default NULL,
  `sex` tinyint(2) default NULL,
  `city` varchar(250) default NULL,
  `photo` varchar(250) default NULL,
  `email` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `user_profiles` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(250) default NULL,
  `password` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`login`,`password`) values (1,'god','759730a97e4373f3a0ee12805db065e3a4a649a5'),(2,'damir','759730a97e4373f3a0ee12805db065e3a4a649a5');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
