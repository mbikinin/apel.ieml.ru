<?php
    //error_reporting(E_ALL);
    //$GLOBALS['Con']->debug = 1;
    date_default_timezone_set('Europe/Moscow');
    error_reporting(E_ALL & ~E_NOTICE);
	require_once '../config/environment.inc';
	force_no_cache();

	$Dispatcher = new Dispatcher();

	list ($url) = explode('?', $_SERVER['REQUEST_URI']);
	$url = substr($url, 7);
	try
	{
		$Dispatcher->dispatch($url);
	}
	catch (Exception $e)
	{
		if ($Config->server == '!')
		{
			if ($e->getCode() == 404)
				Url::redirect('/404.html');
			else
				Url::redirect('/error.html');
		}
		else
		{
			$Dispatcher->dispatchWithException($e);
		}
	}