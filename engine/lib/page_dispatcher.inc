<?
	class PageDispatcher extends Dispatcher
	{
		protected
			$page;

		protected function recognizeRoute($url)
		{
			extract(parse_url($url), EXTR_PREFIX_ALL, 'u');
			$domain = "$u_scheme://$u_host";

			$path = '';
			$urls = array();
			foreach (preg_split('!/!', $u_path, -1, PREG_SPLIT_NO_EMPTY) as $pc)
			{
				$path .= "/$pc";
//				$urls[] = $domain.$path;
				$urls[] = $path;
			}

			$params = array(
				'all'	=> 0,
			);

			if ($urls)
			{
				$params += array(
					'order_by'	=> "LENGTH(url) DESC",
					'limit'		=> 1,
				) + ActiveRecord::$Con->createInFilter('url', $urls);
			}
			else
			{
				$params += array(
					'cond'	=> compact('url'),
				);
			}

			$params['cond']['draft'] = 0;

			$Page = new Page();
			if (!$this->page = $Page->find($params))
			{
				$this->raise("Cannot find $url", 'URL not found', 404);
			}

			$routed_url = substr($url, strlen($this->page->url));

			$Router = new Router();
			require_once FWK_ENV.'page_routes.inc';

//			$route = (array)$Router->findRoute($routed_url) + array(
//				'controller'	=> $this->page->user_controller_name,
//			);

			$route = array_merge(
				array(
					'controller' 	=> $this->page->user_controller_name,
					'url'			=> $url,
				),
				(array)$Router->findRoute($routed_url)
			);

			return $route;
		}

    	protected function createController($controller_class_name, $controller_path, $controller_name)
    	{
    		return new $controller_class_name($this->page, $controller_path, $controller_name);
    	}
	}
?>