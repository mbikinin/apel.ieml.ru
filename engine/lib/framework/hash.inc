<?
	class Hash
	{
		static function filterKey($hash, $wipe, $key)
		{
			$args = func_get_args();
			$khash = array_flip(array_slice($args, 2));
			return call_user_func($wipe ? 'array_diff_key' : 'array_intersect_key', $hash, $khash);
		}

		static function wipeKey($hash, $key)
		{
			$args = func_get_args();
			array_shift($args);
			array_unshift($args, $hash, 1);
			return call_user_func_array(array(__CLASS__, 'filterKey'), $args);
		}

		static function leaveKey($hash, $key)
		{
			$args = func_get_args();
			array_shift($args);
			array_unshift($args, $hash, 0);
			return call_user_func_array(array(__CLASS__, 'filterKey'), $args);
		}

		static function fromArray($key_array, $value_array = null)
		{
			$hash = array();

			foreach ($key_array as $idx => $key)
				$hash[$key] = func_num_args() == 1
					? $key
					: (is_array($value_array) ? $value_array[$idx] : $value_array);

			return $hash;
		}

		static function popKey(&$hash, $key)
		{
			$value = @$hash[$key];
			unset($hash[$key]);
			return $value;
		}
	}
?>