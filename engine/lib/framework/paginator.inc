<?
	class Paginator
	{
		public static
			$pages_template = '<p class="pages">Страницы: %s</p>';

		static function paginate(&$r_params, ActiveRecord $object, $find_params = null, $params = null)
		{
			is_array($find_params) or $find_params = array();
			is_array($params) or $params = array();

			if (array_key_exists('total', $params))
			{
				$total = $params['total'];
				unset($params['total']);
			}
			else
			{
				$total = $object->count($find_params);
			}

			$params = array_merge(array(
				'per_page'		=> @$object->cfg['per_page'],
				'show_pages'	=> @$object->cfg['show_pages'],
				'template'		=> self::$pages_template,
				'page_var'		=> 'page',
			), array(
				'per_page'		=> 10,
				'show_pages'	=> 10,
			), $params);

			$pages = self::pager($r_params[$params['page_var']], $total, $params);

			$find_params = array_merge($find_params, array(
				'offset'	=> ($r_params[$params['page_var']] - 1) * $params['per_page'],
				'limit'		=> $params['per_page'],
			));

			return array($pages, $object->find($find_params));
		}

		static function pager(&$page, $total, $options = null)
		{
			is_array($options) or $options = array();

			$options = array_merge(array(
				'url'			=> $_SERVER['REQUEST_URI'],
				'larrow'		=> '&lt;&lt;',
				'rarrow'		=> '&gt;&gt;',
				'template'		=> '%s',
				'page_var'		=> 'page',
				'current_page'	=> '( %s )',
			), $options);

			$pages = ceil($total / $options['per_page']);
			$page = max(1, min((int)$page, $pages));

			if ($pages < 2)
				return '';

			$url = String::makeSprintfSafe($options['url']);
			$url = str_replace("#{$options['page_var']}#", '%u', $url);
			$url = Url::setQueryParam($options['page_var'], '%u', $url);

			$href = '<a href="'.$url.'">%s</a>';

			$firstblock = 0;
			$lastblock  = floor(($pages - 1) / $options['show_pages']);
			$curblock   = floor(($page - 1) / $options['show_pages']);
			$pb_start   = $curblock * $options['show_pages'] + 1;
	        $pb_end     = min($pages, $pb_start + $options['show_pages'] - 1);

			$ret = array();

			if ($curblock > $firstblock)
				$ret[] = sprintf($href, $pb_start - 1, $options['larrow']);

			for ($p = $pb_start; $p <= $pb_end; $p++)
				$ret[] = $p == $page ? sprintf($options['current_page'], $p) : sprintf($href, $p, $p);

			if($curblock < $lastblock)
				$ret[] = sprintf($href, $pb_end + 1, $options['rarrow']);

			return sprintf($options['template'], implode(' ', $ret));
		}
	}
?>