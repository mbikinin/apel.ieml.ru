<?
	/**
	 * @package FormHelper
	 * @version Thu Mar 13 14:21:36 MSK 2008
	 *
	 */

	class FormHelper extends Helper
	{
		public
			$auto_index,
			$errors = '';

		public static
			$calendar_img_path			= '/img/calendar/',
			$default_field_options		= array('size' => 30),
			$default_radio_options		= array(),
			$default_text_area_options	= array('cols' => 40, 'rows' => 20),
			$default_wysiwyg_options	= array('width' => 400, 'height' => 150),
			$default_datepicker_format	= 'd.m.Y';

	    function tagName($attribute_name = '')
	    {
	    	$attribute_name or $attribute_name = $this->attribute_name;

	    	if (!$this->object_name)
	    		return $attribute_name;

	    	if (preg_match('/^(\w+)(\[.*\]+)$/', $attribute_name, $m))
	    		return "{$this->object_name}[{$m[1]}]{$m[2]}";

	    	return "{$this->object_name}[{$attribute_name}]";

	  //      return $this->object_name ? "{$this->object_name}[{$attribute_name}]" : $attribute_name;

	    }

	    function tagNameWithIndex($index)
	    {
	        return "{$this->object_name}[{$this->attribute_name}][{$index}]";
	    }

	    function tagId($attribute_name = '')
	    {
	    	$attribute_name or $attribute_name = $this->attribute_name;
	        return ($this->object_name ? $this->object_name.'_' : '').$attribute_name;
	    }

	    function tagIdWithIndex($index)
	    {
	        return $this->tagId()."_{$index}";
	    }

	    function addDefaultNameAndId($options)
	    {
	        if(array_key_exists("index", $options))
	        {
	            $options["name"] or $options["name"] = $this->tagNameWithIndex($options["index"]);
	            $options["id"] or $options["id"] = $this->tagIdWithIndex($options["index"]);
	            unset($options["index"]);
	        }
	        elseif($this->auto_index)
	        {
	            $options["name"] or $options["name"] = $this->tagNameWithIndex($this->auto_index);
	            $options["id"] or $options["id"] = $this->tagIdWithIndex($this->auto_index);
	        }
	        else
	        {
	            $options["name"] or $options["name"] = $this->tagName();
	            $options["id"] or $options["id"] = $this->tagId();
	        }
	        return $options;
	    }

	    function toInputFieldTag($field_type, $options = array())
	    {
	        $default_size = $options["maxlength"] or $default_size = self::$default_field_options['size'];
	        $options["size"] or $options["size"] = $default_size;
	        $options = array_merge(self::$default_field_options, $options);
	        if($field_type == "hidden")
	        {
	            unset($options["size"]);
	        }
	        $options["type"] = $field_type;
	        if($field_type != "file")
	        {
	        	$options['value'] = array_key_exists('value', $options) ? $options['value'] : $this->value();
	            //$options["value"] or $options["value"] = $this->value();
	        }
	        $options = $this->addDefaultNameAndId($options);
	        return $this->errorWrapping($this->tag("input", $options));
	    }

	    function errorWrapping($html_tag, $has_error = null)
	    {
	    	if (is_null($has_error) && $this->object instanceof ActiveRecord)
	    		$has_error = $this->object->getErrorsForAttributeAsString($this->attribute_name);

	        return $has_error ? $this->contentTag('span', $html_tag, array('class' => 'field_with_errors')) : $html_tag;
	    }

		/**
		 *  Avialble functions for use in views
		 *  Example: $FormHelper->textField("title");
		 *  Result: <input type="text" id="post_title" name="post[title]" value="$post->title" />
		 */
		function textField($field, $options = array())
		{
			$this->attribute_name = $field;
		    return $this->toInputFieldTag('text', $options);
		}

		/**
		 *  Works just like textField, but returns a input tag of the "password" type instead.
		 * 	Example: $FormHelper->passwordField("password");
		 *  Result: <input type="password" id="user_password" name="user[password]" value="$user->password" />
		 */
		function passwordField($field, $options = array())
		{
			$this->attribute_name = $field;
		    return $this->toInputFieldTag('password', $options);
		}

		/**
		 *  Works just like textField, but returns a input tag of the "hidden" type instead.
		 *  Example: $FormHelper->hiddenField("title");
		 *  Result: <input type="hidden" id="post_title" name="post[title]" value="$post->title" />
		 */
		function hiddenField($field, $options = array())
		{
			$this->attribute_name = $field;
		    return $this->toInputFieldTag('hidden', $options);
		}

		/**
		 * Works just like textField, but returns a input tag of the "file" type instead, which won't have any default value.
		 */
		function fileField($field, $options = array())
		{
			$this->attribute_name = $field;

			if ($f_value = $this->value("f_$field"))
			{
				if ($options['show_value'])
				{
					$value = $this->contentTag('div', $this->linkTo($this->value(), $f_value),
						array('class="file-upload-value"'));
				}
				elseif ($options['show_value_as_image'])
				{
					$value = '<br />'.$this->tag('img', array(
						'src' 	=> $f_value,
						'class'	=> 'file-upload-value',
					));
				}
			}
			else
				$value = '';

		    return $this->toInputFieldTag('file', $options).$value;
		}

		/**
		 *  Example: $FormHelper->textArea("body", array("cols" => 20, "rows" => 40));
		 *  Result: <textarea cols="20" rows="40" id="post_body" name="post[body]">$post->body</textarea>
		 */
		function textArea($field, $options = array())
		{
			$this->attribute_name = $field;

	        if ($options['size'])
	        {
	            list ($options['cols'], $options['rows']) = explode('x', $options['size']);
	            unset($options['size']);
	        }
	        $options = array_merge(self::$default_text_area_options, $options);
	        $options = $this->addDefaultNameAndId($options);
	        return $this->errorWrapping($this->contentTag('textarea', htmlspecialchars($this->value($field)), $options));
		}

		/**
		 * Returns a checkbox tag tailored for accessing a specified attribute (identified by $field) on an object
		 * assigned to the template (identified by $object). It's intended that $field returns an integer and if that
		 * integer is above zero, then the checkbox is checked. Additional $options on the input tag can be passed as an
		 * array with $options. The $checked_value defaults to 1 while the default $unchecked_value
		 * is set to 0 which is convenient for boolean values. Usually unchecked checkboxes don't post anything.
		 * We work around this problem by adding a hidden value with the same name as the checkbox.
		#
		 * Example: Imagine that $post->validated is 1:
		 *   $FormHelper->checkBox("validated");
		 * Result:
		 *   <input type="checkbox" id="post_validate" name="post[validated] value="1" checked="checked" />
		 *   <input name="post[validated]" type="hidden" value="0" />
		#
		 * Example: Imagine that $puppy->gooddog is no:
		 *   checkBox("puppy", "gooddog", array(), "yes", "no");
		 * Result:
		 *     <input type="checkbox" id="puppy_gooddog" name="puppy[gooddog] value="yes" />
		 *     <input name="puppy[gooddog]" type="hidden" value="no" />
		*/
	    function checkBox($field, $options = array(), $checked_value = 1, $unchecked_value = 0)
	    {
			$this->attribute_name = $field;

			$caption = Hash::popKey($options, 'caption');

	        $options['type'] = 'checkbox';
	        $options['value'] = $checked_value;

	        $value = $this->value();

			if ($value instanceof MyArrayIterator)
			{
				$checked = $value->propFind($options['prop_name'], $options['index']);
			}
			else
				$checked = ($value == $checked_value);

	        if ($checked || $options['checked'])
	        {
	            $options['checked'] = 'checked';
	        }
	        else
	        {
	            unset($options['checked']);
	        }

	        $options = $this->addDefaultNameAndId($options);

	        if (!array_key_exists('hidden', $options) || $options['hidden'])
	        {
		        $hidden_options = array(
		        	'name'	=> $options['name'],
		        	'type'	=> 'hidden',
		        	'value'	=> $unchecked_value
		        );
		        $checkbox = $this->tag('input', $hidden_options);
	        }
	        else
	        	$checkbox = '';

	        unset($options['hidden']);

	        $checkbox .= $this->tag('input', $options);

	        if ($caption)
	        {
	        	$checkbox = $this->contentTag('label', $checkbox.$caption, array('for' => $options['id']));
	        }

	        return $this->errorWrapping($checkbox);
	    }

		/**
		 * Returns a radio button tag for accessing a specified attribute (identified by $field) on an object
		 * assigned to the template (identified by $object). If the current value of $field is $tag_value the
		 * radio button will be checked. Additional $options on the input tag can be passed as a
		 * hash with $options.
		 * Example: Imagine that $post->category is "trax":
		 *   radioButton("post", "category", "trax");
		 *   radioButton("post", "category", "java");
		 * Result:
		 *     <input type="radio" id="post_category" name="post[category] value="trax" checked="checked" />
		 *     <input type="radio" id="post_category" name="post[category] value="java" />
		*/
		function radioButton($field, $tag_value, $options = array())
		{
			$this->attribute_name = $field;

			$caption = Hash::popKey($options, 'caption');

	        $options = array_merge(self::$default_radio_options, $options);
	        $options["type"] = "radio";
	        $options["value"] = $tag_value;
	        if($this->value($field) == $tag_value)
	        {
	            $options["checked"] = "checked";
	        }

	        $pretty_tag_value = preg_replace(array('/\W/', '/\s/'), array('', '_'), strtolower($tag_value));
	        $object_name = $this->object_name ? $this->object_name.'_' : '';
 	        $options["id"] = $this->auto_index
	        	? $object_name.$this->auto_index.'_'.$this->attribute_name.'_'.$pretty_tag_value
	        	: $object_name.$this->attribute_name.'_'.$pretty_tag_value;

	        $options = $this->addDefaultNameAndId($options);
	        $radio = $this->tag('input', $options);

	        if ($caption)
	        {
	        	$radio = $this->contentTag('label', $radio.$caption, array('for' => $options['id']));
	        }

	        return $this->errorWrapping($radio);
		}

	    /**
	     *  Returns a string with a div containing all the error messages for the object located as an instance variable by the name
	     *  of <tt>object_name</tt>. This div can be tailored by the following options:
	     *
	     * <tt>header_tag</tt> - Used for the header of the error div (default: h2)
	     * <tt>id</tt> - The id of the error div (default: errorExplanation)
	     * <tt>class</tt> - The class of the error div (default: errorExplanation)
	     */
	    function errorMessages($options = array())
	    {
	    	if (is_callable(array($this->object, 'getAllFieldErrorsAsString')))
	    		$errors = $this->object->getAllFieldErrorsAsString();
	    	elseif (is_callable(array($this->controller_object, 'getAllErrorsAsString')))
	    		$errors = $this->controller_object->getAllErrorsAsString();
	    	elseif ($this->errors)
	    		$errors = $this->errors;
	    	else
	    		$errors = '';

	    	return $errors
	    		? $this->contentTag('div', $errors, array('class' => 'error_explanation', 'id' => 'error_explanation'))
	    		: '';
	    }

	    /**
	     *  Accepts a container (hash, array, enumerable, your type) and returns a string of option tags.
	     * Given a container where the elements respond to first and last (such as a two-element array),
	     * the "lasts" serve as option values and the "firsts" as option text. Hashes are turned into this form
	     * automatically, so the keys become "firsts" and values become lasts.
	     * If +selected+ is specified, the matching "last" or element will get the selected option-tag.
	     * +Selected+ may also be an array of values to be selected when using a multiple select.
	     *
	     *  Examples (call, result):
	     *    options_for_select([["Dollar", "$"], ["Kroner", "DKK"]])
	     *      <option value="$">Dollar</option>\n<option value="DKK">Kroner</option>
	     *
	     *    options_for_select([ "VISA", "MasterCard" ], "MasterCard")
	     *      <option>VISA</option>\n<option selected="selected">MasterCard</option>
	     *
	     *    options_for_select({ "Basic" => "$20", "Plus" => "$40" }, "$40")
	     *      <option value="$20">Basic</option>\n<option value="$40" selected="selected">Plus</option>
	     *
	     *    options_for_select([ "VISA", "MasterCard", "Discover" ], ["VISA", "Discover"])
	     *      <option selected="selected">VISA</option><option>MasterCard</option>\n<option selected="selected">Discover</option>
	     *
	     *  NOTE: Only the option tags are returned, you have to wrap this call in a regular HTML select tag.
	     */
	    function optionsForSelect($choices, $selected_value = null)
	    {
	        $options = array();

	        if(is_array($choices))
	        {
	            foreach($choices as $choice_value => $choice_text)
	            {
//                    $selected = (empty($choice_value) ? $choice_text : $choice_value) == $selected_value
//                    	? ' selected = "selected"' : '';
                    $selected = $choice_value == $selected_value ? ' selected = "selected"' : '';

                    $options[] = '<option value="'.htmlspecialchars($choice_value).'"'.$selected.'>'.
                    		str_replace(' ', '&nbsp;', htmlspecialchars($choice_text)).'</option>';
	            }
	        }

	        return implode("\n", $options);
	    }

	    /**
	     * Returns a string of option tags that have been compiled by iterating over the +collection+ and assigning the
	     * the result of a call to the +value_method+ as the option value and the +text_method+ as the option text.
	     * If +selected_value+ is specified, the element returning a match on +value_method+ will get the selected option tag.
	     *
	     * Example (call, result). Imagine a loop iterating over each +person+ in <tt>@project.people</tt> to generate an input tag:
	     *   options_from_collection_for_select(@project.people, "id", "name")
	     *     <option value="#{person.id}">#{person.name}</option>
	     *
	     * NOTE: Only the option tags are returned, you have to wrap this call in a regular HTML select tag.
	     */
	    function optionsFromCollectionForSelect($collection, $attribute_value, $attribute_text, $selected_value = null)
	    {
	        $options = array();

            foreach((array)$collection as $object)
            {
                if(is_object($object))
                {
                    $options[$object->$attribute_value] = $object->$attribute_text;
                }
	        }

	        return $this->optionsForSelect($options, $selected_value);
	    }

		/**
		 * Avialble functions for use in views
		 * Create a select tag and a series of contained option tags for the provided object and method.
		 * The option currently held by the object will be selected, provided that the object is available.
		 * See options_for_select for the required format of the choices parameter.
		 *
		 * Example with $post->person_id => 1:
		 *   $person = new Person;
		 *   $people = $person->find_all();
		 *   foreach($people as $person) {
		 *      $choices[$person->id] = $person->first_name;
		 *   }
		 *   select("post", "person_id", $choices, array("include_blank" => true))
		 *
		 * could become:
		 *
		 *   <select name="post[person_id]">
		 *     <option></option>
		 *     <option value="1" selected="selected">David</option>
		 *     <option value="2">Sam</option>
		 *     <option value="3">Tobias</option>
		 *   </select>
		 *
		 * This can be used to provide a functionault set of options in the standard way: before r}ering the create form, a
		 * new model instance is assigned the functionault options and bound to @model_name. Usually this model is not saved
		 * to the database. Instead, a second model object is created when the create request is received.
		 * This allows the user to submit a form page more than once with the expected results of creating multiple records.
		 * In addition, this allows a single partial to be used to generate form inputs for both edit and create forms.
		 */
		function select($attribute_name, $choices, $options = array(), $html_options = array())
		{
			$this->attribute_name = $attribute_name;

	        $html_options = $this->addDefaultNameAndId($html_options);
	        return $this->errorWrapping($this->contentTag('select',
	        	$this->addOptions($this->optionsForSelect($choices, $this->value()),
	        	$options, $this->value()), $html_options));
		}

		/**
		 * Return select and option tags for the given object and method using
		 * options_from_collection_for_select to generate the list of option tags.
		 *
		 * Example with $post->person_id => 1:
		 *   $person = new Person;
		 *   $people = $person->find_all();
		 *   collection_select("post", "person_id", $people, "id", "first_name", array("include_blank" => true))
		 *
		 * could become:
		 *
		 *   <select name="post[person_id]">
		 *     <option></option>
		 *     <option value="1" selected="selected">David</option>
		 *     <option value="2">Sam</option>
		 *     <option value="3">Tobias</option>
		 *   </select>
		 *
		 */
		function collectionSelect($attribute_name, $collection, $attribute_value, $attribute_text, $options = array(), $html_options = array())
		{
			$this->attribute_name = $attribute_name;

	        $html_options = $this->addDefaultNameAndId($html_options);
	        return $this->contentTag('select',
	        	$this->addOptions(
	        		$this->optionsFromCollectionForSelect($collection, $attribute_value, $attribute_text, $this->value()),
	        		$options, $this->value()
	        	),
	        	$html_options
	        );
		}

	    private function addOptions($option_tags, $options, $value = null)
	    {
	    	$ret = '';

	        if ($options['include_blank'] == true)
	        {
	            $ret = "<option value=\"\">{$options['prompt']}</option>\n";
	        }

	        return $ret.$option_tags;
	    }

	    function wysiwyg($field, $options = array())
	    {
	    	//$this->needJs('FCKeditor/fckconfig');
	    	$this->needJs('FCKeditor/fckeditor');
	    	$this->needJs('fckeditor');

	    	$this->attribute_name = $field.'_view';

	        if ($options['size'])
	        {
	            list ($options['width'], $options['height']) = explode('x', $options['size']);
	            unset($options['size']);
	        }
	        $iframe_options = $this->addDefaultNameAndId(array_merge(self::$default_wysiwyg_options, $options));
	        $iframe_options['src'] = '/js/FCKeditor/fckblank.html';

	        $hidden_id = $this->tagId($field);

	        $button_options = array(
	        	'id'		=> $hidden_id.'_btn',
	        	'name'		=> $this->tagName($field.'_btn'),
	        	'onclick'	=> "inlineEditor('$hidden_id', '{$options['width']}', '{$options['height']}', '{$options['toolbar']}')",
	        	'value'		=> 'Редактировать',
	        );

	        return $this->errorWrapping($this->contentTag('iframe', '', $iframe_options)).
	        		'<br />'.$this->toInputFieldTag('button', $button_options).
	        		$this->hiddenField($field);
	    }

	    public function datePickerAnyTime($field, $options = array())
	    {
			$this->needCss('anytime');
			$this->needJs('jquery');
			$this->needJs('anytimec');

			$input_id = $this->tagId($field);
			$format = isset($options['format']) ? $options['format'] : self::$default_datepicker_format;
			$options['maxlength'] = $options['time'] ? 16 : 10;

			if ($options['time'])
			{
				// not supported
				$format .= ' H:i';
				unset($options['time']);
			}

			if ($options['use_f_value'])
			{
				$options['value'] = $this->value("f_$field");
			}
			elseif (is_numeric($value = $this->value($field)))
			{
				$options['value'] = date($format, $value);
			}

			$options['class'] = isset($options['class_name']) ? $options['class_name'] : 'anytime-dp';

			$cal_format = $this->convertFormatToAnyTimeCal($format);

			return $this->textField($field, $options).<<<HTML
<script type="text/javascript">
$(document).ready(function() {
AnyTime.picker("{$input_id}", {
	format: "{$cal_format}",
	labelDayOfMonth: "Дата месяца",
	labelHour: "Час",
	labelMinute: "Минута",
	labelMonth: "Месяц",
	labelSecond: "Секунда",
	labelTimeZone: "Зона",
	labelTitle: "Календарь",
	labelYear: "Год",
	monthAbbreviations: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
	monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	dayAbbreviations: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
	dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
	firstDOW: 1
});
});
</script>
HTML;
	    }

	    public function datePickerJQuery($field, $options = array())
	    {
			$this->needJs('jquery');
			$this->needJs('jquery-ui');
			$this->needJs('jquery.ui.datepicker-ru');
			$this->needCss('redmond/jquery-ui');
//			$this->needCss('ui-lightness/jquery-ui');

			$input_id = $this->tagId($field);
			$format = self::$default_datepicker_format;
			$options['maxlength'] = $options['time'] ? 16 : 10;

			if ($options['time'])
			{
				// not supported
				//$format .= ' H:i';
				unset($options['time']);
			}

			if ($options['use_f_value'])
			{
				$options['value'] = $this->value("f_$field");
			}
			elseif (is_numeric($value = $this->value($field)))
			{
				$options['value'] = date($format, $value);
			}

			$cal_format = $this->convertFormatToJQueryCal($format);

			return $this->textField($field, $options).<<<HTML
<script type="text/javascript">
$(document).ready(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
	$("#{$input_id}").datepicker({
		dateFormat: '{$cal_format}',
		autoSize: true,
		showOn: "button",
		buttonImage: "/design/images/calendar.gif",
		buttonImageOnly: true
	});
});
</script>
HTML;
	    }

		function datePicker($field, $options = array())
		{
			$this->needCss(Helper::$js_base.'/zpcal/themes/bluexp');
			$this->needJs('zpcal/src/utils');
			$this->needJs('zpcal/src/calendar');
			$this->needJs('zpcal/lang/calendar-ru');
			$this->needJs('zpcal/src/calendar-setup');

			$img = self::$calendar_img_path;
			$input_id = $this->tagId($field);
			$button_id = $input_id.'_btn';
			$cal_var_name = $input_id.'_cal';

			$format = self::$default_datepicker_format;

			$options['maxlength'] = $options['time'] ? 16 : 10;

			if ($options['time'])
			{
				$format .= ' H:i';
				unset($options['time']);
				$shows_time = 'true';
			}
			else
				$shows_time = 'false';

			if ($options['use_f_value'])
			{
				$options['value'] = $this->value("f_$field");
			}
			elseif (is_numeric($value = $this->value($field)))
			{
				$options['value'] = date($format, $value);
			}

			$cal_format = $this->convertFormatToZpCal($format);

			if ($options['button_img'])
			{
				$button = $this->tag('img', array(
					'id' 	=> $button_id,
					'src' 	=> $options['button_img'],
					'style'	=> 'cursor: pointer',
				));
			}
			else
				$button = $this->buttonTag(' ... ', array('id' => $button_id));

			return $this->textField($field, $options).<<<HTML
&nbsp;
$button
<script type="text/javascript">
	var $cal_var_name = new Zapatec.Calendar.setup({

	inputField	: '$input_id',
	singleClick	: false,
	ifFormat	: '$cal_format',
	showsTime	: $shows_time,
	button		: '$button_id',
	timeFormat	: 24,
	firstDay	: 1

	});
</script>
HTML;
		}

		protected function convertFormatToJQueryCal($format)
		{
/*
d - day of month (no leading zero)
dd - day of month (two digit)
o - day of the year (no leading zeros)
oo - day of the year (three digit)
D - day name short
DD - day name long
m - month of year (no leading zero)
mm - month of year (two digit)
M - month name short
MM - month name long
y - year (two digit)
yy - year (four digit)
*/
			return strtr($format, array(
				'j' => 'd',
				'd' => 'dd',
				'z' => 'o',
//				'z' => 'oo',
//				'D' => 'D',
				'l' => 'DD',
				'n' => 'm',
				'm' => 'mm',
//				'M' => 'M',
				'F' => 'MM',
//				'y' => 'y',
				'Y' => 'yy',
			));
		}

		protected function convertFormatToAnyTimeCal($format)
		{
			return strtr($format, array(
				'd' => '%d',
				'j' => '%e',
				'm' => '%m',
				'Y' => '%Y',
				'H' => '%H',
				'i' => '%i',
			));
		}

		protected function convertFormatToZpCal($format)
		{
			return strtr($format, array(
				'D'	=> '%a',
				'l'	=> '%A',
				'M'	=> '%b',
				'F'	=> '%B',
				'd'	=> '%d',
				'j'	=> '%e',
				'H'	=> '%H',
				'h'	=> '%I',
				'z'	=> '%j',
				'G'	=> '%k',
				'g'	=> '%l',
				'm'	=> '%m',
				'i'	=> '%M',
				"\n"=> '%n',
				'A' => '%p',
				'a' => '%P',
				's' => '%S',
				'U' => '%s',
				"\t"=> '%t',
				'W' => '%W',
				'N' => '%w',
				'w' => '%u',
				'y' => '%y',
				'Y' => '%Y',
			));
		}

		function imageCropper($field, $type = 0)
		{
			$this->needJs('cropper');

			return
				$this->icUpload($field).
				'<br />'.
				$this->icPreview($field).
				'<br />'.
				$this->icTools($field).
				$this->icCreateObject($field, $type);
		}

		function icCreateObject($field, $type)
		{
			$field_id = $this->tagId($field);

			return <<<HTML
<script type="text/javascript">
	var {$field}ImageAdjust = new imageAdjust('$field_id', '{$field}ImageAdjust', $type);
</script>
HTML;
		}

		function icUpload($field)
		{
			return
				$this->hiddenField($field.'-delete').
				$this->hiddenField($field.'-adj').
				$this->fileField($field, array(
					'onchange' => $field."ImageAdjust.preview(this.value, false)",
				));
		}

		function icPreview($field)
		{
			$src = $this->value("f_$field");
			$field_id = $this->tagId($field);

			return <<<HTML
<div id="{$field_id}-div-preview">
	<img style="display: none;" id="{$field_id}-img-tuned" width="0" height="0" />
	<img id="{$field_id}-img-preview" src="$src" />
</div>
HTML;
		}

		function icTools($field)
		{
			return
				/*$this->buttonTag('Удалить изображение').
				'&nbsp'.*/
				$this->buttonTag('Настроить размер', array(
					'style' 	=> 'visibility: hidden',
					'id'		=> $this->tagId($field.'-adj-panel'),
					'onclick'	=> $field.'ImageAdjust.tune()',
				));
		}

		function wikiEditor($field, $options = array())
		{
			$this->needCss('/js/wikieditor/css/wikieditor');
			$this->needJs('wikieditor/wikieditor.js');

			$field_id = $this->tagId($field);
			$toolbar_id = 'wiki-toolbar-'.$field_id;
			$js_init = <<<JS

<script language="JavaScript" type="text/javascript">

	var we = new MyWikiEditor();
	we.field_name = '$field_id';
	we.toolbar = $('$toolbar_id');
	we.run();

</script>
JS;

			return $this->contentTag('div', '', array(
				'class' => 'wiki-toolbar',
				'id'	=> $toolbar_id,
			))."\n".$this->textArea($field, $options).$js_init;
		}

		function colorPicker($field, $options = array())
		{
			$this->needCss('/js/colorpicker/js_color_picker_v2.css');
			$this->needJs('colorpicker/color_functions.js');
			$this->needJs('colorpicker/js_color_picker_v2.js');

			if ($color = $this->value($field))
				$options['style'] .= "; background-color: $color";

			$field_id = $this->tagId($field);
			return $this->textField($field, $options).$this->buttonTag(' ... ', array(
				'style'		=> 'margin-left: 10px',
				'onclick'	=> "showColorPicker(this, $('$field_id'))",
			));
		}
	}
?>