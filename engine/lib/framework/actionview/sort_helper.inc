<?
	class SortHelper extends Helper
	{
		public static
			$image_path 	= '/design/img/',
			$arrows			= array(
				'asc'	=> array('a_up.gif', 'a_up_h.gif'),
				'desc'	=> array('a_down.gif', 'a_down_h.gif'),
			),
			$Inflector		= null;

		public
			$sort_var_name 	= 'sort',
			$meth_var_name	= 'meth',
			$url;

		protected
			$props = array(),
			$templates = array(),
			$template = '#sort# #meth#';

		/**
		 * Possible variants:
		 * <ul>
		 * 	<li>new SortHelper([string $url, ]ActiveRecord $obj, array $field_names);</li>
		 * 	<li>new SortHelper([string $url, ]array $field_info);</li>
		 * 	<li>new SortHelper(string $url);</li>
		 * 	<li>new SortHelper();</li>
		 * </ul>
		 *
		 */
		function __construct()
		{
			$args = func_get_args();

			if (count($args) > 0 && is_string($args[0]))
			{
				$this->url = array_shift($args);
			}
			else
				$this->url = $_SERVER['REQUEST_URI'];

			if (count($args) == 2 && ($args[0] instanceof ActiveRecord) && is_array($args[1]))
				$this->loadProperties($args[0], $args[1]);
			elseif (count($args) == 1 && is_array($args[0]))
				$this->setProperties($args[0]);
		}

		/**
		 * Loads properties from ActiveRecord object
		 *
		 * @param ActiveRecord $object
		 * @param array $field_names
		 */
		function loadProperties(ActiveRecord $object, $field_names)
		{
			foreach ((array)$field_names as $field_name)
				$this->addProperty($field_name, $object->fieldGetCommentOrName($field_name));
		}

		/**
		 * Sets entire array of properties
		 *
		 * @param array $props ($field_name => $caption)
		 */
		function setProperties($props)
		{
			foreach ((array)$props as $field_name => $caption)
				$this->addProperty($field_name, $caption);
		}

		function addProperty($field_name, $caption)
		{
			$this->props[$field_name] = $caption;
		}

		function mkSort($field_name, $caption = null)
		{
			func_num_args() == 1 and $caption = $this->props[$field_name];

			return
				$this->mkSortArrow($field_name, 'asc').
				'&nbsp;'.$caption.'&nbsp;'.
				$this->mkSortArrow($field_name, 'desc');
		}

		protected function mkSortArrow($field_name, $meth)
		{
			$hilite = $_REQUEST[$this->sort_var_name] == $field_name && $_REQUEST[$this->meth_var_name] == $meth;
			$url = setQueryParamEx(array('sort' => $field_name, 'meth' => $meth), $this->url);
			return $this->linkTo($this->tag('img', array('src' => self::$image_path.self::$arrows[$meth][$hilite])), $url);
		}

		function getOrderBy($def_sort = '', $def_meth = 'asc')
		{
			$def_sort or $def_sort = key($this->props);

			if (!isset($this->props[$sort = &$_REQUEST[$this->sort_var_name]]))
				$sort = $def_sort;

			if (!in_array($meth = &$_REQUEST[$this->meth_var_name], array('asc', 'desc')))
				$meth = $def_meth;

			if (isset($this->templates[$sort]))
				$template = $this->templates[$sort];
			else
				$template = $this->template;

			return String::substVars($template, compact('sort', 'meth'));
		}

		/**
		 * @param string $template - recognized substs: #sort#, #meth#
		 *
		 */
		function setTemplate($template, $field_1 = null)
		{
			if (func_num_args() == 1)
				$this->template = $template;
			else
			{
				for ($n = 1; $n < func_num_args(); $n++)
					$this->templates[func_get_arg($n)] = $template;
			}
		}
	}
?>