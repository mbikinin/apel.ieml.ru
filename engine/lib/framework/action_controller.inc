<?
	/**
	 * @package ActionController
	 * @version Wed Mar 19 17:45:52 MSK 2008
	 *
	 */

	class ActionController
	{
		public
			$layout,
			$render_text,
			$render_action,
			$controller_fullname,
			$controller_path,
			$controller_name,
			$action,
			$action_return_vars,
			$params,
			$Hlp,
			$Helper,
			$back,
			$default_back_url = '/',
			$xhr;

		public static
			$Config,
			$Inflector,
			$Session,
			$Con;

		protected
			$cfg,
			$back_url_ns = 'back_ex',
			$urld,
			$capture_current_url = 1,
			$config_ns,
			$mapped_props = array();

		function __construct()
		{
			$this->xhr = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';

			if (func_num_args() == 2)
			{
				$this->controller_path = func_get_arg(0);
				$this->controller_name = func_get_arg(1);
			}
			else
			{
				$ci = $this->getControllerPathAndName(get_class($this));
				$this->controller_path = $ci['path'];
				$this->controller_name = $ci['name'];
			}

			$this->controller_fullname = $this->controller_path.$this->controller_name;

			$this->Hlp = $this->createHelper();
			$this->Helper = $this->createApplicationHelper();

			$this->urld = new BackUrlDetector();
			$this->back = $this->urld->getUrl();

			//$this->createModels();
			$this->onLoad();
			$this->loadConfig();
		}

		/**
		 * Called when controller object is instantiated
		 * (mainly to avoid passing parameters to parent::__construct)
		 */
		protected function onLoad() {}

		/**
		 * Tries to instantiate ActiveRecord vars
		 */
		protected function createModels()
		{
			foreach (get_object_vars($this) as $var_name => $var_value)
				if (String::isUpper($var_name{0}) && is_null($var_value) && __autoload($var_name))
					$this->$var_name = new $var_name;
		}

		protected function loadConfig()
		{
			$this->config_ns or
			$this->config_ns = self::$Inflector->underscore(preg_replace('/Controller$/', '', get_class($this)));

			$this->cfg = self::$Config->existsnamespace($this->config_ns)
				? self::$Config->vars2extract($this->config_ns)
				: array();
		}

		/**
		 * Override this method to execute actions before rendering
		 *
		 */
		function beforeFilter() {}

		function afterFilter() {}

		function redirectTo($options = null)
		{
			Url::redirect($this->Hlp->urlFor($options));
		}

		function goBack()
		{
			$this->redirectTo($this->back);
		}

		function errorTo($options, $message)
		{
			self::$Session->set('error', $message);
			$this->redirectTo($options);
		}

		function errorBack($message)
		{
			$this->errorTo($this->back, $message);
		}

		function noticeTo($options, $message)
		{
			self::$Session->set('notice', $message);
			$this->redirectTo($options);
		}

		function noticeBack($message)
		{
			$this->noticeTo($this->back, $message);
		}

		function createApplicationHelper()
		{
    		if (is_file($helper_file = FWK_HELPERS.$this->controller_fullname.'_helper.inc'))
    		{
    			include_once $helper_file;
    			$helper_class_name = self::$Inflector->camelize($this->controller_name).'Helper';
    		}
    		elseif (is_file($app_helper_file = FWK_HELPERS.'application_helper.inc'))
    		{
    			include_once $app_helper_file;
    			$helper_class_name = 'ApplicationHelper';
    		}
    		else
    			$helper_class_name = null;

    		if ($helper_class_name)
    		{
    			$Helper = new $helper_class_name();
    		}
    		else
    			$Helper = null;

    		$Helper->Hlp = $this->Hlp;
    		return $Helper;
		}

		function createHelper()
		{
			return new Helper($this);
		}

		function createFormHelper(ActiveRecord $object = null, $object_name = null)
		{
			if (func_num_args() == 1)
				$object_name = $object->getObjectNameForFormHelper();

			return new FormHelper($this, $object, $object_name, $this->params);
		}

		function renderAction($action, $params = array())
		{
			if ($this->xhr)
				$this->layout = '';

			$this->action = $action;
			$this->params = $params;

	    	ob_start();

	    	$this->beforeFilter();

	    	$method = self::$Inflector->camelize($action);

	    	if (method_exists($this, $method))
	    	{
		    	$this->action_return_vars = $this->{$method}(&$this->params);
	    	}
	    	elseif (!is_file($view_file = $this->getViewFileForAction($action)))
	    		$this->raise("No action responded to $action (view $view_file)", 'Unknown action');

	    	$this->afterFilter();

	    	if ($this->render_text === false)
	    		return;

    		// render stuff
	    	if ($this->render_text != '')
	    		echo $this->render_text;
	    	else
	    	{
	    		$render_action = $this->render_action or $render_action = $action;

		    	if (is_file($view_file = $this->getViewFileForAction($render_action)))
		    	{
		    		$this->render($view_file);
	    		}
	    		elseif ($this->xhr && !is_array($this->action_return_vars))
	    			echo $this->action_return_vars;
	    		else
	    			$this->raise("No view file $view_file found for action $action", 'View not found');
	    	}

	    	if ($this->layout)
	    	{
	    		if (is_file($layout_file = $this->getLayoutPath($this->layout)))
	    		{
		    		$content_for_layout = ob_get_contents();
		    		ob_end_clean();

		    		$this->render($layout_file, compact('content_for_layout'));

			    	if ($this->capture_current_url)
			    		$this->urld->captureUrl();
	    		}
	    		else
	    			$this->raise("Unable to find layout $layout_file", 'Layout not found');
	    	}
	    	else
	    		ob_end_flush();
		}

		protected function getLayoutPath($layout)
		{
			$pi = pathinfo($layout);

			if ($pi['dirname'] == '.')
				$pi['dirname'] = $this->controller_path;

			return preg_replace('!/+!', '/', FWK_LAYOUTS.$pi['dirname'].'/'.$pi['basename'].'.inc');
		}

		protected function getViewFile($file_name)
		{
			$parents = (array)class_parents($this);
			$controller_fullname = $this->controller_fullname;
			$v0 = '';

			while ($parents)
			{
				if (is_file($view_file = FWK_VIEWS.$controller_fullname.'/'.$file_name))
					return $view_file;

				if (!$v0)
					$v0 = $view_file;

				$ci = $this->getControllerPathAndName(array_shift($parents));
				$controller_fullname = $ci['path'].$ci['name'];
			}

			return $v0;
		}

		protected function getViewFileForAction($action)
		{
			return $this->getViewFile($action.'.inc');
		}

		protected function getPartialPath($partial, $root)
		{
			$pi = pathinfo($partial);

			$file_name = "/_{$pi['basename']}.inc";

			if ($pi['dirname'] == '.')
				$partial = $this->getViewFile($file_name);
			else
				$partial = $root.$pi['dirname'].$file_name;

			return preg_replace('!/+!', '/', $partial);
		}

	    function raise($message, $header = '', $code = '500')
	    {
	    	$header or $header = get_class($this);
    	    throw new ActionControllerException($message, $header, $code);
    	}

		function renderPartial($partial, $params = array())
		{
			$silent = Hash::popKey($params, 'silent');
			if (!$root = Hash::popKey($params, 'root'))
				$root = FWK_VIEWS;

			if (is_file($partial_file = $this->getPartialPath($partial, $root)))
			{
				$this->render($partial_file, $params);
			}
			elseif (!$silent)
			{
				$this->raise("Cannot find partial $partial_file", 'Partial not found');
			}
		}

		function renderLayout($layout, $params = array())
		{
			$params['root'] = FWK_LAYOUTS;
			return $this->renderPartial($layout, $params);
		}

		function getRenderedPartial($partial, $params = array())
		{
			ob_start();
			$this->renderPartial($partial, $params);
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}

		function getRenderedLayout($layout, $params = array())
		{
			ob_start();
			$this->renderLayout($layout, $params);
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}

		function render($file_name, $params = array())
		{
			if (basename($file_name) == $file_name)
				$file_name = $this->getViewFile($file_name);

			if (is_file($file_name))
			{
				$local_vars = null;

				if (isset($params['locals']) && $params['locals'])
				{
					$local_vars = $params['locals'];
				}
				elseif (isset($params['object']) && $params['object'])
				{
					$local_vars = array('object' => &$params['object']);
				}
				else
					$local_vars = $params;

				@extract($local_vars, EXTR_REFS);
				@extract($this->action_return_vars, EXTR_REFS | EXTR_SKIP);
	    		@extract(get_object_vars($this), EXTR_SKIP);
		    	@extract(get_class_vars(get_class($this)), EXTR_SKIP);

		    	chdir(dirname($file_name));
	   			require $file_name;
			}
			else
			{
				$this->raise("Cannot render $file_name", 'File not found');
			}
		}

		function ajaxResponse($response = '')
		{
			exit($response);
		}

		function __get($property_name)
		{
			if (method_exists($this, $method = 'get'.self::$Inflector->camelize($property_name)))
			{
				if (!array_key_exists($property_name, $this->mapped_props))
					$this->mapped_props[$property_name] = $this->$method(/*$property_name*/);

				return $this->mapped_props[$property_name];
			}
		}

		protected function getControllerPathAndName($class_name)
		{
			$pi = pathinfo(self::$Inflector->folderize($class_name));
			return array(
				'path' => $pi['dirname'].'/',
				'name' => preg_replace('/_controller$/', '', $pi['basename']),
			);
		}
	}
?>