<?
	class Mail
	{
		public static
			$encoding = 'Windows-1251';

		static function mimeEncode($str)
		{
			return preg_match("/[\x80-\xFF]/", $str) ? '=?'.self::$encoding.'?B?'.base64_encode($str).'?=' : $str;
		}

		static function toBase64($binary_data)
		{
			return chunk_split(base64_encode($binary_data), 76, "\n");
		}

		static function fileToBase64($file_name)
		{
			return self::toBase64(file_get_contents($file_name));
		}

		static function strToCharEntity($str, $fmt)
		{
			$ln = strlen($str);

			for ($i = 0; $i < $ln; $i++)
			{
				$ch = $str{$i};
				$code = ord($ch);
				$ret .= $code < 128 ? "&#$code;" : $ch;
			}

			return str_replace('%s', $ret, $fmt);
		}

		static function simpleCode($str, $offset)
		{
			$ln = strlen($str);

			for ($i = 0; $i < $ln; $i++)
				if (($code = ord($str{$i})) < 128)
					$str{$i} = chr($code ^ $offset);

			return $str;
		}

		static function strToJs($str, $fmt = '%s', $crypt_offset = null)
		{
			$js = <<<JS
<script type="text/javascript"><!--
var s='%s',c,i
for(i=0;i<%d;i++)if((c=s.charCodeAt(i)^%d)<128)s=s.substr(0,i)+String.fromCharCode(c)+s.substr(i+1)
document.write('%s'.replace(/%%s/g,s))
//--></script>
JS;

			if (is_null($crypt_offset))
				$crypt_offset = mt_rand(5, 31);

			// code email
			$crypt_str = self::simpleCode(htmlspecialchars($str), $crypt_offset);

			return sprintf($js, Js::quote($crypt_str), strlen($crypt_str), $crypt_offset, Js::quote($fmt));
		}

		static function secureEmail($email, $coding = 0, $fmt = '%s')
		{
			$methods = array(
				'strToCharEntity',
				'strToJs',
			);

			$method = $methods[$coding];

			return self::$method($email, $fmt);
		}
	}
?>