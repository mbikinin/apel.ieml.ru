<?
	class String
	{
		static function isUpper($string)
		{
			return strcmp($string, strtoupper($string)) == 0;
		}

		static function isLower($string)
		{
			return strcmp($string, strtolower($string)) == 0;
		}

		/**
		 * Substitutes things like #var# in string with corresponding keys of $subst
		 *
		 * @param string $string - string to perform substitution
		 * @param array $subst - array with substitutions
	 	 *
		 */
		static function substVars($string, $subst)
		{
			return preg_replace('/#([^#]+)#/e', '$subst[$1]', $string);
		}

		static function uniqueId()
		{
			return sha1(uniqid(mt_rand(), 1));
		}

		static function makeSprintfSafe($string)
		{
			return str_replace('%', '%%', $string);
		}

		static function genPassword($pwdlen, $alphabet='')
		{
			$alphabet or $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

			return substr(str_shuffle($alphabet), mt_rand(0, strlen($alphabet) - $pwdlen), $pwdlen);
/*

			for($password = '', $n = 0; $n < $pwdlen; $n++)
				$password .= substr($alphabet, mt_rand(0, strlen($alphabet) - 1), 1);

			return $password;
*/
		}

		static function changeFileExt($file_name, $new_ext = '')
		{
			$new_ext and $new_ext = ".$new_ext";
			return preg_replace('/\.\w+$/', '', $file_name).$new_ext;
		}
	}
?>