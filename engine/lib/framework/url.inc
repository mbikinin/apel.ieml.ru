<?
	class Url
	{
		static function redirect($url)
		{
			header("Location: $url");
			exit;
		}

		static function setQueryParam($param_name, $param_value, $uri = null, $always_set = false, $html_url = false)
		{
			$uri or $uri = $_SERVER['REQUEST_URI'];

			$q_param_name = preg_quote($param_name, '/');
			$URI = preg_replace('/\?$/', '', $uri);
			$URI = preg_replace('/&+$/', '&', $URI);
			$URI = preg_replace('/&$/', '', $URI);
			$URI = preg_replace("/\?$q_param_name=[\w\%\.\,\-\{\}]*$/", '', $URI);
			$URI = preg_replace("/&$q_param_name=[\w\%\.\,\-\{\}]*/", '', $URI);
			$URI = preg_replace("/\?$q_param_name=[\w\%\.\,\-\{\}]*&/", '?', $URI);

			if (!$param_value && !$always_set)
				return $URI;

			$sep = strpos($URI, '?') !== false ? '&' : '?';
			preg_match('/^([^#]*)(#.*)?$/', $URI, $r);
			$data = "$sep$param_name=$param_value";
			$URI = $r[1].$data.(isset($r[2]) ?  $r[2] : '');

			if ($html_url)
				$URI = getValidHmlUrl($URI);

			return $URI;
	    }

	    static function dropQueryParam($param_name, $uri = null)
	    {
	    	return self::setQueryParam($param_name, null, $uri);
	    }

	    static function setQueryParamHash($hParams, $uri = null, $always_set = false, $html_url = false)
	    {
	    	foreach($hParams as $param_name => $param_value)
	    		$uri = self::setQueryParam($param_name, $param_value, $uri, $always_set);

			if ($html_url)
				$uri = self::getValidHmlUrl($uri);

			return $uri;
	    }

		static function htmlize($url)
		{
			return str_replace('&', '&amp;', $url);
		}

	    static function setQueryDNSID($uri = null)
	    {
	    	is_null($uri) and $uri = $_SERVER['REQUEST_URI'];
	    	list($DNSID_name, $DNSID_value) = SplitSID();
	    	return SetQueryParam($DNSID_name, $DNSID_value, $uri);
	    }

		static function wipeQueryString($url)
		{
			return preg_replace('/\?.*$/', '', $url);
		}

		static function isSubmittedBy($button_name)
	    {
	    	return isset($_REQUEST[$button_name]) || isset($_REQUEST[$button_name.'_x']);
	    }

	    static function addTrailingSlash($url)
	    {
	    	if (substr($url, -1) != '/')
	    		return "$url/";
	    }
	}
?>