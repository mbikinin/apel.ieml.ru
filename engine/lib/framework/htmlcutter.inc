<?
	class HtmlCutter
	{
		static function cutChar($html, $max_chars, &$cut, $append = '')
		{
			return self::cut($html, $max_chars, 0, $cut, $append);
		}

		static function cutSentences($html, $max_sentences, &$cut, $append = '')
		{
			return self::cut($html, $max_sentences, 1, $cut, $append);
		}

		protected static function cut($html, $max_len, $type, &$cut, $append)
		{
			$tags = array();
			$ret = '';
			$cut = false;

			$state = 0; // 0 - text, 1 - tag, 2 - " in tag, 3 - ' in tag
			$tpassed = 0; // text passed

			for ($i = 0; $i<strlen($html); $i++)
			{
				$c = substr($html, $i, 1); // multibyte

				switch ($state)
				{
					case 0:
						if ($c == '<')
						{
							$state = 1;
							if (preg_match('/^\w+/', substr($html, $i + 1), $r))
							{
								$tags[] = strtolower($r[0]);
							};

							if (preg_match('/^\/(\w+)/', substr($html, $i + 1), $r))
							{
								$tag_to_close = strtolower($r[1]);

								for ($j = count($tags) - 1; $j>=0; $j--)
									if ($tags[$j] == $tag_to_close)
									{
										array_splice($tags, $j, 1);
										break;
									};
							};
						}
						else
						{
							if ($tpassed >= $max_len)
							{
								$ret .= $append;

								for ($j = count($tags) - 1; $j>=0; $j--)
									$ret .= '</'.$tags[$j].'>';

								$cut = true;

								return $ret;
							};

							if ($type == 0 ||($type == 1 && $c == '.'))
								$tpassed++;
						};
						break;

					case 1:
						if ($c == '"')
							$state = 2;
						elseif ($c == "'")
							$state = 3;
						elseif ($c == '>')
						{
							if (substr($html, $i - 1, 1) == '/')
								array_pop($tags);

							$state = 0;
						};
						break;

					case 2:
						if ($c == '"')
							$state = 1;

						break;

					case 3:
						if ($c == "'")
							$state = 1;

						break;
				};

				$ret .= $c;
			}

			return $ret;
		}
	}
?>