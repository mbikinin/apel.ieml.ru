<?
	class RTF
	{
		static function quote($str)
		{
			mb_substitute_character('long');
			$str = mb_convert_encoding($str, 'Windows-1251');
			$str = preg_replace('/U\+([0-9A-F]{1,3})/e', 'sprintf("\\u%04u?", hexdec("$1"))', $str);
			$str = str_replace('<br />', '\line', nl2br($str));
			$str = preg_replace('/([\x80-\xFF])/e', 'sprintf("\\\'%02x", ord($0))', $str);

			return $str;
		}

		static private function substituteVars($str, $vars_source)
		{
			$regs = $values = array();
			foreach ($vars_source as $var => $value)
			{
				$regs[] = "/%\\s*$var\\s*%/s";
				$values[] = self::quote($value);
			}

			$regs[] = '/%\w+%/';
			$values[] = '';

			return preg_replace($regs, $values, $str);
		}

		static function getFile($filename, $vars_source)
		{
			$file = Read_File_Contents($filename);
			return self::substituteVars($file, $vars_source);
		}

		static function sendFile($template, $file_name, $vars)
		{
			$file = self::getFile($template, $vars);
			Http::downloadHeaders($file_name, 'text/rtf', strlen($file));
			echo $file;
		}
	}
?>