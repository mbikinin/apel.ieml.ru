<?
	class Validate
	{
		/**
		 * Validates that the specified attribute is not blank
		 *
		 * @param array $params, supported keys
		 * 	<ul>
		 * 		<li>array - array of possible values (in_array)
		 * 		<li>hash - hash of possible values (isset)
		 * 	</ul>
		 */
		static function presenceOf($value, $params = null)
		{
			//if ($value === '')
			if (is_string($value) ? $value == '' : is_null($value))
				return false;

			is_array($params) or $params = array();

			if ($params['hash'])
				return array_key_exists($value, $params['hash']);

			if ($params['array'])
				return in_array($value, $params['array']);

			return true;
		}

		/**
		 * Validates that the specified attribute matches the length restrictions supplied
		 *
		 * @param array $params, supported keys
		 * 	<ul>
		 * 		<li>len - the exact size of the attribute
		 * 		<li>min - minimum size of the attribute
		 * 		<li>max - maximum size of the attribute
		 * 	</ul>
		 */
		static function lengthOf($value, $params)
		{
			$len = strlen($value);

			if (array_key_exists('len', $params) && $params['len'] != $len)
				return false;

			if (array_key_exists('min', $params) && $params['min'] > $len)
				return false;

			if (array_key_exists('max', $params) && $params['max'] < $len)
				return false;

			return true;
		}

		/**
		 * Validates that the specified attribute matches the length restrictions supplied
		 *
		 * @param array $params, supported keys
		 * 	<ul>
		 * 		<li>with - the regular expression used to validate the format with
		 * 	</ul>
		 */
		static function formatOf($value, $params)
		{
			return preg_match($params['with'], $value);
		}

		/**
		 * Validates whether the value of the specified attribute is numeric
		 *
		 * @param array $params, supported keys
		 * 	<ul>
		 * 		<li>only_integer - the value must be integer
		 * 		<li>gt - greater than
		 * 		<li>gte - greater than or equal
		 * 		<li>eq - equal to
		 * 		<li>lt - less than
		 * 		<li>lte - less than or equal
		 * 		<li>even - even number
		 * 		<li>odd - odd number
		 * 	</ul>
		 */
		static function numericalityOf($value, $params)
		{
			if (!($params['only_integer'] ? preg_match('/^-?\d+$/', $value) : is_numeric($value)))
				return false;

			if (array_key_exists('gt', $params) && $value <= $params['gt'])
				return false;

			if (array_key_exists('gte', $params) && $value < $params['gte'])
				return false;

			if (array_key_exists('eq', $params) && $value != $params['eq'])
				return false;

			if (array_key_exists('lt', $params) && $value >= $params['lt'])
				return false;

			if (array_key_exists('lte', $params) && $value > $params['lte'])
				return false;

			$is_even = ($value >> 1) & 1;

			if (array_key_exists('even', $params) && !$is_even)
				return false;

			if (array_key_exists('odd', $params) && $is_even)
				return false;

			return true;
		}

		/**
		 * Encapsulates the pattern of wanting to validate a password or email address field with a confirmation.
		 *
		 * @param array $params, supported keys
		 * 	<ul>
		 * 		<li>confirm_value - values needs compared with $value
		 * 	</ul>
		 */
		static function confirmationOf($value, $params)
		{
			return $value == $params['confirm_value'];
		}

		static function netmask($value, $params)
		{
			$octets = explode('.', trim($value));

			if (!$params['single'])
				while ($octets && $octets[count($octets) - 1] == '')
					array_pop($octets);

			for ($n = 0; $n < count($octets); $n++)
				if (!is_numeric($octets[$n]) || $octets[$n] < 0 ||
					($n == 0 && $octets[$n] > 224) ||
					($n == 3 && $octets[$n] == 0) ||
					$octets[$n] > 255)
					return false;

			return true;
		}

		static function emailAddress($email)
		{
			return strlen ($name) <= 254 && preg_match('/^[a-zA-Z0-9][a-zA-Z0-9\-\.\_]{0,62}[\@]([a-zA-Z0-9\-]{1,62}[\.]){2,}$/', "$email.");
		}

		static function uri($url, $params = array())
		{
			$uchar			= "A-Za-z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)\\,\\%\\~";
			$toplabel		= '[A-Za-z][A-Za-z0-9\-]{0,62}';
			$domainlabel	= '[A-Za-z0-9][A-Za-z0-9\-]{0,62}';
			$hostname		= "($domainlabel\\.)*$toplabel";
			$hostnumber		= '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}';
			$host			= "($hostname|$hostnumber)";
			$port			= '\d+';
			$hostport		= "$host(\\:$port)?";
			$search			=
			$hsegment		=
			$anchor			= "[$uchar\\;\\:\\@\\&\\=]*";
			$hpath			= "$hsegment(\\/$hsegment)*";
			$proto			= $params['proto'] or $proto = '(https?|ftp|news)';

			return preg_match("/^(($proto\\:\\/\\/)?$hostport)?(\\/\\~?$hpath(\\?$search)?)?(\\#$anchor)?$/", $url);
		}
	}
?>