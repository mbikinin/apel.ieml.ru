<?
	/**
	 * @package Oracle_Date
	 * @version Wed Mar 26 18:00:27 MSK 2008
	 *
	 */
	class Oracle_Date
	{
		static function fetch($value)
		{
			return $value ? Date::oracleDateToTime($value) : null;
		}

		static function set($value)
		{
			return $value ? Date::textDateToTime($value) : null;
		}

		static function save($value)
		{
			return $value ? Date::timeToOracleDate($value) : null;
		}

		static function format($format, $value)
		{
			return $value ? cyrdate($format, self::fetch($value)) : null;
		}
	}
?>