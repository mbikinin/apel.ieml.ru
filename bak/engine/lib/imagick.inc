<?
	class Imagick
	{
		protected
			$handle,
			$image_file,
			$quality = 75;

		function load($file_name)
		{
			$this->close();

			if (!is_file($file_name) || !($this->handle = imagick_readimage($file_name)))
				$this->error('read failed');
		}

		function close()
		{
			if ($this->handle)
			{
				imagick_destroyhandle($this->handle);
				$this->handle = null;
			}
		}

		function __destruct()
		{
			$this->close();
		}

		function save($file_name, $close = 1)
		{
			if (!imagick_writeimage($this->handle, $file_name))
				$this->error('write failed');

			if ($close)
				$this->close();
		}

		function resize($width, $height, $filter = IMAGICK_FILTER_LANCZOS)
		{
			if (!imagick_resize($this->handle, $width, $height, $filter, 1, '!'))
				$this->error('resize failed');
		}

		function crop($x, $y, $width, $height)
		{
			if (!imagick_crop($this->handle, $x, $y, $width, $height))
				$this->error('crop failed');
		}

		protected function getCropOffset($actual_size, $size, $type)
		{
			switch ($type)
			{
				case 0: // center
					return intval(($actual_size - $size) / 2);

				case 1: // left || top
					return 0;

				case 2: // right || bottom
					return $actual_size - $size;
			}
		}

		function resizeAndCrop($width, $height, $crop_offset = 0)
		{
			$old_width = $this->getWidth();
			$old_height = $this->getHeight();

			$need_crop = 1;
			$height_r = intval($old_height * $width / $old_width);
			$width_r = $width;
			if ($height_r > $height)
			{
				$crop_x = 0;
				$crop_y = $this->getCropOffset($height_r, $height, $crop_offset);
			}
			elseif ($height_r < $height)
			{
				$width_r = intval($old_width * $height / $old_height);
				$height_r = $height;
				$crop_x = $this->getCropOffset($width_r, $width, $crop_offset);
				$crop_y = 0;
			}
			else
			{
				$width_r = $width;
				$height_r = $height;
				$crop_x = $crop_y = 0;
				$need_crop = 0;
			}

			$this->resize($width_r, $height_r);

			if ($need_crop)
				$this->crop($crop_x, $crop_y, $width, $height);
		}

		function adjust($resize_data)
		{
			list($coef, $x, $y, $size) = explode('|', $resize_data);
			list ($wd, $hd) = explode('x', $size);

			$coef	= (double)$coef;
			$x		= (int)$x;
			$y		= (int)$y;
			$wd		= (int)$wd;
			$hd		= (int)$hd;

			$ws = $this->getWidth();
			$hs = $this->getHeight();

		    if ($x < 0 || $x + $wd > $ws * $coef || $y < 0 || $y + $hd > $hs * $coef || $coef == 0)
		    	$this->error('Область выделена неверно');

			if ($coef != 1)
			{
				$this->scale($coef);
			}

			$this->crop($x, $y, $wd, $hd);
			$this->setQuality($this->quality);
		}

		function getWidth()
		{
			if (($width = imagick_getwidth($this->handle)) === false)
				$this->error('getwidth failed');

			return $width;
		}

		function getHeight()
		{
			if (($height = imagick_getheight($this->handle)) === false)
				$this->error('getheight failed');

			return $height;
		}

		function scale($factor)
		{
			$this->resize($this->getWidth() * $factor, $this->getHeight() * $factor);
		}

		function setQuality($quality)
		{
			if (!imagick_set_image_quality($this->handle, $quality))
	       		$this->error('set_quality failed');
		}

		protected function error($message)
		{
			if ($reason = imagick_failedreason($this->handle))
				$reason = "\nReason: $reason";

			if ($description = imagick_faileddescription($this->handle))
				$description = "\nDescription: $description";

			throw new ImagickException($message.$description.$reason, $message);
		}
		
		public function stroke()
		{
			// Output the image
			$output = imagick_getimageblob($this->handle);
			$outputtype = imagick_getformat($this->handle);

			header("Content-type: $outputtype");
			echo $output;
			exit;
		}
	}

	class ImagickException extends FrameworkException {}
?>