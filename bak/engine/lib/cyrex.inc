<?
	//----------------------------------------------------------------------------------------------
	// Cyrillic-related functions v0.03E
	//----------------------------------------------------------------------------------------------
	// Подстановки дат, рус.
	//	1)	Месяц
	// 	Формат записи:
	// 	(м|М)[upperflag][shortflag][additive][case]
	//
	// 	м|М: если М - то название месяца начинается с большой буквы
	// 	upperflag - буква 'б', если присутствует, то все слово большими буквами
	// 	shortflag - буква 'с', если присутствует, то месяц из 3-х букв
	// 	additive - буква, 'н', если с нарастающим итогом (от января)
	// 	case - падеж, варианты: 'и' - им., 'р'- род. 'д' - дат., 'в' - вин., 'т' - тв., 'п' - предл.
	//
	// 	Примеры:
	// 		м - март, М - Март, мб - МАРТ, мс - мар, Мс - Мар, мбс - МАР, мр - марта, мбд - МАРТУ
	//
	// 	2)	День недели
	// 		Формат записи:
	// 		(н|Н)[upperflag][shortflag]
	//
	// 		н|Н: если Н - то назваие недели начинается с большой буквы
	// 		upperflag - буква 'б', если присутствует, то все слово большими буквами
	// 		shortflag - буква 'с', если присутствует, то название из 2-х букв
	//
	// 		Примеры:
	// 			н - среда, Н - Среда, нб - СРЕДА, нс - ср, Нс - Ср, нбс - СР
	// 	3)	Номер квартала
	// 		Формат записи:
	// 		к[rflag]
	//
	// 		rflag - буква, 'р', если число нужно записать в римскими цифрами
	//
	// 		Примеры:
	// 			к - 4, кр - IV
	// 	4)	Номер полугодия
	// 		Формат записи:
	// 		п[rflag]
	//
	// 		rflag - буква, 'р', если число нужно записать в римскими цифрами
	//
	// 		Примеры:
	// 			п - 2, пр - II
	//----------------------------------------------------------------------------------------------

	define("CYR_UPPERCHARS", "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁQWERTYUIOPASDFGHJKLZXCVBNM");
	define("CYR_LOWERCHARS", "йцукенгшщзхъфывапролджэячсмитьбюёqwertyuiopasdfghjklzxcvbnm");

	$CYR_MONTHS = array(
		'и' => array(1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'),
		'р' => array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'),
		'д' => array(1 => 'январю', 'февралю', 'марту', 'апрелю', 'маю', 'июню', 'июлю', 'августу', 'сентябрю', 'октябрю', 'ноябрю', 'декабрю'),
		'в' => array(1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'),
		'т' => array(1 => 'январём', 'февралём', 'мартом', 'апрелем', 'маем', 'июнем', 'июлем', 'августом', 'сентябрём', 'октябрём', 'ноябрём', 'декабрём'),
		'п' => array(1 => 'январе', 'феврале', 'марте', 'апреле', 'мае', 'июне', 'июле', 'августе', 'сентябре', 'октябре', 'ноябре', 'декабре'),
	);

	$CYR_WEEKDAYS = array('воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота');
	$CYR_SHORTWEEKDAYS = array('вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб');

	function cyrstrtoupper($str)
	{
		return mb_convert_case($str, MB_CASE_UPPER);
//		return strtr($str, CYR_LOWERCHARS, CYR_UPPERCHARS);
	};

	function cyrstrtolower($str)
	{
		return mb_convert_case($str, MB_CASE_LOWER);
//		return strtr($str, CYR_UPPERCHARS, CYR_LOWERCHARS);
	};

	function cyrucfirst($str)
	{
//		$str[0] = cyrstrtoupper($str[0]);
//		return $str;
		return mb_convert_case($str, MB_CASE_TITLE);
	};

	function cyr_get_month($p, $month, $cut = false)
	{
		global $CYR_MONTHS;

		$ret = $CYR_MONTHS[$p][$month];

		if ($cut)
			$ret = substr($ret, 0, 3);

		return $ret;
	}

	function cyrdate_r($str, $time)
	{
		global $CYR_MONTHS, $CYR_WEEKDAYS, $CYR_SHORTWEEKDAYS;

		$is_upper = false;

		if (substr($str, 0, 2) == '/Q')
			return substr($str, 2, -2);

		$cmd = cyrstrtolower(substr($str, 0, 1));
		if (substr($str, 1, 0) == 'б')
		{
			$is_upper = true;
			$params = substr($str, 2);
		}
		else
			$params = substr($str, 1);

		switch ($cmd)
		{
			case 'м':
				$month = date('n', $time);

				$cut = $additive = false;

				if (substr($params, 0, 1) == 'с')
				{
					$params = substr($params, 1);
					$cut = true;
				};

				if (substr($params, 0, 1) == 'н')
				{
					$additive = true;
					$p = substr($params, 1, 1);
				}
				else
					$p = substr($params, 0, 1);

				$p or $p = 'и';

				$ret = $additive && $month != 1 ? cyr_get_month($p, 1, $cut).'-' : '';

				$ret .= cyr_get_month($p, $month, $cut);

				break;

			case 'н':
				$weekday = date('w', $time);
				$ret = substr($params, 0, 1) == 'с' ? $CYR_SHORTWEEKDAYS[$weekday] : $CYR_WEEKDAYS[$weekday];
				break;

			case 'к':
				$ret = floor((date('n', $time) - 1)/3) + 1;

				if ($str{1} == 'р')
					$ret = $ret < 4 ? str_repeat('\I', $ret) : '\I\V';

				break;

			case 'п':
				$ret = floor((date('n', $time) - 1)/6) + 1;

				if ($str{1} == 'р')
					$ret = str_repeat('\I', $ret);

				break;


			default:
				$ret = $str;
		};

		if ($cmd != substr($str, 0, 1))
			$ret = cyrucfirst($ret);

		if ($is_upper)
			$ret = cyrstrtoupper($ret);

		return $ret;
	};

	function cyrdate($str, $time = null)
	{
		global $CYR_MONTHS, $CYR_WEEKDAYS, $CYR_SHORTWEEKDAYS;
		if (is_null($time)) $time = time();

		$p = "[ирдвтп]";

		$month = date('n', $time);

		return date(preg_replace("/(?<!\\\\)([мМ]б?с?н?$p?|[нН]б?с?|кр?|пр?)|\\/Q.*\\/E/ue", 'cyrdate_r(\'\\0\', $time)', $str), $time);
	};

	function cyrdate_txt($format, $txt_date)
	{
		split_datetime_txt($txt_date, $day, $month, $year, $hour, $minute, $second);
		return cyrdate($format, mktime($minute, $second, $hour, $month, $day, $year));
	}

/*
	function cyrdate_txt($format, $txt_date)
	{
		global $CYR_MONTHS_NOM, $CYR_MONTHS_GEN, $CYR_WEEKDAYS, $CYR_SHORTWEEKDAYS;

		split_datetime_txt($txt_date, $day, $month, $year, $hour, $minute, $second);

		$month = (int)$month;

		$nom = $CYR_MONTHS_NOM[$month];
		$gen = $CYR_MONTHS_GEN[$month];

		$timestamp = mktime($minute, $second, $hour, $month, $day, $year);
		$weekday = $CYR_WEEKDAYS[date('w', $timestamp)];
		$weekdayshort = $CYR_SHORTWEEKDAYS[date('w', $timestamp)];

		return strtr($format, array('м' => $nom,
			"М" => cyrucfirst($nom),
			"КБ" => cyrstrtoupper($weekdayshort),
			"р" => $gen, "Р" => cyrucfirst($gen),
			"н" => $weekday, "Н" => cyrucfirst($weekday),
			"к" => $weekdayshort, "К" => cyrucfirst($weekdayshort),
			'мс' => substr($nom, 0, 3), 'd' => sprintf('%02d', $day),
			'j' => (int)$day, 'm' => sprintf('%02d', $month),
			'Y' => $year, 'H' => sprintf('%02d', $hour), 'i' => sprintf('%02d', $minute),
			's' => sprintf('%02d', $second)
		));
	}
*/

	function ConvertString($str)
	{
		global $_SERVER;
		$trans = array("windows-1251" => "w", "cp1251" => "w", "koi8-r" => "k", "ibm866" => "d", "iso-8859-5" => "i", "x-mac-cyrillic" => "m");
		return isset($trans[$_SERVER['CHARSET']]) ? convert_cyr_string($str, strtr($_SERVER['CHARSET'], $trans), "k") : $str;
	};

	function ConvertBackString($str)
	{
		global $_SERVER;
		$trans = array("windows-1251" => "w", "cp1251" => "w", "koi8-r" => "k", "ibm866" => "d", "iso-8859-5" => "i", "x-mac-cyrillic" => "m");
		return isset($trans[$_SERVER['CHARSET']]) ? convert_cyr_string($str, "k", strtr($_SERVER['CHARSET'], $trans)) : $str;
	};
?>