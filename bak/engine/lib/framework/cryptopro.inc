<?
	class CryptoPro
	{
	    static function checkSignatureFile($filename, $sign_filename)
	    {
	        $content = Read_File_Contents($filename);
	        $sign = Read_File_Contents($sign_filename);

	        return cpCheckSignatureText($content, $sign);
	    }

	    static function checkSignatureText($content, $sign)
	    {
	        $content = base64_encode($content);
	        $content = iconv('iso8859-1', 'UNICODELITTLE', $content);

	        $message = base64_decode($sign);

	        $ret = $content && $message
	            ? cryptopro_VerifyDetached($content, $message)
	            : null;

			if ($ret)
	           $ret = new CryptoPro_SignatureInfo($ret);

			return $ret;
	    }
	}

	class CryptoPro_SignatureInfo
	{
		protected
			$data = array();

		public
			$raw_data,
			$rdn;

		public static
			$Inflector;

		function __get($property)
		{
			if (array_key_exists($property, $this->data))
				return $this->data[$property];

			return null;
		}

	    function parseRDN($text)
	    {
	        $ret = array();

	        $parts = explode(', ', $text);
	        foreach ($parts as $part)
	        {
	            if (preg_match('/^(\w+)\s*=\s*(.*)$/', $part, $r))
	                $ret[$r[1]] = $r[2];
	        }

	        return $ret;
	    }

		function formatSigningTime($format, $no_data)
		{
			return $this->SigningTime
				? date('d.m.Y H:i', strtotime($this->SigningTime))
				: $no_data;
		}

		function __construct($data)
		{
			$this->raw_data = $data;

			foreach ($data as $k => $v)
			{
				$this->data[self::$Inflector->camelize(strtr($k, ' ', '_'))] =
					$this->raw_data[$k] = iconv('windows-1251', 'utf-8', $v);
			}

			$this->rdn = new MyArrayObject($this->parseRDN($this->Subject));
		}
	}
?>