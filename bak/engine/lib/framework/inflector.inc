<?
	/**
 	* Pluralize and singularize English words.
 	*
 	*/
	class Inflector
	{
		private
			$rules = array(
				array(// plurals
					'/(s)tatus$/i'				=> '\1\2tatuses',
					'/^(ox)$/i'					=> '\1\2en',     # ox
					'/([m|l])ouse$/i'			=> '\1ice',      # mouse, louse
					'/(matr|vert|ind)ix|ex$/i'	=> '\1ices',     # matrix, vertex, index
					'/(x|ch|ss|sh)$/i'			=> '\1es',       # search, switch, fix, box, process, address
					'/([^aeiouy]|qu)y$/i'		=> '\1ies',      # query, ability, agency
					'/(hive)$/i'				=> '\1s',        # archive, hive
					'/(?:([^f])fe|([lr])f)$/i'	=> '\1\2ves',    # half, safe, wife
					'/sis$/i'					=> 'ses',        # basis, diagnosis
					'/([ti])um$/i'				=> '\1a',        # datum, medium
//					'/(p)erson$/i'				=> '\1eople',    # person, salesperson
					'/(m)an$/i'					=> '\1en',       # man, woman, spokesman
					'/(s)taff$/i'				=> '\1taff',     # staff
					'/(c)alendar$/i'			=> '\1alendar',  # calendar
					'/(c)hild$/i'				=> '\1hildren',  # child
					'/(buffal|tomat)o$/i'		=> '\1\2oes',    # buffalo, tomato
					'/(bu)s$/i'					=> '\1\2ses',    # bus
					'/(alias)/i'				=> '\1es',       # alias
					'/(octop|vir)us$/i'			=> '\1i',        # octopus, virus - virus has no defined plural (according to Latin/dictionary.com), but viri is better than viruses/viruss
					'/(ax|cri|test)is$/i'		=> '\1es',       # axis, crisis
					'/s$/'						=> 's',          # no change (compatibility)
					'/$/'						=> 's',
				),
				array(// singulars
					'/(s)tatuses$/i'			=> '\1\2tatus',
					'/status$/i'				=> 'status',
					'/(matr)ices$/i'			=>'\1ix',
					'/(vert|ind)ices$/i'		=> '\1ex',
					'/^(ox)en/i'				=> '\1',
					'/(alias)es$/i'				=> '\1',
					'/([octop|vir])i$/i'		=> '\1us',
					'/(cris|ax|test)es$/i'		=> '\1is',
					'/(shoe)s$/i'				=> '\1',
					'/(o)es$/i'					=> '\1',
					'/(bus)es$/i'				=> '\1',
					'/([m|l])ice$/i'			=> '\1ouse',
					'/(x|ch|ss|sh)es$/i'		=> '\1',
					'/(m)ovies$/i'				=> '\1\2ovie',
					'/(s)eries$/i'				=> '\1\2eries',
					'/([^aeiouy]|qu)ies$/i'		=> '\1y',
					'/([lr])ves$/i'				=> '\1f',
					'/(tive)s$/i'				=> '\1',
					'/(hive)s$/i'				=> '\1',
					'/(drive)s$/i'				=> '\1',
					'/([^f])ves$/i'				=> '\1fe',
					'/(^analy)ses$/i'			=> '\1sis',
					'/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\1\2sis',
					'/([ti])a$/i'				=> '\1um',
//					'/(p)eople$/i'				=> '\1\2erson',
					'/(m)en$/i'					=> '\1an',
					'/(c)hildren$/i'			=> '\1\2hild',
					'/(n)ews$/i'				=> '\1\2ews',
					'/(s)taff$/i'				=> '\1taff',
					'/(c)alendar$/i'			=> '\1alendar',
					'/s$/i'						=> '',
				)
			),
			$uninflected = array(
				'.*[nrlm]ese', '.*deer', '.*fish', '.*measles', '.*ois', '.*pox', '.*rice',
				'.*sheep', 'Amoyese', 'bison', 'Borghese', 'bream', 'breeches', 'britches',
				'buffalo', 'cantus', 'carp', 'chassis', 'clippers', 'cod', 'coitus', 'Congoese',
				'contretemps', 'corps', 'debris', 'diabetes', 'djinn', 'eland', 'elk', 'equipment',
				'Faroese', 'flounder', 'Foochowese', 'gallows', 'Genevese', 'Genoese',
				'Gilbertese', 'graffiti', 'headquarters', 'herpes', 'hijinks', 'Hottentotese',
				'information', 'innings', 'jackanapes', 'Kiplingese', 'Kongoese', 'legis', 'Lucchese',
				'mackerel', 'Maltese', 'mews', 'moose', 'mumps', 'Nankingese', 'nexus',
				'Niasese', 'Pekingese', 'Piedmontese', 'pincers', 'Pistoiese', 'pliers',
				'Portuguese', 'proceedings', 'rabies', 'rhinoceros', 'salmon', 'Sarawakese',
				'scissors', 'sea[- ]bass', 'series', 'Shavese', 'shears', 'siemens', 'species'/*, '.*status'*/,
				'swine', 'testes', 'trousers', 'trout', 'tuna', 'Vermontese', 'Wenchowese',
				'whiting', 'wildebeest', 'Yengeese'
			),
			$irregular = array(array(
				'atlas'     => 'atlases',
				'beef'      => 'beefs',
				'brother'   => 'brothers',
				'child'     => 'children',
				'corpus'    => 'corpuses',
				'cow'       => 'cows',
				'ganglion'  => 'ganglions',
				'genie'     => 'genies',
				'genus'     => 'genera',
				'graffito'  => 'graffiti',
				'hoof'      => 'hoofs',
				'loaf'      => 'loaves',
				'man'		=> 'men',
				'money'		=> 'monies',
				'mongoose'	=> 'mongooses',
				'move'		=> 'moves',
				'mythos'	=> 'mythoi',
				'numen'		=> 'numina',
				'occiput'	=> 'occiputs',
				'octopus'	=> 'octopuses',
				'opus'		=> 'opuses',
				'ox'		=> 'oxen',
				'penis'		=> 'penises',
//				'person'	=> 'people',
				'sex'		=> 'sexes',
				'soliloquy'	=> 'soliloquies',
				'testis'	=> 'testes',
				'trilby'	=> 'trilbys',
				'turf'		=> 'turfs',
			)),
			$uninflected_re,
			$irregular_re = array(),
			$inflected = array();

		public static
			$Memcache;

		function __construct()
		{
			$this->uninflected_re = $this->enclose(implode( '|', $this->uninflected));
			$this->irregular[1] = array_flip($this->irregular[0]);

			for ($n = 0; $n < 2; $n++)
			{
    			$this->irregular_re[$n] = $this->enclose(implode( '|', array_keys($this->irregular[$n])));
			}

			if (self::$Memcache)
			{
				$this->inflected = (array)self::$Memcache->get('inflected');
			}
		}

		function __call($method_name, $args)
		{
			if (method_exists($this, $method = '_'.$method_name))
			{
				$a = func_get_args();
				$key = serialize($a);
				if (!array_key_exists($key, $this->inflected))
				{
					$this->inflected[$key] = call_user_func_array(array($this, $method), $args);

					if (self::$Memcache)
					{
						self::$Memcache->set('inflected', $this->inflected);
					}
				}
				return $this->inflected[$key];
			}

			$class_name = get_class($this);
			throw new Exception("Call to undefined function {$class_name}::{$method_name}");
		}

		/**
 		* Return $word in plural form.
 		*
 		* @param string $word Word in singular
 		* @return string Word in plural
 		*/
    	function _pluralize($word)
    	{
    		return $this->processWord($word, 0);
    		//return $word;
    	}

		/**
 		* Return $word in singular form.
 		*
 		* @param string $word Word in plural
 		* @return string Word in singular
 		*/
    	function _singularize ($word)
    	{
    		return $this->processWord($word, 1);
    		//return $word;
    	}

    	private function processWord($word, $n)
    	{
			if (preg_match('/^('.$this->uninflected_re.')$/i', $word, $m))
				return $word;

        	if (preg_match('/(.*)\\b('.$this->irregular_re[$n].')$/i', $word, $m))
        	{
            	return $m[1].$this->irregular[$n][strtolower($m[2])];
        	}

        	foreach ($this->rules[$n] as $pattern => $replacement)
        	{
        		if (preg_match($pattern, $word))
        			return preg_replace($pattern, $replacement, $word);
        	}

        	return $word;
    	}

		/**
 		* Returns given $lower_case_and_underscored_word as a camelCased word.
 		*
 		* @param string $lower_case_and_underscored_word Word to camelize
 		* @return string Camelized word. likeThis.
 		*/
    	function _camelize($lowerCaseAndUnderscoredWord, $lcfirst = 0)
    	{
      		$str = str_replace(' ', '', ucwords(str_replace('_', ' ', $lowerCaseAndUnderscoredWord)));

      		if ($lcfirst)
      			$str = strtolower($str{0}).substr($str, 1);

      		return $str;
    	}

		/**
 		* Returns an underscore-syntaxed ($like_this_dear_reader) version of the $camel_cased_word.
 		*
 		* @param string $camel_cased_word Camel-cased word to be "underscorized"
 		* @return string Underscore-syntaxed version of the $camel_cased_word
 		*/
    	function _underscore($camelCasedWord)
    	{
      		return strtolower(preg_replace('/(?<=\\w)(?<!_)([A-Z])/', '_\\1', $camelCasedWord));
    	}

		/**
 		* Returns a human-readable string from $lower_case_and_underscored_word,
 		* by replacing underscores with a space, and by upper-casing the initial characters.
 		*
 		* @param string $lower_case_and_underscored_word String to be made more readable
 		* @return string Human-readable string
 		*/
    	function _humanize($lowerCaseAndUnderscoredWord)
    	{
      		return ucwords(str_replace('_', ' ', $lowerCaseAndUnderscoredWord));
    	}

		/**
 		* Returns corresponding table name for given $class_name. ("posts" for the model class "Post").
 		*
 		* @param string $class_name Name of class to get database table name for
 		* @return string Name of the database table for given class
 		*/
    	function _tableize($className)
    	{
      		return $this->pluralize($this->underscore($className));
    	}

		/**
 		* Returns Cake model class name ("Post" for the database table "posts".) for given database table.
 		*
 		* @param string $tableName Name of database table to get class name for
 		* @return string
 		*/
    	function _classify($tableName)
    	{
      		return $this->camelize($this->singularize($tableName));
    	}

    	/**
    	 * Returns foreign key for given class_name ("post_id" for the model class "Post").
    	 *
    	 * @param string $class_name Name of class to get foreign key for
    	 * @return string
    	 */
    	function _foreignKey($class_name)
    	{
    		return $this->underscore($class_name).'_id';
    	}

    	function _folderize($camel_case_word)
    	{
    		return $this->underscore(str_replace('_', '/', $camel_case_word));
    	}

    	/**
    	 * Returns underscored string for given relative url
    	 * @return string
    	 */
    	function _deurlize($url)
    	{
    		return str_replace('-', '_', $url);
    	}

    	private function enclose($string)
		{
    		return '(?:'.$string.')';
		}
	}
?>