<?
	class _Array
	{
		static function trim($array)
		{
			$r = array();

			foreach ($array as $n => $elt)
			{
				$r[$n] = is_array($elt) ? self::trim($elt) : trim($elt);
			}

			return $r;
		}
	}
?>