<?
	class Http
	{
		static function downloadHeaders($file_name, $mime_type, $bytes = 0)
		{
			header("Content-Disposition: attachment; filename=\"$file_name\"\r\n");
			header("Content-Type: $mime_type\r\n");
			header("Content-Type: application/force-download\r\n", false);
			header("Content-Type: application/download\r\n", false);
			header("Content-Transfer-Encoding: binary\r\n");

			if ($bytes)
				header("Content-Length: $bytes\r\n");
		}
	}
?>