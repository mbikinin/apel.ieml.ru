<?
	abstract class MySQLTreeActiveRecord extends ActiveRecord
	{
		protected
			$parent_id_name = 'parent_id',
			$order_by = 'lft',
			$rgts;

		function getRoot()
		{
			return $this->findByLft(1);
		}

		function isRoot()
		{
			return $this->lft == 1;
		}

		function getParents()
		{
			return $this->find(array('all' => 1, 'where' => array("? BETWEEN lft AND rgt", $this->lft)));
		}

		function getChildren($direct = 0)
		{
			$where = $direct
				? array('parent_id = ?', $this->id)
				: array('lft BETWEEN #lft# AND #rgt#', $this->getAttributes());

			return $this->find(array('all' => 1, 'where' => $where));
		}

		function getParent()
		{
			return $this->find($this->parent_id);
		}

		function insert()
		{
			$this->Con->Lock("{$this->table_name} WRITE");

			$parent = clone($this);
			if (!$parent->find($this->__get($this->parent_id_name)))
			{
				$this->Con->Unlock();
				return null;
			}

			$x = $parent->rgt - 1;
			$lft = $parent->rgt;
			$rgt = $parent->rgt + 1;

			$this->Con->Query(array("UPDATE {$this->table_name} SET rgt = rgt + 2 WHERE rgt > ?", $x));
			$this->Con->Query(array("UPDATE {$this->table_name} SET lft = lft + 2 WHERE lft > ?", $x));

			$this->__set('lft', $lft);
			$this->__set('rgt', $rgt);

			return parent::insert();
		}

		function free()
		{
			parent::free();
			$this->rgts = array();
		}

		function fetch()
		{
			if ($obj = parent::fetch())
			{
				$obj->fields['level'] = $this->getLevel($obj->rgt);
			}

			return $obj;
		}

		protected function getLevel($rgt)
		{
			while (count($this->rgts) > 0 && $this->rgts[count($this->rgts)-1] < $rgt)
				array_pop($this->rgts);

			$level = count($this->rgts);

			$this->rgts[] = $rgt;

			return $level;
		}

		function move($up)
		{
			self::$Con->Lock("{$this->table_name} WRITE");

			if (!$this->lft || !$this->rgt)
			{
				self::$Con->Unlock();
				return false;
			}

			$lft_b = $this->lft;
			$rgt_b = $this->rgt;

			if ($up)
			{
				$lft_c = $lft_b;
				$rgt_c = $rgt_b;

				$rgt_b = $lft_c - 1;

				if (!$r = $this->findByRgt($rgt_b))
				{
					self::$Con->Unlock();
					return false;
				}

				$lft_b = $r->lft;
			}
			else
			{
				$lft_c = $rgt_b + 1;

				if (!$l = $this->findByLft($lft_c))
				{
					self::$Con->Unlock();
					return false;
				}

				$rgt_c = $l->rgt;
			}

			$size_c = $rgt_c - $lft_c + 1;
			$size_b = $rgt_b - $lft_b + 1;

			$SQL = <<<SQL
UPDATE {$this->table_name} SET
rgt = rgt + IF(lft<$lft_c, $size_c, - $size_b),
lft = lft + IF(lft<$lft_c, $size_c, - $size_b)
WHERE lft BETWEEN $lft_b AND $rgt_c
SQL;

			self::$Con->Query($SQL);
			self::$Con->Unlock();
			return true;
		}

		function drop()
		{
/*
			$this->Con->Lock("{$this->table_name} WRITE");

			$this->Con->Query("DELETE FROM {$this->table_name} WHERE lft BETWEEN {$this->lft} AND {$this->rgt}");

			$gap = $this->rgt - $this->lft + 1;

			$SQL = <<<SQL
UPDATE {$this->table_name}
SET lft = IF(lft > {$this->lft}, lft - $gap, lft), rgt = IF(rgt >  {$this->lft}, rgt - $gap, rgt)
SQL;
			$this->Con->Query($SQL);

			$this->Con->Unlock();
*/
		}

		function moveTo($id, $newparent)
		{
/*
			$Con->Lock("{$this->table} WRITE");

			list ($lft, $rgt) = $Con->EzQuery("SELECT lft, rgt FROM {$this->table} WHERE {$this->id}='$id'");

			if ($lft)
			list ($lft_p, $rgt_p) = $Con->EzQuery("SELECT lft, rgt FROM {$this->table} WHERE {$this->id}='$newparent'");

			$subtree = FALSE;

			if (!$lft || !$lft_p || $id == $newparent || $lft == $lft_p || ($subtree = ($lft_p >= $lft && $lft_p <= $rgt)))
			{
				$this->error_msg = '�??�??�??�??�??�?? �??�??�??�??�??�??�??�??�??�??�??' . ($subtree ? ': �??�??�??�??�??�??�??�??�??�?? �??�??�??�??�??�??�??�??�??�??�?? �??�??�??�?? �??�?? �??�??�??�??�?? �??�??�?? �??�??�??�??�??�??�??�??' : '');
				$this->Con->Unlock();
				return FALSE;
			}

			$gap = $rgt - $lft + 1;
			$y = $rgt_p - 1;
			$ofs = $rgt_p - $lft;

			if ($rgt_p > $rgt)
				$ofs -= $gap;

			if ($y > $lft)
			{
				$m1 = $lft + 1;
				$m2 = $y;

				$SQL = "UPDATE {$this->table} SET ".
					"lft = lft + IF(@c:=lft BETWEEN $lft AND $rgt, $ofs, ".
						"IF(lft BETWEEN $m1 AND $m2, -$gap, 0)), ".
					"rgt = rgt + IF(@c, $ofs, ".
						"IF(rgt BETWEEN $m1 AND $m2, -$gap, 0)) ".
					"WHERE (lft BETWEEN $lft AND $m2) OR ".
						"(rgt BETWEEN $m1 AND $m2)";
			}
			else
			{
				$m1 = $y + 1;
				$m2 = $lft + 1;
				$m3 = $y + $gap;

				$SQL = "UPDATE {$this->table} SET ".
							"lft = lft + IF(@c:=lft BETWEEN $lft AND $rgt, $ofs, ".
						"IF(lft BETWEEN $m1 AND $lft, $gap, ".
						"IF(lft BETWEEN $m2 AND $m3, -$gap, 0))), ".
							"rgt = rgt + IF(@c, $ofs, ".
						"IF(rgt BETWEEN $m1 AND $lft, $gap, ".
						"IF(rgt BETWEEN $m2 AND $m3, -$gap, 0))) ".
						"WHERE (lft BETWEEN $m1 AND $rgt) OR ".
						"(rgt BETWEEN $m1 AND $m3)";
			}

			$this->Con->Query($SQL);

			$this->Con->Query("UPDATE {$this->table} SET {$this->parent_id} = '$newparent' WHERE {$this->id} = '$id'");

			$this->Con->Unlock();

			$this->error_msg = '';
			return TRUE;
*/
		}
	}
?>