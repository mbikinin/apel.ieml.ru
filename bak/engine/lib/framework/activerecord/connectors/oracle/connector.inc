<?
	/**
	 * @package Oracle_ActiveRecordConnector
	 * @version Wed Mar 12 15:51:40 MSK 2008
	 *
	 */

	class Oracle_ActiveRecordConnector extends ActiveRecordConnector
	{
		protected
			$CommitMode,
			$log_data;

		function __construct($params)
		{
			$params['db'] and $params['db'] = '/'.$params['db'];
			$db = $params['host'].$params['db'];

			if ($params['persistent'])
				$this->dbh = oci_pconnect($params['user'], $params['pass'], $db, $params['charset']);
			elseif ($params['new'])
				$this->dbh = oci_new_connect($params['user'], $params['pass'], $db, $params['charset']);
			else
				$this->dbh = oci_connect($params['user'], $params['pass'], $db, $params['charset']);

			if (!is_resource($this->dbh))
				$this->raise();

			$this->CommitOn();
		}

		function getLastError($stmt = null)
		{
			$handle = $stmt or $handle = $this->dbh;

			if ($handle)
				$error = oci_error($handle);
			else
				$error = oci_error();

			return $error
				? "{$error['message']} (offset {$error['offset']})<pre>{$error['sqltext']}</pre>"
				: null;
		}

		function ListTables()
		{
			$Ret = array();

			$Res = $this->Query("SELECT * FROM tab");
			while($row = $this->FetchRow($Res))
				$Ret[] = $row[0];
			$this->FreeResult($Res);

			return $Ret;
		}

		function Close()
		{
			$this->IsConnected = FALSE;
			return @oci_close($this->dbh);
		}

		function Parse($query, &$raw_sql)
		{
			if ($query instanceof ActiveRecordStatement)
			{
				list ($sql, $bvars) = $query->buildQuery();
			}
			elseif (is_array($query))
			{
				if (is_array($query[1]))
				{
					list ($sql, $bvars)	= $query;
				}
				else
				{
					$sql = array_shift($query);
					$bvars = array();
					$n = 0;
					$sql = preg_replace('/\?/e', '$this->phToBindVar($query, $bvars, $n)', $sql);
				}
			}
			else
				$sql = $query;

//			if ($this->debug)
//				echo $this->prepareQuery(array($sql, $bvars));

			if (($stmt = OCIParse($this->dbh, $sql)) === false)
				$this->raise();

			if (is_array($bvars))
			{
				$pe = new ARS_ParamExtractor($sql, $bvars);
				$bvars = $pe->extract();

				extract($bvars);
				foreach (array_keys($bvars) as $__name)
				{
					if (is_integer(${$__name}))
					{
						oci_bind_by_name($stmt, ":$__name", ${$__name}, -1, SQLT_INT);
					}
					else
					{
						oci_bind_by_name($stmt, ":$__name", ${$__name});
					}
				}
			}

/*
			$raw_sql = trim($this->prepareQuery(array($sql, $bvars)));
			$this->log_data = '';
			if (preg_match('/^(DELETE|INSERT\s+INTO|UPDATE)/i', $raw_sql))
			{
				$bt = '';
				foreach (debug_backtrace() as $_)
				{
					if ($_['class'])
						$_['class'] .= '::';

					$bt .= "{$_['class']}{$_['function']} at {$_['file']} line {$_['line']}\n";
				}

				$req = var_export($_REQUEST, 1);
				$time = date('r');

				$this->log_data = <<<ENT
$time {$_SERVER['REQUEST_URI']}
-------------------------------
$bt
---------
$req
---------
$raw_sql
ENT;
			}
*/
			if ($this->debug)
				$raw_sql = $this->prepareQuery(array($sql, $bvars));

			return $stmt;
		}

/*
		function saveLogData($rows_affected)
		{
			if ($this->log_data)
			{
				file_put_contents(String::changeFileExt(__FILE__, 'log'), "{$this->log_data}\n$rows_affected rows affected\n\n\n", FILE_APPEND);
			}
		}
*/
		function query($stmt, $parse=TRUE)
		{
			if ($this->debug)
				$mt = microtime(1);

			$query = '';
			if ($parse)
			{
				$stmt = $this->Parse($stmt, $query);
			}

			if (@oci_execute($stmt, $this->CommitMode) === false)
				$this->raise($stmt);

//			$this->saveLogData(@oci_num_rows($stmt));

			if ($this->debug)
				$this->debugQuery($query, microtime(1) - $mt);

			return $stmt;
		}

		protected function phToBindVar($pvars, &$bvars, &$n)
		{
			$key = "bindvar$n";
			$bvars[$key] = $pvars[$n];
			$n++;
			return ":$key";
		}

		function AffectedRows($stmt)
		{
			return OCIRowCount($stmt);
		}

		function Freeresult($stmt)
		{
			is_resource($stmt) and OCIFreeStatement($stmt);
		}

		function Insertid($sequencer)
		{
			$Res = $this->Query("SELECT $sequencer.CURRVAL FROM dual");
			list($value) = $this->Fetchrow($Res);
			$this->Freeresult($Res);
			return $value;
		}

		function GetSeqNextVal($seq_name)
		{
			$Res = $this->Query("SELECT $seq_name.NEXTVAL FROM dual");
			list ($value) = $this->Fetchrow($Res);
			$this->Freeresult($Res);

			return $value;
		}

/*
		function LobExtractRow(&$row)
		{
			foreach ((array)$row as $key => $value)
				if (is_object($value) && get_class($value) == 'OCI-Lob')
					$row[$key] = $value->load();
		}
*/
		function Fetchrow($stmt)
		{
			return oci_fetch_array($stmt, OCI_NUM | OCI_RETURN_NULLS | OCI_RETURN_LOBS);
//			OCIFetchInto($stmt, $row, OCI_NUM | OCI_RETURN_NULLS | OCI_RETURN_LOBS);
//			$this->LobExtractRow($row);
//			return $row;
		}

		function Fetcharray($stmt, $key_case = CASE_LOWER)
		{
//			OCIFetchInto($stmt, $row, OCI_ASSOC | OCI_RETURN_NULLS | OCI_RETURN_LOBS);
//			$this->LobExtractRow($row);
			$row = oci_fetch_array($stmt, OCI_ASSOC | OCI_RETURN_NULLS | OCI_RETURN_LOBS);
			unset($row['#']);

			return is_null($key_case) || !is_array($row) ? $row : array_change_key_case($row, $key_case);
		}

		function fetchAssoc($stmt, $key_case = CASE_LOWER)
		{
			return $this->fetchArray($stmt, $key_case);
		}

		function FetchAll($stmt, $skip=0, $maxrows=-1, $flags=OCI_FETCHSTATEMENT_BY_ROW)
		{
			OCIFetchStatement($stmt, $ret, $skip, $maxrows, $flags);
			return $ret;
		}

		function Commit()
		{
			OCICommit($this->dbh);
		}

		function Rollback()
		{
			OCIRollback($this->dbh);
		}

		function EzQuery($Query, $Params=array())
		{
			$this->_ezQueryInit($Params);

			$Res = $this->Query($Query);

			if($Params['vertical'])
			{
				$im_Ret = $this->FetchAll($Res, 0, -1, OCI_FETCHSTATEMENT_BY_COLUMN |
					($Params['assoc'] ? OCI_ASSOC : OCI_NUM));

				if($Params['lowercase'])
					$im_Ret = array_change_key_case($im_Ret, CASE_LOWER);
			}
			else
			{
				$im_Ret = $this->FetchAll($Res, 0, -1, OCI_FETCHSTATEMENT_BY_ROW |
					($Params['assoc'] ? OCI_ASSOC : OCI_NUM));

				if($Params['lowercase'])
					for($n=0; $n<count($im_Ret); $n++)
						$im_Ret[$n] = array_change_key_case($im_Ret[$n], CASE_LOWER);
			}
			$this->FreeResult($Res);

			$Rows = count($im_Ret);

			if($Params['makehash'] || $Params['callback'] || $Params['simplify'] || $Params['template'] || $Params['multihash'])
			{
				$Ret = $Params['initret'];
				for($Row=0; $Row<$Rows; $Row++)
					$this->_ezQueryProcess($Ret, $im_Ret[$Row], $Params, $Row, $Rows);
			}
			else
				$Ret = array_merge($Params['initret'], $im_Ret);

			return $this->_ezQueryFinalize($Ret, $Rows, $Params);
		}

		function Limit($SQL, $rows, $offset=0)
		{
			if (is_array($SQL))
			{
				$sql = &$SQL[0];
			}
			else
			{
				$sql = &$SQL;
			}

			$sql = sprintf(	'SELECT * FROM ( SELECT rownum "#", "~".* FROM (%s) "~") '.
							'WHERE "#" > %u and rownum <= %u', $sql, $offset, $rows);

			return $SQL;
		}

		function IsRows($SQL)
		{
			$xSQL = $this->Limit($SQL, 1);

			$res = $this->Query($xSQL) or $this->PrintError(__FILE__, __LINE__);
			$row = $this->Fetchrow($res);
			$this->Freeresult($res);

			return is_array($row);
		}

		function BindLob($stmt, $type, $lobVar)
		{
			//well, first you have to create a descriptor
			if (($lob = OCINewDescriptor($this->dbh, OCI_D_LOB)) === false)
				$this->raise();

			//then you have to bind :THE_CLOB to $lob
			if (@OCIBindByName($stmt, $lobVar, &$lob, -1, $type) == false)
				$this->raise($stmt);

			return $lob;
		}

		function BindLobEx($stmt, $type, &$lobData)
		{
			for( $i = 0; $i < count($lobData); $i++ )
			{
				// $lob[0] - key, like ':THE_LOB'. $lob[1] - big value
				// in $lob[2] we save lob descriptor
				$lobData[$i][2] = $this->BindLob($stmt, $type, $lobData[$i][0]);
			}
		}


		function SaveLob($lob, $lobValue, $freeDesc = TRUE)
		{
			$lob->save($lobValue);

			// free your descriptor if need
			if( $freeDesc )
				OCIFreeDescriptor($lob);
		}

		function SaveLobEx($lobData)
		{
			foreach ($lobData as $lob)
			{
				$this->SaveLob($lob[2], $lob[1], FALSE);
			}
		}

		function FreeDescriptor(&$lob)
		{
//			$lob->close();
		}

		function LobQuery($query, $lobType, $lobData = array())
		{
			if ($this->debug)
				$mt = microtime(1);

			$stmt = $this->Parse($query, $raw_sql);

			//then you have to bind :THE_CLOB to $clob
			$this->BindLobEx($stmt, $lobType, $lobData);

			if (@OCIExecute($stmt, OCI_DEFAULT) == false)
				$this->raise($stmt);

//			$this->saveLogData(@oci_num_rows($stmt));

			//and save the information to the db via the descriptor
			$this->SaveLobEx($lobData);

			$this->Commit();

			//free your descriptor
			foreach($lobData as $lob)
				$this->FreeDescriptor($lob[2]);

			if ($this->debug)
				$this->debugQuery($raw_sql, microtime(1) - $mt);

			return $stmt;
		}

		//------------------------------------------------------------------------------------------
		// Private members
		//------------------------------------------------------------------------------------------
		protected function _ezQueryInit(&$Params)
		{
			isset($Params['lowercase'])	or $Params['lowercase'] = TRUE;	//convert field names to lowercase
			parent::_ezQueryInit(&$Params);
		}

		protected function _ezQueryProcess(&$Ret, $aRow, $Params, $Row, $Rows)
		{
			unset($Params['vertical']);
			parent::_ezQueryProcess($Ret, $aRow, $Params, $Row, $Rows);
		}

		function Escape($string)
		{
			return str_replace("'", "''", $string);
		}

		function EscapeFieldName($field_name)
		{
			return '"'.strtoupper($field_name).'"';
		}

		function CommitOn()
		{
			$this->CommitMode = OCI_COMMIT_ON_SUCCESS;
		}

		function CommitOff()
		{
			$this->CommitMode = OCI_DEFAULT;
		}

		function tableInfo($table_name, $link_name = '')
		{
			$table_name = strtoupper($table_name);

			if ($link_name)
				$link_name = "@$link_name";

			$SQL = <<<SQL
SELECT LOWER(ucc.column_name)
FROM user_cons_columns$link_name ucc
INNER JOIN user_constraints$link_name uc USING(constraint_name)
WHERE uc.table_name = :table_name AND uc.constraint_type = 'P'
SQL;
			$primary_key = $this->ezQuery(array($SQL, compact('table_name')));

			$SQL = <<<SQL
SELECT LOWER(column_name) column_name, data_type, data_scale, data_default, nullable, char_length
FROM user_tab_cols$link_name
WHERE table_name = :table_name
SQL;
			$native_types = $pdo_types = $defaults = $nullables = $char_lengths = array();
			//$native_type = $pdo_types = $defaults = $nullables = $char_lengths = array();
			while ($_ = $this->AutoFetch(array($SQL, compact('table_name')), $rs))
			{
				switch ($_['data_type'])
				{
					case 'NUMBER':
						$db_types[$_['column_name']] =
						$native_types[$_['column_name']] = $_['data_scale'] > 0 ? 'float' : 'int';
						//$native_type[$_['column_name']] = $_['data_scale'] > 0 ? 'float' : 'int';
						break;

					case 'FLOAT':
						$db_types[$_['column_name']] =
						$native_types[$_['column_name']] = 'float';
						//$native_type[$_['column_name']] = 'float';
						break;

					case 'BLOB':
					case 'CLOB':
					case 'NCLOB':
						$db_types[$_['column_name']] = 'lob';
						$native_types[$_['column_name']] = 'string';
						//$native_type[$_['column_name']] = 'string';
						break;

					case 'DATE':
						$db_types[$_['column_name']] = 'date';
						$native_types[$_['column_name']] = 'string';
						//$native_type[$_['column_name']] = 'string';
						break;

					default:
						$db_types[$_['column_name']] =
						$native_types[$_['column_name']] = 'string';
						//$native_type[$_['column_name']] = 'string';
				}

				$defaults[$_['column_name']] = $_['data_default'];
				$char_lengths[$_['column_name']] = (int)$_['char_length'];
				$nullables[$_['column_name']] = $_['nullable'] == 'Y';
			}

			$SQL = <<<SQL
SELECT LOWER(column_name) column_name, comments
FROM user_col_comments$link_name
WHERE table_name = :table_name
SQL;
			$comments = $this->ezQuery(array($SQL, compact('table_name')), array('makehash' => 1));

			return compact('primary_key', 'native_types', 'db_types', 'defaults', 'comments', 'char_lengths', 'nullables');
			//return compact('primary_key', 'native_type', 'db_types', 'defaults', 'comments', 'char_lengths', 'nullables');
		}

		function upsert($update, $table_name, $fields, $where = '', $field_types = array())
		{
			$set = $names = $values = $lob_fields = $lob_vars = $lob_data = $query_params = array();

			foreach ($fields as $name => $fvalue)
			{
				$esc_name = $this->EscapeFieldName($name);

				if ($field_types[$name] == 'lob')
				{
					$value = 'EMPTY_CLOB()';

					if (strlen($value) > 0)
					{
						$lob_key = ':CLOB_'.strtoupper($name);
						$lob_fields[] = $esc_name;
						$lob_vars[] = $lob_key;
						$lob_data[] = array($lob_key, $fvalue);
					}
				}
				else
				{
					$value = ":$name";
					$query_params[$name] = $fields[$name];
				}

				if ($update)
					$set[] = "$esc_name = $value";
				else
				{
					$names[] = $esc_name;
					$values[] = $value;
				}
			}

			foreach (array('set', 'names', 'values', 'lob_fields', 'lob_vars') as $_)
				$$_ = implode(', ', $$_);

			if ($update)
			{
				$SQL = "UPDATE $table_name SET $set";
				if ($where)
				{
					$st = $this->getStatement();
					$st->whereAdd($where);
					$SQL .= " WHERE {$st->where}";
					$query_params = array_merge($query_params, $st->params);
				}
			}
			else
			{
				$SQL = "INSERT INTO $table_name ($names) VALUES($values)";
			}

//Debug::varDump($SQL, $query_params, $lob_vars);
			if ($lob_fields)
			{
				$SQL .= " RETURNING $lob_fields INTO $lob_vars";
				$stmt = $this->LobQuery(array($SQL, $query_params), OCI_B_CLOB, $lob_data);
			}
			else
			{
				$stmt = $this->query(array($SQL, $query_params));
			}

			$this->Freeresult($stmt);
		}

		function getNextId($table_name, $seq_suffix = '_id_seq')
		{
			$sequence_name = strtoupper($table_name.$seq_suffix);

			$this->query("ALTER SEQUENCE $sequence_name NOCACHE");
			return $this->ezQuery("SELECT last_number FROM user_sequences WHERE sequence_name = '$sequence_name'");
		}

		protected function raise($stmt = null)
		{
			parent::raise($this->getLastError($stmt), 0);
		}

		function isNull($value)
		{
			return is_null($value) || $value === '';
		}

		function createInFilter($field_name, $values)
		{
			$bvars = $bvalues = array();
			foreach ($values as $n => $v)
			{
				$key = "bvs$n";
				$bvalues[$key] = $v;
				$bvars[] = ":$key";
			}

			while (count($splice = array_splice($bvars, 0, 1000)) > 0)
			{
				$splices[] = "$field_name IN(".implode(",", $splice).")";
			}

			return array(
				'where'		=> implode(' OR ', $splices),
				'params'	=> $bvalues,
			);
		}

		function getFullTableName($table_name, $link_name)
		{
			$ret = $table_name;

			if ($link_name)
				$ret .= "@{$link_name}";

			return $ret;
		}

		function isValid()
		{
			return (bool)$this->ezQuery("SELECT 1 FROM dual");
		}
	}
?>