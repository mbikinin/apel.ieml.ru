<?
	abstract class OraTreeActiveRecord extends ActiveRecord
	{
		protected
			$parent_id_name = 'parent_id',
			$root_ids,
			$multiple_roots = 0,
			$root_set = false;

		public static
			$pad_char = '&nbsp;';

		function __construct()
		{
			parent::__construct();

			$this->root_ids = array((int)$this->findByParent_id(0, array(
				'fetch_as'	=> self::FETCH_AS_ARRAY,
				'select'	=> 'id',
			)));
		}

		protected function applyRootCond($SQL = null, $equals = 1, $root_ids = null)
		{
			if ($SQL === null)
				$SQL = "{$this->id_name} #root_cond#";

			if ($root_ids === null)
				$root_ids = $this->root_ids;

			$root_ids = (array)$root_ids;

			if (count($root_ids) == 1)
			{
				$root_id = $root_ids[0];
				$root_cond = ($equals ? '='  : '<>')." :root_id";
				$params = compact('root_id');
			}
			else
			{
				$root_cond = ($equals ? '' : 'NOT ')."IN(".implode(',', array_map('intval', $root_ids)).")";
				$params = null;
			}

			$SQL = String::substVars($SQL, compact('root_cond'));

			if ($params)
				$SQL = array($SQL, $params);

			return $SQL;
		}

		function setRoot($id)
		{
			$this->multiple_roots = func_num_args() > 1 || is_array($id);

			$this->root_ids = func_num_args() == 1
				? (array)$id
				: func_get_args();

			$this->root_set = true;

//			if (count($this->root_ids) > 1)
//				$this->root_cond = implode(',', array_map('intval', $this->root_ids));

//			$this->setRootFilter();
		}

		protected function setRootFilter($args)
		{
			if (!$this->root_set)
				return;

			$table_alias = is_array($args) ? $args['table_alias'] : '';
			$id_name = $this->applyTableAlias($this->id_name, $table_alias);

			$SQL = <<<SQL
$id_name IN(
	SELECT {$this->id_name}
	FROM {$this->table_name}
	START WITH {$this->id_name} #root_cond#
	CONNECT BY PRIOR {$this->id_name} = {$this->parent_id_name}
)
SQL;
			$this->setFilter($this->applyRootCond($SQL));
		}

		function find($args = array())
		{
			$this->setRootFilter($args);

			if (is_array($args) && $args['as_tree'])
			{
				if (!$args['select'] || $args['select'] == '*')
					$args['select'] = $this->makeJoinFields($args);

				if (!isset($args['fetch_level']))
					$args['fetch_level'] = $args['fetch_as'] != self::FETCH_AS_ARRAY;

				if ($args['fetch_level'])
				{
					if ($args['select'] == '*')
						$args['select'] = "{$this->table_name}.*";

					$args['select'] .= ", LEVEL";
				}

				$order_by = $args['order_by'] or $order_by = $this->order_by;
				$args['order_by'] = '';

				$p_args = Hash::wipeKey($args, 'as_tree', 'connect', 'parent_id', 'limit', 'offset', 'include_root', 'to_peak');
				$st = $this->makeSelectQuery($p_args);

				if (!array_key_exists('include_root', $args))
				{
					$args['include_root'] = 1;
				}

				if ($args['connect_by'])
				{
					$st->connect_by = $args['connect_by'];
				}
				else
				{
					$parent_id_name = $this->applyTableAlias($this->parent_id_name);
					$id_name = $this->applyTableAlias($this->id_name);
					$st->connect_by = $args['to_peak']
						? "PRIOR $parent_id_name = $id_name"
						: "PRIOR $id_name = $this->parent_id_name";

					if ($args['to_peak'] && !$args['include_root'])
					{
//						$st->connect_by = $this->applyRootCond(null, 0);
						$st->where = $this->applyRootCond(null, 0);
					}
				}

				if (!($stw_set = array_key_exists('start_with', $args)) || is_numeric($args['start_with']))
				{
					$field_name = $this->applyTableAlias($args['include_root'] || $args['to_peak']
						? $this->id_name
						: $this->parent_id_name
					);

					$start_with_id = $stw_set ? $args['start_with'] : $this->root_ids;
					$start_with = $this->applyRootCond("$field_name #root_cond#", 1, $start_with_id);
				}
				else
					$start_with = $args['start_with'];

				$st->start_with = $start_with;

				if (array_key_exists('order_siblings_by', $args) && $args['order_siblings_by'] == '')
				{
					$st->order_by = $order_by;
				}
				else
				{
					$st->order_siblings_by = $order_by;
				}

				if ($args['limit'])
				{
					$st->limit((int)$args['limit'], (int)$args['offset']);
				}

				$this->setRootFilter($st, $args);

		if ($args['q']) Debug::varDump($st->buildQuery());

				return $this->findBySQL($st, $args);
			}

			$args = func_get_args();
			return call_user_func_array(array(parent, find), $args);
		}

		function getTree()
		{
			return $this->find(array('as_tree' => 1, 'fetch_as' => self::FETCH_AS_HASH));
		}

		protected function makeHashedFieldsParams($field_list, $args)
		{
			$params = parent::makeHashedFieldsParams($field_list, $args);
			$params['fetch_level'] = 1;
			return $params;
		}

		protected function hashedFields($field_list, $args)
		{
			$params = $this->makeHashedFieldsParams($field_list, $args);

			if ($params['as_tree'])
			{
				$ret = array();
				foreach($this->find($params) as $id => $_)
				{
					if (is_array($_))
					{
						$level = array_pop($_);
						$ret[$id] .= str_repeat(self::$pad_char, 3 * ($level - 1)).$_[0];
					}
					else
						$ret[$id] = $_;
				}
				return $ret;
			}

			return $this->find($params);
		}

		function getDirectChildren($params = array())
		{
			return $this->findAllByParent_id($this->id, $params);
		}

		function getSiblings($params = array())
		{
			return $this->findAllByParent_id($this->parent_id, $params);
		}

		function getAllChildren($params = array())
		{
			$options = array_merge($params, array(
				'as_tree' 		=> 1,
				'start_with'	=> "id = $this->id",
			));
			return $this->find($options);
		}

		function getAllParents($params = array())
		{
			$params = array_merge((array)$params, array(
				'as_tree' 		=> 1,
				'start_with'	=> $this->id,
				'to_peak'		=> 1,
			));

			return $this->find($params);
		}

		function getParent()
		{
			return $this->find($this->parent_id);
		}

		function isChildOf(OraTreeActiveRecord $ota)
		{
			return $this->isChildOfId($ota->id);
		}

		function isChildOfId($id)
		{
			if ($this->isNewRecord())
				return false;

			return $this->find(array(
				'all'		=> 0,
				'as_tree'	=> 1,
				'select'	=> 'id',
				'fetch_as'	=> self::FETCH_AS_ARRAY,
				'start_with'=> $id,
				'where'		=> array("id = ?", $this->id),
			));
		}

		function isParentOf(OraTreeActiveRecord $ota)
		{
			return $this->isParentOfId($ota->id);
		}

		function isParentOfId($id)
		{
			if ($this->isNewRecord())
				return false;

			return $this->find(array(
				'as_tree'	=> 1,
				'select'	=> 'id',
				'all'		=> 0,
				'fetch_as'	=> self::FETCH_AS_ARRAY,
				'start_with'=> $this->id,
				'where'		=> array("id = ?", $id),
			));
		}

		function getRoot()
		{
			$find_params = array(
				'where'	=> $this->applyRootCond(),
				'all'	=> intval(count($this->root_ids) > 1),
			);

			return $this->find($find_params);
		}

		function dropSubTree()
		{
			return $this->dropAll(array(
				'where'	=> <<<SQL
id IN (
	SELECT id
	FROM {$this->table_name}
	START with id = {$this->id}
	CONNECT BY PRIOR id = parent_id
)
SQL
			));
		}

		function getRootId()
		{
			return $this->multiple_roots ? $this->root_ids : $this->root_ids[0];
		}

		function isRoot()
		{
			return in_array($this->id, $this->root_ids);
		}
	}
?>