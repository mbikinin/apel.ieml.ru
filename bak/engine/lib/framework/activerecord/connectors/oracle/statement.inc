<?
	class Oracle_ActiveRecordStatement extends ActiveRecordStatement
	{
		protected
			$clauses= array(
				'update'			=> '',
				'set'				=> '',
				'select'			=> '',
				'from'				=> '',
				'@join'				=> '',
				'where'				=> '',
				'start_with'		=> '',
				'connect_by'		=> '',
				'union'				=> '',
				'group_by'			=> '',
				'order_by'			=> '',
				'order_siblings_by'	=> '',
				'having'			=> '',
				'@raw_sql'			=> '',
			),
			$addable_clauses = array(
				'where',
				'start_with',
				'connect_by',
			);

		function limit($limit, $offset = 0)
		{
			$this->limit = true;
			$this->mergeParams(compact('limit', 'offset'));
		}

		protected function limitClause($query)
		{
			if ($this->limit)
				$query = <<<SQL
SELECT * FROM (
	SELECT rownum "#", "~".* FROM ($query) "~"
)
WHERE "#" > :offset and rownum <= :limit
SQL;

			return $query;
		}
	}
?>