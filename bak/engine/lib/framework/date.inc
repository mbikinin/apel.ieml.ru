<?
	class Date
	{
		static function oracleDateToTime($str)
		{
			list ($year, $month, $day, $hour, $minute, $second) = array_map('intval', self::extractOracleDate($str));

			return mktime($hour, $minute, $second, $month, $day, $year);
		}

		static function extractOracleDate($str)
		{
			list ($date, $time) = explode(' ', $str);
			list ($year, $month, $day) = explode('-', $date);
			list ($hour, $minute, $second) = explode(':', $time);

			return array($year, $month, $day, $hour, $minute, $second);
		}

		static function extractTextDate($str_date)
		{
			list ($date, $time) = explode(' ', $str_date);
			list ($d, $m, $y) = explode('.', $date);
			list ($h, $i, $s) = explode(':', $time);
			return array((int)$y, (int)$m, (int)$d, (int)$h, (int)$i, (int)$s);
		}

		/**
		 * creates Oracle date from parts
		 *
		 * Example1: createOracleDate($year, $month, $day, $hour, $minute, $second)
		 * Example2: createOracleDate(array('year' => 2006, 'month' => 1, 'day' => 12))
		 *
		 * @return string
		 */
		static function createOracleDate()
		{
			$args = func_get_args();

			if (count($args) == 1 && is_array($args[0]))
				extract($args[0]);
			else
				list ($year, $month, $day, $hour, $minute, $second) = $args;

			return sprintf('%04d-%02d-%02d %02d:%02d:%02d', $year, $month, $day, $hour, $minute, $second);
		}

		static function timeToOracleDate($time)
		{
			return date('Y-m-d H:i:s', (int)$time);
		}

		static function textDateToTime($str_date)
		{
			list ($y, $m, $d, $h, $i, $s) = self::extractTextDate($str_date);
			return mktime($h, $i, $s, $m, $d, $y);
		}

		static function formatRange($rng1_parts, $rng2_parts, $date_delimiter = '.', $range_delimiter = ' - ')
		{
			for ($n = count($rng2_parts) - 1; $n >= 0; $n--)
				if ($rng2_parts[$n] == $rng1_parts[$n])
					unset($rng1_parts[$n]);

			if ($rng1 = implode($date_delimiter, $rng1_parts))
				$rng1 .= $range_delimiter;

			return $rng1.implode($date_delimiter, $rng2_parts);
		}

		static function buildDateAssoc($data = null, $prefix = '')
		{
			$data or $data = $_REQUEST;
			return mktime($data[$prefix.'hour'], $data[$prefix.'minute'], $data[$prefix.'second'],
				$data[$prefix.'month'], $data[$prefix.'day'], $data[$prefix.'year']);
		}

		static function splitDateAssoc($timestamp, $time = true)
		{
			$ret = array();

			if ($time)
			{
				list ($ret['year'], $ret['month'], $ret['day'], $ret['hour'], $ret['minute'], $ret['second']) =
					explode('-', date('Y-m-d-H-i-s', (int)$timestamp));
			}
			else
			{
				list ($ret['year'], $ret['month'], $ret['day']) = explode('-', date('Y-m-d', (int)$timestamp));
			}

			return $ret;
		}

		function checkDateAssoc($data = null, $prefix = '', $time = true)
		{
			$data or $data = $_REQUEST;
			return checkdate($data[$prefix.'month'], $data[$prefix.'day'], $data[$prefix.'year']) && (!$time ||
				($data[$prefix.'hour'] >= 0 && $data[$prefix.'hour'] < 24 &&
				$data[$prefix.'minute'] >= 0 && $data[$prefix.'minute'] < 59 &&
				$data[$prefix.'second'] >= 0 && $data[$prefix.'second'] < 59));
		}

		static function checkTextDate($str_date)
		{
			list ($d['year'], $d['month'], $d['day'], $d['hour'], $d['minute'], $d['second']) = self::extractTextDate($str_date);
			return self::checkDateAssoc($d);
		}

		static function textDateToOracle($str_date)
		{
			return self::timeToOracleDate(self::textDateToTime($str_date));
		}

		static function timeBySeconds($seconds)
		{
			$days = $mins = $hours = -1;
			$mins = floor($seconds / 60);
			$seconds = $seconds % 60;
			if ($mins > 0)
			{
				$hours = floor($mins / 60);
				$mins = $mins % 60;
				if ($hours > 0)
				{
					$days = floor($hours / 24);
					$hours = $hours % 24;
					if ($days == 0)
						$days = -1;
				}
				else
					$hours = -1;
			}
			else
				$mins = -1;
			return array($seconds, $mins, $hours, $days);
		}

		static function textBySeconds($seconds, $showSeconds = false)
		{
			$textualValue = '';
			list($seconds, $mins, $hours, $days) = self::timeBySeconds($seconds);
			if ($days > 0)
			{
				$textualValue .= $days . ' дн. ';
			}
			if ($hours > 0)
			{
				$textualValue .= $hours . ' ч. ';
			}
			if ($mins > 0)
			{
				if ($days <= 0)
				{
					$textualValue .= $mins . ' мин. ';
				}
			}
			if ($showSeconds || $textualValue == '')
			{
				if ($days <= 0)
				{
					$textualValue .= $seconds . ' сек. ';
				}
			}
			return $textualValue;
		}

	}
?>