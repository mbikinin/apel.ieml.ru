<?php
	class TreeHelper
	{
		public
            $Obj,
			$Hlp;

        function __construct(ActiveRecord $Obj)
        {
            $this->Obj = $Obj;
            $this->Hlp = new Helper();
        }

		public function loadTree()
		{
			$records = $this->Obj->findAll(array(
                'where' => 'parent_id = :parent_id',
                'params' => array('parent_id' => 0)
            ));
            $html = <<<HTML
{$this->Hlp->startFormTag()}
<table>
HTML;

            foreach($records as $r)
            {
				$hidden = $this->Hlp->hiddenFieldTag("ordi[{$r->id}]");
                $html .= <<<HTML
    <tr>
        <td>{$hidden}{$r->name}</td>
        <td>
			{$this->Hlp->linkTo('редактировать', array('controller' => '/admin/articles', 'action' => 'edit', 'id' => $r->id))}
			{$this->Hlp->linkTo('удалить', array('controller' => '/admin/articles', 'action' => 'drop', 'id' => $r->id), array('onclick' => 'return confirm(\'Вы уверены ? \');'))}
		</td>
    </tr>
HTML;
                if (count($childs = $r->childs))
                {
                    foreach($childs as $c)
                    {
						$hidden = $this->Hlp->hiddenFieldTag("ordi[{$r->id}][{$c->id}]");					
                        $html .= <<<HTML
    <tr>
        <td>{$hidden}&nbsp&nbsp&nbsp{$c->name}</td>
        <td>
            {$this->Hlp->linkTo('редактировать', array('controller' => '/admin/articles', 'action' => 'edit', 'id' => $c->id))}
			{$this->Hlp->linkTo('удалить', array('controller' => '/admin/articles', 'action' => 'drop', 'id' => $c->id), array('onclick' => 'return confirm(\'Вы уверены ? \');'))}
        </td>
    </tr>
HTML;
                    }
                }
            }

            $html .= <<<HTML
</table>
{$this->Hlp->linkTo('добавить', array('controller' => '/admin/articles', 'action' => 'edit'))}
{$this->Hlp->submitTag()}
{$this->Hlp->endFormTag()}
HTML;
			return $html;
		}
	}
?>