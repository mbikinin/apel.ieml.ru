<ul>
	<?php
	foreach($articles as $a)
	{
	?>
		<li>
		<?php 
		if ($a->id == $this->current_article)
		{
		?>
			<div class="current"><span><?= $a->name ?></span></div>
		<?php
		}
		else
		{
			echo $this->Hlp->linkTo("<span>{$a->name}</span>", $a->alias);
		}
		?>
		</li>
	<?php
	}
	?>
</ul>