<?php
	if ($failure)
	{
?>
	<div id="error_explanation" class="error_explanation">Искомая комбинация логина и пароля не найдена</div>
<?php
	}
?>


    <h1>Электронная повестка дня</h1>
    <h2>АРМ оператора</h2>
	<?php echo $Form->startFormTag() ?>
		<h3>Авторизация</h3>
		<p class="in">
			<label for="<?php echo $Form->tagId('login') ?>">Логин:</label>
			<?php echo $Form->textField('login') ?>
		</p>
		<p class="in">
			<label for="<?php echo $Form->tagId('password') ?>">Пароль:</label>
			<?php echo $Form->passwordField('password') ?>
		</p>

		<p class="submit"><?php echo $Form->submitTag('Вход') ?></p>
	<?php echo $Form->endFormTag() ?>