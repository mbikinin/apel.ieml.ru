<!doctype html>
<title>Электронная повестка дня - Авторизация пользователя</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<?php echo $this->Hlp->getJs() ?>
<link href="<?php echo Helper::$css_base ?>/auth.css" media="all" rel="stylesheet" type="text/css">
<?php echo $this->Hlp->getCss() ?>

<div id="wrapper" class="<?php //echo ServerConfig::$class ?>">
	<div id="page">
		<div id="form">
			<?php
				if ($notice = $Session->push('notice'))
				{
					?><div class="notice"><?php echo $notice ?></div><?php
				}
			?>
			<?php
				if ($error = $Session->push('error'))
				{
					?><div class="error_explanation"><?php echo $error ?></div><?php
				}
			?>
			<?php echo $content_for_layout ?>
		</div>
		<div id="s-footer">
<?php /*
	switch (ServerConfig::$class)
	{
		case 'tatneft' :
?>
			<p><p>&copy; Copyright <?php echo date('Y') ?> ОАО &laquo;Татнефть&raquo;</p></p>

<?php
			break;
			
		case 'gossovet' :
?>
			<p><p>&copy; Copyright <?php echo date('Y') ?> Государственный Совет Республики Татарстан</p></p>
<?php
			break;
	}*/
?>
			
		</div>
	</div>
</div>