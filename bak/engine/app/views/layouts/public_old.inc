<!doctype html>

<title>Негосударственное образовательное учреждение 
&laquo;Центр Подготовки кадров - Татнефть&raquo; 
- <?php echo h($this->page_title) ?></title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link href="<?php echo Helper::$css_base ?>/master.css" media="all" rel="stylesheet" type="text/css" />
<?php echo $this->Hlp->getCss() ?>

<script type="text/javascript" src="<?php echo Helper::$js_base ?>/jquery.js"></script>
<?php echo $this->Hlp->getJs() ?>
<script type="text/javascript" src="<?php echo Helper::$js_base ?>/<?php echo $this->user_group == 'moderator' ? 'employee' : $this->user_group;?>/script.js"></script>

<script src="/javascript/table.js" type="text/javascript"></script>

<script src="/javascript/cufon.js" type="text/javascript"></script>
<script src="/javascript/Pragmatica_CondITT_400.font.js" type="text/javascript"></script>
<script type=text/javascript>Cufon.replace('.cufon-rp')</script>

<div id="wrapper" class="<?= ($this->is_start_page) ? 'start-page' : 'inner-page' ?>">
	<div id="container">
		<div id="header">    		
			<div class="h">
				<h1>Негосударственное образовательное 
				учреждение &laquo;Центр Подготовки кадров -
				Татнефть&raquo;</h1>
				<a class="back-tn png" href="http://tatneft.ru"><ins>
					Стартовая страница портала ОАО Татнефть
				</ins></a>
				<a class="back-cpk png" href="/"><ins>
					Стартовая страница портала ЦПК
				</ins></a>
				<a class="back-cpk-big png" href="/"><ins>
					Стартовая страница портала ЦПК
				</ins></a>
				<div class="search-panel">
					<form action="#self">
						<input type="text" class="myquery" id="myquery" placeholder="Поиск..." value="">
						<input type="submit" class="mysubmit" value="">
					</form>
					<div class="usability">
						<a class="home png" href="/"><ins>Стартовая страница портала</ins></a>
						<a class="mail-us png" href="/faq"><ins>Напишите нам</ins></a>
						<a class="sitemap png" href="/outer/sitemap"><ins>Карта сайта</ins></a>
					</div>
				</div>
			</div>
		</div>			
		<div id="page">
			<div class="h">
				<div id="sidebar">
					<div class="user-navigation">
						
					</div>
					<div class="buttons-set">
						<a href="/auth/logout">Выход</a>
					</div>
				</div>
				<div id="content">
					<div class="breadcrumbs"><a href="/">Главная</a> \ Личные данные</div>
					<h1><?php echo $this->page_title;?></h1>
					<? if ($notice = $Session->push('notice')) { ?>
						<div class="notice"><?php echo $notice ?></div>
					<? } ?>
					<? if ($error = $Session->push('error')) { ?>
						<div class="error_explanation"><?php echo $error ?></div>
					<? } ?>					
					<?= $content_for_layout ?>
				</div>
			</div>
		</div>			
		<div id="footer">
			<div class="h">
				<p>Copyright 2007 - <?= date("Y", time()) ?> ОАО &laquo;Татнефть&raquo;</p>
				<p>Техподдержка: <a href="mailto:admin@azs.tatneft.ru">admin@azs.tatneft.ru</a></p>
				<p>Портал работает в режиме тестовой эксплуатации</p>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			var errors = $('.errors');			
			if (errors){
				$(errors).each(function(er){
					$(this).delay(10000).fadeOut('slow');
				});	
			}
			Cufon.now();
		});		
	</script>
</div>