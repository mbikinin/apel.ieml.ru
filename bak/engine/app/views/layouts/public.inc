<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Актуальные проблемы экономики и права</title>
<link rel="stylesheet" href="<?php echo Helper::$css_base ?>/reset.css" type="text/css" />
<link rel="stylesheet" href="<?php echo Helper::$css_base ?>/apel.css" type="text/css" />
<?php echo $this->Hlp->getCss() ?>
<script language="JavaScript" src="/javascript/jquery.js" type="text/javascript"></script>
<!--[if IE 6]><script src="/design/images/<?php echo Helper::$js_base ?>/dd_belatedpng.js"></script><script>DD_belatedPNG.fix('.png');</script><![endif]-->

</head>

<body>

	<div class="all">
    
    <div class="top">&nbsp;</div>

<table style="margin:0 auto;"><tr><td width="1280">
    
	<table class="text">
	   
        <tr>
        
        <td class="left"><div class="logo">Актуальные<br />проблемы<br />экономики<br />и&nbsp;права</div><p class="lang"><a href="">in English</a></p></td>
        
        <td class="content">
        
        <div class="main_menu"><?php $this->renderPartial('/articles/top_menu', array('articles' => $this->top_articles, 'current' => $this->current_article)); ?></div>
		
		</td>
        
        <td class="right"><p><i>Журнал включен в&nbsp;<a href="http://vak.ed.gov.ru/ru/help_desk/list/" target="_blank">Перечень&nbsp;ВАК</a> Минобрнауки&nbsp;РФ</i></p><p class="sans issn">ISSN 1993-047X</p></td>
        </tr>
        
        <tr>
        
        <td>&nbsp;</td>
        <td><img src="/design/images/bsh.png" class="bsh png" /></td>
        <td>&nbsp;</td>
        </tr>        
   
        <tr>
        
        <td class="left">
        
         <div class="left_menu"><?php $this->renderPartial('/articles/left_menu', array('articles' => $this->top_articles, 'current' => $this->current_article)); ?></div>
        
        </td>
        
        <td class="content">
        <?= $this->renderPartial('/admin/menu') ?>
        <?= $content_for_layout ?>
        </td>
        
        <td class="right">
        
        <h3 class="awards_header">Отличные условия для&nbsp;авторов</h3>
        
        <ul>
            <li>Публикация наиболее актуальных материалов;</li>
            <li>Широкий спектр рассматриваемых проблем;</li>
            <li>Абсолютное соблюдение требований к&nbsp;опубликованию научных работ;</li>
            <li>Квалифицированный редакционный совет и&nbsp;редакционная коллегия;</li>
            <li>Бесплатный перевод статей, переводчик&nbsp;&mdash; член Гильдии переводчиков РТ;</li>
            <li>Бесплатная публикация аспирантов;</li>
            <li>Принятие решения о&nbsp;публикации в&nbsp;течение месяца.</li>
        </ul>

        <div class="pub"><a href="">Правила<br />публикации</a></div>
        
        <h3>Полезные ссылки</h3>
        
        <p><a href="#">Оформление статей по&nbsp;ГОСТу</a></p>
	    
        </td>
        </tr>
        
   </table>
   
</td></tr></table>

	<div class="empty">&nbsp;</div>

	</div>

<div class="footer">

<table style="margin:0 auto;"><tr><td width="1280">

	<div class="scissor">&nbsp;</div>
    
    <table class="text">
    
    <tr>
        
        <td class="left">&nbsp;</td>
        <td class="content"><p>&copy 1996&ndash;2011 Научный журнал &laquo;Актуальные проблемы экономики и&nbsp;права&raquo;</p> </td>
        <td class="right"><p>Сделано в&nbsp;студии&nbsp;&nbsp;<a href="http://gutdesign.ru" class="gutdesign">Гутдизайн</a></p></td>        
                     
        </tr>
    
    </table>

 </td></tr></table>

</div>

</body>

</html>