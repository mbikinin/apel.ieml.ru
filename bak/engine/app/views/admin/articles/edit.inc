<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<form action="<?= $this->Hlp->urlFor() ?>">
<div>
    <label>Тип:</label>
    <div>
        <select name="type" onchange="this.form.submit()">
        <?php
        foreach(ArticleType::instance()->list as $id => $v)
        {
        ?>
            <option value="<?= $id ?>" <?= ($Form->object->type == $id ? 'selected' : '') ?>><?= $v ?></option>
        <?php
        }
        ?>
        </select>
    </div>
</div>
</form>

<?= $Form->startFormTag() ?>

<div class="form">
	<?php echo $Form->errorMessages() ?>
    <?= $Form->hiddenField('type', array('value' => ($this->params['type'] ? $this->params['type'] : null))) ?>

    <div>
        <label>Заголовок:</label>
        <div><?= $Form->textField('name', array('size' => 50)) ?></div>
    </div>

    <?php
    if ($Form->object->type == ArticleType::page)
    {
    ?>
    <div>
        <label>Содержание:</label>
        <div><?= $Form->textArea('text') ?></div>
    </div>
    <?php
    }
    elseif($Form->object->type == ArticleType::link)
    {
    ?>
    <div>
        <label>Ссылка:</label>
        <div><?= $Form->textField('alias') ?></div>
    </div>
    <?php
    }
    ?>

    <div>
        <ins><?= $Form->submitTag('Сохранить') ?></ins>
        <?= $this->Hlp->linkTo('Вернуться назад', $this->back); ?>
    </div>
</div>

<?= $Form->endFormTag() ?>