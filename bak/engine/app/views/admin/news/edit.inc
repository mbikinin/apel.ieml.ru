<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?= $Form->startFormTag() ?>

<div class="form">
	<?php echo $Form->errorMessages() ?>

    <div>
        <label>Заголовок:</label>
        <div><?= $Form->textField('header', array('size' => 50)) ?></div>
    </div>

    <div>
        <label>Дата:</label>
        <div><?= $Form->datePicker('date') ?></div>
    </div>

    <div>
        <label>Превью:</label>
        <div><?= $Form->textArea('preview') ?></div>
    </div>

    <div>
        <label>Текст:</label>
        <div><?= $Form->textArea('text') ?></div>
    </div>

    <div>
        <ins><?= $Form->submitTag('Сохранить') ?></ins>
        <?= $this->Hlp->linkTo('Вернуться назад', $this->back); ?>
    </div>
</div>

<?= $Form->endFormTag() ?>