<?php
if (count($news))
{
?>
<table>
    <tr>
        <td>№</td>
        <td>Заголовок</td>
        <td>Действия</td>
    </tr>
    <?php
    foreach($news as $n)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= ++$n->header ?></td>
        <td>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit', 'id' => $n->id)) ?>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $n->id)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    Записей нет
<?php
}
?>

<br/><br/>

<?= $this->Hlp->linkTo('Добавить', array('action' => 'edit')); ?>
