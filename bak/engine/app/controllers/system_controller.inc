<?php

class SystemController extends AppController
{
	public function flushcache($params)
	{
		$mem = ActiveRecord::$Memcache;
		echo $mem->flush();

		exit;
	}

	public function hashedPassword($params)
	{
		$password = $params['password'];
//		$hashed_password = md5($params['password']);
//		$hashed_password = base64_encode($params['password']);
        $hashed_password = User::hashed($params['password']);
		Debug::varDump($password, $hashed_password);
	}

	public function i($params)
	{
		$this->layout = false;
		phpinfo();
	}
}