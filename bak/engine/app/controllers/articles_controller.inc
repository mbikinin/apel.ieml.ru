<?php

class ArticlesController extends AppController
{
	public $Article;

    protected function onLoad()
    {
        parent::onLoad();

        $this->Article = new Article();
    }

    public function index($params)
    {
        Debug::varDump($params);
    }

    public function dispatch($params)
    {
        if (!$article = $this->Article->find($params['id']))
        {
            $this->goBack();
        }
        
        $this->current_article = $article->id;
        $this->page_title = $article->name;

        return compact('article');
    }
}
