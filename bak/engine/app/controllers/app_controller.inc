<?php

class AppController extends ActionController
{
	public $layout = '/public';
	public $page_title = '';
	public $section = '';
	public $logged_in = false;
	public $ipad_logged_in = false;
	public $lang;
	public static $global_class;
	public $user;
    public $top_articles;
    public $current_article;
	
	protected $list_page_params = array('action' => 'index');
	
	protected function onLoad()
	{
		parent::onLoad();

		$this->user = Logon::getLoggedUser();
        ActiveRecordTranslate::$lang = isset($_GET['lang']) ? constant('Language::' . $_GET['lang']) : Language::def;
        $this->top_articles = ActiveRecord::factory('Article')->findAll(array('where' => 'parent_id=0'));
	}
	
	public function beforeFilter()
	{
		parent::beforeFilter();

		if (isset($this->params['debug']))
		{
			$GLOBALS['Con']->debug = 1;
		}

	//	$this->recognizeLang();
	}

	/**
	* Alias ��� edit-action
	*/
	public function add($params)
	{
		$this->redirectTo(array('action' => 'edit'));
	}
    
    public function index($params)
    {
        Debug::varDump($params);
    }
}
