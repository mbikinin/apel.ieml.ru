<?php

class StartController extends AppController
{
	public function index($params)
	{				
		$this->redirectTo(array('controller' => 'auth/login', 'action' => 'index'));
	}
}