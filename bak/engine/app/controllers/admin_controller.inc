<?php

class AdminController extends AppController
{
    protected function onLoad()
    {
        parent::onLoad();
//Debug::varDump($this->user);
        if (!($this->user instanceof User) || !$this->user->isAdmin())
        {
			$backurl = $this->Hlp->urlFor();
			$this->redirectTo(array('controller' => '/auth', 'action' => 'login', 'backurl' => $backurl));
		}
    }

    public function index($params)
    {
        //Debug::varDump('yeeeeeeeeeeees');
    }
}