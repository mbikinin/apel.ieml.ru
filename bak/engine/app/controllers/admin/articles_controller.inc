<?php

class Admin_ArticlesController extends AdminController
{
	public $Article;

    protected function onLoad()
    {
        parent::onLoad();

        $this->Article = new Article();
    }

    public function index($params)
    {
        $this->page_title = 'Меню';

        if (getenv('REQUEST_METHOD') == 'POST')
        {
            Debug::varDump($params);
        }

        $TreeHelper = new TreeHelper($this->Article);

        return compact('TreeHelper');
    }

    public function edit($params)
    {
        $this->page_title = 'Новости';

        if ($params['id'] && !($article = $this->Article->find($params['id'])))
        {
            $this->errorBack('Неизвестная новость');
        }
        elseif(!$params['id'])
        {
            $article = $this->Article->create();
        }

        $Form = $this->createFormHelper($article);

        if (getenv('REQUEST_METHOD') == 'POST')
        {
            self::$Con->ttsbegin();

            if ($article->saveFromPost())
            {
                self::$Con->ttscommit();
                $this->noticeTo(array('action' => 'index'), 'данные сохранены');
            }

            self::$Con->ttsabort();
        }
        elseif ($Form->object->isNewRecord())
        {
            $Form->object->type = $params['type'] ? $params['type'] : ArticleType::link;
        }
        else
        {
            !$params['type'] or $Form->object->type = $params['type'];
        }

        return compact('Form', 'article');
    }

    public function drop($params)
    {
        if (!$params['id'] || !($article = $this->Article->find($params['id'])))
        {
            $this->noticeBack('Запись не найдена');
        }

        $article->drop();
        $this->noticeBack('Запись удалена');
    }
}
