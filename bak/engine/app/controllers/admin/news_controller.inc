<?php

class Admin_NewsController extends AdminController
{
    public $News;

    protected function onLoad()
    {
        parent::onLoad();

        $this->News = new News();

        if ($_GET['lang'] && @constant('Language::'.$_GET['lang']))
            $this->News->lang = @constant('Language::'.$_GET['lang']);
    }

    public function index($params)
    {
        $this->page_title = 'Новости';

        $news = $this->News->findAll();

        return compact('news');
    }

    public function edit($params)
    {
        $this->page_title = 'Новости';

        if ($params['id'] && !($news = $this->News->find($params['id'])))
        {
            $this->errorBack('Неизвестная новость');
        }
        elseif(!$params['id'])
        {
            $news = $this->News->create();
        }

        $Form = $this->createFormHelper($news);

        if (getenv('REQUEST_METHOD') == 'POST')
        {
            self::$Con->ttsbegin();

            if ($news->saveFromPost())
            {
                self::$Con->ttscommit();
                $this->noticeTo(array('action' => 'index'), 'данные сохранены');
            }

            self::$Con->ttsabort();
        }
        elseif(!$params['id'])
        {
            $Form->params['news']['date'] = date('d.m.Y');
        }

        return compact('Form', 'news');
    }
}
