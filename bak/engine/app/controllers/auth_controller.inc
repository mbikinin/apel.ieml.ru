<?php

class AuthController extends AppController
{
	public $layout = '/auth';

	protected function onLoad()
	{
		parent::onLoad();

		$this->Hlp->needJs('jquery');
	}

	public function index($params)
	{
		$this->redirectTo(array('action' => 'login'));
	}

	public function login($params)
	{
		$failure = false;
	
		if ($this->user_group)
		{
			$this->redirectTo(array('controller' => ($this->user_group == 'moderator' ? 'employee' : $this->user_group).'/personal', 'action' => 'index'));
		}
	
		if (getenv('REQUEST_METHOD') == 'POST')
		{
			$user = Logon::login($params['login'], $params['password'], isset($params['ipad']));
			$failure = is_object($user) ? false : true;	
			
			if (!$failure)
			{
				if (isset($params['backurl']))
				{
					$this->redirectTo($params['backurl']);
				}
				else
				{
					switch($user->group)
					{
						case 'company':
							$this->redirectTo(array('controller' => 'company/personal', 'action' => 'index'));
						break;
						case 'employee':						
						case 'moderator':
							$this->redirectTo(array('controller' => 'employee/personal', 'action' => 'index'));
						break;
						default:
							$this->redirectTo('');
						break;
					}					
				}
			}
		}

		$Form = $this->createFormHelper();
		return compact('Form', 'failure');
	}

	public function logout($params)
	{
		Logon::logout();
		$this->redirectTo(array('action' => 'login'));
	}
}