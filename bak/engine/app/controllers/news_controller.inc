<?php

class NewsController extends AppController
{
    public $News;

    protected function onLoad()
    {
        parent::onLoad();

        $this->News = new News();
    }

    public function index($params)
    {
        $this->page_title = 'Новости';

        if (!$params['id'] || !($news = $this->News->find($params['id'])))
        {
            $this->errorBack('Неизвестная новость');
        }

        return compact('news');
    }
}
