<?php

class User extends ActiveRecord
{
	const
		name_max_length = 250,

		login_max_length = 16,
		login_min_length = 3,

		password_max_length = 20,
		password_min_length = 5;

	protected function afterFetch(& $fields)
	{
	//	unset($fields['password']);
	}

	static public function hashed($string)
	{
		return sha1($string);
	}

	static public function getUser($login, $password = NULL)
	{
		$where = array('login = :login');
		$params = array('login' => $login);

		if (!is_null($password))
		{
			$where[] = 'password = :password';
			$params['password'] = self::hashed($password);
		}

		$find_params = array(
			'where' => join(' AND ', $where),
			'params' => $params
		);
		return self::factory('User')->findFirst($find_params);
	}

    public function isAdmin()
    {
        return ($this->group == 1);
    }
}