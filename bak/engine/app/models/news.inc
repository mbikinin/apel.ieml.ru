<?php

class News extends ActiveRecordTranslate
{
    protected $translate_fields = array(
        'header',
        'preview',
        'text'
    );

    protected function afterFetch(& $fields)
    {
        parent::afterFetch(& $fields);

        $fields['date'] = date('d.m.Y', strtotime($fields['date']));
    }

    protected function beforeSave()
    {
        parent::beforeSave();
        
        $this->date = date('Y-m-d', strtotime($this->date));
    }
}