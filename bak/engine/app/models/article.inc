<?php

class Article extends ActiveRecordTranslate
{
    protected $order_by = 'ordi';

    protected $translate_fields = array(
        'name',
        'text'
    );

    public function getChilds()
    {
        return $this->findAll(array('where' => 'parent_id = :parent_id',
                                    'params' => array('parent_id' => $this->id)));
    }

    static public function findRoute($url)
    {
        $self = new self();
        if ($article = $self->findFirst(array('where' => "alias='{$url}'")))
            return array(
                'controller'    => 'articles',
                'action'        => 'dispatch',
                'id'            => $article->id
            );
    }
}