<?php

class ActiveRecordTranslate extends ActiveRecord
{
    public static $lang = Language::def;
    protected $translations; // $translations[lang][field_name] = $value;

    protected function afterFetch(& $fields)
	{
	    $this->translations = self::$Con->EzQuery("SELECT lang, field_name, value FROM translations WHERE table_name='{$this->table_name}' AND item_id={$fields[id]}",
                                        array('multihash' => 2));
//Debug::varDump(self::$lang);
        foreach ($this->translations[self::$lang] as $field_name => $value)
            $fields[$field_name] = $value;
	}

    protected function beforeSave()
    {
        if ($this->isNewRecord())
        {
            foreach(Language::instance()->list as $lang => $v)
                foreach($this->translate_fields as $field_name)
                    //Debug::varDump("INSERT INTO translations SET item_id={$this->getNextId()}, table_name='{$this->table_name}', field_name='{$field_name}', lang={$lang}", false);
                    self::$Con->Query("INSERT INTO translations SET item_id={$this->getNextId()}, table_name='{$this->table_name}', field_name='{$field_name}', lang={$lang}");
            //die;
        }
    }

    protected function afterSave()
	{
        foreach($this->translate_fields as $field_name)
        {
            $this->translations[$this->lang][$field_name] = $this->$field_name;
        }

        foreach($this->translations as $lang => $fields)
            foreach($fields as $field_name => $value)
                self::$Con->Query("UPDATE translations set value='{$value}' WHERE table_name='{$this->table_name}' AND field_name='{$field_name}' AND item_id={$this->id} AND lang=" . self::$lang);
	}
}