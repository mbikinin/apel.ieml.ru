<?php

class ArticleType extends Reference
{
	const link = 1;
	const page = 2;

    const def = self::link;

	protected function __construct()
	{
		$this->list = array(
			self::link => 'Ссылка',
			self::page => 'Страница'
		);
	}
}