<?php

class Language extends Reference
{
	const rus = 1;
	const eng = 2;
    
    const def = self::rus;

	protected function __construct()
	{
		$this->list = array(
			self::rus => 'Русский',
			self::eng => 'Английский'
		);
	}
}