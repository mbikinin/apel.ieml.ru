<?php

	$Router->connect('', array(
		'controller'	=> 'start',
	));
	
	$Router->connect('auth', array(
        'requirements' => array(
            'controller'	=> 'auth',
            'action'		=> 'login',
        )
    ));
	
	$Router->connect('/logout', array(
        'requirements' => array(
            'controller'	=> 'auth',
            'action'		=> 'logout',
        )
    ));
	
	$Router->connectDefault();