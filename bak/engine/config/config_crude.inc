[_common]
PROJECT_PATH
	*			'/projects/apel/'
	!			'/projects/apel/'
STORAGE_PATH	{$->_common.PROJECT_PATH}.'public/storage/'
[memcache]
HOST			'localhost'
PREFIX			'apel_mc_'
[db]
DSN				'mysql://apel:Oosae3ru@mysql/apel?charset=UTF8'
[session]
KEY				false
CFG				array('lifetime' => '3600', 'tmp_prefix' => '', 'usecookies' => 1, 'cookiepath' => '/', 'tmp_path' => {$->_common.PROJECT_PATH}.'tmp/sessions')