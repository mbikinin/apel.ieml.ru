[_common]
PROJECT_PATH
	*			'/home/s/siteieml/apel.ieml.ru/'
	!			'/home/s/siteieml/apel.ieml.ru/'
STORAGE_PATH	{$->_common.PROJECT_PATH}.'public/storage/'
[memcache]
HOST			'localhost'
PREFIX			'apel_mc_'
[db]
DSN				'mysql://siteieml_apel:Oosae3ru@localhost/siteieml_apel?charset=UTF8'
[session]
KEY				false
CFG				array('lifetime' => '3600', 'tmp_prefix' => 'apelsess_', 'usecookies' => 1, 'cookiepath' => '/', 'tmp_path' => {$->_common.PROJECT_PATH}.'tmp/sessions')

[achievements]
base_path		{$->_common.STORAGE_PATH}.'achievements/'
url_path		'/storage/achievements/'

[archive_issues]
base_path		{$->_common.STORAGE_PATH}.'archive_issues/'
url_path		'/storage/archive_issues/'

[proposals]
base_path		{$->_common.STORAGE_PATH}.'proposals/'
url_path		'/storage/proposals/'
