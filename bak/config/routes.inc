<?php
/*    $Router->connect('/:controller/:action/:content_id/:n', array(
        'requirements' => array(
            'controller'	=> 'admin/page',
            'action'		=> 'show',
            'content_id'	=> '\d{1,20}',
            'n'				=> '\d{1,20}',
        )
    ));

    $Router->connect('/:controller/:action/:content_id/:n', array(
        'requirements' => array(
            'controller'	=> 'admin/page',
            'action'		=> 'show_preview',
            'content_id'	=> '\d{1,20}',
            'n'				=> '\d{1,20}',
        )
    ));*/

	$Router->connect('', array(
		'controller'	=> 'start',
	));
	
	$Router->connect('auth', array(
        'requirements' => array(
            'controller'	=> 'auth',
            'action'		=> 'login',
        )
    ));
	
	$Router->connect('/logout', array(
        'requirements' => array(
            'controller'	=> 'auth',
            'action'		=> 'logout',
        )
    ));
		
	$Router->connect('/:controller/years/:year', array(
        'requirements' => array(
            'controller'	=> '/archive',
            'action'		=> 'years',
			'year'		=> '\d{1,20}',
        )
    ));
	
	$Router->connectDefault();