<?php
	$__dir__ = dirname(__FILE__).'/';
	$__lib__ = $__dir__.'../lib/';

	require_once $__dir__.'dn_server.inc';

	mb_internal_encoding("UTF-8");

	define('FWK_ENV', 			$__dir__);
	define('FWK_ROOT', 			dirname($__dir__).'/app/');
	define('FWK_BIN', 			dirname($__dir__).'/bin/');
	define('FWK_CONTROLLERS',	FWK_ROOT.'controllers/');
	define('FWK_HELPERS',		FWK_ROOT.'helpers/');
	define('FWK_MODELS',		FWK_ROOT.'models/');
	define('FWK_VIEWS',			FWK_ROOT.'views/');
	define('FWK_CLASSES',		FWK_ROOT.'classes/');
	define('FWK_INTERFACES',	FWK_ROOT.'interfaces/');
	define('FWK_ENUMS',			FWK_ROOT.'enums/');
	define('FWK_LAYOUTS',		FWK_VIEWS.'layouts/');
	define('FWK_ROUTES',		$__dir__.'routes.inc');
	define('FWK_HELPER_LIB',	$__lib__.'actionview/');
	define('FWK_CONFIG_DIR',	$__dir__);

	require_once $__lib__.'framework/framework.inc';

	// User libs
	require $__lib__.'timer.inc';

	// Helper libs
	require $__lib__.'autoloader.inc';
	require $__lib__.'lib.inc';

	// Legacy libs
	require $__lib__.'cyrex.inc';
	require $__lib__.'file_upload.inc';
#	require $__lib__.'imagick.inc';
	require $__lib__.'mconfig.inc';
	require $__lib__.'msession.inc';
	// отправка сообщений 
	require $__lib__.'mail.inc';

	// user defined interfaces
	require FWK_INTERFACES.'test.inc';

	// user defined classes
	require FWK_ENUMS.'reference.inc';

	require FWK_CLASSES.'logon.inc';
	require FWK_CLASSES.'active_record_translation.inc';
	require FWK_CLASSES.'mysql_tree_active_record_translation.inc';

	require_once $__dir__.'config.inc';

	$Config = new DN_Config($__dir__);
	$Config->vars2globals('MEMCACHE_', 'memcache');

	$Memcache = new MyMemcache;
	$Memcache->prefix = $MEMCACHE_PREFIX;
	// $Memcache->connect($MEMCACHE_HOST);

	Inflector::$Memcache = $Memcache;
	$Inflector = new Inflector();

	Router::$Memcache = $Memcache;

	$Config->vars2globals('DB_', 'db');
	$Con = ActiveRecordConnector::factory($DB_DSN);

	$Config->vars2globals('SESSION_', 'session');
	$Session = new DNModernSession($SESSION_KEY, $SESSION_CFG);

	ActiveRecord::$Con = $Con;
	ActiveRecord::$Inflector = $Inflector;
	ActiveRecord::$Config = $Config;
	ActiveRecord::$Memcache = $Memcache;
	ActiveRecord::$Session = $Session;

	ActionController::$Config = $Config;
	ActionController::$Inflector = $Inflector;
	ActionController::$Session = $Session;
	ActionController::$Con = $Con;

	Dispatcher::$Inflector = $Inflector;

	Helper::$Session = $Session;
	Helper::$js_base = '/js';
	Helper::$css_base = '/design/css';

	ApplicationHelper::$Inflector = $Inflector;
	ApplicationHelper::$Config = $Config;

	BackUrlDetector::$Session = $Session;
	BackUrlDetector::$realms[] = '/admin';

	Mail::$encoding = 'UTF-8';

	Logon::$Session = $Session;
	
	ActiveRecord_Translation::$current_lang = $_GET['lang'];
	MysqlTreeActiveRecord_Translation::$current_lang = $_GET['lang'];

    JsTree_BaseHelper::$Inflector = $Inflector;
