-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 07, 2011 at 12:24 AM
-- Server version: 5.1.40
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apel`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `alias` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ordi` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `name`, `text`, `alias`, `type`, `parent_id`, `ordi`) VALUES
(1, 'О журнале', '123213123', '/about', 1, 0, 1),
(2, 'Авторам', '2222222222222222', '/authors', 1, 0, 2),
(3, 'Редакционная коллегия', '213213', '/about/editorial_board', 2, 1, 1),
(4, '', '', '', 2, 1, 2),
(5, '', '', '', 2, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `header` varchar(250) NOT NULL,
  `preview` text NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `date`, `header`, `preview`, `text`) VALUES
(1, '2011-08-09', '', '', ''),
(10, '2011-08-09', 'Заголовок', 'Превью', 'Текст'),
(14, '2011-08-24', 'new', 'nnnn', '22222');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `lang` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `item_id`, `table_name`, `field_name`, `value`, `lang`) VALUES
(1, 1, 'news', 'header', 'Признание международного уровня', 1),
(2, 1, 'news', 'preview', 'Журнал ИЭУП «Актуальные проблемы экономики и права», внесенный в Перечень ВАК, признан «Лучшим вузовским периодическим изданием». А учебный комплекс «Экономика (учебное пособие и практикум)», разработанный коллективом авторов под редакцией ректора, профессора В. Г. Тимирясова, победил в номинации «Лучший комплект для школ».', 1),
(3, 1, 'news', 'header', 'Recognition of the international level', 2),
(4, 1, 'news', 'preview', 'preview', 2),
(35, 10, 'news', 'header', 'Заголовок', 1),
(36, 10, 'news', 'preview', 'Превью', 1),
(37, 10, 'news', 'text', 'Текст', 1),
(38, 10, 'news', 'header', '', 2),
(39, 10, 'news', 'preview', '', 2),
(40, 10, 'news', 'text', '', 2),
(41, 1, 'articles', 'name', 'О журнале', 1),
(42, 1, 'articles', 'name', 'About', 2),
(43, 2, 'articles', 'name', 'Авторам', 1),
(44, 2, 'articles', 'name', 'Authors', 2),
(45, 3, 'articles', 'name', 'Редакционная коллегия', 1),
(46, 3, 'articles', 'name', 'Редакционная коллегия', 2),
(47, 4, 'articles', 'name', 'Политика журнала', 1),
(48, 4, 'articles', 'name', 'Политика журнала', 2),
(49, 5, 'articles', 'name', 'Наши авторы', 1),
(50, 5, 'articles', 'name', 'Наши авторы', 2),
(69, 14, 'news', 'header', 'Новая новость', 1),
(70, 14, 'news', 'preview', '111', 1),
(71, 14, 'news', 'text', '2222', 1),
(72, 14, 'news', 'header', 'new', 2),
(73, 14, 'news', 'preview', 'nnnn', 2),
(74, 14, 'news', 'text', '22222', 2),
(75, 6, 'articles', 'name', '', 1),
(76, 6, 'articles', 'text', '', 1),
(77, 6, 'articles', 'name', '', 2),
(78, 6, 'articles', 'text', '', 2),
(79, 7, 'articles', 'name', '', 1),
(80, 7, 'articles', 'text', '', 1),
(81, 7, 'articles', 'name', '', 2),
(82, 7, 'articles', 'text', '', 2),
(83, 8, 'articles', 'name', '', 1),
(84, 8, 'articles', 'text', '', 1),
(85, 8, 'articles', 'name', '', 2),
(86, 8, 'articles', 'text', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `group` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `group`) VALUES
(2, 'admin', '759730a97e4373f3a0ee12805db065e3a4a649a5', 1);
