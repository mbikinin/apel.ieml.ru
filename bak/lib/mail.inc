<?
	class MailAddress
	{
		public $name = "", $email = "";

		public function MailAddress($email, $name = '')
		{
			$this->email = $email;
			$this->name = $name;
			
		}
		
		public function toString()
		{
			if ($this->name)
				return sprintf('"%s" <%s>',  MailMessage::mimeEncode($this->name), $this->email);
			else
				return $this->email;
		}
	}
	
	class MailAttachment
	{
		public $name = "", $filename = "";

		public function MailAttachment($filename, $name = "")
		{
			$this->filename = $filename;
			$this->name = $name;
		}
	}
	
	class MailAttachmentList
	{
		public $list = array();
		
		public function add($attachment)
		{
			if (is_string($attachment))
				$attachment = new MailAttachment($attachment);
				
			$this->list[] = $attachment;
		}
		
		public function count()
		{
			return count($this->list);
		}
	}
	
	class MailAddressList
	{
		public $list = array();
		
		public function add($address)
		{
			if (is_string($address))
				$address = new MailAddress($address);
				
			$this->list[] = $address;
		}
		
		public function toString()
		{
			$ret = '';
			$delimiter = ', ';
			
			foreach ($this->list as $ma)
			{
				if ($ret)
					$ret .= $delimiter;
				
				$ret .= $ma->toString();
			};

			return $ret;
		}
		
		public function count()
		{
			return count($this->list);
		}
	}
	
	class MailMessage
	{
		public static
			$encoding = 'utf-8';
		
		public $to, $from, $subject, $body, $attachments, $content_type, $charset;
		
		public function MailMessage()
		{
			$this->to = new MailAddressList();
			$this->from = new MailAddress('');
			$this->subject = '';
			$this->body = '';
			$this->attachments = array();
			$this->content_type = 'text/plain';
			$this->charset = self::$encoding;
			$this->attachments = new MailAttachmentList();
		}
		
		public function getHeaderValue($name, $value)
		{
			return sprintf("%s: %s", $name, $value);
		}
		
		/*static function mimeEncodeCB($r)
		{
			 return '=?'.self::$encoding.'?B?'.base64_encode($r[0]).'?=';
		}

		static function mimeEncode($str)
		{
			return preg_replace_callback("/[ \x80-\xFF]+/", array(self, 'mimeEncodeCB'), $str);
		}*/
		
		static function mimeEncode($str)
		{
			return preg_match("/[\x80-\xFF]/", $str) ? '=?'.self::$encoding.'?B?'.base64_encode($str).'?=' : $str;
		}

		public function getHeadersBody($exclude_headers = array())
		{
			$t_headers = '';
			
			$body_content_type = $this->content_type.'; charset='.$this->charset;
			$is_multipart = $this->attachments->count() > 0;
			
			if ($is_multipart)
			{
				$boundary = '----------'.uniqid('');
				$content_type = 'multipart/mixed; boundary="'.$boundary.'"';
			} else 
				$content_type = $body_content_type;
			
			$headers = array(
				'To' => $this->to->toString(),
				'From' =>  $this->from->toString(),
				'Subject' =>  self::mimeEncode($this->subject),
				'Content-type' => $content_type,
			);
			
			$body = $this->body;
			
			foreach ($headers as $k => $v)
			{
				if (!in_array($k, $exclude_headers))
					$t_headers .= $this->getHeaderValue($k, $v)."\n";
			};
			
			$t_body = '';
			
			if ($is_multipart)
			{
				$b_body = chunk_split(base64_encode($body), 70, "\n");

				$t_body .= <<<TXT

--$boundary
Content-Type: $body_content_type
Content-Transfer-Encoding: base64

$b_body

TXT;
				foreach ($this->attachments->list as $attachment)
				{
					$name = $attachment->name;
					$ct = $this->getHeaderValue('Content-Type', 'application/octet-stream; name="'.self::mimeEncode($name).'"');
					$cd = $this->getHeaderValue('Content-Disposition', 'attachment; filename="'.self::mimeEncode($name).'"');
					
					$t_body .= <<<TXT

--$boundary
$ct
Content-Transfer-Encoding: base64
$cd


TXT;

					$t_body .= chunk_split(base64_encode(file_get_contents($attachment->filename)), 70, "\n")."\n";
				}
				
				$t_body .= "--$boundary--\n";
			} else 
			{
				$t_body .= $body;
			};
						
			return array($t_headers, $t_body);
		}
		
		public function send()
		{
			list ($t_headers, $t_body) = $this->getHeadersBody(array('To', 'Subject'));

			mail($this->to->toString(), self::mimeEncode($this->subject), $t_body, $t_headers);
		}
	}
?>