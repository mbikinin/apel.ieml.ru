<?
	function __autoload($class_name)
	{
		global $Inflector;
		static $resolved = array();

		if (!$include_file =& $resolved[$class_name])
		{
			if (is_file($class_file = $Inflector->underscore(FWK_CLASSES.$class_name).'.inc'))
			{
				$include_file = $class_file;
			}
			else if (is_file($enum_file = $Inflector->underscore(FWK_ENUMS.$class_name).'.inc'))
			{
				$include_file = $enum_file;
			}
			else
			{
				$class_file = $Inflector->underscore(str_replace('_', '/', $class_name)).'.inc';

				if (preg_match('/_controller\.inc$/', $class_file) && is_file($controller_file = FWK_CONTROLLERS.$class_file))
				{
					$include_file = $controller_file;
				}
				elseif (preg_match('/_helper\.inc$/', $class_file) && is_file($helper_file = FWK_HELPERS.$class_file))
				{
					$include_file = $helper_file;
				}
				elseif (is_file($model_file = FWK_MODELS.$class_file))
				{
					$include_file = $model_file;
				}
			}

			include_once FWK_CONFIG_DIR.'autoload.inc';
		}

		if ($include_file)
		{
			include_once $include_file;
		}
	}
?>