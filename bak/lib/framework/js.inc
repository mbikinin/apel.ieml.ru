<?
	class Js
	{
		static function toArray($array, $quote = true)
		{
			$a_hash = array();

			foreach($array as $value)
			{
				$a_hash[] = is_array($value)
					? self::toArray($value, $quote)
					: self::autoQuote($value, $quote);
			}

			return '['.implode(',', $a_hash).']';
		}

		static function autoQuote($value, $quote = true)
		{
			$quote &= (string)(int)$value != (string)$value;
			return $quote ? "'".self::quote($value)."'" : $value;
		}

		static function quote($string)
		{
			return addcslashes($string, "'\\\r\n");
		}

		static function toHash($hash, $quote = true)
		{
			$a_hash = array();

			foreach($hash as $key => $value)
			{
				$pref = self::autoQuote($key, $quote).':';
				$a_hash[] = is_array($value)
					? $pref.self::toHash($value, $quote)
					: $pref.self::autoQuote($value, $quote);
			}

			return '{'.implode(',', $a_hash).'}';
		}

		static function toArrayOfHashes($array_of_hashes, $quote = true)
		{
			$ret = array();

			foreach ($array_of_hashes as $_)
				$ret[] = self::toHash($_, $quote);

			return '['.implode(',', $ret).']';
		}
	}
?>