<?
	// router.inc, Wed Feb 06 14:56:10 MSK 2008

	class Router
	{
		public static
			$Memcache;

		private
			$routes = array(),
			$find_default_route = 0;

		function connectDefault()
		{
			$this->find_default_route = 1;
		}

		function connect($path, $params = null)
		{
			if (!is_array($params))
				$params = array();

//			$s_params = Hash::leaveKey($params, 'controller', 'action', 'id');
			$s_params = Hash::wipeKey($params, 'requirements', 'defaults');
			$path_re = '';
			$re_idx = 1;
			$defaults = array();

			foreach (preg_split('!/!', $path, -1, PREG_SPLIT_NO_EMPTY) as $token)
			{
				if ($token{0} == ':')
				{
					$token = substr($token, 1);

					if ($params['requirements'][$token])
						$token_re = $params['requirements'][$token];
					else
						$token_re = '[^/]+';

					if (!preg_match('/^\([^\?].*?\)$/', $token_re))
						$token_re = "($token_re)";

					$token_re = "/$token_re";

					if (array_key_exists($token, $params))
					{
						$token_re = "(?:$token_re)?";

						if (!is_null($params[$token]))
							$defaults[$token] = $params[$token];
					}

					$s_params[$token] = '$'.($re_idx++);

					$path_re .= $token_re;
				}
				else
					$path_re .= "/$token";
			}

			$this->routes[$path_re] = array(
				'replacement'	=> urldecode(http_build_query($s_params)),
				'defaults'		=> $defaults,
			);

//			Debug::varDump($this->routes);
		}

		function findRoute($url)
		{
//Debug::varDump($url, $this->routes, false);

			$url = self::prepareUrl($url);

			foreach ($this->routes as $regexp => $_)
				if (preg_match($re = '!^'.$regexp.'$!', $url))
				{
					$r_url = preg_replace($re, $_['replacement'], $url);
					parse_str($r_url, $params);
//Debug::varDump($_['replacement'], $r_url, $params);

					foreach ($_['defaults'] as $param_name => $default_value)
						if (!$params[$param_name])
							$params[$param_name] = $default_value;
//Debug::varDump($url, $params, false);

					return $this->addDefaultParams($params);
				}

			// default route
			return $this->findDefaultRoute($url);
		}

		function findDefaultRoute($url)
		{
			if (!$this->find_default_route)
				return null;

			for ($non_controller = array(), $parts = explode('/', substr($url, 1));
				count($parts) > 0 && !is_file(FWK_CONTROLLERS.implode('/', $parts).'_controller.inc');
				$non_controller[] = array_pop($parts));

			if (count($parts) > 0)
			{
				$params = array(
					'controller' => implode('/', $parts),
				);

				$non_controller = array_reverse($non_controller);

				if ($non_controller && !is_numeric($non_controller[0]))
					$params['action'] = array_shift($non_controller);

				if ($non_controller && is_numeric($non_controller[0]))
					$params['id'] = array_shift($non_controller);

				while (count($non_controller) > 1)
					if (array_key_exists($non_controller[0], $params))
						break;
					else
						$params[array_shift($non_controller)] = array_shift($non_controller);

				if ($non_controller)
					$params['unparsed'] = $non_controller;

				return $this->addDefaultParams($params);
			}

			return null;
		}

		function addDefaultParams($params)
		{
			if (!array_key_exists('action', $params))
				$params['action'] = 'index';

			return $params;
		}

		static function prepareUrl($url, $dash_to_underscore = 1)
		{
			$url = preg_replace('!/$!', '', $url);

			if ($dash_to_underscore)
			{
				$url = strtr($url, '-', '_');
			}

			return $url;
		}
	}
?>