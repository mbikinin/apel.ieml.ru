<?
	/**
	 * @package Helper
	 * @version Mon Mar 03 11:25:48 MSK 2008
	 *
	 */

	class Helper
	{
		public
			$controller_object,
			$object,
			$object_name,
			$attribute_name,
			$extra_url_params = array(),
			$params;

		public static
			$Session,
			$js_base = '/js',
			$css_base = '/d/c',
			$sources = array(),
			$required = '<span class="required">*</span>';

	    function __construct(ActionController $controller_object = null, ActiveRecord $object = null, $object_name = null, $params = null)
	    {
        	$this->controller_object = $controller_object;
        	$this->object = $object;
       		$this->object_name = $object_name;
       		$this->params = is_null($params) ? $_REQUEST : $params;
    	}

	   	function value($attribute_name = '')
	    {
	    	$r = $this->params;

	    	if ($this->object_name)
	    		$r = $r[$this->object_name];

	    	if (!$attribute_name)
	    		$attribute_name = $this->attribute_name;

	    	list ($attr_name, $attr_indexes) = $this->fetchIndexes($attribute_name);

        	if (!($value = $r[$attr_name]) && $this->object instanceof ActiveRecord && $attr_name)
           	{
               	$value = $this->object->$attr_name;
        	}

        	if ($attr_indexes)
        		$value = $this->getIndexedValue($value, $attr_indexes);

        	return $value;
    	}

    	protected function fetchIndexes($attribute_name)
    	{
    		if (preg_match('/^(\w+)\[(.*)\]+$/', $attribute_name, $m))
    			return array($m[1], $m[2] != '' ? explode('][', $m[2]) : array());
    		else
	    		return array($attribute_name, array());
    	}

    	protected function getIndexedValue($value, $indexes)
    	{
    		if (!is_array($value) || !$indexes)
    			return $value;

    		for ($v = $value; is_array($v) && $indexes; $v = $v[array_shift($indexes)]);
    			return $v;
    	}

	    protected function tagOptions($options)
	    {
        	if(count($options))
        	{
            	$html = array();
            	foreach($options as $key => $value)
            	{
                	$html[] = $key == 'action' ? $key.'="'.$value.'"' : $key.'="'.@htmlspecialchars($value, ENT_COMPAT).'"';
            	}
            	sort($html);
            	$html = implode(" ", $html);
        	}
        	return $html;
    	}

	    protected function convertOptions($options = array())
	    {
        	foreach(array('disabled', 'readonly', 'multiple') as $a)
        	{
            	$this->booleanAttribute(&$options, $a);
        	}
        	return $options;
    	}

	    protected function booleanAttribute(&$options, $attribute)
	    {
        	if($options[$attribute])
        	{
            	$options[$attribute] = $attribute;
        	}
        	else
        	{
            	unset($options[$attribute]);
        	}
    	}

    	/**
     	*
     	* Returns a CDATA section for the given +content+.  CDATA sections
     	* are used to escape blocks of text containing characters which would
     	* otherwise be recognized as markup. CDATA sections begin with the string
     	* <tt>&lt;![CDATA[</tt> and end with (and may not contain) the string
     	* <tt>]]></tt>.
     	*/
    	function cdataSection($content)
    	{
        	return "<![CDATA[".$content."]]>";
    	}

    	/**
     	*  Generate an HTML or XML tag with optional attributes
     	*
     	*  Example: tag("br");
     	*   Results: <br />
     	*  Example: tag("input", array("type" => "text"));
     	* <input type="text" />
     	*/
    	function tag($name, $options = array(), $open = false)
    	{
    		return '<'.$name.' '.$this->tagOptions($options).($open ? '>' : ' />');
    	}

    	/**
     	*  Generate an open/close pair of tags with content between
     	*
     	*  Example: contentTag("p", "Hello world!");
     	*  Result: <p>Hello world!</p>
     	*  Example: contentTag("div", content_tag("p", "Hello world!"), array("class" => "strong")) =>
     	*  Result:<div class="strong"><p>Hello world!</p></div>
     	*/
    	function contentTag($name, $content, $options = array())
    	{
    		return '<'.$name.' '.$this->tagOptions($options).'>'.$content.'</'.$name.'>';
    	}

	    /**
	     *
     	*/
	    function toContentTag($tag_name, $options = array()) {
	        return $this->contentTag($tag_name, $this->value(), $options);
	    }

	    /**
	     * Creates a link tag of the given +name+ using an URL created by the set of +options+.
	     * It's also possible to pass a string instead of an options hash to
	     * get a link tag that just points without consideration. If null is passed as a name, the link itself will become the name.
	     * The $html_options have a special feature for creating javascript confirm alerts where if you pass ":confirm" => 'Are you sure?',
	     * the link will be guarded with a JS popup asking that question. If the user accepts, the link is processed, otherwise not.
	     *
	     * Example:
	     *   linkTo("Delete this page", array(":action" => "delete", ":id" => $page->id), array(":confirm" => "Are you sure?"))
	     */
	    function linkTo($name, $options = array(), $html_options = array())
	    {
	        $html_options = $this->convertConfirmOptionToJavascript($html_options);

            $url = $this->urlFor($options);

            if (!$name)
            {
                $name = $url;
            }

	         $href = array('href' => $url);

			if(count($html_options) > 0)
			{
				$html_options = array_merge($html_options, $href);
			}
			else
			{
				$html_options = $href;
	        }

	        if (@$html_options['title_as_name'])
	        {
	        	unset($html_options['title_as_name']);
	        	$html_options['title'] = $name;
	        }

	        //$html_options = array_merge(array('title' => $name), $html_options);
	        return $this->contentTag('a', $name, $html_options);
	    }

	    function convertConfirmOptionToJavascript($html_options)
	    {
	        if(@$html_options['confirm'])
	        {
	            $html_options['onclick'] = "return confirm('".addslashes($html_options['confirm'])."');";
	            unset($html_options['confirm']);
	        }
	        return $html_options;
	    }

	    function convertBooleanAttributes(&$html_options, $bool_attrs)
	    {
	        foreach($bool_attrs as $x)
	        {
	            if(@array_key_exists($x, $html_options))
	            {
	                $html_options[$x] = $x;
	            }
	        }
	        return $html_options;
	    }

	    function buttonTo($name, $options = array(), $html_options = null)
	    {
	        $html_options = !is_null($html_options) ? $html_options : array();
	        $html_options = $this->convertBooleanAttributes($html_options, array('disabled'));
	        //$html_options = $this->convertConfirmOptionToJavascript($html_options);
/*
	        if (is_string($options))
	        {
	            $url = $options;
	            $name = (!is_null($name) ? $name : $options);
	        }
	        else
	        {
	            $url = urlFor($options);
	            $name = (!is_null($name) ? $name : url_for($options));
	        }
*/
			$url = $this->urlFor($options);
			$name or $name = $url;

	        $html_options = array_merge($html_options, array(
	        	'type' 		=> 'button',
	        	'value' 	=> $name,
	        	'class'		=> 'button-to',
	        	'onclick'	=> "location.href='$url'",
	        ));
	        return $this->tag('input', $html_options);
	    }

	    /**
	     *  Return the URL for the set of $options provided.
	     */
	    function urlFor($options = array())
	    {	
	        $url_base = null;
	        $url = array();
	        $extra_params = array();
	        if(is_string($options))
	        {
	            //$url[] = $options;
	            return $options == 'back' ? $this->controller_object->back : $options;
	        }
	        elseif(is_array($options))
	        {
	        	if (count($options) == 0)
	        		return $_SERVER['REQUEST_URI'];

	            $url_base = $_SERVER['HTTP_HOST'];
	            if(substr($url_base, -1) == "/")
	            {
	                # remove the ending slash
	                $url_base = substr($url_base, 0, -1);
	            }

	            if(@$_SERVER['HTTPS'])
	            {
	                $url_base = "https://".$url_base;
	            }
	            else
	            {
	                $url_base = "http://".$url_base;
	            }

	            $add_defaults = 1;
	            $defaults = @array(
	            	'controller'	=> $this->controller_object->controller_name,
	            	'action'		=> $this->controller_object->action,
	            	'id'			=> $this->controller_object->params['id'],
	            );
	            foreach ($defaults as $pn => $_pv)
	            {
	            	$pv = '';

	            	if (array_key_exists($pn, $options))
	            	{
	            		$pv = $options[$pn];

	            		if ($pn == 'id' && is_object($pv))
	            			$pv = $pv->getId();

	            		$add_defaults = 0;
	            	}
	            	elseif ($add_defaults)
	            		$pv = $_pv;

	            	if ($pn == 'controller')
	            	{
	            		$pv = $pv{0} == '/' ? substr($pv, 1) : $this->controller_object->controller_path.$pv;
	            	}

	            	if ($pv)
	            		$url[] = $pv;
	            }

//				if (count($url) > 1 && ($id = $options['id']))
//					$url[] = is_object($id) ? $id->getId() : $id;

				$extra_params = Hash::wipeKey($options, 'controller', 'action', 'id') + $this->extra_url_params;

	            #if(count($options)) {
	            #    foreach($options as $key => $value) {
	            #        if(!strstr($key, ":")) {
	            #            $extra_params[$key] = $value;
	            #        }
	            #    }
	            #}
	        }

	        if(count($url) && substr($url_base, -1) != '/')
	            $url_base .= '/';

	        return self::$Session->setURL($url_base.implode('/', $url).(count($extra_params) ? '?'.http_build_query($extra_params) : ''));
	    }

	    function formTag($options = array(), $url_for_options = array())
	    {
//	    	is_array($url_for_options) or $url_for_options = array();
	        $html_options = array_merge(array('method' => 'post'), $options);
		
	        if($html_options['multipart'])
	        {
	            $html_options['enctype'] = 'multipart/form-data';
	            unset($html_options['multipart']);
	        }
		
	        $html_options['action'] = $this->urlFor($url_for_options);
			
	        return $this->tag('form', $html_options, true);
	    }

	    function startFormTag()
	    {
	        $args = func_get_args();
	        return call_user_func_array(array($this, 'formTag'), $args);
	    }

	    function selectTag($name, $option_tags = null, $options = array())
	    {
	    	$options = array_merge(array('name' => $name, 'id' => $name), $this->convertOptions($options));
	        return $this->contentTag('select', $option_tags, $options);
	    }

	    function textFieldTag($name, $value = null, $options = array())
	    {
	    	$options = array_merge(
	    		array('type' => 'text', 'name' => $name, 'id' => $name, 'value' => $value),
	    		$this->convertOptions($options)
	    	);
	        return $this->tag('input', $options);
	    }

	    function hiddenFieldTag($name, $value = null, $options = array())
	    {
	        return $this->textFieldTag($name, $value, array_merge($options, array('type' => 'hidden')));
	    }

	    function fileFieldTag($name, $options = array())
	    {
	    	$options = array_merge($this->convertOptions($options), array('type' => 'file'));
	        return $this->textFieldTag($name, null, $options);
	    }

	    function passwordFieldTag($name = 'password', $value = null, $options = array())
	    {
	    	$options = array_merge($this->convertOptions($options), array('type' => 'password'));
	        return $this->textFieldTag($name, $value, $options);
	    }

	    function textAreaTag($name, $content = null, $options = array())
	    {
	        if ($options["size"])
	        {
	            list ($options['cols'], $options['rows']) = explode('x', $options['size']);
	            unset($options['size']);
	        }

	        $options = array_merge(array('name' => $name, 'id' => $name), $this->convertOptions($options));
	        return $this->contentTag('textarea', $content, $options);
	    }

	    function checkBoxTag($name, $value = 1, $checked = false, $options = array())
	    {
	        $html_options = array_merge(
	        	array('type' => 'checkbox', 'name' => $name, 'id' => $name, 'value' => $value),
	        	$this->convertOptions($options)
	        );

	        if ($checked)
	        	$html_options['checked'] = 'checked';

	        return $this->tag('input', $html_options);
	    }

	    function radioButtonTag($name, $value, $checked = false, $options = array())
	    {
	        $html_options = array_merge(
	        	array('type' => 'radio', 'name' => $name, 'id' => $name, 'value' => $value),
	        	$this->convertOptions($options)
	        );

	        if ($checked)
	        	$html_options['checked'] = 'checked';

	        return $this->tag('input', $html_options);
	    }

	    function submitTag($value = 'Сохранить', $options = array())
	    {
	    	$options = array_merge(
	    		array('type' => 'submit'/*, 'name' => 'commit'*/, 'value' => $value),
	    		$this->convertOptions($options)
	    	);
	        return $this->tag('input', $options);
	    }

	    function imageSubmitTag($source, $options = array())
	    {
	    	$options = array_merge(
	    		array('type' => 'image', 'src' => /*image_path(*/$source/*)*/),
	    		$this->convertOptions($options)
	    	);
	        return $this->tag('input', $options);
	    }

	    function buttonTag($caption, $options = array())
	    {
	    	$options = array_merge($options, array(
	    		'value'	=> $caption,
	    		'type'	=> 'button',
	    	));
	    	return $this->tag('input', $options);
	    }

		function endFormTag()
		{
	    	return '</form>';
		}

		function closeWin()
		{
			exit('<script>window.close()</script>');
		}

		function addSource($tag, $source, $base, $extension)
		{
			$p = parse_url($source);

			if (!$p['query'] && !preg_match("/\.$extension$/", $p['path']))
				$source .= ".$extension";

			if (!$p['scheme'] && substr($p['path'], 0, 1) != '/')
				$source = "$base/$source";

			if (!$this->sources[$tag][$source])
				self::$sources[$tag][$source] = 1;
		}

		function needJs($script)
		{
			$this->addSource('script', $script, self::$js_base, 'js');
		}

		function needCss($stylesheet)
		{
			$this->addSource('style', $stylesheet, self::$css_base, 'css');
		}

		function getJs()
		{
			$ret = '';

			if (isset(self::$sources['script']))
				foreach (array_keys((array)self::$sources['script']) as $source)
					$ret .= $this->contentTag('script', '', array(
						'type'		=> 'text/javascript',
						'language'	=> 'JavaScript',
						'src'		=> $source,
					))."\n";

			return $ret;
		}

		function getCss()
		{
			$ret = '';

			if (isset(self::$sources['style']))
				foreach (array_keys((array)self::$sources['style']) as $source)
					$ret .= $this->tag('link', array(
						'rel'	=> 'stylesheet',
						'href'	=> $source,
						'type'	=> 'text/css',
						'media'	=> 'all',
					))."\n";

			return $ret;
		}
	}
?>