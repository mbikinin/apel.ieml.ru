<?
	class Clamav
	{
		public static
			$clamdscan = '/usr/bin/clamdscan --stdout';

		public static function scanFile($file_name)
		{
			exec(self::$clamdscan.' '.escapeshellarg($file_name), $output, $return_code);

			switch ($return_code)
			{
				case 0: // No virus found
					return null;

				case 1: // Virus(es) found
					$fq = preg_quote($file_name);
					foreach ($output as $line)
						if (preg_match("!^$fq:!", $line))
						{
							$virus_name = preg_replace("!^$fq: (.+) FOUND!", '$1', $line);
							return "Файл инфицирован вирусом \"$virus_name\"";
						}

				case 2: // An error occured
					return $output[0];
			}
		}
	}
?>