<?
	abstract class ActiveRecordStatement
	{
		protected
			$limit		= false,
			$clauses	= array(
				'update'	=> '',
				'set'		=> '',
				'select'	=> '',
				'from'		=> '',
				'@join'		=> '',
				'where'		=> '',
				'union'		=> '',
				'group_by'	=> '',
				'order_by'	=> '',
				'having'	=> '',
				'@raw_sql'	=> '',
			),
			$addable_clauses = array(
				'where',
				'start_with',
				'connect_by',
			),
			$placeholder_count = 0,
			$params	= array();

		function __get($prop_name)
		{
			if (array_key_exists($prop_name, $this->clauses))
				return $this->clauses[$prop_name];

			if ($prop_name == 'params')
				return $this->params;
		}

		function __set($prop_name, $value)
		{
			if (in_array($prop_name, array('join', 'raw_sql')))
				$prop_name = "@$prop_name";

			if ($prop_name == 'cond' && is_array($value))
			{
				$params = array();
				foreach ($value as $f_name => $f_value)
				{
					$ph_name = 'cph'.(++$this->placeholder_count);
					$this->where = "$f_name = :$ph_name";
					$params[$ph_name] = $f_value;
				}

				$this->params += $params;
				return;
			}

			if (array_key_exists($prop_name, $this->clauses))
			{
				if ($prop_name == 'set' && is_array($value) && !is_numeric(key($value)))
					$value = $this->processAssocSetClause($value);
				else
					$value = $this->processQuery($value);

				if (in_array($prop_name, $this->addable_clauses))
				{
					$this->clauseAdd($prop_name, $value);
				}
				else
				{
					$this->clauses[$prop_name] = $value;
				}
			}

			if ($prop_name == 'params')
			{
				$this->mergeParams($value);
			}
		}

		function setParam($param_name, $value)
		{
			$this->params[$param_name] = $value;
		}

		function clauseAdd($clause, $where, $logic = 'AND')
		{
			if (!in_array($clause, $this->addable_clauses))
				return;

			if (!$where = $this->processQuery($where))
				return;

			$qlogic = $this->clauses[$clause] ? " $logic " : '';
			$this->clauses[$clause] .= "$qlogic$where";
		}

		function whereAdd($where, $logic = 'AND')
		{
			$this->clauseAdd('where', $where, $logic);
		}

		/**
		 * Adds several conditions in one call
		 * Usage: $stmt->whereBatchAdd('AND', $where1, $where2, ..., $whereN);
		 *
		 * @param string $logic
		 * @param mixed $where - string || array
		 */
		function whereBatchAdd($logic, $where)
		{
			for ($n = 1; $n < func_num_args(); $n++)
				$this->whereAdd(func_get_arg($n), $logic);
		}

		/**
		 * Shortcut to whereBatchAnd with logic = AND
		 *
		 * @param mixed $where - string || array
		 */
		function whereBatchAnd($where)
		{
			$args = func_get_args();
			array_unshift($args, 'AND');
			return call_user_func_array(array($this, 'whereBatchAdd'), $args);
		}

		/**
		 * Shortcut to whereAdd
		 * Usage: $stmt->whereAnd($SQL, $placeholder1, $placeholder2, ... $placeholderN)
		 * or $stmt->whereAnd($SQL, array('bindvar1' => 'value1', ...))
		 *
		 * @param string $where
		 */
		function whereAnd($where)
		{
			$args = func_get_args();
			$this->whereAdd($args);
		}

		function whereClear()
		{
			$this->clauses['where'] = '';
		}

		function queryAdd($query)
		{
			if (!$query = $this->processQuery($query))
				return;

			$this->clauses['@raw_sql'] .= " $query";
		}

		function processAssocSetClause($data)
		{
			$set = $params = array();
			foreach ($data as $field_name => $value)
			{
				$ph = 'sph'.($this->placeholder_count++);
				$set[] = "$field_name = :$ph";
				$params[$ph] = $value;
			}

			$this->mergeParams($params);
			return implode(', ', $set);
		}

		function processQuery($query)
		{
			if (!$query)
				return '';

			if (!is_array($query))
				return $query;

			$this->processPlaceholders($query);
			$this->processSubsts($query);

			if (is_array($query))
				list ($query, $query_params) = $query;
			else
				$query_params = array();

			$pe = new ARS_ParamExtractor($query, $query_params);
			$this->mergeParams($pe->extract());
			return $query;
		}

		function mergeParams($params)
		{
			$this->params = array_merge($this->params, $params);
		}

		protected function processPlaceholders(&$where)
		{
			if (!is_array($where) || count($where) < 2 || !preg_match('/\?/', $where[0]))
				return;

			$p = new ARS_PlaceholderConverter($where, &$this->placeholder_count);
			$where = $p->process();
		}

		protected function processSubsts(&$where)
		{
			if (!is_array($where) || count($where) != 2 || !is_array($where[1]) || !preg_match('/#([^#]+)#/', $where[0]))
				return;

			$where[0] = preg_replace('/#([^#]+)#/', ':$1', $where[0]);
		}

		function buildQuery()
		{
			$query = array();
			foreach ($this->clauses as $clause_name => $clause_value)
			{
				if ($clause_value != '')
				{
					$name = $clause_name{0} == '@' ? '' : strtoupper(strtr($clause_name, '_', ' ')).' ';
					$query[] = $name.$clause_value;
				}
			}
			$query = $this->limitClause(implode(" \n", $query));
			$pe = new ARS_ParamExtractor($query, $this->params);
			return array($query, $pe->extract());
		}

		function mergeStatement(ActiveRecordStatement $stmt)
		{
			foreach (array_keys($this->clauses) as $clause)
			{
				$value = $stmt->$clause;

//				if ($name == 'where')
//					$value = substr($value, 1, -1);

				if ($value)
					$this->$clause = $value;
			}

			$this->mergeParams($stmt->params);
		}

		function paramAdd($key, $value)
		{
			$this->params[$key] = $value;
		}
	}

	class ARS_PlaceholderConverter
	{
		protected
			$sql,
			$bvars,
			$pvars,
			$bindvar_count,
			$bindvar_idx;

		function __construct($where, $bindvar_count)
		{
			$this->sql = array_shift($where);
			$this->pvars = $where;
			$this->bvars = array();
			$this->bindvar_count = &$bindvar_count;
			$this->bindvar_idx = 0;
		}

		function process()
		{
			$sql = preg_replace_callback('/\?/', array($this, 'replace'), $this->sql);
			return array($sql, $this->bvars);
		}

		function replace($m)
		{
			$key = "bindvar{$this->bindvar_count}";
			$this->bvars[$key] = $this->pvars[$this->bindvar_idx++];
			$this->bindvar_count++;
			return ":$key";
		}
	}

	class ARS_ParamExtractor
	{
		public
			$query,
			$params,
			$captured_params = array();

		function __construct($query = '', $params = array())
		{
			$this->query = $query;
			$this->params = $params;
		}

		function extract()
		{
			preg_replace_callback('/:(\w+)/', array(&$this, 'captureParam'), $this->query);
			return $this->captured_params;
		}

		protected function captureParam($m)
		{
			list (, $var_name) = $m;
			$this->captured_params[$var_name] = $this->params[$var_name];
		}
	}
?>