<?
	class ActiveRecordConnector
	{
		protected
			$dbh;

		public
			$debug = 0;

		static function factory($dsn)
		{
			$dsn_params = parse_url($dsn);
			$dsn_params['db'] = substr($dsn_params['path'], 1);
			unset($dsn_params['path']);
			parse_str($dsn_params['query'], $query_params);
			$params = array_merge($dsn_params, $query_params);
			unset($params['query']);

			$class_name = "{$params['scheme']}_ActiveRecordConnector";
			return new $class_name($params);
		}

		function getStatement()
		{
			$class_name = preg_replace('/Connector$/i', 'Statement', get_class($this));
			return new $class_name();
		}

	    protected function raise($message = '', $db_error = 1)
	    {
	    	if ($message && $db_error)
	    		$message .= '<br />';

	    	if ($db_error)
	    		$message .= $this->getLastError();

    	    throw new ActiveRecordConnectorException($message, 'ActiveRecordConnector error');
    	}

    	function __destruct()
    	{
    		$this->close();
    	}

    	function close() {}

		function qRows($query)
		{
			$res = $this->query($query);
			$rows = $this->numRows($res);
			$this->freeResult($res);
			return $rows;
		}

		function isValid()
		{
			return $this->query("SELECT 1") ? TRUE : FALSE;
		}

		function prepareQuery($Query)
		{
			if (!is_array($Query))
				return $Query;

			if ($hashed = (count($Query) == 2 && is_array($Query[1])))
			{
				list ($SQL, $params) = $Query;
				$SQL = preg_replace('/#(\w+)#/e', '$this->escapeValue($params["$1"])', $SQL);
				$SQL = preg_replace('/:(\w+)/e', '$this->escapeValue($params["$1"])', $SQL);
			}
			else
			{
				$params = $Query;
				$SQL = array_shift($params);
				$SQL = preg_replace('/\?/e', '$this->escapeValue(array_shift($params))', $SQL);
			}


			return $SQL;
		}

		function escapeValue($value)
		{
			if (is_int($value) || is_float($value))
				return $value;
			elseif (is_null($value))
				return 'NULL';
			else
				return "'".$this->escape($value)."'";
		}

		function ezQuery($Query, $Params=array())
		{
			$this->_ezQueryInit($Params);
			
         	$Res = $this->query($Query);
         	$Ret = $Params['initret'];
         	$Rows = $this->NumRows($Res);
         	for($Row=0; $Row<$Rows; $Row++)
         	{
         		$aRow = $Params['assoc'] ? $this->FetchArray($Res) : $this->FetchRow($Res);
         		$this->_ezQueryProcess($Ret, $aRow, $Params, $Row, $Rows);
         	}
         	$this->FreeResult($Res);

         	return $this->_ezQueryFinalize($Ret, $Rows, $Params);
		}

		function autoFetch($Query, &$Res, $Method='fetchArray')
		{
			is_resource($Res) or $Res = $this->query($Query);
			if(!$Row = call_user_func(array(&$this, $Method), $Res))
			{
				$this->FreeResult($Res);
				$Res = NULL;
			}

			return $Row;
		}

		protected function _ezQueryInit(&$Params)
		{
         	isset($Params['template'])		or $Params['template']	= FALSE;	//vsprintf fetched data into template
         	isset($Params['callback'])		or $Params['callback']	= FALSE;	//Use callback function
         	isset($Params['clb_args']) && is_array($Params['clb_args'])	or $Params['clb_args']	= array();	//Array of extra arguments to pass to the callback function
         	isset($Params['simplify'])		or $Params['simplify']	= TRUE;		//Returns the element instead of the array containing one element
         	isset($Params['concat'])		or $Params['concat']	= TRUE;		//Concat the strings parsed into template
         	isset($Params['default'])		or $Params['default']	= NULL;		//Default value (if no rows selected)
         	isset($Params['makehash'])		or $Params['makehash']	= FALSE;	//Make hash instead of array
         	isset($Params['assoc'])			or $Params['assoc']		= FALSE;	//Fetch as associative array
         	isset($Params['vertical'])		or $Params['vertical']	= FALSE;	//use vertical order
         	isset($Params['initret']) && is_array($Params['initret'])	or $Params['initret']	= array();	//initial result array
         	isset($Params['multihash']) && is_int($Params['multihash'])	or $Params['multihash']	= 0;
         	isset($Params['simplehash'])	or $Params['simplehash'] = false;	//used in combination with makehash = 'array'
		}

		protected function _ezQueryProcess(&$Ret, $aRow, $Params, $Row, $Rows)
		{
			if($Params['vertical'])
         	{
         		foreach($aRow as $key => $value)
         		{
         			isset($Ret[$key]) or $Ret[$key] = array();
					array_push($Ret[$key], $value);
				}
				return;
         	}

         	if($Params['makehash'])
         	{
         		$key = array_shift($aRow);
         		$value = $Params['simplify'] && count($aRow) == 1 ? $aRow[0] : $aRow;

         		if($Params['makehash'] === 'array' && (!$Params['simplehash'] || ($isset = isset($Ret[$key]))))
         		{
         			is_array($Ret[$key]) or $Ret[$key] = $isset ? array($Ret[$key]) : array();
         			$Ret[$key][] = $value;
         		}
         		else
         		{
         			$Ret[$key] = $value;
				}

         		return;
         	}

         	if($Params['multihash'])
         	{
       			$tmp = &$Ret;
         		for($n=0; $n<$Params['multihash']; $n++)
         			$tmp = &$tmp[array_shift($aRow)];
       			$tmp = $Params['simplify'] && count($aRow) == 1 ? $aRow[0] : $aRow;

         		return;
         	}

         	if($Params['callback'] && is_null($aRow = call_user_func_array(
         		$Params['callback'],
         		array_merge(array($aRow, $Row, $Rows), $Params['clb_args']))))
			return;

       		if($Params['template'])
       		{
				if(is_array($aRow[0]))
         		{
         			for($szRow='', $i=0; $i<count($aRow); $i++)
         				$szRow .= vsprintf($Params['template'], $aRow[$i]);

         			$aRow = $szRow;
         		}
       			else $aRow = vsprintf($Params['template'], $aRow);
			}
         	elseif($Params['simplify']  && !$Params['assoc'] && is_array($aRow) && count($aRow) == 1)
				$aRow = array_shift($aRow);

         	array_push($Ret, $aRow);
		}

		protected function _ezQueryFinalize($Ret, $Rows, $Params)
		{
			if($Rows == 0)
         		return count($Params['initret']) > 0 ? $Params['initret'] : $Params['default'];

         	if(!$Params['makehash'] && !$Params['multihash'])
         	{
				if($Params['template'] && $Params['concat'])
       				return implode("\n", $Ret);

       			if($Params['simplify'])
       				if($Params['vertical'])
       				{
       					foreach((array)$Ret as $key => $value)
       						if(count($value) == 1)
       							$Ret[$key] = $value[0];

						return $Ret;
       				}
       				elseif(count($Ret) == 1)
       					return array_shift($Ret);
       		};

       		return $Ret;
		}

		protected function debugQuery($query)
		{
			echo "<p><b>Query:</b><pre>$query</pre><b>Duration:</b> $duration sec</p>";
		}
	}
?>