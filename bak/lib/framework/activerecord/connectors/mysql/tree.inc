<?
	abstract class MySQLTreeActiveRecord extends ActiveRecord
	{
		protected
			$parent_id_name = 'parent_id',
			$order_by = 'lft',
			$rgts;

		function getRoot()
		{
			return $this->findByLft(1);
		}
		
		function getRootId()
        {
            return $this->getRoot()->id;
        }

		function isRoot()
		{
			return $this->lft == 1;
		}

		function getParents($args = array())
		{
			is_array($args) or $args = array();
			$this->addFilter("? BETWEEN lft AND rgt", $this->lft);
			return $this->find(array_merge(array('order_by' => 'lft'), $args, array('all' => 1)));
		}

		function getChildren($direct = 0)
		{
			$where = $direct
				? array('parent_id = ?', $this->id)
				: array('lft BETWEEN #lft# AND #rgt#', $this->getAttributes());

			return $this->find(array('all' => 1, 'where' => $where));
		}
		
		function getDirectChildren()
		{
			return $this->getChildren(1);
		}

		function getParent()
		{
			return $this->find($this->parent_id);
		}

		function insert()
		{
//Debug::varDump($this->getAttributes());
			self::$Con->Lock("{$this->table_name} WRITE");

			if (!$parent = $this->find($this->__get($this->parent_id_name)))
			{
				if ($this->findByLft(1))
				{
					self::$Con->Unlock();
					return null;
				}

				$lft = 1;
				$rgt = 2;
			}
			else
			{
				$x = $parent->rgt - 1;
				$lft = $parent->rgt;
				$rgt = $parent->rgt + 1;

				self::$Con->query(sprintf("UPDATE {$this->table_name} SET rgt = rgt + 2 WHERE rgt > %d", $x));
				/*$this->updateAll(array(
					'set'	=> 'rgt = rgt + 2',
					'where'	=> array("rgt > ?", $x),
				));*/

				self::$Con->query(sprintf("UPDATE {$this->table_name} SET lft = lft + 2 WHERE lft > %d", $x));
				/*$this->updateAll(array(
					'set'	=> 'lft = lft + 2',
					'where'	=> array("lft > ?", $x),
				));*/
			}

			$this->__set('lft', $lft);
			$this->__set('rgt', $rgt);

			$ret = parent::insert();
			self::$Con->unlock();

			return $ret;
		}

		function free()
		{
			parent::free();
			$this->rgts = array();
		}

		function fetch()
		{
			if ($obj = parent::fetch())
			{
				if(!is_array($obj) && is_object($obj))
					$obj->fields['level'] = $this->getLevel($obj->rgt);
			}

			return $obj;
		}

		protected function getLevel($rgt)
		{
			while (count($this->rgts) > 0 && $this->rgts[count($this->rgts)-1] < $rgt)
				array_pop($this->rgts);

			$level = count($this->rgts);

			$this->rgts[] = $rgt;

			return $level;
		}

		function move($up)
		{
			self::$Con->Lock("{$this->table_name} WRITE");

			if (!$this->lft || !$this->rgt)
			{
				self::$Con->Unlock();
				return false;
			}

			$lft_b = $this->lft;
			$rgt_b = $this->rgt;

			if ($up)
			{
				$lft_c = $lft_b;
				$rgt_c = $rgt_b;

				$rgt_b = $lft_c - 1;

				if (!$r = $this->findByRgt($rgt_b))
				{
					self::$Con->Unlock();
					return false;
				}

				$lft_b = $r->lft;
			}
			else
			{
				$lft_c = $rgt_b + 1;

				if (!$l = $this->findByLft($lft_c))
				{
					self::$Con->Unlock();
					return false;
				}

				$rgt_c = $l->rgt;
			}

			$size_c = $rgt_c - $lft_c + 1;
			$size_b = $rgt_b - $lft_b + 1;

			$SQL = <<<SQL
UPDATE {$this->table_name} SET
rgt = rgt + IF(lft<$lft_c, $size_c, - $size_b),
lft = lft + IF(lft<$lft_c, $size_c, - $size_b)
WHERE lft BETWEEN $lft_b AND $rgt_c
SQL;

			self::$Con->Query($SQL);
			self::$Con->Unlock();
			return true;
		}

		function drop()
		{
			if (!$this->id)
				return false;
			
			self::$Con->Lock("{$this->table_name} WRITE");

			#$this->rebuildTree(null, true);
			
			list($this->lft, $this->rgt) = self::$Con->EzQuery("SELECT lft, rgt FROM {$this->table_name} WHERE id = '{$this->id}' LIMIT 1");
			
			if (!$this->lft || !$this->rgt)
				return false;

			//$this->dropAll(array(
			self::$Con->query(sprintf("DELETE FROM {$this->table_name} WHERE lft BETWEEN %d AND %d", $this->lft, $this->rgt));

			/*parent::drop(array(
				'where'	=> array("lft BETWEEN ? AND ?", $this->lft, $this->rgt),
			));*/

			$gap = $this->rgt - $this->lft + 1;

			self::$Con->query("UPDATE {$this->table_name} SET lft = IF(lft > {$this->lft}, lft - $gap, lft), rgt = IF(rgt >  {$this->lft}, rgt - $gap, rgt)");
			
			/*$this->updateAll(array(
				'set' => "lft = IF(lft > {$this->lft}, lft - $gap, lft), rgt = IF(rgt >  {$this->lft}, rgt - $gap, rgt)",
			));*/

			self::$Con->unlock();
		}

		function appendChild(MySQLTreeActiveRecord $child)
		{
			self::$Con->Lock("{$this->table_name} WRITE");

			$subtree = false;

			if (!$child->lft || !$this->lft || $this->id == $child->id || $child->lft == $this->lft ||
				($subtree = ($this->lft >= $child->lft && $this->lft <= $child->rgt)))
			{
				$this->raise('Ошибка перемещения' . ($subtree ? ': невозможно переместить узел на место его потомков' : ''));
				self::$Con->Unlock();
				return false;
			}

			$gap = $child->rgt - $child->lft + 1;
			$y = $this->rgt - 1;
			$ofs = $this->rgt - $child->lft;

			if ($this->rgt > $child->rgt)
				$ofs -= $gap;

			if ($y > $child->lft)
			{
				$m1 = $child->lft + 1;
				$m2 = $y;

				self::$Con->query(array("UPDATE {$this->table_name} SET lft = lft + IF(@c:=lft BETWEEN :lft AND :rgt, :ofs, IF(lft BETWEEN :m1 AND :m2, -:gap, 0)),
rgt = rgt + IF(@c, :ofs, IF(rgt BETWEEN :m1 AND :m2, -:gap, 0)) WHERE (lft BETWEEN :lft AND :m2) OR (rgt BETWEEN :m1 AND :m2)", array(
						'lft'	=> $child->lft,
						'rgt'	=> $child->rgt,
						'ofs'	=> $ofs,
						'm1'	=> $m1,
						'm2'	=> $m2,
						'gap'	=> $gap,
					)));
				/*$this->updateAll(array(
					'set'		=> <<<SQL
lft = lft + IF(@c:=lft BETWEEN :lft AND :rgt, :ofs, IF(lft BETWEEN :m1 AND :m2, -:gap, 0)),
rgt = rgt + IF(@c, :ofs, IF(rgt BETWEEN :m1 AND :m2, -:gap, 0))
SQL
					,
					'where'		=> "(lft BETWEEN :lft AND :m2) OR (rgt BETWEEN :m1 AND :m2)",
					'params'	=> array(
						'lft'	=> $child->lft,
						'rgt'	=> $child->rgt,
						'ofs'	=> $ofs,
						'm1'	=> $m1,
						'm2'	=> $m2,
						'gap'	=> $gap,
					),
				));*/
			}
			else
			{
				$m1 = $y + 1;
				$m2 = $child->lft + 1;
				$m3 = $y + $gap;

				self::$Con->query(array("UPDATE {$this->table_name} SET lft = lft + IF(@c:=lft BETWEEN :lft AND :rgt, :ofs, IF(lft BETWEEN :m1 AND :lft, :gap,
	IF(lft BETWEEN :m2 AND :m3, -:gap, 0))),
rgt = rgt + IF(@c, :ofs, IF(rgt BETWEEN :m1 AND :lft, :gap, IF(rgt BETWEEN :m2 AND :m3, -:gap, 0)))
 WHERE (lft BETWEEN :m1 AND :rgt) OR (rgt BETWEEN :m1 AND :m3)", array(
						'lft'	=> $child->lft,
						'rgt'	=> $child->rgt,
						'ofs'	=> $ofs,
						'm1'	=> $m1,
						'm2'	=> $m2,
						'm3'	=> $m3,
						'gap'	=> $gap,
					)));
				/*$this->updateAll(array(
					'set'	=> <<<SQL
lft = lft + IF(@c:=lft BETWEEN :lft AND :rgt, :ofs, IF(lft BETWEEN :m1 AND :lft, :gap,
	IF(lft BETWEEN :m2 AND :m3, -:gap, 0))),
rgt = rgt + IF(@c, :ofs, IF(rgt BETWEEN :m1 AND :lft, :gap, IF(rgt BETWEEN :m2 AND :m3, -:gap, 0)))
SQL
					,
					'where'	=> "(lft BETWEEN :m1 AND :rgt) OR (rgt BETWEEN :m1 AND :m3)",
					'params'	=> array(
						'lft'	=> $child->lft,
						'rgt'	=> $child->rgt,
						'ofs'	=> $ofs,
						'm1'	=> $m1,
						'm2'	=> $m2,
						'm3'	=> $m3,
						'gap'	=> $gap,
					),
				));*/
			}

			$child->parent_id = $this->id;
			$child->saveWithoutValidation();

			self::$Con->unlock();
			return true;
		}
		
		/*function afterUpdate()
		{
			parent::afterUpdate();
			$affectedFields = (array)$this->getAffectedFields();
			if (in_array('parent_id', array_keys($affectedFields)))
			{
				$this->rebuildTree();
			}
		}*/
		
		function update($args = array())
		{
			$moveNode = false;

			$where = $args['where'] or $where = array(
				"{$this->id_name} = :{$this->id_name}", array($this->id_name => $this->{$this->id_name})
			);

			$this->beforeUpdate();

			$this->applyAllConversions('save', $this->fields);

			if (!$affected_fields = $this->getAffectedFields())
				return;

			if (in_array('parent_id', array_keys($affected_fields)))
			{
				$moveNode = true;
				$new_parent_id = $affected_fields['parent_id'];
				unset($affected_fields['parent_id']);
			}
			
			if ($affected_fields)
				self::$Con->upsert(1, $this->full_table_name, $affected_fields, $where, $this->field_info['db_types']);
			
			if ($moveNode)
			{
				if (!$this->moveTo($new_parent_id))
				{
					$this->addError('parent_id', 'Ошибка перемещения');
				}
			}
			
			$this->afterUpdate();
		}
		
		function moveTo($new_parent_id)
		{
			self::$Con->Lock("{$this->table_name} WRITE");

			$id = $this->id;
			
			list ($lft, $rgt) = self::$Con->EzQuery("SELECT lft, rgt FROM {$this->table_name} WHERE id='$id'");

			if ($lft)
			list ($lft_p, $rgt_p) = self::$Con->EzQuery("SELECT lft, rgt FROM {$this->table_name} WHERE id='$new_parent_id'");

			$subtree = FALSE;

			if (!$lft || !$lft_p || $id == $new_parent_id || $lft == $lft_p || ($subtree = ($lft_p >= $lft && $lft_p <= $rgt)))
			{
				/*$this->error_msg = 'Ошибка перемещения' . ($subtree ? ': невозможно переместить узел на место его потомков' : '');*/
				self::$Con->Unlock();
				return FALSE;
			}

			$gap = $rgt - $lft + 1;
			$y = $rgt_p - 1;
			$ofs = $rgt_p - $lft; 

			if ($rgt_p > $rgt)
				$ofs -= $gap;

			if ($y > $lft)
			{
				$m1 = $lft + 1;
				$m2 = $y;

				$SQL = "UPDATE {$this->table_name} SET ".
					"lft = lft + IF(@c:=lft BETWEEN $lft AND $rgt, $ofs, ".
						"IF(lft BETWEEN $m1 AND $m2, -$gap, 0)), ".
					"rgt = rgt + IF(@c, $ofs, ".
						"IF(rgt BETWEEN $m1 AND $m2, -$gap, 0)) ".
					"WHERE (lft BETWEEN $lft AND $m2) OR ".
						"(rgt BETWEEN $m1 AND $m2)";
			} 
			else
			{
				$m1 = $y + 1;
				$m2 = $lft + 1;
				$m3 = $y + $gap;

				$SQL = "UPDATE {$this->table_name} SET ".
							"lft = lft + IF(@c:=lft BETWEEN $lft AND $rgt, $ofs, ".
						"IF(lft BETWEEN $m1 AND $lft, $gap, ".
						"IF(lft BETWEEN $m2 AND $m3, -$gap, 0))), ".
							"rgt = rgt + IF(@c, $ofs, ".
						"IF(rgt BETWEEN $m1 AND $lft, $gap, ".
						"IF(rgt BETWEEN $m2 AND $m3, -$gap, 0))) ".
						"WHERE (lft BETWEEN $m1 AND $rgt) OR ".
						"(rgt BETWEEN $m1 AND $m3)";
			}

			self::$Con->Query($SQL);

			self::$Con->Query("UPDATE {$this->table_name} SET parent_id = '$new_parent_id' WHERE id = '$id'");

			self::$Con->Unlock();

			#$this->error_msg = '';
			return TRUE;
		}
		
        
        /*function rebuildTree($left = null, $no_lock = false)
        {
           $firstRecursion = (bool)!$left;
           if ($firstRecursion)
           {
           	   if (!$no_lock)
           	   		self::$Con->Lock("{$this->table_name} WRITE");
           	   self::$Con->Query("UPDATE {$this->table_name} SET lft='0', rgt='0'");
               $left = 1;
               $parent_id = 0;   
           }
           else
               $parent_id = $this->id;
           
           $children = (array)$this->find(array('where' => array('parent_id = ?', $parent_id)));
           foreach($children as $child)
           {
               $right = $left+1;
               $right = $child->rebuildTree($right, $no_lock);
               self::$Con->Query("UPDATE {$this->table_name} SET lft='{$left}', rgt='{$right}' WHERE id='{$child->id}'");
               $left = $right+1;
           }
           
           if ($firstRecursion && !$no_lock)
           		self::$Con->unlock();
           if ($right)
               $right++;
           else
               $right = $left;
           
           return $right;           
        }*/

	}
?>