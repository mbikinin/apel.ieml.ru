<?
	class MySQL_ActiveRecordConnector extends ActiveRecordConnector
	{
		function __construct($params)
		{
			if ($params['persistent'])
				$this->dbh = mysql_pconnect($params['host'], $params['user'], $params['pass']);
			else
				$this->dbh = mysql_connect($params['host'], $params['user'], $params['pass']);

			if (!is_resource($this->dbh))
				$this->raise('Connection failed');

			mysql_select_db($params['db']) or $this->raise();

			if ($params['charset'])
				$this->query("SET NAMES {$params['charset']}");
		}

		function escape($value)
		{
			// Stripslashes
			if (get_magic_quotes_gpc())
			{
				$value = stripslashes($value);
			}
			// Quote if not a number or a numeric string
			if (!is_numeric($value))
			{
				$value = mysql_real_escape_string($value, $this->dbh);
			}
			return $value;
		}

		function escapeFieldName($field_name)
		{
			return '`'.$field_name.'`';
		}

		function listTables()
		{
			$Ret = array();

			$res = mysql_list_tables($this->dbname, $this->dbh);
			for ($i = 0; $i < $this->numRows($res); $i++)
				$Ret[] = mysql_tablename($res, $i);

			return $Ret;
		}

		function close()
		{
			return @mysql_close($this->dbh);
		}

		function query($query, $unbuffered=false)
		{
			if ($this->debug || $this->log)
				$mt = microtime(1);

			if ($query instanceof ActiveRecordStatement)
			{
				$query = $query->buildQuery();
			}

			$query = $this->prepareQuery($query);

			if ($unbuffered)
				$res = @mysql_unbuffered_query($query, $this->dbh);
			else
				$res = @mysql_query($query, $this->dbh);

			$res or $this->raise($query);

			if ($this->debug)
				$this->debugQuery($query, microtime(1) - $mt);
			
			if ($this->log)
				$this->logQuery($query, microtime(1) - $mt);

			return $res;
		}

		function affectedRows()
		{
			return mysql_affected_rows($this->dbh);
		}

		function isTableExists($table)
		{
			$this->Query("SELECT * FROM $table LIMIT 1");
			return !$this->ErrNo();
		}

		function numRows($res)
		{
			return mysql_num_rows ($res);
		}

		function freeResult($res)
		{
			return mysql_free_result ($res);
		}

		function insertId()
		{
			return mysql_insert_id ($this->dbh);
		}

		function fetchRow($res)
		{
			return mysql_fetch_row ($res);
		}

		function fetchArray($res)
		{
			return mysql_fetch_array($res, MYSQL_ASSOC);
		}

		function fetchAssoc($res)
		{
			return $this->fetchArray($res);
		}

		function dataSeek($res, $row)
		{
			return mysql_data_seek($res, $row);
		}

		function lock($info)
		{
			return $this->Query("LOCK TABLES $info");
		}

		function unlock()
		{
			return $this->Query('UNLOCK TABLES');
		}

		function getAutoIncrement($table)
		{
			$data = $this->EzQuery(
				sprintf('SHOW TABLE STATUS LIKE "%s"', $table),
				array('assoc' => true)
			);

			return $data['Auto_increment'];
		}

		function getNextId($table)
		{
			return $this->getAutoIncrement($table);
		}

		function tableInfo($table)
		{
			$native_types = $db_types = $defaults = $comments = $lengths = $nullables = array();
			$primary_key = '';

			while ($_ = $this->AutoFetch("SHOW FULL COLUMNS FROM $table", $rs))
			{
				$defaults[$_['Field']] = $_['Default'];
				$char_lengths[$_['Field']] = 0;
				$comments[$_['Field']] = $_['Comment'];
				$nullables[$_['Field']] = $_['Null'] == 'YES';

				if ($_['Key'] == 'PRI')
					$primary_key = $_['Field'];

				if (preg_match('/^(tiny|small|medium|big)?int\(/', $_['Type']))
				{
					$db_types[$_['Field']] = $native_types[$_['Field']] = 'int';
				}
				elseif (preg_match('/^(?:var)?char\((\d+)\)/', $_['Type'], $m))
				{
					$db_types[$_['Field']] = $native_types[$_['Field']] = 'string';
					$char_lengths[$_['Field']] = (int)$m[1];
				}
				elseif (preg_match('/^(blob|text)$/', $_['Type']))
				{
					$db_types[$_['Field']] = 'lob';
					$native_types[$_['Field']] = 'string';
				}
				elseif (preg_match('/^((decimal|numeric)\(|float|double)$/', $_['Type']))
				{
					$db_types[$_['Field']] = $native_types[$_['Field']] = 'float';
				}
				elseif (preg_match('/^(datetime|date)$/', $_['Type']))
				{
					$db_types[$_['Field']] = 'date';
					$native_types[$_['Field']] = 'string';
				}
			}

			return compact('primary_key', 'native_types', 'db_types', 'defaults', 'comments', 'char_lengths', 'nullables');
		}

		function Limit($SQL, $rows, $offset = 0)
		{
			return $SQL." LIMIT $rows".($offset ? " OFFSET $offset" : '');
		}

		function upsert($update, $table_name, $fields, $where = '')
		{
			$set = $names = $values = $query_params = array();

			foreach ($fields as $name => $fvalue)
			{
				$esc_name = $this->escapeFieldName($name);
				$value = ":$name";
				$query_params[$name] = $fields[$name];

				if ($update)
					$set[] = "$esc_name = $value";
				else
				{
					$names[] = $esc_name;
					$values[] = $value;
				}
			}

			foreach (array('set', 'names', 'values') as $_)
				$$_ = implode(', ', $$_);

			if ($update)
			{
				$SQL = "UPDATE $table_name SET $set";
				if ($where)
				{
					$st = $this->getStatement();
					$st->whereAdd($where);
					$SQL .= " WHERE {$st->where}";
					$query_params = array_merge($query_params, $st->params);
				}
			}
			else
			{
				$SQL = "INSERT INTO $table_name ($names) VALUES($values)";
			}

			$this->query(array($SQL, $query_params));
		}

		function escapeWildcard($string)
		{
			return addcslashes($string, '%_');
		}

		function getLastError()
		{
			return mysql_error($this->dbh);
		}

		function isNull($value)
		{
			return is_null($value);
		}

		function createInFilter($field_name, $values)
		{
			$bvars = $bvalues = array();
			foreach ($values as $n => $v)
			{
				$key = "bvs$n";
				$bvalues[$key] = $v;
				$bvars[] = ":$key";
			}

			while (count($splice = array_splice($bvars, 0, 1000)) > 0)
			{
				$splices[] = "$field_name IN(".implode(",", $splice).")";
			}

			return array(
				'where'		=> implode(' OR ', $splices),
				'params'	=> $bvalues,
			);
		}

		function getFullTableName($table_name, $link_name)
		{
			$ret = $table_name;

			if ($link_name)
				$ret .= "@{$link_name}";

			return $ret;
		}
		
		public function ttsbegin()
		{
			//mysql_query("SET autocommit=0");
			mysql_query("BEGIN", $this->dbh);
		}

		public function ttscommit()
		{
			mysql_query("COMMIT", $this->dbh);
			//mysql_query("SET autocommit=1");
		}

		public function ttsabort()
		{
			mysql_query("ROLLBACK", $this->dbh);
			//mysql_query("SET autocommit=1");
		}
	}
?>