<?
	class MyArrayIterator extends ArrayIterator
	{
		function last()
		{
			return $this->offsetGet($this->count() - 1);
		}

		function first()
		{
			return $this->offsetGet(0);
		}

		function propFind($property_name, $property_value)
		{
			for ($this->rewind(); $this->valid(); $this->next())
				if ($this->current()->$property_name == $property_value)
					return true;

			return false;
		}

		function toArray($property_name = null)
		{
			$ret = array();

			for ($this->rewind(); $this->valid(); $this->next())
			{
				$elt = $this->current();

				if ($property_name)
					$elt = $elt->$property_name;

				$ret[] = $elt;
			}

			return $ret;
		}

		function appendArray($array)
		{
			foreach ($array as $elt)
				$this->append($elt);
		}

		function pop()
		{
			$elt = $this->last();
			$this->offsetUnset($this->count() - 1);
			return $elt;
		}

		function shift()
		{
			$elt = $this->first();
			$this->offsetUnset(0);
			return $elt;
		}
	}

	class MyArrayObject
	{
		private
			$data;

		function __construct($data = array())
		{
			$this->data = $data;
		}

		function __get($prop_name)
		{
			return $this->data[$prop_name];
		}

		function __set($prop_name, $value)
		{
			return $this->data[$prop_name] = $value;
		}
	}
?>