<?
	class FrameworkException extends Exception
	{
		protected $header = '';

		function __construct($message, $header, $code = 500)
		{
			parent::__construct($message, $code);
			$this->header = $header;
		}

		final function getHeader()
		{
			return $this->header;
		}

		function dispatch()
		{
    		$code = $this->getCode();
    		$message = $this->getMessage();
    		$header = $this->getHeader();
    		$trace = $this->getTraceAsString();

    		@header("HTTP/1.0 $code $header");
    		@header("Status: $code $header");

    		echo <<<HTML
<h1>$header</h1>
<h2>$message</h2>
<pre>$trace</pre>
HTML;
		}
	}

	class DispatcherError extends FrameworkException {}

	class ActiveRecordError extends FrameworkException {}

	class ActionControllerException extends FrameworkException {}

	class ActiveRecordConnectorException extends FrameworkException {}
?>