<?
	class Dispatcher
	{
		public static
			$Inflector;

	    function raise($message, $header, $code = '500')
	    {
    	    throw new DispatcherError($message, $header, $code);
    	}

    	function dispatchWithException(Exception $exception)
    	{
    		$exception->dispatch();
    	}

		protected function recognizeRoute($url)
		{
			$Router = new Router();
			require_once FWK_ROUTES;
			return $Router->findRoute($url);
		}

		protected static function controllerPathToPrefix($controller_path)
		{
			return strtr(ucwords(strtr($controller_path, '/', ' ')), ' ', '_');
		}

		static function controllerInfo($rt_controller)
		{
			$path = ($d = dirname($rt_controller)) != '.' ? "$d/" : '';
			$name = basename($rt_controller);
	    	$class_name = self::controllerPathToPrefix($path).self::$Inflector->camelize($name).'Controller';

	    	return compact('path', 'name', 'class_name');
		}

	    function dispatch($url)
	    {
			if (!$route = $this->recognizeRoute($url))
			{
				$this->raise("Cannot find $url", 'URL not found', 404);
			}

			$route = array_merge(array('action' => 'index'), $route);

			extract($route, EXTR_PREFIX_ALL, 'rt');

			extract(self::controllerInfo($rt_controller), EXTR_PREFIX_ALL, 'controller');
	    	if (is_file($controller_class_file = FWK_CONTROLLERS.$rt_controller.'_controller.inc'))
	    	{
	    		include_once $controller_class_file;
				$controller_object = $this->createController($controller_class_name, $controller_path, $controller_name);
	    	}
	    	else
	    	{
	    		$this->raise("Failed to instantiate controller object $controller_class_name ($controller_class_file)", 'Controller not found');
	    	}

			$params = _Array::trim(array_merge($_REQUEST, $route));
	    	$controller_object->renderAction($rt_action, $params);
	    }

    	protected function createController($controller_class_name, $controller_path, $controller_name)
    	{
    		return new $controller_class_name($controller_path, $controller_name);
    	}
	}
?>