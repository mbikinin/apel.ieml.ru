<?
	class MyWiki
	{
		protected function insTag($wiki_tag, $string)
		{
			$tags = array(
				'**'	=> 'b',
				'//'	=> 'i',
				'__'	=> 'u',
				'--'	=> 'strike',
			);

			if (array_key_exists($wiki_tag, $tags))
			{
				$tag = $tags[$wiki_tag];
				$string = "<$tag>$string</$tag>";
			}

			return $string;
		}

		protected function insLink($link)
		{
#debug::vardump($link, false);
			list ($url, $caption) = preg_split('/\s/', $link, 2);
#			var_dump($link);
			$caption or $caption = $url;
			return '<a href="'.$url.'">'.$caption.'</a>';
		}

		protected function insAnchor($name)
		{
			return '<a name="'.$name.'"></a>';
		}

		function process($string)
		{
			return preg_replace(
				array(
					'/\({2}(.*?)\){2}/e',
					'/\{{2}a(?:nchor)? +name=&quot;(.*?)&quot;\}{2}/e',
					'/([\*_\-]{2})(.*?)\1/e',
					'/(?<!http:)([\/]{2})(.*?)\1/e',
				),
				array(
					'$this->insLink(\'$1\')',
					'$this->insAnchor(\'$1\')',
					'$this->insTag(\'$1\', \'$2\')',
					'$this->insTag(\'$1\', \'$2\')',
				),
				$string
			);
		}

		static function applyMarkupTo(ActiveRecord $object, $wiki_fields)
		{
			$Wiki = new MyWiki();

			foreach ((array)$this->wiki_fields as $field_name)
				$object->$field_name = $Wiki->process(nl2br(h($object->$field_name)));
		}

		static function applyMarkup($value)
		{
			$Wiki = new MyWiki();
			return $Wiki->process(nl2br(h($value)));
		}
	}
?>