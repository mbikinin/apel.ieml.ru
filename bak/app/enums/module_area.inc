<?php

class ModuleArea extends Reference
{
	const top = 1;
    const left = 2;
    const right = 3;
    const bottom = 4;
    const center = 5;

    function __construct()
    {
        $this->list = array(
            self::top => 'Верхний',
            self::left => 'Левый',
            self::right => 'Правый',
            self::bottom => 'Нижний',
            self::center => 'Центральный'
        );
    }
}