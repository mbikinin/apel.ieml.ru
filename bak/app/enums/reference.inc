<?php

abstract class Reference
{
	public $list;

	protected function __construct()
	{
		$this->list = array();
	}

	/**
	* @return string
	*/
	public function asString($index)
	{
		if (!$this->isValid($index))
			return '';

		return $this->list[$index];
	}

	/**
	* @return bool
	*/
	public function isValid($index)
	{
		return array_key_exists($index, $this->list);
	}

	/**
	* @return Reference
	*/
	final public static function instance()
	{
		static $instance;

		if (!isset($instance))
		{
			$class_name = get_called_class();
			$instance = new $class_name();
		}

		return $instance;
	}
}