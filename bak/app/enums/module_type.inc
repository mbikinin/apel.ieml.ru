<?php

class ModuleType extends Reference
{
	const html = 1;
    const menu = 2;
    const auth = 3;
    const gallery = 4;
	const news = 5;
	const achievement = 6;
	const archive_years = 7;
	const proposal_1 = 8;
	const proposal_2 = 9;
	
	public 
		$alias,
		$class_name,
        $editable;

    function __construct()
    {
        $this->list = array(
            self::html => 'HTML блок',
            self::menu => 'Меню',
            self::auth => 'Форма авторизации',
            self::gallery => 'Галлерея',
			self::news => 'Новости',
			self::achievement => 'Достижения',
			self::archive_years => 'Архив, годы',
			self::proposal_1 => 'Заявка на публикацию',
			self::proposal_2 => 'Заявка на приобретение',
        );

        $this->alias = array(
            self::html => 'html',
            self::menu => 'menu',
            self::auth => 'auth',
            self::gallery => 'gallery',
			self::news => 'news',
			self::achievement => 'achievement',
			self::archive_years => 'archive_years',
			self::proposal_1 => 'proposal_1',
			self::proposal_2 => 'proposal_2'
        );
		
		$this->class_name = array(
			self::proposal_1 => 'proposal',
			self::proposal_2 => 'proposal'
        ) + $this->alias;
		
		$this->editable = array(
            self::html
        );
    }
	
	public function isEditable($type)
    {
        return in_array($type, $this->editable);
    }
}