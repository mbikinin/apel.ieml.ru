<?php

class Sex extends Reference
{
	const man = 1;
    const woman = 2;

    function __construct()
    {
        $this->list = array(
            self::man => 'Мужской',
            self::woman => 'Женский'
        );
    }
}