<?
	class PageController extends AppController
	{
		protected
			$page,
			$tclass;

		function __construct(Page $page)
		{
			$this->page = $page;
            $this->current_page = $page;
			$this->tclass = $this->page->type_class;
			$this->title = h($page->name);
            $this->page_title = ($page->show_name ? h($page->name) : '');

			if (func_num_args() > 1)
			{
				list (, $controller_path, $controller_name) = func_get_args();
				parent::__construct($controller_path, $controller_name);
			}
			else
			{
				parent::__construct();
			}
		}

		function getCrumb()
		{
			$crumb = array('<a href="/">Главная</a>');
            $parents = $this->page->getParentsHash();

            if ($parents[key($parents)]['lft'] == 2)
                unset($parents[key($parents)]);

            foreach ($parents as $id => $p)
            {
                $p = (object)$p;
                $crumb[] = $id == $this->page->id ? $p->name : $this->Hlp->linkTo($p->name, $p->url);
            }

			return implode(' \ ', $crumb);
		}
	}
?>