<?php

class AuthController extends AppController
{
	public function index($params)
	{
		$this->redirectTo(array('action' => 'login'));
	}

	public function login($params)
	{
		$failure = false;
	
		if ($this->user_group instanceof User)
		{
			$this->redirectTo(array('controller' => '/personal', 'action' => 'index'));
		}
	
		if (getenv('REQUEST_METHOD') == 'POST')
		{
			$user = Logon::login($params['login'], $params['password'], isset($params['ipad']));
			$failure = is_object($user) ? false : true;
			
			if (!$failure)
			{
				if (isset($params['backurl']))
				{
					$this->redirectTo($params['backurl']);
				}
				else
				{
			        $this->redirectTo(array('controller' => '/personal', 'action' => 'index'));
				}
			}
		}

		$Form = $this->createFormHelper();
		return compact('Form', 'failure');
	}

    public function logout($params)
	{
		Logon::logout();
		$this->redirectTo(array('action' => 'login'));
	}

    public function registration($params)
    {
        $this->page_title = 'Регистрация';
        $Form = $this->createFormHelper(new UserProfile());

        return compact('Form');
    }
}