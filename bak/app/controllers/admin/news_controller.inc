<?php
	class Admin_NewsController extends AdminController
	{
		public $News;

		protected function onLoad()
		{
			parent::onLoad();

            $this->News = new News();
		}

        public function index($params)
        {
            $this->page_title = 'Новости';
            $news = $this->News->findAll();

			return compact('news');
        }

        public function edit($params)
		{
            if ($params['id'] && !($news = $this->News->find($params['id'])))
            {
                $this->errorBack('Запись не найдена');
            }
            elseif (!$params['id'])
            {
                $news = $this->News->create();
            }
			
			$news->lang = $this->lang;

            $this->page_title = 'Редактирование элемента';

            $Form = $this->createFormHelper($news);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

				if ($news->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}
			elseif (!$params['id'])
			{
				$Form->object->date = date('Y-m-d');
			}

			return compact('Form');
		}
		
		public function drop($params)
		{
			if (!$params['id'] || !($News = $this->News->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}
			
			$News->drop();
			$this->noticeBack('Запись удалена');
		}
	}
?>