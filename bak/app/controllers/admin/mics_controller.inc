<?
	class Admin_MicsController extends AdminController
	{
		public $Mic;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Mic = new Mic();
		}

        public function edit($params)
		{
			$this->page_title = 'Элементы сайта';
            $mic = $this->Mic->findFirst();           

            $Form = $this->createFormHelper($mic);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();
				
				if ($mic->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}

			return compact('Form');
		}
	}
?>