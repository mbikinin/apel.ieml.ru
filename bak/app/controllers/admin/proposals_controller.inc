<?php
	class Admin_ProposalsController extends AdminController
	{
		public $Proposal;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Proposal = new Proposal();
		}

        public function index($params)
        {
            $this->page_title = ProposalType::instance()->list[$params['filter']['type']];
			
			if ($params['filter'])
				$this->Proposal->setFilter($params['filter']);
			
			if ($params['order_by'])
				$this->Proposal->setOrderBy($params['order_by']);
			
            list($pages, $proposals) = Paginator::paginate($params, $this->Proposal, array(), array('per_page' => 25));
			$Form = $this->createFormHelper();
			
			return compact('Form', 'proposals');
        }

        public function view($params)
		{
            if (!$params['id'] || !($proposal = $this->Proposal->find($params['id'])))
                $this->errorBack('Запись не найдена');

            $this->page_title = 'Просмотр заявки';

			return compact('proposal');
		}
		
		public function drop($params)
		{
			if (!$params['id'] || !($proposal = $this->Proposal->find($params['id'])))
				$this->errorBack('Запись не найдена');
			
			$proposal->drop();
			$this->noticeBack('Запись удалена');
		}
	}
?>