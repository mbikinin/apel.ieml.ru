<?
	class Admin_ModulesPagesController extends Admin_TreeController
	{
		protected
			$Page,
            $Module;

		protected function onLoad()
		{
			parent::onLoad('Page');

            $this->Module = new Module();
		}

        function loadData($params)
		{
			if (!is_object($this->TreeObj))
				return;

			$this->capture_current_url = 0;
			$parent_id = (int)$params['id'];
            $module = $this->Module->find($params['module_id']);
			$data = $this->getChildrenDataForJsTree($parent_id, $module);
			$this->ajaxResponse("var node_id = $parent_id, d = ".Js::toArray($data, 0));
		}

        protected function getChildrenDataForJsTree($parent_id, $module)
		{
			$params = array(
				'table_alias'	=> 't',
				'select'		=> <<<SQL
t.*, (
	SELECT COUNT(*)
	FROM {$this->TreeObj->table_name}
	WHERE parent_id = t.id
) has_children
SQL
				,
				'cond'	=> array(
					'parent_id'	=> $parent_id,
				),
			);

			if (!$this->user->admin)
			{
				if ($parent_id == 0)
				{
					$params = array_merge($params, array(
						'cond'	=> null,
						'where'	=> <<<SQL
id IN(
	SELECT table_id
	FROM tree_admins
	WHERE user_id = :user_id AND table_name = :table_name
)
SQL
						,
						'params'=> array(
							'user_id' 		=> 1/*$this->user->id*/,
							'table_name'	=> $this->TreeObj->table_name,
						),
					));
				}
			}

			$data = array();

			foreach ($this->TreeObj->find($params) as $c)
			{
				$attrs = array_merge($this->getNodeAttributes($c), array(
					'onscreen'	    => 0,
					'tools'		    => $this->getJsTreeTools($c),
					'show_module'   => in_array($c->id, (array)$module->page_ids) ? 1 : 0,
				));

				$data[] = "new JsTreeNode(".Js::toHash($attrs).")";
			}

			return $data;
		}
	}
?>