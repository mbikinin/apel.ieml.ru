<?
	class Admin_GalleriesController extends AdminController
	{
		public $Gallery;
        public $Photo;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Gallery = new Gallery();
            $this->Photo = new GalleryPhoto();
		}

        public function index($params)
        {
            $this->page_title = 'Фотогаллереи';

            $galleries = $this->Gallery->findAll();
            $Form = $this->createFormHelper();

			return compact('Form', 'galleries');
        }

        public function edit($params)
		{
            if ($params['id'] && !($gallery = $this->Gallery->find($params['id'])))
            {
                $this->errorBack('Запись не найдена');
            }
            elseif (!$params['id'])
            {
                $gallery = $this->Gallery->create();
            }

            $this->page_title = 'Редактирование элемента';

            $Form = $this->createFormHelper($gallery);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

				if ($gallery->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}

			return compact('Form');
		}

        public function photos($params)
        {
            if (!$params['id'] || !($gallery = $this->Gallery->find($params['id'])))
            {
                $this->errorBack('Галлерея не найдена');
            }

            $this->page_title = 'Фотографии';

            return compact('gallery');
        }

        public function editPhoto($params)
        {
            if ($params['id'] && !($photo = $this->Photo->find($params['id'])))
            {
                $this->errorBack('Фотография не найдена');
            }
            elseif (!$params['id'])
            {
                $photo = $this->Photo->create();
                $photo->gallery_id = $params['gallery_id'];
            }

            $this->page_title = 'Редактирование';
            $Form = $this->createFormHelper($photo);

            if (getenv('REQUEST_METHOD') == 'POST')
            {
                if ($photo->saveFromPost())
                {
                    $this->noticeBack('Изменения внесены');
                }
            }

            return compact('Form', 'gallery');
        }
	}
?>