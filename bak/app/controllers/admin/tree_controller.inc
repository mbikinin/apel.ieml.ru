<?
	abstract class Admin_TreeController extends AdminController
	{
		protected
			$TreeObj,
			$helper_class_name;

		protected function onLoad($treeobj_class_name = '')
		{
			parent::onLoad();

			$class_name = get_class($this);
			$strip_class_name = preg_replace('/^Admin_(.+)Controller$/', '$1', $class_name);

			if (!$this->helper_class_name)
				$this->helper_class_name = "JsTree_{$strip_class_name}Helper";

			if ($treeobj_class_name)
			{
				$this->TreeObj = $this->$treeobj_class_name = new $treeobj_class_name();
			}
		}

		function beforeFilter()
		{
			parent::beforeFilter();
			$this->truncate();

            /*****/

            if ($this->action == 'index')
            {
                unset(Helper::$sources['script'][Helper::$js_base . '/jquery.js']);
                $this->Hlp->needJs('prototype.js');
            }
		}

		protected function truncate()
		{return;
			if ($this->user->admin)
				return;

			$TreeAdmin = new TreeAdmin();
			$root_ids = $TreeAdmin->getTreeIds($this->TreeObj, $this->user);
			if (!count($root_ids))
				$this->errorTo('back', 'Вы не назначены администратором какой-либо вершины');

			$this->TreeObj->setRoot($root_ids);
		}

		function index($params)
		{
			$js_tree_helper = new $this->helper_class_name($this->TreeObj);
			$js_tree_helper->expand_nodes = $this->getOpenNodes();

			return compact('js_tree_helper');
		}

		function edit($params)
		{
			if (!$C = $this->findOrCreateByParent($params))
				$this->errorBack('Неизвестная категория');

			if (!$this->editable($C))
				$this->errorBack('Невозможно редактировать категорию');

			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if ($C->saveFromPost())
				{
					$this->goBack();
				}
			}

			$Frm = $this->createFormHelper($C);
			return compact('Frm');
		}

		protected function getNsForJsTree()
		{
			return str_replace('/', '_', $this->controller_fullname).'_open_nodes';
		}

		function updateTree($params)
		{
			$this->capture_current_url = 0;
			$ns = $this->getNsForJsTree();

			if ($params['show'])
				self::$Session->set($ns, $params['id'], 1);
			else
			{
				if (method_exists($this, 'getNodesToRemove'))
					$nodes = (array)$this->getNodesToRemove($params);
				else
					$nodes = array($params['id']);

				foreach ($nodes as $node)
				{
					self::$Session->wipe($ns, $node);
				}
			}

			$this->render_text = false;
		}

		protected function getOpenNodes()
		{
			if (is_array($nodes = self::$Session->getNs($this->getNsForJsTree())))
			{
				$open_nodes = array_keys($nodes);
			}
			elseif ($this->TreeObj)
			{
				$open_nodes = (array)$this->TreeObj->getRootId();
			}
			else
			{
				$open_nodes = array();
			}

			return $open_nodes;
		}

		protected function getNodesToRemove($params)
		{
			if (!is_object($this->TreeObj))
				return;
/*
			$params = array(
				'start_with'=> $params['id'],
				'as_tree'	=> 1,
				'select'	=> 'id',
			);*/
			
			return array_merge(array((int)$params['id']), $this->TreeObj->find($params['id'])->direct_children->toArray('id'));
			// return $this->TreeObj->find($params)->toArray('id');
		}

		function loadData($params)
		{
			if (!is_object($this->TreeObj))
				return;

			$this->capture_current_url = 0;
			$parent_id = (int)$params['id'];
			$data = $this->getChildrenDataForJsTree($parent_id);
			$this->ajaxResponse("var node_id = $parent_id, d = ".Js::toArray($data, 0));
		}
/*
        function loadCheckData($params)
		{
			if (!is_object($this->TreeObj))
				return;

			$this->capture_current_url = 0;
			$parent_id = (int)$params['id'];
			$data = $this->getChildrenDataForJsTree($parent_id, $params['module_id']);
			$this->ajaxResponse("var node_id = $parent_id, d = ".Js::toArray($data, 0));
		}

        protected function getCheckChildrenDataForJsTree($parent_id)
        {
            ActiveRecord::factory('Modules_Page')
        }*/

		protected function getChildrenDataForJsTree($parent_id)
		{
			$params = array(
				'table_alias'	=> 't',
				'select'		=> <<<SQL
t.*, (
	SELECT COUNT(*)
	FROM {$this->TreeObj->table_name}
	WHERE parent_id = t.id
) has_children
SQL
				,
				'cond'	=> array(
					'parent_id'	=> $parent_id,
				),
			);

			if (!$this->user->admin)
			{
				if ($parent_id == 0)
				{
					$params = array_merge($params, array(
						'cond'	=> null,
						'where'	=> <<<SQL
id IN(
	SELECT table_id
	FROM tree_admins
	WHERE user_id = :user_id AND table_name = :table_name
)
SQL
						,
						'params'=> array(
							'user_id' 		=> 1/*$this->user->id*/,
							'table_name'	=> $this->TreeObj->table_name,
						),
					));
				}
			}

			$data = array();

			foreach ($this->TreeObj->find($params) as $c)
			{
				$attrs = array_merge($this->getNodeAttributes($c), array(
					'onscreen'	=> 0,
					'tools'		=> $this->getJsTreeTools($c),
//					'admins'	=> array_flip($c->getAdminIds()),
				));

				$data[] = "new JsTreeNode(".Js::toHash($attrs).")";
			}

			return $data;
		}

		protected function getNodeAttributes($c)
		{
			return Hash::leaveKey($c->getAttributes(), 'id', 'parent_id', 'name', 'has_children');
		}

		protected function getJsTreeTools($c)
		{
			$tools = array();

			if ($this->addable($c))
				$tools[1] = 1;

			if ($this->orderable($c))
				$tools[2] = 1;

			if ($this->editable($c))
				$tools[3] = 1;

			if ($this->droppable($c))
				$tools[4] = 1;

			if ($this->adminEditable($c))
				$tools[5] = 1;

			return $tools;
		}

		protected function findOrCreateByParent($params)
		{
			if (!$T = $this->TreeObj->findOrCreate($params['id']))
				return null;

			if ($T->isNewRecord())
			{
				if (!$Parent = $this->TreeObj->find($params['parent_id']))
					return null;

				$T->parent_id = $Parent->id;
			/*	$T->ordi = 1 + (int)$this->TreeObj->max('ordi', array(
					'cond' => array(
						'parent_id'	=> $Parent->id,
					),
				));*/
			}

			return $T;
		}

		protected function editable($c)
		{
			return $c->isNewRecord() || $c->parent_id > 0;
		}

		protected function addable($c)
		{
			return 1;
		}

		protected function orderable($c)
		{
			return 1;
		}

		protected function droppable($c)
		{
			return 1;//$this->editable($c) && $c->isEmpty() && !$c->has_children;
		}

		protected function adminEditable($c)
		{
//			return $this->user->admin;
		}

		function drop($params)
		{
			if (!$c = $this->TreeObj->find($params['id']))
				$this->errorBack("Объект #{$params['id']} не найден");

			if (!$this->droppable($c))
				$this->errorBack('Удаление невозможно');

			$c->drop();

			$this->goBack();
		}

		function order($params)
		{
			if (!$c = $this->TreeObj->find($params['id']))
				$this->errorBack("Объект #{$params['id']} не найден");

			if (!$this->orderable($c))
				$this->errorBack('Упорядочение невозможно');

			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
                $lftrgt = array();

                foreach ($c->getDirectChildren() as $ch)
                    $lftrgt[] = array('lft' => $ch->lft, 'rgt' => $ch->rgt);

				ActiveRecord::$Con->ttsbegin();

				foreach ((array)$params['ordi'] as $key => $c_id)
				{
                    $this->TreeObj->find($c_id)->save($lftrgt[$key]);
				}

				ActiveRecord::$Con->ttscommit();
				$this->goBack();
			}

			$children_data = array();
			foreach ($c->getDirectChildren() as $ch)
			{
				$children_data[] = Hash::leaveKey($ch->getAttributes(), 'id', 'name');
			}

			$this->controller_fullname = 'inner/tree';
 
			$Frm = $this->createFormHelper($c);
			return compact('Frm', 'children_data');
		}

		protected function validateAdminParams($params)
		{
			if (!$node = $this->TreeObj->find($params['id']))
				$this->raise('Неизвестная вершина');

			if (!$this->adminEditable($node))
				$this->raise('Невозможно назначение администратора для данной вершины');

			if (!$user = User::factory('User')->find($params['user_id']))
				$this->raise('Неизвестный пользователь');

			$TreeAdmin = new TreeAdmin();

			return compact('node', 'user', 'TreeAdmin');
		}

		function addAdmin($params)
		{
			extract($this->validateAdminParams($params));

			if ($TreeAdmin->userExists($node, $user))
				$this->raise('Пользователь уже назначен');

			$TreeAdmin->addUser($node, $user);
			$this->calculateUserPermissions(1, $node, $user);

			$this->ajaxResponse();
		}

		function removeAdmin($params)
		{
			extract($this->validateAdminParams($params));

			if (!$TreeAdmin->userExists($node, $user))
				$this->raise('Пользователь не назначен');

			$TreeAdmin->removeUser($node, $user);
			$this->calculateUserPermissions($TreeAdmin->hasPermissions($node, $user), $node, $user);

			$this->ajaxResponse();
		}

		protected function calculateUserPermissions($has_permission, $node, $user)
		{
			$cond = array(
				'user_id'		=> $user->id,
				'permission_id'	=> $this->permission,
			);

			$find_params = array(
				'cond'	=> $cond,
			);

			$UsersPermission = new UsersPermission();
			$has_user_perm = $UsersPermission->count($find_params);

			if ($has_permission)
			{
				if (!$has_user_perm)
				{
					$up = $UsersPermission->create($cond);
					$up->saveWithoutValidation();
				}
			}
			else
			{
				if ($has_user_perm)
				{
					$UsersPermission->dropAll($find_params);
				}
			}
		}
	}
?>