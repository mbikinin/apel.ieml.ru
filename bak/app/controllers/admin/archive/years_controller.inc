<?php
	class Admin_Archive_YearsController extends AdminController
	{
		public $Year;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Year = new Archive_Year();
		}

        public function index($params)
        {
            $this->page_title = 'Годы';
            $years = $this->Year->findAll();

			return compact('years');
        }

        public function edit($params)
		{
            if ($params['id'] && !($year = $this->Year->find($params['id'])))
            {
                $this->errorBack('Запись не найдена');
            }
            elseif (!$params['id'])
            {
                $year = $this->Year->create();
            }

            $this->page_title = 'Редактирование элемента';

            $Form = $this->createFormHelper($year);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

				if ($year->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}

			return compact('Form');
		}
		
		public function drop($params)
		{
			if (!$params['id'] || !($year = $this->Year->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}
			
			$year->drop();
			$this->noticeBack('Запись удалена');
		}
	}
?>