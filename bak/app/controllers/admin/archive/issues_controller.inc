<?php
	class Admin_Archive_IssuesController extends AdminController
	{
		public $Issue;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Issue = new Archive_Issue();
		}

        public function index($params)
        {
            $this->page_title = 'Номера';
            $list = $this->Issue->findAll();

			return compact('list');
        }

        public function edit($params)
		{
            if ($params['id'] && !($issue = $this->Issue->find($params['id'])))
            {
                $this->errorBack('Запись не найдена');
            }
            elseif (!$params['id'])
            {
                $issue = $this->Issue->create();
            }

            $this->page_title = 'Редактирование элемента';

            $Form = $this->createFormHelper($issue);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

				if ($issue->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}

			return compact('Form');
		}
		
		public function drop($params)
		{
			if (!$params['id'] || !($issue = $this->Issue->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}
			
			$issue->drop();
			$this->noticeBack('Запись удалена');
		}
		
		public function parse($params)
		{
			if (!$params['id'] || !($issue = $this->Issue->find($params['id'])))
				$this->errorBack('Номер не найден');
		
			if (getenv('REQUEST_METHOD') == 'POST')
			{
				$file = $issue->cfg['base_path'] . $issue->file;
				$stream = fopen($file, 'r');
				
				$xml = simplexml_load_file($file);
				$articles = array();
				$titles = array();
				$keywords = array();
				$literatures = array();
				$authors = array();
				$sections = array();
				$Archive_Author = new Archive_Author();
				$Archive_Article = new Archive_Article();
				$Archive_Keyword = new Archive_Keyword();
				$Archive_Section = new Archive_Section();
				
				$i = 0;
				
				foreach($xml->journal->issue->jseparate as $title)
				{			
					$titles[++$i] = trim(preg_replace("~\s+~", " ", (string)$title->segtitle));
				}
				
				$i = 0;
				
				self::$Con->ttsbegin();
				
				foreach($titles as $t)
				{
					if (!($section = $Archive_Section->findByName($t)))
					{
						$section = $Archive_Section->create(array('name' => $t));
						$section->save();
					}				
					
					$sections[++$i] = $section->id;
				}
				
				$i = 0;		
				
				foreach($xml->journal->issue->article as $a)
				{	
					$i++;
					$article = $Archive_Article->create();			

					$article->title = trim(preg_replace("~\s+~", " ", (string)$a->arttitles->arttitle));
					$article->abstract = trim(preg_replace("~\s+~", " ", (string)$a->abstracts->abstract->i));
					$article->udk = trim(preg_replace("~\s+~", " ", (string)$a->udk));
					$article->archive_section_id = $sections[$i];
					$article->archive_issue_id = $issue->id;
					$articles[$i] = $article;
					
					$article->save();
					
					foreach($a->authors->author as $au)
					{
						$author = $Archive_Author->create();
						$author->fio = trim(preg_replace("~\s+~", " ", (string)$au->individInfo->surname . ' ' . (string)$au->individInfo->fname));
						$author->work = trim(preg_replace("~\s+~", " ", (string)$au->individInfo->auwork));
						$author->post = trim(preg_replace("~\s+~", " ", (string)$au->individInfo->auinf));
						
						$authors[$i][] = $author;
						$author->save(array('archive_article_id' => $article->id));
					}
					
					foreach($a->keywords->kwdGroup->keyword as $k)
					{
						$keyword = $Archive_Keyword->create();
						$keyword->string = trim(preg_replace("~\s+~", " ", (string)$k));
						
						$keywords[$i][] = $keyword;
						$keyword->save(array('archive_article_id' => $article->id));
					}
					
					if (!isset($a->nobiblist))
					{
						foreach($a->biblist->blistpart as $l)
						{
							$literature = new Archive_Literature();
							$literature->name = trim(preg_replace("~\s+~", " ", (string)$l));
							
							$literatures[$i][] = $literature;
							$literature->save(array('archive_article_id' => $article->id));
						}
					}					
				}
				// die;
				$issue->saveWithoutValidation(array('parsed' => 1));
				
				self::$Con->ttscommit();			
				
				$this->noticeBack('Файл обработан');
			}
			
			return array('Form' => $this->createFormHelper($issue));
		}
	}
?>