<?php

class ArchiveController extends AppController
{
    public
		$Year,
		$year;

    protected function onLoad()
    {
        parent::onLoad();

		$this->current_page = ActiveRecord::factory('Page')->findByUrl('/archive');
        $this->Year = new Archive_Year();
    }
	
	public function index($params)
	{
		$this->redirectTo(array('action' => 'years', 'lang' => $this->lang));
	}
	
	public function years($params)
	{	
		if (!($year = $this->Year->findByName($params['id'])))
			$year = $this->Year->findFirst();
		
		$this->page_title = "Архив номеров за {$year->name} год";
		$this->year = $year;
		
		if (!$params['issue_id'])
			$this->params['issue_id'] = reset($year->issues)->id;
		
		return compact('year');
	}
	
	public function article($params)
	{
		if (!$params['id'] || !($article = ActiveRecord::factory('Archive_Article')->find($params['id'])))
			$this->errorBack('Запись не найдена');
			
		$this->page_title = "Архив - номер {$article->issue->title} за {$article->issue->year->name} год";
			
		return compact('article');
	}
}

?>