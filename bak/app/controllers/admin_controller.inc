<?php

class AdminController extends AppController
{
    public $layout = '/admin';


    protected function onLoad()
    {
        parent::onLoad();

        $this->Hlp->needCss('admin/common.css');
    }

    public function beforeFilter()
    {
        parent::beforeFilter();

        if (!$this->user || !$this->user->isAdmin())
        {
            $backurl = $this->Hlp->urlFor();
			$this->redirectTo(array('controller' => '/auth', 'action' => 'login', 'backurl' => $backurl));
        }
    }

    public function index()
    {
        $this->redirectTo(array('controller' => '/admin/page'));
    }
}
