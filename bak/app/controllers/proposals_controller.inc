<?php

class ProposalsController extends AppController
{
    public $Proposal;

    protected function onLoad()
    {
        parent::onLoad();

        $this->Proposal = new Proposal();
    }
	
	public function send($params)
	{
		if (getenv('REQUEST_METHOD') == 'POST')
		{
			$proposal = $this->Proposal->create($params);
		
			if ($proposal->saveFromPost())
			{
				self::$Session->set('proposals', 'notice', 'Заявка успешно отправлена');
				$this->goBack();
			}
			else
			{
				self::$Session->set('proposals', 'error', $proposal->getAllFieldErrorsAsString());
				self::$Session->set('proposals', 'attrs', $proposal->getAttributes());
				$this->goBack();
			}
		}
	}
}

?>