<?php

class UserPermission extends ActiveRecord
{
    protected function initRelations()
    {
        $this->belongsTo('user');
    }
}