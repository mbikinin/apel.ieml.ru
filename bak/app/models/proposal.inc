<?php

class Proposal extends ActiveRecord
{
	protected $order_by = 'cdate desc';

	function __construct()
	{
		parent::__construct();
		
		$this->hasFileField('file');
		// $this->hasDateField('cdate', array('format' => 'd.m.Y'));
	}
	
	protected function beforeSave()
	{
		if ($this->isNewRecord())
			$this->cdate = date('Y-m-d H:i:s');
	}
	
	public function getFileUrl()
	{
		return $this->cfg['url_path'] . $this->file;
	}
	
	public function validate()
	{
		if ($this->type == ProposalType::publication)
		{
			foreach($this->fields as $field_value)
			{
				if (!$field_value && $field_name != 'file' && $field_name != 'cdate')
				{
					$this->addError('fio', 'Все поля обязательны для заполнения');
					break;
				}
			}
		}
		elseif($this->type == ProposalType::subscription)
		{
			foreach($this->fields as $field_name => $field_value)
			{
				if (!$field_value && $field_name != 'address' && $field_name != 'issues' && $field_name != 'file' && $field_name != 'cdate')
				{
					$this->addError('fio', 'Все поля обязательны для заполнения');
					break;
				}
			}
		}
	}
}