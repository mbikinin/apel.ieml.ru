<?php

class Archive_Literature extends ActiveRecord_Translation
{
	protected $order_by = 'name desc';
	
	public $lang_fields = array(
		'name'
	);
}