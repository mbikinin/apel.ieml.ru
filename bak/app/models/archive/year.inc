<?php

class Archive_Year extends ActiveRecord_Translation
{
	protected $order_by = 'name desc';
	
	protected function initRelations()
	{
		$this->hasMany('issues', array('foreign_key' => 'archive_year_id', 'class_name' => 'Archive_Issue'));
	}
}