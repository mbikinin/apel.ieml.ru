<?php

class Archive_Article extends ActiveRecord_Translation
{
	public $lang_fields = array(
		'title',
		'udk',
		'abstract'
	);

	protected $order_by = 'archive_issue_id asc';
	
	protected function initRelations()
	{
		$this->belongsTo('issue', array('foreign_key' => 'archive_issue_id', 'class_name' => 'Archive_Issue'));
		$this->belongsTo('section', array('foreign_key' => 'archive_section_id', 'class_name' => 'Archive_Section'));
		$this->hasMany('authors', array('foreign_key' => 'archive_article_id', 'class_name' => 'Archive_Author'));
		$this->hasMany('keywords', array('foreign_key' => 'archive_article_id', 'class_name' => 'Archive_Keyword'));
		$this->hasMany('literatures', array('foreign_key' => 'archive_article_id', 'class_name' => 'Archive_Literature'));
	}
	
	public function getAuthorsHash()
	{
		return self::factory('Archive_Author')->getIdFioHash(array('where' => 'archive_article_id = ' . $this->id));
	}
	
	public function getKeywordsHash()
	{
		return self::factory('Archive_Keyword')->getIdStringHash(array('where' => 'archive_article_id = ' . $this->id));
	}
	
	protected function beforeDrop()
	{
		foreach($this->authors as $a)
			$a->drop();
			
		foreach($this->keywords as $k)
			$k->drop();
			
		foreach($this->literatures as $l)
			$l->drop();
	}
}