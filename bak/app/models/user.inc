<?php

class User extends ActiveRecord
{
	public  $newpassword,
			$newpassword_repeat,
			$password_present,
			$newlogin;
	
	const
		name_max_length = 250,

		login_max_length = 16,
		login_min_length = 3,

		password_max_length = 20,
		password_min_length = 5;
	
	private $password_presented = false;

	protected function validatePass()
	{	
		if ($this->password == self::hashed($this->password_present))
		{			
			if (isset($this->newpassword_repeat))
			{
				if (strcmp($this->newpassword, $this->newpassword_repeat) !== 0)
				{
					$this->addError('newpassword', 'Пароли не совпадают');
				}
				else if (self::password_max_length < strlen($this->newpassword) || strlen($this->newpassword) < self::password_min_length)
				{
					$this->addError('newpassword',
						'Длина пароля должна быть от ' . self::password_min_length . ' до ' . self::password_max_length . ' символов');
				}
				else if (!preg_match('/^[a-z|_|0-9]+$/i', $this->newpassword))
				{
					$this->addError('newpassword',
						'Пароль должен состоять из английских букв и цифр и знака подчеркивания');
				}else
				{
					$this->password = $this->newpassword;
					$this->password = self::hashed($this->password);
				}
			}			
		}
        else
        {
			$this->addError('password_present',
					'Старый пароль введен неправильно');				
		}
	}

	protected function beforeSave()
	{
		if ($this->password !== NULL)
		{		
		//	$this->password = self::hashed($this->password);
		}
	}

	protected function afterFetch(& $fields)
	{
	//	unset($fields['password']);
	}

	static public function hashed($string)
	{
		return sha1($string);
	}

	static public function getUser($login, $password = NULL)
	{
		$where = array('login = :login');
		$params = array('login' => $login);

		if (!is_null($password))
		{
			$where[] = 'password = :password';
			$params['password'] = self::hashed($password);
		}

		$find_params = array(
			'where' => join(' AND ', $where),
			'params' => $params
		);

		return self::factory('User')->findFirst($find_params);
	}

    public function isAdmin()
    {
        return $this->hasPermission(Permission::admin);
    }

    public function isUser()
    {
        return $this->hasPermission(Permission::user);
    }

    public function hasPermission($permission)
    {
        return isset($this->permissions[$permission]);
    }

    public function getPermissions()
    {
        $find_params = array(
            'select' => 'up.permission_id as pi, up.*',
            'table_alias' => 'up',
            'where' => 'up.user_id = :user_id',
            'params' => array(
                'user_id' => $this->id
            ),
            'hash_level' => 1,
            'fetch_as' => ActiveRecord::FETCH_AS_HASH_OBJECT,
            'simplify' => true,
        );

        return self::factory('UserPermission')->findAll($find_params);
    }
}