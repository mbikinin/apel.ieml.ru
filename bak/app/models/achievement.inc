<?php

class Achievement extends ActiveRecord_Translation
{
	public $lang_fields = array(
		'text'
	);

	function __construct()
	{
		parent::__construct();
		
		$this->hasFileField('picture');
	}
	
	public function getUrl()
	{
		return $this->cfg['url_path'] . $this->picture;
	}
}