<?php

class Module extends ActiveRecord
{
	const show_in_all_pages = 1;
	
    public 
		$_item,
		$show_in_all_pages;
    protected $order_by = 'ordi';
    protected $cache;

    protected function initRelations()
    {
//        $this->hasAndBelongsToMany('page');
    }
	
	protected function afterFetch(&$fields)
	{
		
	}
	
	public function showInAllPages()
	{
		if (is_null($this->show_in_all_pages))
			$this->show_in_all_pages = (bool)self::factory('ModulesPage')->findFirst(array('where' => "page_id IS NULL AND module_id = " . (int)$this->id));
		
		return $this->show_in_all_pages;
	}

    protected function beforeSave()
    {
        if ($this->type == ModuleType::html)
        {
            if ($this->isNewRecord())
            {
                $item = self::factory('HtmlModule')->create();
                $item->save($this->_item);
                $this->item_id = $item->id;                
            }
            else
                $this->item->save($this->_item);
        }
    }

    public function getItem()
    {
        if ($this->type == ModuleType::html)
            return self::factory('HtmlModule')->find($this->item_id);
        else
            return self::factory(ucfirst(ModuleType::instance()->class_name[$this->type]))->find($this->item_id);
    }

    public function getContent()
    {
        return $this->item->content;
    }

    public function savePages($pages)
    {
        $this->dropPages();

		if (self::show_in_all_pages)
		{
			self::factory('ModulesPage')->create()->save(array(
				'module_id' => $this->id,
				// 'page_id' => null
			));
		}
		else
		{
			foreach ((array)$pages as $page_id => $bool)
				self::factory('ModulesPage')->create()->save(array(
					'module_id' => $this->id,
					'page_id' => $page_id
				));
		}       
    }

    public function dropPages()
    {
        self::factory('ModulesPage')->dropAll(array(
            'where' => 'module_id = ' . $this->id
        ));
    }
    // на каких страницах будет отображаться модуль
    public function getPageIds()
    {
        if (!isset($this->cache['page_ids']))
        {
            if (!self::factory('ModulesPage')->findByModule_idAndPage_id($this->id, null))
                return self::factory('ModulesPage')->findAllByModule_id($this->id)->toArray('page_id');
            else
                return self::factory('Page')->findAll()->toArray('id');
        }

        return $this->cache['page_ids'];
    }
}