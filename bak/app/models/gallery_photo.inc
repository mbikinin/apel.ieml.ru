<?php

class GalleryPhoto extends ActiveRecord
{
    static public $upload_options = array(
		'extensions' => array(
			'jpg',
			'png',
			'jpeg',
		)
	);

    function __construct()
    {
        parent::__construct();

        $this->hasFileField('url', array('antivirus' => false));
    }

    protected function initRelations()
    {
        $this->belongsTo('gallery');
    }

    public function getUrl()
    {
        return $this->cfg['base_url'] . $this->url;
    }
}