<?php

class Page extends MySQLTreeActiveRecord
{
    public $item_attrs;
    public $_attributes;
    public $_parents_hash;

    public static $item_classes = array(
        PageType::html => 'HtmlPage'
    );
	
	public $lang_fields = array(
		'name'
	);

    protected function initRelations()
    {
        $this->hasAndBelongsToMany('module');
    }

    protected function beforeSave()
    {
        if (self::$item_classes[$this->type])
        {
            if (!$this->item_id || $this->prev_attributes['type'] != $this->type)
            {
                $item = self::factory(self::$item_classes[$this->type])->create($this->item_attrs);
                $item->save();
                $this->item_id = $item->id;
            }
            else
                $this->item->save($this->item_attrs);
        }

        if (is_array($this->_attributes))
            $this->attributes = serialize($this->_attributes);
    }

    public function getAttributesArr()
    {
        if (!is_array($this->_attributes))
            $this->_attributes = unserialize($this->attributes);

        is_array($this->_attributes) or $this->_attributes = array();
        
        return $this->_attributes;
    }

    public function getItem()
    {
        return isset(self::$item_classes[$this->type]) ? self::factory(self::$item_classes[$this->type])->find($this->item_id) : null;
    }

    public function getTypeS()
    {
        return PageType::instance()->list[$this->type];
    }

    public function getTypeAlias()
    {
        return PageType::instance()->alias[$this->type];
    }

    public function getFirstLevel()
    {
        $find_params = array(
            'where' => join(' AND ', array(
                'parent_id = :parent_id',
                'lft <= :lft',
                'rgt >= :rgt'
            )),
            'params' => array(
                'parent_id' => $this->root->id,
                'lft' => $this->lft,
                'rgt' => $this->rgt
            )
        );

        $first_level = reset($this->getParents($find_params));

        $this->clearFilter();

        return $first_level;
    }

    public function getParentsHash()
    {
        if (!is_array($this->_parents_hash))
        {
            $this->addFilter("id <> ?", 1);

            $this->_parents_hash = $this->getParents(array('fetch_as' => ActiveRecord::FETCH_AS_HASH, 'simplify' => 1, 'hash_level' => 1));

            $this->clearFilter();
        }

        return $this->_parents_hash;
    }

    public function isParent(Page $page)
    {
        return isset($page->parents_hash[$this->id]); 
    }

    public function getLvl()
    {
        return count($this->parents_hash);
    }

    public function isMain()
    {
        return $this->lft == 2;        
    }
}