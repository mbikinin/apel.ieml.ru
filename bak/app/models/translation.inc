<?php

class Translation extends ActiveRecord
{
	protected function validate()
	{
		$this->validateLengthOf('table_name', array('max' => 255));
		$this->validateLengthOf('field_name', array('max' => 255));
		$this->validateLengthOf('lang', array('max' => 10));
	}
}