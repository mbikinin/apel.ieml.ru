<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Фамилия</label>
			<?php echo $Form->textField('surname') ?>
		</div>

        <div class="line full-size">
			<label>Имя</label>
			<?php echo $Form->textField('name') ?>
		</div>

        <div class="line full-size">
			<label>Отчество</label>
			<?php echo $Form->textField('patronymic') ?>
		</div>

        <div>
            <label>Дата рождения</label>
            <?php echo $Form->datePicker('birht_date') ?>
        </div>

        <div class="line full-size">
			<label>Пол</label>
			<?php echo $Form->select('sex', array('') + Sex::instance()->list) ?>
		</div>

        <div class="line full-size">
			<label>Город</label>
			<?php echo $Form->textField('city') ?>
		</div>

        <div class="line full-size">
			<label>email</label>
			<?php echo $Form->textField('email') ?>
		</div>

        <div>
            <label>Логин</label>
            <?php echo $Form->textField('login') ?>
        </div>

        <div>
            <label>Пароль</label>
            <?php echo $Form->passwordField('password') ?>
        </div>

        <div>
            <label>Пароль повторно</label>
            <?php echo $Form->passwordField('password_repeat') ?>
        </div>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('Отправить') ?></ins>
			</p>
		</div>
	</div>

<?php echo $Form->endFormTag() ?>