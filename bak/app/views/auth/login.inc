<?php
	if ($failure)
	{
?>
	<div id="error_explanation" class="error_explanation">Искомая комбинация логина и пароля не найдена</div>
<?php
	}
?>
	<div class="register_page">
		<div class="register">
		
		<?php echo $Form->startFormTag() ?>
			<p class="header">Авторизация</p>
			<p class="in">
				<label for="<?php echo $Form->tagId('login') ?>">Логин:</label>
				<?php echo $Form->textField('login') ?>
			</p>
			<p class="in">
				<label for="<?php echo $Form->tagId('password') ?>">Пароль:</label>
				<?php echo $Form->passwordField('password') ?>
			</p>

			<?php echo $Form->submitTag('Вход', array('class' => 'register_button')) ?>
		<?php echo $Form->endFormTag() ?>
		
		</div>
	</div>