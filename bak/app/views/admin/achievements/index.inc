<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php
if (count($achievements))
{
?>
<table class="table">
    <tr>
        <td>№</td>
        <td>Текст</td>
		<td>Картинка</td>
        <!--<td>Опубликован</td>-->
        <td>Действия</td>
    </tr>
    <?php
    foreach($achievements as $a)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= $a->text ?></td>
		<td><img src="<?= $a->url ?>" /></td>
        <!--<td><?= $a->show == 1 ? 'да' : 'нет' ?></td>-->
        <td>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit', 'id' => $a->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $a->id), array('onclick' => $this->confirm)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    На данный момент нет записей
<?php
}
?>

<?= $this->Hlp->linkTo('добавить', array('action' => 'edit')); ?>