<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag(array('multipart' => 1)) ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Название</label>
			<?php echo $Form->textField('title', array('size' => 50)) ?>
		</div>

        <div class="line full-size">
			<label>Файл</label>
			<?php echo $Form->fileField('url') ?>
		</div>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>