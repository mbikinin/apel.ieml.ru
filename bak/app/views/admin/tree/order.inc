<h1>Упорядочение подкатегорий категории <?= $Frm->object->name ?></h1>

<?= $Frm->errorMessages() ?>
<?= $Frm->startFormTag() ?>

<input type="button" value="Отсортировать по алфавиту" onclick="my_sort()" />
<br />

<div id="item-div"></div>
<br />

<script type="text/javascript">
<!--
	var g_items = <?= Js::toArrayOfHashes($children_data) ?>,
		g_odiv = $('item-div');

	function build_div()
	{
		var cnt = '';
		for(var n=0; n<g_items.length; n++)
		{
			cnt +=
				'<div>' +
				g_items[n].name +
				'&nbsp;&nbsp;<input type="button" value="^" onClick="move_up(' + n + ')" />'+
				'&nbsp;<input type="button" value="v" onClick="move_down(' + n + ')" />' +
				'<input type="hidden" name="ordi[]" value="' + g_items[n].id + '" />' +
				'</div>';
		}

		g_odiv.innerHTML = cnt;
	}

	Array.prototype.swap = array_swap;
	function array_swap(idx1, idx2)
	{
		var tmp = this[idx1];
		this[idx1] = this[idx2];
		this[idx2] = tmp;
	}

	function move_up(idx)
	{
		if(idx != 0)
		{
			g_items.swap(idx, idx - 1);
		}
		else
		{
			g_items.push(g_items.shift());
		}

		build_div();
	}

	function move_down(idx)
	{
		if(idx != g_items.length - 1)
		{
			g_items.swap(idx, idx + 1);
		}
		else
		{
			g_items.unshift(g_items.pop());
		}

		build_div();
	}

	function my_sort_func(a, b)
	{
		return a.name < b.name ? -1 : (a.name > b.name ? 1 : 0);
	}

	function my_sort()
	{
		g_items.sort(my_sort_func)

		build_div();
	}

	function delimDrop(idx)
	{
		var items = [];

		if (idx > 0)
			items = g_items.slice(0, idx);

		if (idx < g_items.length - 1)
			items = items.concat(g_items.slice(idx+1));

		g_items = items;
		build_div();
	}

	function delimAdd()
	{
		g_items.push({id:0, type:11, name:'<?=$delimiter?>'});
		build_div();
	}

	build_div();

//-->
</script>

<div class="left">
	<?= $Frm->submitTag('Сохранить') ?>
</div>

<div class="right">
	<?= $Frm->buttonTo('Назад', $this->back) ?>
</div>

<?= $Frm->endFormTag() ?>