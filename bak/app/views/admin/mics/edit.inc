<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php $this->renderPartial('/admin/lang') ?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>ISSN</label>
			<?php echo $Form->textField('issn') ?>
		</div>

        <div class="line full-size">
			<label>Футер правая часть</label>
			<?php echo $Form->textField('footer') ?>
		</div>

        <div class="line full-size">
			<label>Футер левая часть</label>
			<?php echo $Form->ckeditor('powered') ?>
		</div>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>
		
		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>