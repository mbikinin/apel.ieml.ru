<table class="table">
    <tr>
        <td>ФИО <?= $this->Hlp->linkTo('по убыв.', array_merge($_GET, array('order_by' => 'fio desc'))); ?>&nbsp;<?= $this->Hlp->linkTo('по возр.', array_merge($_GET, array('order_by' => 'fio asc'))); ?></td>
		<td>Электронная почта</td>
		<td>Контактный телефон</td>
		<td>Публикация</td>
        <td>Дата подачи<?= $this->Hlp->linkTo('по убыв.', array_merge($_GET, array('order_by' => 'cdate desc'))); ?>&nbsp;<?= $this->Hlp->linkTo('по возр.', array_merge($_GET, array('order_by' => 'cdate asc'))); ?></td>
        <td>Действия</td>
    </tr>
    <?php
    foreach($proposals as $p)
    {
    ?>
    <tr>
        <td><?= $p->fio ?></td>
		<td><?= $p->email ?></td>
		<td><?= $p->phone ?></td>
		<td><?= $this->Hlp->linkTo('скачать', $p->file_url) ?></td>
        <td><?= $p->cdate ?></td>
        <td>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $p->id), array('onclick' => $this->confirm)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>