<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php $this->renderPartial('/admin/lang') ?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Заголовок</label>
			<?php echo $Form->textField('title', array('size' => 50)) ?>
		</div>

        <div class="line full-size">
			<label>Тип</label>
			<?php echo $Form->select('type', array('') + ModuleType::instance()->list, array(), $Form->object->isNewRecord() ? array('onchange' => 'changeType(this); return false;') : array()) ?>
		</div>

        <div class="line full-size">
			<label>Область</label>
			<?php echo $Form->select('area', array('') + ModuleArea::instance()->list) ?>
		</div>

        <!--<div class="line full-size">
			<label><?php echo $Form->checkBox('show') ?> опубликовано</label>
		</div>-->

        <?php $this->renderEditModule($Form->object); ?>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

        <div>
            <label>Отображать на всех страницах <?= $Form->checkBox('show_in_all_pages', array('onchange' => "$('Page-container').toggle()")) ?></label>

            <?= $js_tree_helper->out() ?>
        </div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>

<script>

<?php
if ($Form->object->showInAllPages())
{
?>
$('Page-container').hide();
<?php
}
?>

function changeType(obj)
{
	if (confirm("Данные введенные вами будут потеряны! Вы уверены ?"))
	{
		obj.form.method = 'GET';
		obj.form.submit();
	}
}

</script>