<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
	
	for ($i = 2000; $i <= date('Y'); $i++)
		$years[$i] = $i;
?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Год</label>
			<?php echo $Form->select('name', $years) ?>
		</div>		
		
		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>