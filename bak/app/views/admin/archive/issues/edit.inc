<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag(array('multipart' => true)) ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Заголовок</label>
			<?php echo $Form->textField('title', array('size' => 50)) ?>
		</div>
		
		<div class="line full-size">
			<label>Год</label>
			<?php echo $Form->select('archive_year_id', array('') + ActiveRecord::factory('Archive_Year')->getIdNameHash()) ?>
		</div>
		
		<?php
		if ($Form->object->isNewRecord())
		{
		?>
		<div class="line full-size">
			<label>Файл</label>
			<?php echo $Form->fileField('file') ?>
		</div>
		<?php
		}
		else	
		{
		?>
		1
		<?php
		}
		?>
		
		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>