<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php $this->renderPartial('/admin/lang') ?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Заголовок</label>
			<?php echo $Form->textField('title', array('size' => 50)) ?>
		</div>
		
		<div class="line full-size">
			<label>Дата</label>
			<?php echo $Form->datePicker('date') ?>
		</div>

        <div class="line full-size">
			<label>Превью</label>
			<?php echo $Form->textArea('preview') ?>
		</div>

        <div class="line full-size">
			<label>Содержимое</label>
			<?php echo $Form->ckeditor('content') ?>
		</div>
		
        <!--<div class="line full-size">
			<label><?php echo $Form->checkBox('show') ?> опубликовано</label>
		</div>-->

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>