<?php $Frm = $this->createFormHelper(); ?>

<?= $Frm->startFormTag(array('method' => 'GET')) ?>

	<div class="form">
		Язык
		<?= $Frm->select('lang', Lang::instance()->list, null, array('onchange' => 'this.form.submit()')) ?>
	</div>

<?= $Frm->endFormTag() ?>