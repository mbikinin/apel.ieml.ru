<div class="content_main">

<h1>Архив номеров за <?= $year->name ?> год</h1>

<?php 
if (count($year->issues))
{
?>
<dl class="tabs">
	<?php
	foreach($year->issues as $i)
	{
	?>
	<dt <?= $this->params['issue_id'] == $i->id ? 'class="selected"' : '' ?>><?= $i->title ?></dt>
	<dd <?= $this->params['issue_id'] == $i->id ? 'class="selected"' : '' ?>>
		<div class="tab-content">
			<?php
			$archive_section_id = 0;
			
			foreach($i->articles as $a)
			{
				if ($a->archive_section_id != $archive_section_id)
				{
					$archive_section_id = $a->archive_section_id;
				?>
			<h2 class="archive_section"><?= $a->section->name; ?></h2>
				<?php
				}
			
			?>
			<p class="archive_title">
				<?= $this->Hlp->linkTo($a->title, array('action' => 'article', 'id' => $a->id, 'lang' => ($this->lang == Lang::$default_lang ? null : $this->lang))) ?>
				<br>
				<span class="archive_author"><?= join(', ', $a->authors_hash) ?></span>
			</p>
			<?php
			}
			?>
		</div>
	</dd>
	<?php
	}
	?>
</dl>
<?php
}
else
{
?>
	Нет номеров
<?php
}
?>
		
</div>

<script type="text/javascript">
	$(function(){
		$('dl.tabs dt').click(function(){
			$(this)
				.siblings().removeClass('selected').end()
				.next('dd').andSelf().addClass('selected');
		});
	});
</script>