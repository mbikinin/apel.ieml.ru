<style>
.content_main {
    padding: 0 10% 0px 0;
}
</style>

<?php
$Form = $this->createFormHelper(new Proposal());

if ($notice = self::$Session->get('proposals', 'notice'))
{
	self::$Session->wipe('proposals', 'notice');
	echo $notice;
}	
elseif ($error = self::$Session->get('proposals', 'error'))
{	
	$Form->object->setAttributes(self::$Session->get('proposals', 'attrs'));
	self::$Session->wipe('proposals', 'error');
	self::$Session->wipe('proposals', 'attrs');
	echo $error;
}
	
?>

<div class="register_page">
	<div class="register">
		<p class="header">Подписчик</p>
		<?= $Form->startFormTag(array('multipart' => 1), array('controller' => '/proposals', 'action' => 'send', 'type' => ProposalType::subscription)) ?>
			<p><label>Ф.И.О. полностью</label><?= $Form->textField('fio'); ?></p>
			<p><label>Электронная почта</label><?= $Form->textField('email'); ?></p>
			<p><label>Контактный телефон</label><?= $Form->textField('phone'); ?></p>
			<p><label>Почтовый адрес</label><?= $Form->textField('address'); ?></p>
			<p class="header">Год, номер, количество экземпляров</p>
			<p><label><i>Заполняется в&nbsp;произвольной форме</i></label><?= $Form->textArea('issues'); ?></p>
			<p class="header">Платежный документ</p>
			<p><label><i>Сканированная копия</i></label><?= $Form->fileField('file'); ?></p>
			
			<?= $Form->submitTag('Отправить заявку', array('class' => 'register_button')) ?>			
		<?= $Form->startFormTag() ?>
	</div>
</div>