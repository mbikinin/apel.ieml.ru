<?php
$news = ActiveRecord::factory('News')->findAll(array(
	'where' => 'news.show = :show',
	'params' => array(
		'show' => 1
	),
	'limit' => 2,
	'order_by' => 'date desc',
	'lang' => $this->lang
));
?>

<h2><?= $module->title ?></h2>
<table class="text">
	<tr>
		<?php
		foreach($news as $n)
		{
		?>
		<td class="news">
			<p class="date"><?= $n->f_date ?></p>
			<h3><?= $this->Hlp->linkTo($n->title, '#'/*array('controller' => '/news', 'id' => $n->id)*/) ?></h3>
			<p><?= $n->preview ?></p>
		</td>
		<?php
		}
		?>
	</tr>
</table>