<?php 
	if ($this->current_page->first_level instanceof ActiveRecord)
		$this->current_page->first_level->setFilter(array('pages.show' => 1));
?>

<div class="left_menu">
	<ul>   
	<?php
    foreach((array)$this->current_page->first_level->direct_children as $dc)
    {
    ?>
    <li class="<?= ($dc->id == $this->current_page->id ? 'mm_current' : '') ?>"><?= ($dc->id == $this->current_page->id ? $dc->name : $this->Hlp->linkTo($dc->name, $dc->url . ($this->lang == Lang::$default_lang ? '' : '?lang=' . $this->lang))) ?></li>
    <?php
    }
    ?>
	</ul>
</div>