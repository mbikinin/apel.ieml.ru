<div class="left_menu">
	<ul>
	<?php
	foreach(ActiveRecord::factory('Archive_Year')->findAll() as $y)
	{
	?>
		<li <?= $y->id == $this->year->id ? 'class="mm_current"' : '' ?>>
			<?php
			if ($y->id == $this->year->id)
				echo $y->name;
			else
				echo $this->Hlp->linkTo($y->name, array('controller' => '/archive', 'action' => 'years', 'id' => $y->name, 'lang' => ($this->lang == Lang::$default_lang ? null : $this->lang)))
			?>
		</li>
	<?php
	}
	?>
	</ul>
</div>