<style>
.content_main {
    padding: 0 10% 0px 0;
}
</style>

<?php
$Form = $this->createFormHelper(new Proposal());

if ($notice = self::$Session->get('proposals', 'notice'))
{
	self::$Session->wipe('proposals', 'notice');
	echo $notice;
}	
elseif ($error = self::$Session->get('proposals', 'error'))
{	
	$Form->object->setAttributes(self::$Session->get('proposals', 'attrs'));
	self::$Session->wipe('proposals', 'error');
	self::$Session->wipe('proposals', 'attrs');
	echo $error;
}
	
?>

<div class="register_page">
	<div class="register">
		<?= $Form->startFormTag(array('multipart' => 1), $this->Hlp->urlFor(array('controller' => '/proposals', 'action' => 'send', 'type' => ProposalType::publication))) ?>
			<p><label>Ф.И.О. полностью</label><?= $Form->textField('fio'); ?></p>
			<p><label>Электронная почта</label><?= $Form->textField('email'); ?></p>
			<p><label>Контактный телефон</label><?= $Form->textField('phone'); ?></p>
			<p><label>Публикация в zip-архиве</label><?= $Form->fileField('file'); ?></p>
			
			<?= $Form->submitTag('Отправить заявку', array('class' => 'register_button')) ?>			
		<?= $Form->startFormTag() ?>
	</div>
</div>