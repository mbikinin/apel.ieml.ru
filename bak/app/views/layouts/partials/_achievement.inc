<h3 class="awards_header"><?= $module->title ?></h3>

<table class="text">         
	<tbody>
		<tr>
		<?php
		foreach(ActiveRecord::factory('Achievement')->findAll(array('table_alias' => 'a', 'where' => 'a.show = 1', 'limit' => 4)) as $a)
		{
		?>
		<td class="awards">
			<img width="150" height="126" src="<?= $a->url ?>">
			<p><?= $a->text ?></p>
		</td>
		<?php
		}
		?>
		</tr>
	</tbody>
</table>