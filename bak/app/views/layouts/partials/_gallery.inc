<?php
    $this->Hlp->needJs('jquery.tooltip.js');
    $this->Hlp->needJs('fancybox/jquery.fancybox-1.3.4.pack.js');
    $this->Hlp->needJs('jsor/lib/jquery.jcarousel.js');
    $this->Hlp->needCss('fancybox/jquery.fancybox-1.3.4.css');
	$this->Hlp->needCss('jquery-tooltip/jquery.tooltip.css');
    $this->Hlp->needCss('jsor/skins/ie7/skin.css');
    echo $this->Hlp->getCss();
    echo $this->Hlp->getJs();
?>

<div class="eku-photos">
    <ul id="mycarousel" class="jcarousel-skin-ie7">
    <?php
	foreach($module->item->photos as $p)
    {
    ?>
        <li>
            <a href="<?php echo $p->getUrl(); ?>" rel="fancygroup">
                <img src="<?php echo $p->getUrl(); ?>" alt="">
            </a>
        </li>
    <?php
    }
    ?>
    </ul>
</div>

<script type="text/javascript">
$(document).ready(function() {
    jQuery('#mycarousel').jcarousel();
    jQuery('#mycarousel li a').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        fade: 300
    });

    jQuery("a[rel=fancygroup]").fancybox({
        'transitionIn'    : 'none',
        'transitionOut'   : 'none',
        'titlePosition'   : 'over',
        'titleFormat'   : function(title, currentArray, currentIndex, currentOpts) {
            if (1 == currentArray.length) return title.length ? ' &nbsp; ' + title : '';
            return '<span id="fancybox-title-over">Фото ' + (currentIndex + 1) + ' из ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
        }
    });
});
</script>

<style>
.jcarousel-skin-ie7 .jcarousel-next-horizontal {
    background-position: -25px 0;
    right: 10px;
}

#mycarousel li, #mycarousel li a, #mycarousel li a img{
    width:112px;
    height:82px;
}

.jcarousel-skin-ie7 .jcarousel-container-horizontal {
    width: 404px;
}

.jcarousel-skin-ie7 .jcarousel-clip-horizontal {
    width: 407px;
}
</style>