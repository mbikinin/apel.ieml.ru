<script language="JavaScript" type="text/javascript">

	var <?= $this->js_var_name ?>;

	function <?= $this->init_func ?>()
	{
		<?= $this->js_var_name ?> = new <?= $this->js_class_name ?>();
		<?= $this->js_var_name ?>.var_name = '<?= $this->js_var_name ?>';
		<?= $this->js_var_name ?>.container_prefix = '<?= $this->container_prefix ?>';
		<?= $this->js_var_name ?>.nodes = {};
		<?= $this->js_var_name ?>.urls = <?= Js::toHash($this->urls) ?>;
		<?= $this->js_var_name ?>.open_nodes = <?= Js::toArray($this->expand_nodes) ?>;
		<?= $this->js_var_name ?>.potential_admins = <?= Js::toHash($this->potential_admins) ?>;
        <?= $this->js_var_name ?>.tools_class = <?= $this->tools_class ?>;
        <?= $this->js_var_name ?>.tools_class_name = '<?= $this->tools_class ?>';
		<? $this->extraInitStuff() ?>
		<?= $this->js_var_name ?>.render(0);
		<?= $this->js_var_name ?>.expandNodes();
	}

	addEvent(getDocumentRoot(), 'load', <?= $this->init_func ?>);

</script>