<!DOCTYPE HTML>
<html>
	<head>
		<title>Панель администрирования</title>
		<?= $this->Hlp->getCss(); ?>
	    <?= $this->Hlp->getJs(); ?>
	</head>
	<body>
		<div id="container">
			<div id="holder">

				<?php $this->renderPartial('/layouts/admin_navigator') ?>

				<div id="content">
					<h3><?= $this->page_title ?></h3>
                    <? if ($notice = $Session->push('notice')) { ?>
                        <div class="notice"><?php echo $notice ?></div>
                    <? } ?>
                    <? if ($err = $Session->push('error')) { ?>
                        <div class="error_explanation"><?php echo $err ?></div>
                    <? } ?>

                    <?= $content_for_layout ?>
				</div>
				<div class="clear">clear floats</div>
			</div>
			<div id="footer"></div>
		</div>
	</body>
</html>