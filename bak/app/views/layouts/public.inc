<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?= h($this->page_title) ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?= $this->Hlp->getCss(); ?>
<?= $this->Hlp->getJs(); ?>
<link rel="shortcut icon" href="/design/images/favicon.gif" type="image/x-icon" />

<!--[if IE 6]><script type="text/javascript" src="design/js/iepngfix_tilebg.js"></script><![endif]-->

</head>

<body>

	<div class="all">
   
	<div class="top">
    
    <table style="margin:0 auto;"><tr><td width="1280">
	
	<table class="text">
	   
        <tr>
        
        <td class="left">
        <div class="apel"><a href="/"><img src="/design/images/apel.png" width="70" height="50" /></a></div>
		<p class="lang"><?= $this->lang == Lang::rus ? $this->Hlp->linkTo('in English', $this->controller_fullname == 'page/html' ? $this->current_page->url . '?lang=' . Lang::eng : $this->Hlp->urlFor(array('lang' => Lang::eng))) : $this->Hlp->linkTo('по-русски', $this->controller_fullname == 'page/html' ? $this->current_page->url . '?lang=' . Lang::rus : $this->Hlp->urlFor(array('lang' => Lang::rus))) ?></p>
        </td>
        
        <td colspan="2" class="header">
        
		<div class="header_box"><h1 class="logo">Актуальные проблемы<br />экономики и права</h1></div>
        
        </td>
        
        </tr>
        
        <tr>
        
        <td class="left">&nbsp;</td>
        <td class="content">
        
            <?php $this->renderPartial('/layouts/top_menu') ?>

        </td>
        <td class="right"><p class="sans issn"><?= $this->mic->issn ?></p></td>
        
        
        
        </tr>
        
        
   </table>
   
	</td></tr></table>
    
    </div>
   
<table style="margin:0 auto;"><tr><td width="1280">
	
	<table class="text">	   
        <tr>
			<td colspan="3"><p>&nbsp;</p></td>
        </tr>
		<tr>        
			<td class="left"><?= $this->loadModules(ModuleArea::left); ?></td>        
			<td class="content">        
			<?= $content_for_layout ?>		
			<?= $this->loadModules(ModuleArea::center); ?>        
			</td>
			<td class="right">        
				<div class="right_box"><?= $this->loadModules(ModuleArea::right); ?></div>        
			</td>
		</tr>        
   </table>
   
</td></tr></table>

	<div class="empty">&nbsp;</div>

	</div>

<div class="footer">

<div class="scissor">&nbsp;</div>

<table style="margin:0 auto;"><tr><td width="1280">

    <table class="text">
    
    <tr>
        
        <td class="left">&nbsp;</td>
        <td class="content"><p><?= $this->mic->footer ?></p> </td>
        <td class="right"><p style="padding-left:1.25em;"><?= $this->mic->powered ?></p></td>
                     
        </tr>
    
    </table>

 </td></tr></table>

</div>

</body>

</html>