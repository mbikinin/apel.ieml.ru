<?php 
	if ($this->current_page instanceof ActiveRecord)
		$this->current_page->setFilter(array('pages.show' => 1));
?>

<div class="main_menu">
	<ul>
		<?php
		foreach($this->current_page->root->direct_children as $c)
		{
		?>
			<li>
				<?php
				if ($c->isParent($this->current_page))
				{
				?>
				<div class="current"><span><?= $c->name ?></span></div>
				<?php
				}
				else
				{
				?>
				<a href="<?= $c->url . ($this->lang == Lang::$default_lang ? '' : '?lang=' . $this->lang) ?>"><span><?= $c->name ?></span></a>
				<?php
				}
				?>
			</li>
		<?php
		}
		?>
	</ul>
</div>