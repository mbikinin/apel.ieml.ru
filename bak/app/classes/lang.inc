<?php

class Lang extends Reference
{
	const
		rus = 'rus',
		eng = 'eng';

	public $links;
	public static $default_lang = self::rus;

	protected function __construct()
	{
		$this->list = array(
			self::rus => 'Русский',
			self::eng => 'Английский'
		);
		
		$this->links = array(
			self::rus => 'на Русском',
			self::eng => 'in English'
		);
	}

	public function setLanguages($languages)
	{
		foreach ($this->list as $lang => $title)
		{
			if (!in_array($lang, $languages) && $lang != self::$default_lang)
			{
				unset($this->list[$lang]);
			}
		}
	}
}