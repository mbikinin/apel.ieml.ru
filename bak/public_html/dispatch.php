<?php
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    //$GLOBALS['Con']->debug = 1;
    date_default_timezone_set('Europe/Moscow');
    // error_reporting(0);
	require_once '../config/environment.inc';
	force_no_cache();

	$Dispatcher = new Dispatcher();

	list ($url) = explode('?', $_SERVER['REQUEST_URI']);

	try
	{
		try
		{
			$Dispatcher->dispatch($url);
		}
		catch (DispatcherError $e)
		{
			$PageDispatcher = new PageDispatcher();
			$PageDispatcher->dispatch(Router::prepareUrl($url, 0));
		}
	}
	catch (Exception $e)
	{
		if ($Config->server == '!')
		{
			if ($e->getCode() == 404)
			{
				Url::redirect('/404.html');
			}
			else
			{
				Url::redirect('/error.html');
			}
		}
		else
		{
			$Dispatcher->dispatchWithException($e);
		}
	}