function node(id, caption/*, icon='folder', desc=caption, baseclass='tl', tools=''*/)
{
	this.id			= id;
	this.caption	= caption;
	this.children	= null;
	this.icon		= arguments.length > 2 && arguments[2] ? arguments[2] : 'folder';
	this.desc		= arguments.length > 3 ? arguments[3] : caption;
	this.baseclass	= arguments.length > 4 && arguments[4] ? arguments[4] : 'tl';
	this.tools		= arguments.length > 5 ? arguments[5] : '';
	this.onscreen	= false;
	this.level		= 0;
}

function tree(structure, container, varname/*, navigate_fn=null, ec_fn=null, level_class_fn=null*/)
{
	this.tree			= structure;
	this.container		= container;
	this.varname		= varname;
	this.navigate		= arguments.length > 3 && arguments[3] ? arguments[3] : null;
	this.ec_fn			= arguments.length > 4 && arguments[4] ? arguments[4] : null;
	this.level_class_fn	= arguments.length > 5 && arguments[5] ? arguments[5] : null;
	this.img_path		= '/design/images/tree/';
	this.navigate_ec	= false;

	// Methods
	this.out		= tree_out;
	this.ec			= tree_ec;
	this.lookup		= tree_lookup;
	this.append_html= tree_append_html;
}

function tree_out(/*tr=this.tree, container=this.container, level=0*/)
{
	var tr				= arguments.length > 0 ? arguments[0] : this.tree;
	var container		= arguments.length > 1 ? arguments[1] : this.container;
	var level			= arguments.length > 2 ? arguments[2] : 0;

	for(var n=0; n<tr.length; n++)
	{
		tr[n].level = level + 1;

		var has_children = (tr[n].children && tr[n].children != null);
		var children_div = tr[n].id+'_ch';

		var odiv = document.createElement('DIV');
		if(this.level_class_fn != null)
			odiv.className = this.level_class_fn(level);
		else
			odiv.style.cssText = 'margin-left:'+(level > 0 ? 16 : 30)+'px; margin-top: 2px; text-indent:-30px';

		// [+] [-]
		var oplus_minus = document.createElement('IMG');
		oplus_minus.src = this.img_path+(has_children ? 'plus.gif' : 'null.gif');
		oplus_minus.border = 0;
		oplus_minus.width = 9;
		if(has_children)
		{
			oplus_minus.id = tr[n].id+'_pm';
			var oaplus_minus = document.createElement('A');

//				oaplus_minus.href = 'javascript:void('+this.varname+'.ec(\''+tr[n].id+'\'))';
			oaplus_minus.href = '#';
			oaplus_minus.onclick = new Function(this.varname+'.ec(\''+tr[n].id+'\'); return false;');

			oaplus_minus.appendChild(oplus_minus);
			oplus_minus = oaplus_minus;
		}
		odiv.appendChild(oplus_minus);

		// ����
		var onode = document.createElement('SPAN');
		onode.title = tr[n].desc;
		onode.className = tr[n].baseclass;
		onode.id = tr[n].id + '_a';

		var oicon = document.createElement('IMG');
		oicon.src = this.img_path+tr[n].icon+'.gif';
		oicon.id = tr[n].id+'_img';
		oicon.border = 0;
		oicon.width = 16;
		oicon.height = 16;
		oicon.style.cssText = 'margin-bottom:-2px; margin-left: 3px; margin-right: 3px';
		onode.appendChild(oicon);

		this.append_html(onode, tr[n].caption);

		var nav_ec = (this.navigate_ec && has_children);
		if(this.navigate || nav_ec)
		{
			var oa_navigate = document.createElement('A');
//				oa_navigate.href = 'javascript:void('+this.varname+'.'+(nav_ec ? 'ec' : 'navigate')+'(\''+tr[n].id+'\'))';
			oa_navigate.href = '#';
			oa_navigate.onclick = new Function(this.varname+'.'+(nav_ec ? 'ec' : 'navigate')+'(\''+tr[n].id+'\'); return false;');
			oa_navigate.className = tr[n].baseclass;
			oa_navigate.appendChild(onode);
			onode = oa_navigate;
		}
		
		odiv.appendChild(onode);

		if(tr[n].tools)
		{
			var otools = document.createElement('SPAN');
			otools.className = 'tree_tools';
			this.append_html(otools, /*'&nbsp;'+*/tr[n].tools);
			odiv.appendChild(otools);
		}

		// DIV ��� child-��
		if(has_children)
		{
			var ochild_div = document.createElement('DIV');
			ochild_div.id = children_div;
			ochild_div.style.cssText = 'display:none';
			odiv.appendChild(ochild_div);
		}

		var cntr = document.getElementById(container);
		cntr.appendChild(odiv);
	}
}

function tree_lookup(id/*, tr=this.tree*/)
{
	var tr = arguments.length > 1 ? arguments[1] : this.tree;

	for(var n=0; n<tr.length; n++)
		if(tr[n].id == id)
			return tr[n];
		else if(tr[n].children && tr[n].children != null)
		{
			var result = arguments.callee(id, tr[n].children);
			if(result)
				return result;
		}
}

function tree_ec(blk_id/*, do_callback = true, tr2open = null*/)
{
	try
	{
		var blk			= document.getElementById(blk_id+'_ch');
		var need_open	= blk.style.display == 'none';
		var do_callback = arguments.length > 1 ? arguments[1] : true;
		var tr2open		= arguments.length > 2 ? arguments[2] : this.lookup(blk_id);

		if (do_callback && this.ec_fn)
			this.ec_fn(blk_id, need_open)

		if(need_open && !tr2open.onscreen)
		{
			tr2open.onscreen = true;
			this.out(tr2open.children, blk_id+'_ch', tr2open.level);
		}

		blk.style.display = need_open ? 'block' : 'none';
		document.getElementById(blk_id+'_pm').src = this.img_path+(need_open ? 'minus' : 'plus')+'.gif';
		document.getElementById(blk_id+'_img').src = this.img_path+tr2open.icon+(need_open ? '_open' : '')+'.gif';
//			document.getElementById(blk_id+'_cpt').style.cssText = (need_open ? 'text-decoration: underline' : '');
	}
	catch(e)
	{
	};
}

function tree_append_html(obj, html)
{
	if(typeof(obj.insertAdjacentHTML) != 'undefined')
		obj.insertAdjacentHTML('beforeEnd', html);
	else
		obj.innerHTML += html;
}