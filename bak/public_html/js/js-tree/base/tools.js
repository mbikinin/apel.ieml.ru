function JsTreeTools()
{
	this.tools = [];
	this.tool_path = '/design/images/tree/';
}

JsTreeTools.prototype.add = function(params)
{
	this.tools.push(this.create(params));
}

JsTreeTools.prototype.create = function(params)
{
	var img = document.createElement('img');
	img.src = this.tool_path + params.img + '.gif';
	img.border = 0;

	var a = document.createElement('a'),
		onclick = '',
		confirm = '';

	if (typeof(params.confirm) == 'string')
	{
		confirm = params.confirm;
	}
	else if (params.confirm > 0)
	{
		confirm = 'Вы уверены?';
	}

	if (confirm != '')
	{
		a.onclick = new Function('', 'return confirm(\'' + confirm + '\')');
	}

	if (typeof(params.target) == 'string')
	{
		a.target = params.target;
	}

	a.href = params.url;
	a.title = params.title;

	a.appendChild(img);

	return a;
}