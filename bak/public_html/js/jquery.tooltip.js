/* 
	�������

	<div class="tooltip">
		<span>��� ������������ Tooltips</span>
	</div>
*/

(function($) {
	jQuery.fn.extend({ 
	
		tooltip: function(options) {
 
			var defaults = { 
				'fade_speed'		: 400,
				'css_class_name'	: 'tooltip',
				'def_title'			: '������ ������ ��������� � ����������!'
			};

			var options = jQuery.extend(defaults, options||{});    	    

    	    return this.each( function(i) {
				
				var o				= options;
				var $this 			= jQuery(this);				
				var tooltip 		= jQuery('<div id="t'+ i +'" class="'+ o.css_class_name  +'"><span>'+ o.def_title +'</span></div>').css({opacity: 0});
		        
		        if (!$this.attr('rel')) {

					jQuery('body').prepend(tooltip);

					$this.css({color: '#ccc'});

					$this.mouseover(function() {
		
						jQuery('#t' + i).stop().animate( {
							opacity: 1
						}, o.fade_speed);
				                                                      
					}).mousemove(function(kmouse) {
		
						jQuery('#t' + i).animate( {
							left: kmouse.pageX + 15, 
							top: kmouse.pageY + 15
						}, 0);

					}).mouseout(function() {
				
						jQuery('#t' + i).stop().animate({opacity: 0}, o.fade_speed);
					});
				}
	        });	        	        
		}
	});
})(jQuery);