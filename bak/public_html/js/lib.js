function addEvent(element_ptr, event_type, event_func)
{
	if (element_ptr.addEventListener)
	{
		element_ptr.addEventListener(event_type, event_func, false);
	}
	else if (element_ptr.attachEvent)
	{
		element_ptr.attachEvent('on' + event_type, event_func);
	}
}

function getDocumentRoot()
{
	return window.addEventListener || window.attachEvent ? window : document.addEventListener ? document : null;
}

function var_dump(obj) {
   if(typeof obj == "object") {
      return "Type: "+typeof(obj)+((obj.constructor) ? "\nConstructor: "+obj.constructor : "")+"\nValue: " + obj;
   } else {
      return "Type: "+typeof(obj)+"\nValue: "+obj;
   }
}