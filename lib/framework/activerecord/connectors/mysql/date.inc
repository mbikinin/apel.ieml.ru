<?
	class Mysql_Date
	{
		static function fetch($value)
		{
			return $value ? Date::oracleDateToTime($value) : null;
		}

		static function set($value)
		{
			return $value ? Date::textDateToTime($value) : null;
		}

		static function save($value)
		{
			return $value ? Date::timeToOracleDate($value) : null;
		}
	}
?>