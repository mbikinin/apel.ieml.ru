<?
	class MySQL_ActiveRecordStatement extends ActiveRecordStatement
	{
		function limit($limit, $offset = 0)
		{
			$this->limit = true;
			$this->mergeParams(compact('limit', 'offset'));
		}

		protected function limitClause($query)
		{
			if ($this->limit)
				$query .= " LIMIT :offset, :limit";

			return $query;
		}
	}
?>