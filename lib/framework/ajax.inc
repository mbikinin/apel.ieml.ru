<?
	class Ajax
	{
		static function arrayOfHashes($array_of_hashes, $var_name = 'data')
		{
			return self::variable($var_name, Js::toArrayOfHashes($array_of_hashes));
		}

		static function hash($hash, $var_name = 'data')
		{
			return self::variable($var_name, Js::toHash($hash));
		}

		static function toArray($array, $var_name = 'data')
		{
			return self::variable($var_name, Js::toArray($hash));
		}

		static function variable($var_name, $var_value)
		{
			return "var $var_name = $var_value;";
		}
	}
?>