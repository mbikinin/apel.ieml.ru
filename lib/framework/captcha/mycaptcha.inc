<?
	class MyCaptcha
	{
		public
			$angles = array(-20, 20),
			$font_sizes = array(18, 18),
			$image = array(120, 60),
			$word_lengths = array(5, 6),
			$alpha = '23456789abcdeghkmnpqsuvxyz';

		protected
			$font_dir,
			$fonts,
			$key_string;

		function __construct()
		{
			$this->font_dir = dirname(__FILE__).'/fonts/';
			$this->fonts = (array)glob($this->font_dir.'*.ttf');
		}

		function out()
		{
			$im = imagecreate($w = $this->image[0], $h = $this->image[1]);
//			$fg = imagecolorallocate($im, $fg_r = mt_rand(0, 255), $fg_g = mt_rand(0, 255), $fg_b = mt_rand(0, 255));
//			$bg = imagecolorallocate($im, $fg_r ^ 0xFF, $fg_g ^ 0xFF, $fg_b ^ 0xFF);
			$fg = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
			$bg = imagecolorallocate($im, 0x18, 0x6D, 0xA6);
			imagefill($im, 0, 0, $bg);

			$this->key_string = String::genPassword($this->rand($this->word_lengths), $this->alpha);
			$size = $this->rand($this->font_sizes);
			$angle = $this->rand($this->angles);
			$font = $this->fonts[mt_rand(0, count($this->fonts) - 1)];
//Debug::varDump($size, $angle, $text, $font);

			$box = imagettfbbox($size, $angle, $font, $this->key_string);
			$x = ($w - abs($box[4] - $box[0])) / 2;
			$y = ($h + ($angle > 0 ? 1 : -1) * abs($box[5] - $box[1])) / 2;
			imagettftext($im, $size, $angle, $x, $y, $fg, $font, $this->key_string);

/*
			$px = array();
			for ($n = 0; $n < 1024; $n++)
			{
				do
				{
					$x = mt_rand(0, $this->image[0]);
					$y = mt_rand(0, $this->image[1]);
				}
				while (isset($px[$x][$y]));

				$px[$x][$y] = 1;

				imagesetpixel($im, $x, $y, $fg);
			}
*/

			header('Content-type: image/png');
			imagepng($im);

			imagedestroy($im);
		}

		function getKeyString()
		{
			return $this->key_string;
		}

		protected function rand($array)
		{
			return call_user_func_array('mt_rand', $array);
		}
	}
?>