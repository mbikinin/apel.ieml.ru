<?
	$fwk_dir = dirname(__FILE__).'/';

	require_once $fwk_dir.'actionview/helper.inc';
	require_once $fwk_dir.'actionview/form_helper.inc';
	require_once $fwk_dir.'actionview/sort_helper.inc';

	require_once $fwk_dir.'active_record.inc';
	require_once $fwk_dir.'activerecord/connector.inc';
	require_once $fwk_dir.'activerecord/statement.inc';
	// require_once $fwk_dir.'activerecord/connectors/oracle/oracle.inc';
	require_once $fwk_dir.'activerecord/connectors/mysql/mysql.inc';

	require_once $fwk_dir.'captcha/mycaptcha.inc';

	require_once $fwk_dir.'ajax.inc';
	require_once $fwk_dir.'array.inc';
	require_once $fwk_dir.'back.inc';
	require_once $fwk_dir.'clamav.inc';
//	require_once $fwk_dir.'cryptopro.inc';
	require_once $fwk_dir.'date.inc';
	require_once $fwk_dir.'debug.inc';
	require_once $fwk_dir.'hash.inc';
	require_once $fwk_dir.'http.inc';
	require_once $fwk_dir.'js.inc';
	require_once $fwk_dir.'mail.inc';
	require_once $fwk_dir.'memcache.inc';
	require_once $fwk_dir.'rtf.inc';
	require_once $fwk_dir.'string.inc';
	require_once $fwk_dir.'url.inc';
	require_once $fwk_dir.'wiki.inc';

	require_once $fwk_dir.'action_controller.inc';
	require_once $fwk_dir.'dispatcher.inc';
	require_once $fwk_dir.'page_dispatcher.inc';
	require_once $fwk_dir.'exception.inc';
	require_once $fwk_dir.'inflector.inc';
	require_once $fwk_dir.'iterator.inc';
	require_once $fwk_dir.'paginator.inc';
	require_once $fwk_dir.'router.inc';
	require_once $fwk_dir.'validate.inc';

	require_once $fwk_dir.'htmlcutter.inc';
?>