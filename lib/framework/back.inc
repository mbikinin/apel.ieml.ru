<?
	/**
	 * @package BackUrlDetector
	 * @version Tue Feb 26 11:18:37 MSK 2008
	 * @uses msession
	 */
	class BackUrlDetector
	{
		public static
			$Session,
			$realms = array('/');

		protected
			$ns,
			$script_names,
			$query_strings,
			$script_name,
			$query_string,
			$realm,
			$r_script_names,
			$r_query_strings;

		function __construct($ns = 'back_ex')
		{
			rsort(self::$realms);

			$this->ns = $ns;
			$this->getVars();
			$this->script_name = Url::wipeQueryString($_SERVER['REQUEST_URI']);
			$this->query_string = $_SERVER['QUERY_STRING'];

			foreach (self::$realms as $r)
				if (stristr($this->script_name, $r) !== false)
				{
					$this->realm = $r;
					break;
				}

			if ($this->realm)
			{
				$this->setType($this->r_script_names =& $this->script_names[$this->realm]);
				$this->setType($this->r_query_strings =& $this->query_strings[$this->realm]);
			}
			else
			{
				$this->r_script_names &= $this->script_names;
				$this->r_query_strings &= $this->query_strings;
			}
		}

		function getUrl(/*$default = '/'*/)
		{
			if (($pos = array_search($this->script_name, $this->r_script_names)) !== false)
			{
				$this->r_script_names = array_slice($this->r_script_names, 0, $pos);
				$this->r_query_strings = array_slice($this->r_query_strings, 0, $pos);
			}

			if (@trim($url = $this->r_script_names[count($this->r_script_names) - 1].'?'.
				$this->r_query_strings[count($this->r_query_strings) - 1]) == '?')
//				$url = $default;
				$url = $this->realm;

			$url = self::$Session->setUrl($url);

			return $url;
		}

		function captureUrl()
		{
			$this->r_script_names[] = $this->script_name;
			$this->r_query_strings[] = $this->query_string;
			$this->setVars();
		}

		protected function setType(&$var)
		{
			if (!is_array($var))
				$var = array();
		}

		protected function getVars()
		{
			$this->setType($this->script_names = self::$Session->get($this->ns, 'script_names'));
			$this->setType($this->query_strings = self::$Session->get($this->ns, 'query_strings'));
		}

		protected function setVars()
		{
			self::$Session->set($this->ns, 'script_names', $this->script_names);
			self::$Session->set($this->ns, 'query_strings', $this->query_strings);
		}
	}
?>