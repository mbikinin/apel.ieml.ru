<?
	/**
	 * This class manages file uploads (check and store)
	 * @author  Oleg Stepanischev <oleg@e-kazan.ru>
	 * @package File_Upload
	 * @access public
	 */
	class File_Upload
	{
	    /**
    	 * gets error message from error code
	     *
	     * @param    string $err_code	error code
	     * @return   string          	error message
		 * @access public
	     */
		public static function uploadError($err_code, $is_required = false)
		{
			switch($err_code)
			{
				case UPLOAD_ERR_OK:			return null;

				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:	return 'Размер файла превышает максимально допустимый';

				case UPLOAD_ERR_PARTIAL:	return 'Произошла ошибка при загрузке файла на сервер';

				default:
				case UPLOAD_ERR_NO_FILE:	return $is_required ? 'Не выбран файл для закачки' : null;
			}
		}

	    /**
	     * gets uploaded file properties from $_FILES by name
	     *
	     * @param    string $name		name of upload field in form
	     * @return   array          	file properties
		 * @access public
	     */
		public static function getProps($name)
		{
			// get name and index
			if(preg_match('/^(\w+)\[(\w+)\]$/', $name, $match))
			{
				list(, $name, $idx) = $match;

				$ret = array();

				foreach ((array)$_FILES[$name] as $k => $v)
					$ret[$k] = $v[$idx];

				return $ret;
			}
			else
			{
				return $_FILES[$name];
			};
		}

	    /**
	     * check uploaded file
	     *
	     * @param    string $name		name of upload field in form
	     * @param    string $opts		check options, it is array with keys:
		 *   max_size - maximum allowable file size
		 *   types - array of valid content types (*? wildcards is available)
		 *   extensions - array of valid extensions
		 *   img_types - array of valid image types (look at imagesize function)
		 *   dimensions - array of valid dimensions (dimension is indexed array with 2 elements - width and height)
	     * @param    bool $is_required	flag showing if user is required to upload file
	     * @return   string          	error message
	     */
		public static function check($name, $opts, $is_required = false)
		{
			$file = File_Upload::getProps($name);

			// is file uploaded
			if (!is_uploaded_file($file['tmp_name']))
			{
				return File_Upload::uploadError($file['error'], $is_required);
			}
			elseif ($opts['antivirus'] && false) // requires Clamav module
			{
				if ($malware = Clamav::scanFile($file['tmp_name']))
				{
					return $malware;
				}
			}

			// size
			if (isset($opts['max_size']) && $file['size'] > $opts['max_size'])
				return 'Размер файла превышает допустимый';

			// type
			if (isset($opts['types']))
			{
				$types = $opts['types'];
				$match = false;
				foreach ($types as $k => $type)
				{
					$re = preg_quote($type, '/');

					$re = strtr($re, array('\\*' => '.*', '?' => '.?'));

					if (preg_match('/'.$re.'/', $file['type']))
					{
						$match = true;
						break;
					};
				};

				if (!$match)
					return 'Недопустимый формат файла';
			};

			// name (extension)
			if (isset($opts['extensions']))
			{
				$pathinfo = pathinfo($file['name']);

				if (!in_array($pathinfo['extension'], $opts['extensions']))
					return 'Неверное расширение файла';
			};

			// img type && dimensions
			if (isset($opts['img_types']) || isset($opts['dimensions']))
			{
				$imagesize = GetImageSize($file['tmp_name']);

				if (isset($opts['img_types']) && !in_array($imagesize[2], $opts['img_types']))
					return 'Неверный тип изображения';

				if (isset($opts['dimensions']))
				{
					$match = false;
					foreach ($opts['dimensions'] as $dim)
						if($dim[0] == $imagesize[0] && $dim[1] = $imagesize[1])
						{
							$match = true;
							break;
						};

					if (!$match)
						return 'Недопустимые размеры изображения';
				};
			};
		}

	    /**
	     * stores file to specified path/deletes current uploaded file
	     *
	     * @param    string $name			name of upload field in form
	     * @param    string $store_path		path to store directory
	     * @param    string $prefix			prefix of new file name at storage
	     * @param    string $id_value		id of row/file (used to make new file name)
	     * @param    string $current_name 	current file name
	     * @param    bool $delete_current	flag showing if current file is to be deleted
	     * @return   array          		new file name
		 * @access public
	     */
		public static function store($name, $store_path, $prefix, $id_value, $current_name, $delete_current = 0, $adj_data = null)
		{
			$file = File_Upload::getProps($name);

			$pathinfo = pathinfo($file['name']);

			$t_file_name = $file['tmp_name'];

			if (is_uploaded_file($t_file_name))
			{
				if ($adj_data)
				{
					$imagick = new Imagick();
					$imagick->load($t_file_name);
					$imagick->adjust($adj_data);
					$imagick->save($t_file_name);
				}

				// store file
				$prefix and $prefix .= '_';

				if ($id_value)
					$file_name = $prefix.$id_value.'.'.$pathinfo['extension'];
				else
					$file_name = self::createFileName($store_path."/", $pathinfo['extension']);

				$file_path = $store_path.'/'.$file_name;

				if (!move_uploaded_file($t_file_name, $file_path))
				{
					return '';
				}

				chmod($file_path, 0664);

				// delete current file if it's name differs from new name
				$delete_current = $current_name != $file_name;
			}
			else
				$file_name = $delete_current ? '' : $current_name;

			// delete current file
			if ($delete_current)
				@unlink($store_path.'/'.$current_name);

			return $file_name;
		}

		public static function createFileName($path, $ext)
		{
			while (true)
			{
				$name = uniqid();

				$filename = $path.($ext ? $name.'.'.$ext : $name);

				if (!file_exists($filename))
					return basename($filename);
			}
		}
	};
?>