<?
	/**
	 * Modern session class.
	 * @version 1.30
	 *
	 *----------------------------------------------------------------------------------------------
	 * 03.03.2007 version 1.28
	 * [+]	getNs() method added
	 *----------------------------------------------------------------------------------------------
	 * 14.06.2006 version 1.27
	 * [*]	_crypt made binary-safe to work correctly in mbstring.func_overload mode
	 *----------------------------------------------------------------------------------------------
	 * 26.05.2004 version 1.26
	 * [+]	_RemoveOldSessions now use glob for find session files
	 *----------------------------------------------------------------------------------------------
	 * 05.05.2004 version 1.25
	 * [+]	Cookie support added
	 *----------------------------------------------------------------------------------------------
	 * 04.02.2004 version 1.24
	 * [*]	DNSID will not change if session file is empty
	 *----------------------------------------------------------------------------------------------
	 * 26.01.2004 version 1.23
	 * [*]	small bug in InitPrivateNamespaces() fixed
	 *----------------------------------------------------------------------------------------------
	 * 08.07.2003 version 1.22
	 * [*]	typo in AssignNameSpace fixed
	 *----------------------------------------------------------------------------------------------
	 * 28.01.2003 version 1.21
	 * [*]	Exists method improved with array_key_exists
	 *----------------------------------------------------------------------------------------------
	 * 11.10.2002 version 1.20
	 * [+]	AssignNameSpace method added
	 * [+]	SetGlobal, SetGlobalNS methods added
	 *----------------------------------------------------------------------------------------------
	 * 07.10.2002 version 1.19
	 * [+]	onRemoveSession handler added
	 *----------------------------------------------------------------------------------------------
	 * 20.09.2002 version 1.18
	 * [+]	ExistsPrivate method added
	 * [-]	UsingNameSpaces bug fixed: private NS were wiped after call to it
	 *----------------------------------------------------------------------------------------------
	 * 07.09.2002 version 1.17
	 * [*]	improved _flush usage
	 *----------------------------------------------------------------------------------------------
	 * 23.08.2002 version 1.16
	 * [*]	changed retrieval of configuration data from php.ini to a hash
	 *----------------------------------------------------------------------------------------------
	 * 22.08.2002 version 1.15
	 * [-]	InitPrivateNameSpace bug fixed: warning was generated if SessionData was not array
	 *----------------------------------------------------------------------------------------------
	 * 14.08.2002 version 1.14
	 * [-]	bug fixed: __pdata__ namespace was wiped after call to UsingNameSpaces
	 *----------------------------------------------------------------------------------------------
	 * 12.08.2002 version 1.13
	 * [-]	flock bug fixed
	 *----------------------------------------------------------------------------------------------
	 */

	class DNModernSession
	{
		var $key;
		var $SessionID;
		var $SessionData;
		var $SessionIDName;
		var $SessionFilePath;
		var $SessionFilePrefix;
		var $CommonNameSpace;
		var $PrivatePrefix;
		var $PrivateNameSpaces;
		var $LifeTime;
		var $AutoSetCookie;
		var $__pdata__;
		var $is_cookie_set;
		var $mtime;
		var $_lock_fp;

		function DNModernSession($key = false, $cfg = false)
		{
			$this->key = $key;

			if(!is_array($cfg))
				$cfg = array();

			$this->SessionIDName	 = $this->_cfg ($cfg, 'id_name',	"DNSID");
			$this->SessionFilePath	 = $this->_cfg ($cfg, 'tmp_path',	"/tmp");
			$this->SessionFilePrefix = $this->_cfg ($cfg, 'tmp_prefix',	"dnsess_");
			$this->CommonNameSpace	 = $this->_cfg ($cfg, 'ns_common',	"common");
			$this->PrivatePrefix	 = $this->_cfg ($cfg, 'ns_private',	"__private__");
			$this->LifeTime 		 = $this->_cfg ($cfg, 'lifetime',	600);
			$this->onRemoveSession	 = $this->_cfg ($cfg, 'onremovesession', NULL);
			$this->UseCookies		 = $this->_cfg ($cfg, 'usecookies',	FALSE);
			$this->CookiePath		 = $this->_cfg ($cfg, 'cookiepath',	'');
			$this->CookieDomain		 = $this->_cfg ($cfg, 'cookiedomain', '');
			$this->AutoSetCookie     = $this->_cfg ($cfg, 'autosetcookie',	TRUE);
			$this->PrivateNameSpaces = array();
			$this->__pdata__ = '__pdata__';

			$this->is_cookie_set = false;

			$this->_RemoveOldSessions();
			$this->SessionData = $this->_validatesessionid() ? $this->_unser($this->_read()) : array();

			$GLOBALS[$this->SessionIDName] = $sid = $this->SessionIDName.'='.$this->SessionID;

			if ((float) phpversion() > 4.1)
				$_POST[$this->SessionIDName] = $_GET[$this->SessionIDName] = $_REQUEST[$this->SessionIDName] = $sid;

			if (!defined ($this->SessionIDName)) define($this->SessionIDName, $sid);
		}

		function SetCookie()
		{
			if ($this->UseCookies && !headers_sent())
				setcookie($this->SessionIDName, $this->SessionID, 0, $this->CookiePath, $this->CookieDomain);

			$this->is_cookie_set = true;
		}

		function RemoveCookie()
		{
			if ($this->UseCookies && !headers_sent())
				setcookie($this->SessionIDName, NULL, 0);
		}

		function HiddenSID()
		{
			if (!$this->UseCookies)
				return HiddenSID();
		}

		function SetURL($url)
		{
			return Url::setQueryParam($this->SessionIDName, $this->UseCookies ? '' : $this->SessionID, $url);
		}

		function RemoveURL($url)
		{
			return SetQueryParam($this->SessionIDName, '', $url);
		}

		function GetPrivateNameSpaceName($script_name='')
		{
			if($script_name == '') $script_name = $_SERVER['SCRIPT_NAME'];
			return $this->PrivatePrefix.$script_name;
		}

    	function InitPrivateNameSpace()
    	{
    		if(func_num_args())
    			$suffixes = func_get_args();
			else
				$suffixes = array('');

			$this->PrivateNameSpaces = array();
    		foreach($suffixes as $suffix)
    			array_push($this->PrivateNameSpaces, $this->GetPrivateNameSpaceName($suffix));

			$unset = FALSE;
    		foreach((array)$this->SessionData as $ns => $data)
    			if(strpos($ns, $this->PrivatePrefix) === 0 && !in_array($ns, $this->PrivateNameSpaces))
    			{
    				$unset = TRUE;
    				unset($this->SessionData[$ns]);
				}

			if($unset)
				$this->_flush();

			return count($this->PrivateNameSpaces) == 1 ? $this->PrivateNameSpaces[0] : $this->PrivateNameSpaces;
    	}

    	function AssignNameSpace($ds, $space = false)
    	{
			list ($space, $data) = $space === false ? array ($this->CommonNameSpace, $ds) : func_get_args();
			$this->SessionData[$space] = $data;
			$this->_flush();
    	}

		function Exists($ns, $space = false)
		{
			list ($space, $name) = $space === false ? array ($this->CommonNameSpace, $ns) : func_get_args();

			return array_key_exists($name, (array)$this->SessionData[$space]);
		}

		function ExistsPrivate($name)
		{
			return $this->Exists($this->GetPrivateNameSpaceName(), $name);
		}

		function Get($ns, $space = false)
		{
			list ($space, $name) = $space === false ? array ($this->CommonNameSpace, $ns) : func_get_args();
			return isset($this->SessionData[$space][$name]) ? $this->SessionData[$space][$name] : false;
		}

		function getNs($ns)
		{
			return $this->SessionData[$ns];
		}

		function push($ns, $space = false)
		{
			$value = $this->get($ns, $space);
			$this->wipe($ns, $space);
			return $value;
		}

		function GetPrivate($ns, $space = false)
		{
			list ($space, $name) = $space === false ? array ('', $ns) : func_get_args();
			$space = $this->GetPrivateNameSpaceName($space);
			return isset($this->SessionData[$space][$name]) ? $this->SessionData[$space][$name] : false;
		}

		function Set($ns, $nv = false, $value = false)
		{
			list ($space, $name, $value) = func_num_args() == 2 ? array ($this->CommonNameSpace, $ns, $nv) : func_get_args();
			$this->SessionData[$space][$name] = $value;

			$this->_flush();
		}

		function SetGlobal($ns, $name=false)
		{
			$Args = func_get_args();
			$this->_set_global($this->CommonNameSpace, $Args);
		}

		function SetGlobalNS()
		{
			$Args = func_get_args();
			$this->_set_global(array_shift($Args), $Args);
		}

		function SetPrivate($ns, $nv = false, $value = false)
		{
			list ($space, $name, $value) = func_num_args() == 2 ? array ('', $ns, $nv) : func_get_args();
			$space = $this->GetPrivateNameSpaceName($space);
			$this->SessionData[$space][$name] = $value;

			$this->_flush();
		}

		function WipePrivate($name)
		{
			$this->_unset($this->GetPrivateNameSpaceName(), $name);
		}

		function Wipe($ns, $name = FALSE)
		{
			$this->_unset($ns, $name);
		}

		function LoadNameSpace()
		{
			$spaces = func_num_args() ? func_get_args() : array($this->CommonNameSpace);

			foreach($spaces as $space)
			{
       			if (isset($this->SessionData[$space]))
    			foreach($this->SessionData[$space] as $name => $value)
   				$GLOBALS[$name] = $value;
			}
		}

		function GetNamespace($space)
		{
       		return $this->SessionData[$space];
		}

		function DropNameSpace()
		{
			$unset = FALSE;

			if(func_num_args())
			{
	   			foreach (func_get_args() as $space)
	   				if(isset($this->SessionData[$space]))
	   				{
	   					unset($this->SessionData[$space]);
	   					$unset = TRUE;
					}
			}
			elseif(count($this->SessionData) > 0)
			{
				$unset = TRUE;
				$this->SessionData = array();
			}

			if($unset)
				$this->_flush();
		}

		function UsingNameSpaces()	// drop all namespaces except 'common', '__private__', '__pdata__' and given in arguments
		{
			if (!is_array($this->SessionData)) return;
  			$spaces = func_num_args() ? func_get_args() : array();
 			if (func_num_args() == 1 && is_array($spaces[0])) list ($spaces) = $spaces;

  			if(!in_array($this->CommonNameSpace, $spaces))
  				array_push($spaces, $this->CommonNameSpace);
  			if(!in_array($this->__pdata__, $spaces))
  				array_push($spaces, $this->__pdata__);
  			if(count(array_intersect($this->PrivateNameSpaces, $spaces)) == 0)
  				$spaces = array_merge($spaces, $this->PrivateNameSpaces);

  			$unset = FALSE;
  			foreach(array_keys($this->SessionData) as $space)
				if(!in_array($space, $spaces) && isset($this->SessionData[$space]))
				{
					unset($this->SessionData[$space]);
					$unset = TRUE;
				}

			if($unset)
  				$this->_flush();
		}

		function MergeNameSpaces()
		{
			$target = count($args = func_get_args()) ? array_shift ($args) : NULL;
			count($args) or $args = array_keys($this->SessionData);

			$ret = array ();

			foreach($args as $ns)
			{
				if(!isset($this->SessionData[$ns]))
				trigger_error("Non-existent namespace: $ns", E_USER_WARNING); else
				$ret = array_merge($ret, $this->SessionData[$ns]);
			}

			if ($target !== NULL)
			{
				$this->SessionData[$target] = $ret;
				$this->_flush();
			}

			return $ret;
		}

		// private methods
		function _set_global($ns, $names)
		{
			foreach((array)$names as $name)
				$this->SessionData[$ns][$name] = $GLOBALS[$name];

			$this->_flush();
		}

		function _unset($ns, $name = FALSE)
		{
			list ($space, $name) = $name === FALSE ? array ($this->CommonNameSpace, $ns) : func_get_args();

			if (!isset($this->SessionData[$space][$name])) return false;

			unset($this->SessionData[$space][$name]);
			$this->_flush();

			return true;
		}

		function _validatesessionid()
		{
			if ((float) phpversion() >= 4.1)
			{
				$source = &$_REQUEST;

				if ($this->UseCookies)
					$source= &$_COOKIE;

				$this->SessionID = isset($source[$this->SessionIDName]) ? $source[$this->SessionIDName] : NULL;

			}
			else
			{
				if ($this->UseCookies)
				{
					if (isset ($GLOBALS['HTTP_COOKIE_VARS'][$this->SessionIDName]))
						$this->SessionID = $GLOBALS['HTTP_COOKIE_VARS'][$this->SessionIDName];
					elseif (isset($GLOBALS[$this->SessionIDName]))
						$this->SessionID = $GLOBALS[$this->SessionIDName];
				} else
				{
					if (isset ($GLOBALS['HTTP_GET_VARS'][$this->SessionIDName]))
						$this->SessionID = $GLOBALS['HTTP_GET_VARS'][$this->SessionIDName];
					elseif (isset ($GLOBALS['HTTP_POST_VARS'][$this->SessionIDName]))
						$this->SessionID = $GLOBALS['HTTP_POST_VARS'][$this->SessionIDName];
					elseif (isset($GLOBALS[$this->SessionIDName]))
						$this->SessionID = $GLOBALS[$this->SessionIDName];
				};
			};

			$id_length = strlen(String::uniqueId());

			if (!preg_match('/^[a-z0-9]{'.$id_length.'}$/', $this->SessionID))
			{
				do
				{
					$this->SessionID = string::uniqueId();
				}
				while (file_exists($this->_getsessionfile()));

				return FALSE;
			}

			return file_exists($this->_getsessionfile());
		}

		function _getsessionfile()
		{
			return $this->SessionFilePath.'/'.basename($this->SessionFilePrefix.$this->SessionID);
		}

		function _read()
		{
			$name = $this->_getsessionfile();
			if (!file_exists($name)) return '';

			if ($this->_lock_fp)
				$content = @fread ($this->_lock_fp, filesize ($name));
			else
			{
				$fp = @fopen ($name, 'r');
				if (!$fp) return '';
				flock ($fp, defined('LOCK_SH') ? LOCK_SH : 1, $wouldblock = TRUE);
				$content = @fread ($fp, filesize ($name));
				fclose ($fp);
			};

			$this->mtime = filemtime($name);

			return $content;
		}

		function _flush()
		{
			if ($this->AutoSetCookie && !$this->is_cookie_set)
				$this->SetCookie();

			$name = $this->_getsessionfile();
			// Debug::varDump($name);
			if ($this->_lock_fp)
			{
				fseek($this->_lock_fp, 0);
				ftruncate($this->_lock_fp, 0);
				fwrite ($this->_lock_fp, $this->_ser($this->SessionData));
			} elseif ($fp = @fopen ($name, 'w'))
			{
				flock  ($fp, defined('LOCK_EX') ? LOCK_EX : 2, $wouldblock = TRUE);
				fwrite ($fp, $this->_ser($this->SessionData));
				fclose ($fp);
			};
		}

		function _unser($data)
		{
			if ($this->key !== false) $data = $this->_crypt($data, 0);
			return unserialize ($data);
		}

		function _ser($data)
		{
			$data = serialize ($data);
			if ($this->key !== false) $data = $this->_crypt($data, 1);
			return $data;
		}

		function _readIfChanged()
		{
			$name = $this->_getsessionfile();

			clearstatcache();

			if (filemtime($name) != $this->mtime)
				$this->_read();
		}

		function lock()
		{
			$name = $this->_getsessionfile();
			if (!file_exists($name)) return false;

			$this->_lock_fp = @fopen ($name, 'r+');
			if (!$this->_lock_fp) return false;
			flock ($this->_lock_fp, defined('LOCK_EX') ? LOCK_EX : 2, $wouldblock = TRUE);

			$this->_readIfChanged();

			return true;
		}

		function unlock()
		{
			if (!$this->_lock_fp)
				return false;

			fclose($this->_lock_fp);
			unset($this->_lock_fp);

			return true;
		}

		function _RemoveOldSessions()
		{
//			$dir = dir($this->SessionFilePath);
//			$len = strlen ($this->SessionFilePrefix);

			$filePathStrlen = strlen($this->SessionFilePrefix) + strlen($this->SessionFilePrefix) + 1;

			$files = glob($this->SessionFilePath.'/'.$this->SessionFilePrefix.'*');

//			while ($entry = $dir->read())
			if ($files)
				foreach ($files as $fullname)
//				if ($entry != '.' && $entry != '..' && substr ($entry, 0, $len) == $this->SessionFilePrefix)
				{
//					$fullname = $this->SessionFilePath.'/'.$entry;
					if (is_file($fullname) && is_writable($fullname) && abs(time() - fileatime($fullname)) > $this->LifeTime)
					{
						if (!is_null($this->onRemoveSession))
							call_user_func($this->onRemoveSession, $fullname, substr($fullname, $filePathStrlen));

						@unlink($fullname);
					}
				}

//			$dir->close();
		}

		function _crypt($data, $dir = 1)	// RC4. dir = 1 - encrypt
		{
			$key    = $box = array ();
			$keylen = strlen ($this->key);

			for ($i = 0; $i <= 255; $i++)
			{
				$key[$i] = ord(substr($this->key, ($i % $keylen), 1));
				$box[$i] = $i;
			}

			for ($x = $i = 0; $i <= 255; $i++)
			{
				$x = ($x + $box[$i] + $key[$i]) % 256;
				list ($box[$i], $box[$x]) = array ($box[$x], $box[$i]);
			}

			$k = $cipherby = $cipher = '';

			for ($a = $j = $i = 0; $i < strlen($data); $i++)
			{
				$a = ($a + 1) % 256;
				$j = ($j + $box[$a]) % 256;

				list ($box[$a], $box[$j]) = array ($box[$j], $box[$a]);
				$k = $box[(($box[$a] + $box[$j]) % 256)];
				$cipherby = ord(substr($data, $i, 1)) ^ $k;
				$cipher .= chr($cipherby);
			}

			return $cipher;
		}

		function _cfg ($cfg, $name, $default = '')
		{
			return isset($cfg[$name]) ? $cfg[$name] : $default;
		}
	}
?>