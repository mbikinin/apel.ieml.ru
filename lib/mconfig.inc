<?
	//----------------------------------------------------------------------------------------------
	// Modern Config parser v0.65
	//----------------------------------------------------------------------------------------------
	// 04.12.2003 version 0.65
	// [*]	revalidate the cache if it is empty improved in read()
	//----------------------------------------------------------------------------------------------
	// 24.10.2003 version 0.64
	// [*] {$->ns.var} "special value" var bug fixed
	//----------------------------------------------------------------------------------------------
	// 13.02.2003 version 0.63
	// [+] existsnamespace added
	//----------------------------------------------------------------------------------------------
	// 12.02.2003 version 0.62
	// [*] addslashes bug fixed
	//----------------------------------------------------------------------------------------------
	// 06.12.2002 version 0.61
	// [+] vars2extract() method added
	//----------------------------------------------------------------------------------------------
	// 28.10.2002 version 0.60
	// [*] new read/write concept (save original syntax)
	// [+] {$->ns.var} construct
	//----------------------------------------------------------------------------------------------
	// 24.09.2002 version 0.52
	// [*] _expand_var bug fixed
	//----------------------------------------------------------------------------------------------
	// 14.08.2002 version 0.51
	// [+] add new hash syntax
	//----------------------------------------------------------------------------------------------
	// 13.08.2002 version 0.50
	// [*] change cache schema
	// [+] new 'read' method added
	//----------------------------------------------------------------------------------------------
	// 09.08.2002 version 0.41
	// [+] config cache, many-dimensional arrays, array keys typecast, add 'dropnamespace' function
	// [*] change constructor args, change local server detect schema
	//----------------------------------------------------------------------------------------------
	// 08.08.2002 version 0.30
	// [+] eval simple check
	// [-] remove 'serialized' format
	//----------------------------------------------------------------------------------------------
	// 02.08.2002 version 0.20
	// [*] optimization for PHP < 4.1.0, lock pre-implementation
	//----------------------------------------------------------------------------------------------
	// 01.08.2002 version 0.10 - first version
	//----------------------------------------------------------------------------------------------

	class DN_Config
	{
		var $config;
		var $cached;
		var $struct, $rstruct;
		var $server;
		var $err;

		function DN_Config ($config_path = false)
		{
			if ($config_path === false) $config_path = dirname(__FILE__).'/..';

			$this->config = $config_path.'/config_crude.inc';
			$this->cached = $config_path.'/config_cache.inc';
			$this->err	  = 0;
			$this->_determineserver();

			$this->read();
		}

		function read()
		{
			if (file_exists ($this->config))
			{
				if (file_exists ($this->cached) && @filemtime ($this->config) <= @filemtime ($this->cached))
				{
					include $this->cached;

					if(count($struct) > 0)
					{
						$this->struct = $struct;
						$this->rstruct = $rstruct;
						return;
					}
				}

				$fp = @fopen ($this->config, 'r');
				flock($fp, LOCK_SH);
				if (!$fp) return $this->_err(1);
				$cont = fread ($fp, filesize ($this->config));
				fclose ($fp);
				$this->_parse ($cont);
			}
			else
			{
				if (!@touch ($this->config)) return $this->_err(2);
				$this->struct = array ();
			}
		}

		function getnamespaces()
		{
			$spaces = array_keys ($this->struct);
			if (!in_array ('_common', $spaces)) $spaces[] = '_common';
			return $spaces;
		}

		function existsnamespace($ns)
		{
			return array_key_exists($ns, $this->struct);
		}

		function exists()
		{
			switch (func_num_args())
			{
				case 0: return false;

				case 1: list ($var) = func_get_args();
						$space  = '_common';
						$server = $this->server;
						break;

				case 2: list ($space, $var) = func_get_args();
						$server = $this->server;
						break;

				default:list ($server, $space, $var) = func_get_args();
						return isset ($this->struct[$space][$var][$server]);
			}

			if ($server == '*' || !isset ($this->struct[$space][$var]['!']))
			return isset ($this->struct[$space][$var]['*']);
			return isset ($this->struct[$space][$var]['!']);
		}

		function set()
		{
			switch (func_num_args())
			{
				case 0:
				case 1: return false;

				case 2: list ($var, $value) = func_get_args();
						$space  = '_common';
						$server = $this->server;
						break;

				case 3: list ($space, $var, $value) = func_get_args();
						$server = $this->server;
						break;

				default:list ($server, $space, $var, $value) = func_get_args();
						break;
			}

			if (!isset ($this->struct[$space][$var]['x']) || ! (int) $this->struct[$space][$var]['x'])
			{
				$this->struct[$space][$var][$server] = $value;
				$this->rstruct[$space][$var][$server] = $this->_expand_var($value);
			};
			$this->_flush ();

			return true;
		}

		function get()
		{
			switch (func_num_args())
			{
				case 0: return false;

				case 1: list ($var) = func_get_args();
						$space  = '_common';
						$server = $this->server;
						break;

				case 2: list ($space, $var) = func_get_args();
						$server = $this->server;
						break;

				default:list ($server, $space, $var) = func_get_args();
						return $this->struct[$space][$var][$server];
			}

			if ($server == '*' || !isset ($this->struct[$space][$var]['!']))
			return $this->struct[$space][$var]['*'];

			return $this->struct[$space][$var]['!'];

		}

		function wipe()
		{
			switch (func_num_args())
			{
				case 0: return false;

				case 1: list ($var) = func_get_args();
						$space  = '_common';
						break;

				case 2: list ($space, $var) = func_get_args();
						break;

				default:list ($server, $space, $var) = func_get_args();
						unset($this->struct[$space][$var][$server]);
						break;
			}

			unset ($this->struct[$space][$var]);
		}

		function vars2globals()
		{
			switch (func_num_args())
			{
				case 0:  $prefix = '';
						 $spaces = array ('_common');
						 break;

				case 1:  list ($prefix) = func_get_args();
						 $spaces = array ('_common');
						 break;

				default: list ($prefix) = func_get_args();
						 $spaces = array_slice (func_get_args(), 1);
						 break;
			}

			foreach ($spaces as $space)
			foreach ($this->struct[$space] as $var=>$val)
			$GLOBALS[$prefix.$var] = $this->get($space, $var);
		}

		function vars2extract()
		{
			$spaces = func_get_args();
			$ret = array();

			foreach($spaces as $space)
				foreach($this->struct[$space] as $var=>$val)
					$ret[$var] = $this->get($space, $var);

			return $ret;
		}

		function globals2vars ($prefix = '', $space = '_common')
		{
			$plen = strlen ($prefix);

			foreach ($GLOBALS as $key => $val)
			if ($key <> 'GLOBALS' && ($prefix == '' || substr ($key, 0, $plen) == $prefix))
			$this->set ($space, substr ($key, $plen), $val);
		}

		function dropnamespace()
		{
			foreach (func_get_args() as $space)
			unset ($this->struct[$space]);

			$this->_flush();
		}

		// Private methods

		function _err ($err = false)
		{
			if ($err === false) return $this->err; else $this->err = $err;
		}

		function _parse (&$cont)
		{
			$struct  = $rstruct = array ();
			$space   = '_common';
			$cur_var = '_noname';
			$foreval = '';

			$evals = array();

			$token = strtok ($cont, "\n");
			while ($token !== false)
			{
				if ($token <> '')
				switch ($token[0])
				{
					case '[':		$space = substr ($token, 1, -1); break;

//					case "\t":		list ($subsect, $value) = split ("\t+", ltrim ($token), 2);
					case "\t":		list ($subsect, $value) = preg_split ("/\s+/", ltrim ($token), 2);
									$evals[] = "$space.$cur_var.$subsect";
									$rstruct[$space][$cur_var][$subsect] = $value;
									break;

//					default:		$p_token = split ("\t+", $token, 2);
					default:		$p_token = preg_split ("/\s+/", $token, 2);
									if (count($p_token) == 1) $cur_var = trim ($token); else
									{
										list ($cur_var, $value) = $p_token;
										if (strchr ($cur_var, '.') === false)
										{
											$rstruct[$space][$cur_var]['*'] = $value;
											$evals[] = "$space.$cur_var.*";
										};
									};
									break;
				}

				$token = strtok ("\n");
			};

			$struct = $rstruct;
			$foreval = '';

			for ($i=0; $i<count($evals); $i++)
			{
				do
				{
					$needredo = false;
					list ($space, $var, $subsect) = explode('.', $evals[$i]);
					$value = $struct[$space][$var][$subsect];

					if (preg_match_all("/\{\\\$->([^}]+)\}/", $value, $r))
					{
						foreach ($r[1] as $used_var)
						{
							list ($uv_space, $uv_var) = explode('.', $used_var);

#							$uv_subsect = ($this->server == '*' || !isset ($this->struct[$space][$var]['!']))?'*':'!';
							$uv_subsect = ($this->server == '*' || !isset ($struct[$uv_space][$uv_var]['!']))?'*':'!';

							$uv = "$uv_space.$uv_var.$uv_subsect";

							$j = array_search($uv, $evals);
							if (is_null($j) || $j === false)
								die ("<b>Unknown variable $used_var</b>");

							$value = $struct[$space][$var][$subsect] = str_replace("{\$->$used_var}", "\$this->get('$uv_space', '$uv_var')", $value);

							if ($j>$i)
							{
								array_splice($evals, $j, 1);
								array_splice($evals, $i, 0, array($uv));
								$needredo = true;
							};
						};
					};
				} while ($needredo);
				$foreval .= "\$struct['$space']['$var']['$subsect'] = $value;";
			};

			$struct = &$this->struct;
			$struct = array();

			@eval ($foreval);

			$this->rstruct = $rstruct;
			$this->_flushcache ('$struct = '.$this->_expand_var($struct).';$rstruct = '.$this->_expand_var($rstruct));
		}

		function _expand_var($arr)
		{
			switch (gettype($arr))
			{
				case 'boolean': return $arr ? 'true' : 'false';
				case 'string':	return "'".addcslashes($arr, "'")."'";
				case 'NULL':	return 'null';
				case 'array':	if (count($arr))
								{
									$str_arr = array();
									if (count($arr) == 1 && isset($arr[0])) return "array (".$this->_expand_var($arr[0]).")";
									foreach ($arr as $key=>$val)
									$str_arr[] = (is_string($key) ? "'".addslashes($key)."'" : $key)." => ".$this->_expand_var($val);

									return 'array ('.implode (', ', $str_arr).')';
								}
								else
								return 'array()';

				default:		return $arr;
			}
		}

		function _rpack(&$cont)
		{
			$str = $space = $cur_var = '';

			foreach ($cont as $ns=>$ns_val)
			foreach ($ns_val as $var=>$var_val)
			{
				foreach ($var_val as $sub=>$sub_val)
				{
					if ($space <> $ns) $str .= '['.($space = $ns)."]\n";

					if (count($var_val) == 1)
					{
						$str .= ($cur_var = $var);
						if (strlen($var)<4) $str .= "\t";
						$str .= "\t\t$sub_val\n";
					} else
					{
						if ($var <> $cur_var) $str .= ($cur_var = $var)."\n";
						$str .= "\t$sub\t\t$sub_val\n";
					}
				}
			}

			return $str;
		}

		function _determineserver()
		{
			global $DN_SERVER;
			$serv_switch = (float) phpversion() >= 4.1 && isset($_ENV['DN_SERVER']) || isset($DN_SERVER);

			$this->server = $serv_switch ? '*' : '!';
		}

		function _flush()
		{
			@unlink($this->cached);

			$fp = @fopen ($this->config, 'w');
			if (!$fp) return $this->_err(3);
			flock($fp, LOCK_EX);

			fwrite ($fp, $this->_rpack ($this->rstruct));
			fclose ($fp);

			return true;
		}

		function _flushcache($cont)
		{
			$fp = @fopen ($this->cached, 'w');
			if (!$fp) return $this->_err(3);
			flock($fp, LOCK_EX);

			fwrite ($fp, "<?{$cont}?>");
			fclose ($fp);

			return true;
		}
	}
?>