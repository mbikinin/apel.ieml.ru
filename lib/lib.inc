<?php
	function h()
	{
		$args = func_get_args();
		return call_user_func_array('htmlspecialchars', $args);
	}

	function nh($string)
	{
		return nl2br(h($string));
	}

	function noData($r, $cols, $message = 'Нет данных')
	{
		return $r === null ? '<tr class="r0"><td colspan="'.$cols.'" align="center">'.$message.'</td></tr>' : '';
	}

	function intbool($value)
	{
		return (int)(bool)$value;
	}

	function Read_file_contents($filename)
	{
		return file_get_contents($filename);
	}

	function force_no_cache()
	{
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");				// Date in the past
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");		// always modified
		header("Cache-Control: no-store, no-cache, must-revalidate");	// HTTP/1.1
		header("Cache-Control: post-check=0, pre-check=0", FALSE);
		header("Pragma: no-cache");										// HTTP/1.0
	}


	if(!function_exists('get_called_class'))
	{
		function get_called_class($bt = false,$l = 1)
		{
			if (!$bt) $bt = debug_backtrace();
			if (!isset($bt[$l])) throw new Exception("Cannot find called class -> stack level too deep.");
			if (!isset($bt[$l]['type'])) {
				throw new Exception ('type not set');
			}
			else switch ($bt[$l]['type']) {
				case '::':
					$lines = file($bt[$l]['file']);
					$i = 0;
					$callerLine = '';
					do {
						$i++;
						$callerLine = $lines[$bt[$l]['line']-$i] . $callerLine;
					} while (stripos($callerLine,$bt[$l]['function']) === false);
					preg_match('/([a-zA-Z0-9\_]+)::'.$bt[$l]['function'].'/',
						$callerLine,
						$matches);
					if (!isset($matches[1])) {
						// must be an edge case.
						throw new Exception ("Could not find caller class: originating method call is obscured.");
					}
					switch ($matches[1]) {
						case 'self':
						case 'parent':
							return get_called_class($bt,$l+1);
						default:
							return $matches[1];
					}
					// won't get here.
				case '->': switch ($bt[$l]['function']) {
					case '__get':
						// edge case -> get class of calling object
						if (!is_object($bt[$l]['object'])) throw new Exception ("Edge case fail. __get called on non object.");
						return get_class($bt[$l]['object']);
					default: return $bt[$l]['class'];
				}

				default: throw new Exception ("Unknown backtrace method type");
			}
		}
	}

	function size($bytes, $precision = 2)
	{
		$units = array('Б', 'КБ', 'МБ', 'ГБ', 'ТБ');
		$converted = $bytes . ' ' . $units[0];
		for ($i = 0; $i < count($units); $i++)
		{
			if (($bytes/pow(1024, $i)) >= 1)
			{
				$number = sprintf("%.{$precision}f", $bytes/pow(1024, $i));
				$converted = $number . ' ' . $units[$i];
			}
		}
		return $converted;
	}
	
	function get_value($array, $key)
	{
		return $array[$key];
	}