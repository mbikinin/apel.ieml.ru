function JsTreeNode(data)
{
	this.container_prefix = '';

	for (var n in data)
		this[n] = data[n];
}

JsTreeNode.prototype.getIdOfPart = function(suffix)
{
	return this.container_prefix + '-' + this.id + suffix;
}

JsTreeNode.prototype.getIdOfPm = function()
{
	return this.getIdOfPart('-pm');
}

JsTreeNode.prototype.getIdOfCC = function()
{
	return this.getIdOfPart('');
}

JsTreeNode.prototype.getIdOfAc = function()
{
	return this.getIdOfPart('-ac');
}

JsTreeNode.prototype.getIdOfAdmSelect = function()
{
	return this.getIdOfPart('-ads');
}