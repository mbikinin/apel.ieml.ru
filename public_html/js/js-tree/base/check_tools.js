function JsCheckTreeTools()
{
	this.tools = [];
	this.tool_path = '/design/images/tree/';
}

JsCheckTreeTools.prototype.add = function(params)
{
	this.tools.push(this.create(params));
}

JsCheckTreeTools.prototype.create = function(params)
{
	var input = document.createElement('input');
	input.type = 'checkbox';
    input.name = 'nodes[' + params.id + ']';

    if (params.show_module)
        input.checked = true;

	return input;
}