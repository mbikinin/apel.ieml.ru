<?php

class Mic extends ActiveRecord_Translation
{
	public $lang_fields = array(
		'issn',
		'powered',
		'footer',
		'notice_proposal_1',
		'notice_proposal_2',
		'header',
		'keyword',
		'abstract',
		'literature',
		'form_fio',
		'form_email',
		'form_phone',
		'form_address',
		'form_number',
		'form_issues',
		'form_document',
		'form_copy',
		'form_subscriber',
		'form_send',
		'form_pub'
	);
	
	function __construct()
	{
		parent::__construct();
		
		$this->hasFileField('main_picture');
	}
}