<?php
	class News extends ActiveRecord_Translation
	{
		public $lang_fields = array(
			'title',
			'preview',
			'content'
		);
	
		function __construct()
		{
			parent::__construct();
			
			$this->hasDateField('date', array('format' => 'd.m.Y'));
		}
	}
?>