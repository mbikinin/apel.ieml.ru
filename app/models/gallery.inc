<?php

class Gallery extends ActiveRecord
{
    protected function initRelations()
    {
        $this->hasMany('photos', array('class_name' => 'GalleryPhoto', 'foreign_key' => 'gallery_id'));
    }
}