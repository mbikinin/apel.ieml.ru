<?php

class Archive_Literature extends ActiveRecord_Translation
{
	protected $order_by = 'name desc';
	
	public $lang_fields = array(
		'name'
	);
	
	public function processName()
	{
		// $this->lang = Lang::rus;
		return $this->name;
		return preg_replace("(.* URL\:)(.*) (.*)", "$1<a href=$2>$2</a>$3", $this->name);
	}
}