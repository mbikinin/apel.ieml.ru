<?php

class Archive_Issue extends ActiveRecord_Translation
{
	protected $order_by = 'id desc';
	
	public $lang_fields = array(
		'title',
		'jtitle',
		'jabbrev',
		'jpissn',
		'jeissn',
		'jvol',
		'jissue',
		'idoi',
		'jpubyearp',
		'jpubmop',
		'jpubdayp',
		'jpubyearo',
		'jpubmoo',
		'jpubdayo',
	);
	
	function __construct()
	{
		parent::__construct();
		
		$this->hasFileField('file');
	}
	
	protected function initRelations()
	{
		$this->belongsTo('year', array('foreign_key' => 'archive_year_id', 'class_name' => 'Archive_Year'));
		$this->hasMany('articles', array('foreign_key' => 'archive_issue_id', 'class_name' => 'Archive_Article'));		
	}
	
	protected function beforeDrop()
	{
		foreach($this->articles as $a)
			$a->drop();
	}
}