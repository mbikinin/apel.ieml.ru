<?php

class Archive_Article extends ActiveRecord_Translation
{
	public $lang_fields = array(
		'title',
		'udk',
		'abstract',
		'atitle',
		'aOrigLangTitle',
		'aOrigLangText',
		'aOrigLangTextEng',
		'adoi',
		'keywords',
		'citation'
	);
	function __construct()
	{
		parent::__construct();
	}
	protected $order_by = 'archive_issue_id asc';
	
	protected function initRelations()
	{
		$this->belongsTo('issue', array('foreign_key' => 'archive_issue_id', 'class_name' => 'Archive_Issue'));
		$this->belongsTo('section', array('foreign_key' => 'archive_section_id', 'class_name' => 'Archive_Section'));
		$this->hasMany('authors', array('foreign_key' => 'archive_article_id', 'class_name' => 'Archive_Author'));
		$this->hasMany('keywords', array('foreign_key' => 'archive_article_id', 'class_name' => 'Archive_Keyword'));
		$this->hasMany('literatures', array('foreign_key' => 'archive_article_id', 'class_name' => 'Archive_Literature'));
	}
	
	public function getAuthorsHash()
	{
		$find_params = array(
			'select' => 'aa.id as id, t.value as fio',
			'table_alias' => 'aa',
			'@join' => "INNER JOIN translations t ON t.tuple_id = aa.id AND t.field_name = 'fio' AND table_name = 'archive_authors'",
			'where' => join(' AND ', array(
				'aa.archive_article_id = ' . $this->id,
				"t.lang = '" . $this->lang . "'"
			)),
			'order_by' => '',
			'fetch_as' => self::FETCH_AS_HASH,
			'simplify' => 1,
			'hash_level' => 1
		);
		
		return self::factory('Archive_Author')->findAll($find_params);
	}
	
	public function getKeywordsHash()
	{
		$find_params = array(
			'select' => 'aa.id as id, t.value as string',
			'table_alias' => 'aa',
			'@join' => "INNER JOIN translations t ON t.tuple_id = aa.id AND t.field_name = 'string' AND table_name = 'archive_keywords'",
			'where' => join(' AND ', array(
				'aa.archive_article_id = ' . $this->id,
				"t.lang = '" . $this->lang . "'"
			)),
			'order_by' => '',
			'fetch_as' => self::FETCH_AS_HASH,
			'simplify' => 1,
			'hash_level' => 1
		);
		
		return self::factory('Archive_Keyword')->findAll($find_params);
	}
	
	protected function beforeDrop()
	{
		foreach($this->authors as $a)
			$a->drop();
			
		foreach($this->keywords as $k)
			$k->drop();
			
		foreach($this->literatures as $l)
			$l->drop();
	}
}