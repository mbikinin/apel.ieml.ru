<?php
	abstract class JsTree_BaseHelper
	{
		public
			$js_sources = array('base/node', 'base/tools', 'base/base', 'base/check_tools'),
			$container_prefix,
			$js_class_name,
			$js_var_name,
			$init_func,
			$expand_nodes = array(),
			$tree_root_id,
			$potential_admins = array(),
			$urls = array(),
            $tools_class = 'JsTreeTools';

		public static
			$Inflector;

		function __construct(MysqlTreeActiveRecord_Translation $object = null)
		{
			$class = preg_replace('/^JsTree_(\w+)Helper$/i', '$1', get_class($this));

			if (!$this->js_class_name)
				$this->js_class_name = "Js{$class}Tree";

			if (!$this->js_var_name)
				$this->js_var_name = "g_{$class}";

			if (!$this->container_prefix)
				$this->container_prefix = "{$class}-container";

			if (!$this->init_func)
				$this->init_func = "init{$class}Tree";

			if (is_object($object))
			{
                $this->js_sources[] = strtolower(get_class($object)) . '_tree';

				$Inflector = new Inflector();
				$class_name = $Inflector->underscore(get_class($object));
				$dashed_class_name = str_replace('_', '-', $class_name);

				$this->tree_root_id = $object->parent_id;

				$this->urls = array(
					'tool_base'		=> "/admin/$dashed_class_name/",
					'load_data'		=> "/admin/$dashed_class_name/load-data/",
					'update_tree'	=> "/admin/$dashed_class_name/update-tree/",
					'add_admin'		=> "/admin/$dashed_class_name/add-admin/",
					'remove_admin'	=> "/admin/$dashed_class_name/remove-admin/"
				);
			}
		}

		function out()
		{
			$this->renderJs();
			$this->renderContainer();
			$this->renderInit();
		}

		protected function renderJs()
		{
			$this->render('js');
		}

		protected function renderContainer()
		{
			$this->render('container');
		}

		protected function renderInit()
		{
			$this->render('init');
		}

		protected function render($file_name, $vars = array())
		{
			@extract($vars);
			require_once FWK_LAYOUTS."js-tree/$file_name.inc";
		}

		protected function extraInitStuff()
		{

		}
	}