<?php

class Show extends Reference
{
    const no = 0;
	const yes = 1;

    function __construct()
    {
        $this->list = array(
            self::no => 'Нет',
            self::yes => 'Да'
        );
    }
}