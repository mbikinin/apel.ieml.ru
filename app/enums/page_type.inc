<?php

class PageType extends Reference
{
	const link = 1;
    const html = 2;

    public $alias;

    function __construct()
    {
        $this->list = array(
            self::link => 'Ссылка',
            self::html => 'HTML страница'
        );

        $this->alias = array(
            self::link => 'link',
            self::html => 'html'
        );
    }
}