<?php

class ProposalType extends Reference
{
	const publication = 1;
	const subscription = 2;

	function __construct()
	{
		$this->list = array(
			self::publication => 'Заявки на публикацию',
			self::subscription => 'Заявки на подписку'
		);
	}
}