<?php

class Permission extends Reference
{
	const admin = 1;
    const user = 2;

    function __construct()
    {
        $this->list = array(
            self::admin => 'Администратор',
            self::user => 'Пользователь'
        );
    }
}