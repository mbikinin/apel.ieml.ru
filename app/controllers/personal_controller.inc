<?php

class PersonalController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();

        if (!$this->user || !$this->user->isUser())
        {
            $backurl = $this->Hlp->urlFor();
			$this->redirectTo(array('controller' => '/auth', 'action' => 'login', 'backurl' => $backurl));
        }
    }

    public function index()
    {
        $this->redirectTo(array('controller' => '/personal/index'));
    }
}
