<?php

class AppController extends ActionController
{
	public $layout = '/public';
	public $page_title = '';
    public $user;
	public $lang;
	public $mic;
	public $confirm = "return confirm('Вы уверены ?');";
	public static $global_class;

	protected $current_page;
	protected $list_page_params = array('action' => 'index');

	protected function onLoad()
	{
		parent::onLoad();
        $this->user = Logon::getLoggedUser();
        $this->current_page instanceof Page or $this->current_page = new Page();
		$this->mic = ActiveRecord::factory('Mic')->findFirst();

        $this->Hlp->needJs('jquery.js');
        $this->Hlp->needJs('lib.js');        
		$this->Hlp->needCss('reset.css');
		$this->Hlp->needCss('apel.css');
	}
	
	public function beforeFilter()
	{
		parent::beforeFilter();

		if (isset($this->params['debug']))
		{
			$GLOBALS['Con']->debug = 1;
		}
		
		$this->recognizeLang();
	}

	/**
	* Alias for edit-action
	*/
	public function add($params)
	{
		$this->redirectTo(array('action' => 'edit'));
	}

    public function hasModules($area)
    {
        $find_params = array(
            'select' => 'm.*',
            'table_alias' => 'm',
            'where' => join(' AND ', array(
                'm.area = :area',
				'm.show = :show',
                '(mp.page_id = :page_id OR mp.page_id IS NULL)',
            )),
            '@join' => 'INNER JOIN modules_pages as mp ON mp.module_id = m.id',
            'params' => array(
                'area' => $area,
                'page_id' => $this->current_page->id,
				'show' => 1
            )
        );

        return (bool)ActiveRecord::factory('Module')->count($find_params);
    }

    public function loadModules($area)
    {
        $find_params = array(
            'select' => 'm.*',
            'table_alias' => 'm',
            'where' => join(' AND ', array(
                'm.area = :area',
				'm.show = :show',
                '(mp.page_id = :page_id OR mp.page_id IS NULL)',
            )),
            '@join' => 'INNER JOIN modules_pages as mp ON mp.module_id = m.id',
            'params' => array(
                'area' => $area,
                'page_id' => $this->current_page->id,
				'show' => 1
            ),
			'order_by' => 'ordi'
        );

        $modules = ActiveRecord::factory('Module')->findAll($find_params);

        foreach ($modules as $m)
            $this->renderPartial('/layouts/partials/' . ModuleType::instance()->alias[$m->type], array('module' => $m));

//        return $html;
    }

    public function loadLeft()
    {
        $this->renderPartial('/layouts/left_menu');
    }
	
	protected function recognizeLang()
	{
		$Lang = Lang::instance();
		if (isset($this->params['lang']) && $Lang->isValid($this->params['lang']))
		{
			$this->lang = $this->params['lang'];
		}
		else
		{
			$this->lang = Lang::$default_lang;
		}
		
		// ActiveRecord_Translation::$current_lang = $this->lang;Debug::varDump(ActiveRecord_Translation::$current_lang, false);
	}
}
