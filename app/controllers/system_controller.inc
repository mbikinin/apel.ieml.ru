<?php

class SystemController extends AppController
{
	
	public function flushcache($params)
	{
		$mem = ActiveRecord::$Memcache;
		echo $mem->flush();

		exit;
	}

	public function hashedPassword($params)
	{
		$password = $params['password'];
		$hashed_password = sha1($params['password']);

		Debug::varDump($password, $hashed_password);
	}

	public function i($params)
	{
		$this->layout = false;
		phpinfo();
	}
}