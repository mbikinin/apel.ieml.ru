<?php

class NewsController extends AppController
{
    public $News;

    protected function onLoad()
    {
        parent::onLoad();

        $this->News = new News();
    }
	
	public function index($params)
	{
		$this->current_page = ActiveRecord::factory('Page')->find(37);
		$this->page_title = $this->current_page->name;
	
		list($pages, $list) = Paginator::paginate($params, $this->News, array('order_by' => 'date desc'));
		
		return compact('pages', 'list');
	}
	
	public function view($params)
	{
		if (!$params['id'] || !($news = $this->News->find($params['id'])))
			$this->goBack();
	
		$this->current_page = ActiveRecord::factory('Page')->find(37);
		$this->page_title = $this->current_page->name;
		
		return compact('news');
	}
}

?>