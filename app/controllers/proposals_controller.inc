<?php

class ProposalsController extends AppController
{
    public $Proposal;

    protected function onLoad()
    {
        parent::onLoad();

        $this->Proposal = new Proposal();
    }
	
	public function send($params)
	{
		if (getenv('REQUEST_METHOD') == 'POST')
		{
			$proposal = $this->Proposal->create($params);
		
			if ($proposal->saveFromPost())
			{
				$this->sendMail($proposal);
			
				self::$Session->set('proposals', 'notice', $this->mic->{'notice_proposal_' . $params['type']});
				$this->goBack();
			}
			else
			{
				self::$Session->set('proposals', 'error', '<p style="color:red">' . $proposal->getAllFieldErrorsAsString() . '</p>');
				self::$Session->set('proposals', 'attrs', $proposal->getAttributes());
				$this->goBack();
			}
		}
	}
	
	protected function sendMail($proposal)
	{
		$MailAddressList = new MailAddressList();
		$MailAddressList->add($this->mic->email);
		
		$MailMessage = new MailMessage();
		$MailMessage->to = $MailAddressList;
		$MailMessage->from = new MailAddress('apel.ieml.ru');
		$MailMessage->subject = 'Новая заявка';
		$MailMessage->body = 'Новая заявка "' . $proposal->r_type . '" от ' . $proposal->fio;
		$MailMessage->send();
	}
}

?>