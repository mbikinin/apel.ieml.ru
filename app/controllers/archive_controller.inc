<?php

class ArchiveController extends AppController
{
    public
		$Year,
		$year;

    protected function onLoad()
    {
        parent::onLoad();
		$this->current_page = ActiveRecord::factory('Page')->findByUrl('/archive');
        $this->Year = new Archive_Year();
    }
	
	public function index($params)
	{
		$this->redirectTo(array('action' => 'years', 'lang' => $this->lang));
	}
	
	public function years($params)
	{	
		if (!($year = $this->Year->findByName($params['id'])))
			$year = $this->Year->findFirst();
		
		$this->page_title = ($this->lang == Lang::rus ? "Архив номеров за {$year->name} год" : 'Archive ' . $year->name);
		$this->year = $year;
		
		if (!$params['issue_id'])
			$this->params['issue_id'] = reset($year->issues)->id;
		
		return compact('year');
	}

	public function show($params)
	{
		$article = ActiveRecord::factory('Archive_Article')->find($params['id']);
		return compact('article');
	}

	public function article($params)
	{
		if (!$params['id'] || !($article = ActiveRecord::factory('Archive_Article')->find($params['id'])))
			$this->errorBack('Запись не найдена');
			
		$this->page_title = ($this->lang == Lang::rus ? "Архив - номер {$article->issue->title} за {$article->issue->year->name} год" : 'Archive - issue ' . $article->issue->title);
			
		return compact('article');
	}
	
	public function search($params)
	{
		$this->page_title = 'Поиск';
		
		$find_params = array(
			'table_alias' => 'a',
			'select' => 'DISTINCT a.*',
			'@join' => join(' ', array(
				'INNER JOIN archive_authors aa ON aa.archive_article_id = a.id',
				"INNER JOIN translations t1 ON t1.table_name = 'archive_authors' AND t1.field_name = 'fio' AND t1.tuple_id = aa.id",
				'INNER JOIN archive_keywords ak ON ak.archive_article_id = a.id',
				"INNER JOIN translations t2 ON t2.table_name = 'archive_keywords' AND t2.field_name = 'string' AND t2.tuple_id = ak.id",
			)),
			'where' => join(' OR ', array(
				"t1.value LIKE ('%{$params[string]}%')",
				"t2.value LIKE ('%{$params[string]}%')",
			))
		);
		
		$Article = new Archive_Article();
		$articles = $Article->findAll($find_params);
		
		return compact('articles');
	}
}

?>