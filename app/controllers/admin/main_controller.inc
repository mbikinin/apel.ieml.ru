<?
	class Admin_MainController extends AdminController
	{
		public $Module;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Module = new Module();
		}

        public function edit($params)
		{
            $this->page_title = 'Редактирование главной страницы';

            $Form = $this->createFormHelper($module);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

                $module->_item = $params['module']['item'];

				if ($module->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}
			elseif($params['id'])
			{
				$Form->params['module']['item'] = $module->item->getAttributes();
			}

            $js_tree_helper = new JsTree_PageHelper(new Page);
            $js_tree_helper->tools_class = 'JsCheckTreeTools';

			return compact('Form', 'js_tree_helper');
		}
	}
?>