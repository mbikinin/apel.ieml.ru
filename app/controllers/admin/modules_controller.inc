<?
	class Admin_ModulesController extends AdminController
	{
		public $Module;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Module = new Module();
		}

        public function beforeFilter()
        {
            parent::beforeFilter();
            
            if ($this->action == 'edit')
            {
                unset(Helper::$sources['script'][Helper::$js_base . '/jquery.js']);
                $this->Hlp->needJs('prototype.js');
            }
        }

        public function index($params)
        {
            $this->page_title = 'Модули';

            $modules = $this->Module->findAll(array('where' => 'id NOT IN (2, 11)'));
            $Form = $this->createFormHelper();

			return compact('Form', 'modules');
        }

        public function edit($params)
		{
            if ($params['id'] && !($module = $this->Module->find($params['id'])))
            {
                $this->errorBack('Запись не найдена');
            }
            elseif (!$params['id'])
            {
                $module = $this->Module->create();
            }
if ($_GET['dev']) Debug::varDump($module->item);
            $this->page_title = 'Редактирование элемента';

            $Form = $this->createFormHelper($module);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

//                $module->_item = $params['module'];

				if ($module->saveFromPost() && (!ModuleType::instance()->isEditable($module->type) || $module->item->saveFromPost()))
				{
                    $module->savePages($params['module']['show_in_all_pages'] ? Module::show_in_all_pages : $params['nodes']);

					if ($module->type == ModuleType::proposal_1 || $module->type == ModuleType::proposal_2)
					{
						$field_name = 'notice_proposal_' . ($module->type == ModuleType::proposal_1 ? '1' : '2');
						$this->mic->$field_name = $params['module']['notice'];
						$this->mic->saveWithoutValidation();
					}
					
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}
            else
            {
                $Form->object->setAttributesFromGet();
				$Form->object->show_in_all_pages = $Form->object->showInAllPages();
				
				if ($Form->object->type == ModuleType::proposal_1 || $Form->object->type == ModuleType::proposal_2)
				{
					$field_name = 'notice_proposal_' . ($module->type == ModuleType::proposal_1 ? '1' : '2');					
					$Form->params['module']['notice'] = $this->mic->$field_name;
				}					
            }

            $js_tree_helper = new JsTree_PageHelper(new Page);
            $js_tree_helper->expand_nodes = ActiveRecord::factory('Page')->findAll()->toArray('id');
            $js_tree_helper->tools_class = 'JsCheckTreeTools';
            $js_tree_helper->urls = array(
                'tool_base'		=> "/admin/modules_pages/",
                'load_data'		=> "/admin/modules_pages/load-data/".($module->isNewRecord() ? '' : '?module_id=' . $module->id . '&id='),
                'update_tree'	=> "/admin/modules_pages/update-tree/",
                'add_admin'		=> "/admin/modules_pages/add-admin/",
                'remove_admin'	=> "/admin/modules_pages/remove-admin/"
            );

			return compact('Form', 'js_tree_helper');
		}

		public function drop($params)
		{
			if (!$params['id'] || !($module = $this->Module->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}
			
			$module->drop();
			$this->noticeBack('Запись удалена');
		}

        public function renderEditModule($module)
        {
            if (ModuleType::instance()->isEditable($module->type))
                $this->renderPartial('edit_' . $module->type, array('item'  => $module->item));
        }
	}
?>