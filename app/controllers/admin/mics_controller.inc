<?
	class Admin_MicsController extends AdminController
	{
		public $Mic;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Mic = new Mic();
		}

        public function edit($params)
		{
			$this->page_title = 'Элементы сайта';
            $mic = $this->Mic->findFirst();           

            $Form = $this->createFormHelper($mic);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();
				
				$mic->setAttributesFromPost();
				
				if ($mic->saveWithoutValidation())
				{
                    self::$Con->ttscommit();

					$this->noticeTo($this->Hlp->urlFor(), 'Изменения внесены');
				}

                self::$Con->ttsabort();
			}

			return compact('Form');
		}
		
		public function changePassword($params)
		{
			$this->page_title = 'Смена пароля';                     

            $Form = $this->createFormHelper();

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                if ($this->user->password == sha1($params['password_old']))
				{
					if ($params['password_new'] == $params['password_repeat'])
					{
						if (strlen($params['password_repeat']) < 6)
						{
							$Form->errors = 'Длина пароля должна быть не менее 6 символов';
						}
						else
						{
							$this->user->password = sha1($params['password_repeat']);
							$this->user->saveWithoutValidation();
							$this->noticeTo($this->Hlp->urlFor(), 'пароль изменен');
						}
					}
					else
					{
						$Form->errors = 'Пароли не совпадают';
					}
				}
				else
				{
					$Form->errors = 'Не верно введет старый пароль';
				}
			}

			return compact('Form');
		}
	}
?>