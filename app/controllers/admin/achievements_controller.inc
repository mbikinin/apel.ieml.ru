<?php
	class Admin_AchievementsController extends AdminController
	{
		public $Achievement;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Achievement = new Achievement();
		}

        public function index($params)
        {
            $this->page_title = 'Новости';
            $achievements = $this->Achievement->findAll();

			return compact('achievements');
        }

        public function edit($params)
		{
            if ($params['id'] && !($achievement = $this->Achievement->find($params['id'])))
            {
                $this->errorBack('Запись не найдена');
            }
            elseif (!$params['id'])
            {
                $achievement = $this->Achievement->create();
            }

            $this->page_title = 'Редактирование элемента';

            $Form = $this->createFormHelper($achievement);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

				if ($achievement->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}
			elseif (!$params['id'])
			{
				$Form->object->date = date('Y-m-d');
			}

			return compact('Form');
		}
		
		public function drop($params)
		{
			if (!$params['id'] || !($Achievement = $this->Achievement->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}
			
			$Achievement->drop();
			$this->noticeBack('Запись удалена');
		}
	}
?>