<?php

class Admin_ParseController extends AdminController
{
	public function start($params)
	{
		$file = '/home/s/siteieml/apel.ieml.ru/app/controllers/admin/issue_2.xml';	
		$stream = fopen($file, 'r');
		$xml = new Parser();die;
		
		while($data = fread($stream, 4096))
		{
			$xml->parse($data);
		}
		
		Debug::varDump($xml->xml);
		
		die;
	}
	
	public function simpleXml()
	{
		$file = '/home/s/siteieml/apel.ieml.ru/app/controllers/admin/issue_2011_3.xml';	
		$stream = fopen($file, 'r');		
		
		$xml = simplexml_load_file($file);
		$articles = array();
		$titles = array();
		$keywords = array();
		$literatures = array();
		$authors = array();
		$sections = array();
		$Archive_Author = new Archive_Author();
		$Archive_Article = new Archive_Article();
		$Archive_Keyword = new Archive_Keyword();
		$Archive_Section = new Archive_Section();
		
		$i = 0;
		
		foreach($xml->journal->issue->jseparate as $title)
		{			
			$titles[++$i] = trim(preg_replace("~\s+~", " ", (string)$title->segtitle));
		}
		
		$i = 0;
		
		self::$Con->ttsbegin();
		
		foreach($titles as $t)
		{
			if (!($section = $Archive_Section->findByName($t)))
			{
				$section = $Archive_Section->create(array('name' => $t));
				$section->save();
			}				
			
			$sections[++$i] = $section->id;
		}
		
		$i = 0;		
		
		foreach($xml->journal->issue->article as $a)
		{	
			$i++;
			$article = $Archive_Article->create();			

			$article->title = trim(preg_replace("~\s+~", " ", (string)$a->arttitles->arttitle));
			$article->abstract = trim(preg_replace("~\s+~", " ", (string)$a->abstracts->abstract->i));
			$article->udk = trim(preg_replace("~\s+~", " ", (string)$a->udk));
			$article->archive_section_id = $sections[$i];
			$articles[$i] = $article;
			
			$article->save();
			
			foreach($a->authors->author as $au)
			{
				$author = $Archive_Author->create();
				$author->fio = trim(preg_replace("~\s+~", " ", (string)$au->individInfo->surname . ' ' . (string)$au->individInfo->fname));
				$author->work = trim(preg_replace("~\s+~", " ", (string)$au->individInfo->auwork));
				$author->post = trim(preg_replace("~\s+~", " ", (string)$au->individInfo->auinf));
				
				$authors[$i][] = $author;
				$author->save(array('archive_article_id' => $article->id));
			}
			
			foreach($a->keywords->kwdGroup->keyword as $k)
			{
				$keyword = $Archive_Keyword->create();
				$keyword->string = trim(preg_replace("~\s+~", " ", (string)$k));
				
				$keywords[$i][] = $keyword;
				$keyword->save(array('archive_article_id' => $article->id));
			}
			
			foreach((array)$a->biblist->blistpart as $l)
			{
				$literature = new Archive_Literature();
				$literature->name = trim(preg_replace("~\s+~", " ", (string)$l));
				
				$literatures[$i][] = $literature;
				$literature->save(array('archive_article_id' => $article->id));
			}
		}
		
		self::$Con->ttscommit();
		
		die;
	}
}

?>
