<?
	class Admin_PageController extends Admin_TreeController
	{
		protected
			$Page;

		protected function onLoad()
		{
			parent::onLoad('Page');
		}
		
		public function beforeFilter()
        {
            parent::beforeFilter();
			
            if ($this->action == 'order')
            {
                unset(Helper::$sources['script'][Helper::$js_base . '/jquery.js']);
                $this->Hlp->needJs('prototype.js');
            }
        }

        public function index($params)
        {
            $this->page_title = 'Структура';

            $js_tree_helper = new $this->helper_class_name($this->TreeObj);
			$js_tree_helper->expand_nodes = $this->getOpenNodes();

			return compact('js_tree_helper');
        }

        public function edit($params)
		{
            $this->page_title = 'Редактирование элемента';

			if (!$Page = $this->findOrCreateByParent($params))
				$this->errorBack('Неизвестная страница');

			if (!$this->editable($Page))
				$this->errorBack('Невозможно редактировать страницу');

			$Page->type = PageType::html;
            $Form = $this->createFormHelper($Page);
if ($_GET['dev']) Debug::varDump($Page);
			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

                $Page->item_attrs = $this->params['page'];

                foreach ((array)$params['attributes'] as $attr)
                    $Page->_attributes[$attr['name']] = $attr['value'];

				if ($Page->saveFromPost())
				{
                    self::$Con->ttscommit();
					
                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}
            else
            {
                // $item_attrs = $Form->object->item instanceof ActiveRecord ? $Form->object->item->getAttributes() : array();
                // unset($item_attrs['id']);
                // $Form->params['page'] = $item_attrs;
				foreach (($Form->object->item instanceof ActiveRecord ? $Form->object->item->getAttributes() : array()) as $field_name => $v)
					$Form->params['page'][$field_name] = $Form->object->item->$field_name;

                // if ($Form->object->isNewRecord())
                // {
					// $Form->object->type = PageType::html;
                    // $Form->object->url = $Form->object->parent->url;
                // }
            }
			
			$Form->object->item->lang = $this->lang;
            
			return compact('Form');
		}
		
		function order($params)
		{
			if (!$c = $this->TreeObj->find($params['id']))
				$this->errorBack("Объект #{$params['id']} не найден");

			if (!$this->orderable($c))
				$this->errorBack('Упорядочение невозможно');

			$children_data = array();
			foreach ($c->getDirectChildren() as $ch)
			{
				$children_data[] = Hash::leaveKey($ch->getAttributes(), 'id', 'name');
			}
 
			$Frm = $this->createFormHelper($c);
			return compact('Frm', 'children_data');
		}
		
		public function changeOrder($params)
		{
			if (!$c = $this->TreeObj->find($params['id']))
				$this->errorBack("Объект #{$params['id']} не найден");
				
			$c->move($params['up']);
			
			$this->goBack();
		}		
	}
?>