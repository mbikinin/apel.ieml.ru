<?php
	class Admin_Archive_IssuesController extends AdminController
	{
		public $Issue;

		protected function onLoad()
		{
			parent::onLoad();

            $this->Issue = new Archive_Issue();
			$this->Article = new Archive_Article();
		}

		public function index($params)
		{
			$this->page_title = 'Номера';
			$list = $this->Issue->findAll();

			return compact('list');
		}

		public function add($params)
		{
			$articles = array();
			$titles = array();
			$keywords = array();
			$literatures = array();
			$authors = array();
			$sections = array();
			$Archive_Author = new Archive_Author();
			$Archive_Article = new Archive_Article();
			$Archive_Keyword = new Archive_Keyword();
			$Archive_Section = new Archive_Section();
			if ($params['article_id'] && !($article = $this->Article->find($params['article_id'])))
            {
                $this->errorBack('Запись не найдена');
            }
			elseif ($params['id'] && !($issue = $this->Issue->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}
			elseif (!$params['article_id'])
			{
				$article = $Archive_Article->create();
				$author = $Archive_Author->create();
				$article->archive_issue_id = $issue->id;
			}

            $Form = $this->createFormHelper($article);
			if (getenv('REQUEST_METHOD') == 'POST')
			{
				$article->populate_lang_versions = true;

				self::$Con->ttsbegin();

					$author = $Archive_Author->create();
					$author->populate_lang_versions = true;
					$author->save(array('archive_article_id' => $article->id));

				if ($article->saveFromPost())
				{
					self::$Con->ttscommit();

					$this->noticeBack('Изменения внесены');
				}

				self::$Con->ttsabort();
			}

            return compact('Form');
		}

        public function edit($params)
		{
            if ($params['id'] && !($issue = $this->Issue->find($params['id'])))
            {
                $this->errorBack('Запись не найдена');
            }
            elseif (!$params['id'])
            {
                $issue = $this->Issue->create();
            }

            $this->page_title = 'Редактирование элемента';

            $Form = $this->createFormHelper($issue);

			if (getenv('REQUEST_METHOD') == 'POST')
			{
                self::$Con->ttsbegin();

				if ($issue->saveFromPost())
				{
                    self::$Con->ttscommit();

                    $this->noticeBack('Изменения внесены');
				}

                self::$Con->ttsabort();
			}

			$article_list = $issue->articles;

			return compact('Form');
		}

		public function drop($params)
		{
			if (!$params['id'] || !($issue = $this->Issue->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}

			$issue->drop();
			$this->noticeBack('Запись удалена');
		}
		public function articleDrop($params)
		{
			if (!$params['id'] || !($issue = $this->Article->find($params['id'])))
			{
				$this->errorBack('Запись не найдена');
			}

			$issue->drop();
			$this->noticeBack('Запись удалена');
		}
		public function parse($params)
		{
			if (!$params['id'] || !($issue = $this->Issue->find($params['id'])))
				$this->errorBack('Номер не найден');

			if (getenv('REQUEST_METHOD') == 'POST')
			{
				$file = $issue->cfg['base_path'] . $issue->file;
				$stream = fopen($file, 'r');

				$xml = simplexml_load_file($file);
				$articles = array();
				$titles = array();
				$keywords = array();
				$literatures = array();
				$authors = array();
				$sections = array();
				$Archive_Author = new Archive_Author();
				$Archive_Article = new Archive_Article();
				$Archive_Keyword = new Archive_Keyword();
				$Archive_Section = new Archive_Section();

				self::$Con->ttsbegin();

				$count = 0;

				foreach($xml->journal->issue->jseparate as $titles)
				{
					$count++;
					foreach($titles->segtitle as $title)
					{
						$title_name = trim(preg_replace("~\s+~", " ", (string)$title));

						if (!$sections[$count])
						{
							if (!($section = $Archive_Section->find(ActiveRecord::factory('Translation')->findByTable_nameAndField_nameAndValue('archive_sections', 'name', $title_name)->tuple_id)))
							{
								$section = $Archive_Section->create();
								$section->lang = strtolower((string)get_value($title->attributes(), 'lang'));
								$section->name = $title_name;
								$section->save();
							}

							$sections[$count] = $section;
						}

						$section = $sections[$count];
						$section->lang = strtolower((string)get_value($title->attributes(), 'lang'));
						$section->name = $title_name;
						$section->save();
					}
				}

				$i = 0;

				foreach($xml->journal->issue->article as $a)
				{
					$i++;
					$article = $Archive_Article->create();
					$article->populate_lang_versions = true;

					foreach($a->arttitles->arttitle as $arttitle)
					{
						$article->lang = strtolower((string)get_value($arttitle->attributes(), 'lang'));
						$article->title = trim(preg_replace("~\s+~", " ", (string)$arttitle));
					}

					foreach($a->abstracts->abstract as $abstract)
					{
						$article->lang = strtolower((string)get_value($abstract->attributes(), 'lang'));
						$article->abstract = trim(preg_replace("~\s+~", " ", (string)$abstract->i));
					}


					$article->udk = trim(preg_replace("~\s+~", " ", (string)$a->udk));
					$article->archive_section_id = $sections[$i]->id;
					$article->archive_issue_id = $issue->id;
					$articles[$i] = $article;

					$article->save();

					foreach($a->authors->author as $au)
					{
						$author = $Archive_Author->create();
						$author->populate_lang_versions = true;

						foreach($au->individInfo as $l_au)
						{
							$author->lang = strtolower((string)get_value($l_au->attributes(), 'lang'));

							$author->fio = trim(preg_replace("~\s+~", " ", (string)$l_au->surname . ' ' . (string)$l_au->fname));
							$author->work = trim(preg_replace("~\s+~", " ", (string)$l_au->auwork));
							$author->post = trim(preg_replace("~\s+~", " ", (string)$l_au->auinf));
						}

						$authors[$i][] = $author;
						$author->save(array('archive_article_id' => $article->id));
					}

					foreach($a->keywords->kwdGroup as $kwdGroup)
					{
						foreach($kwdGroup as $k)
						{
							$counter++;

							if (!is_object($keyword = $keywords[$i][$counter]))
								$keyword = $Archive_Keyword->create();

							$keyword->populate_lang_versions = true;
							$keyword->lang = strtolower((string)get_value($kwdGroup->attributes(), 'lang'));
							$keyword->string = trim(preg_replace("~\s+~", " ", (string)$k));

							$keywords[$i][$counter] = $keyword;

							$keyword->save(array('archive_article_id' => $article->id));
						}

						$counter = 0;
					}

					if (!isset($a->nobiblist))
					{
						foreach($a->biblist as $b)
						{
							$counter = 0;

							foreach($b->blistpart as $l)
							{
								if (!is_object($literature = $literatures[$i][$counter]))
									$literature = ActiveRecord::factory('Archive_Literature')->create();

								$literature->populate_lang_versions = true;
								$literature->lang = strtolower((string)get_value($b->attributes(), 'lang'));
								$literature->name = trim(preg_replace("~\s+~", " ", (string)$l));

								$literatures[$i][] = $literature;
								$literature->save(array('archive_article_id' => $article->id));

								$counter++;
							}
						}

						// foreach($a->biblist as $b)
						// {
							// foreach($b->blistpart as $l)
							// {
								// $literature = new Archive_Literature();

								// $literature->populate_lang_versions = true;
								// $literature->lang = strtolower((string)get_value($b->attributes(), 'lang'));
								// $literature->name = trim(preg_replace("~\s+~", " ", (string)$l));

								// $literatures[$i][] = $literature;
								// $literature->save(array('archive_article_id' => $article->id));
							// }
						// }
					}
				}

				$issue->saveWithoutValidation(array('parsed' => 1));

				self::$Con->ttscommit();

				$this->noticeBack('Файл обработан');
			}

			return array('Form' => $this->createFormHelper($issue));
		}
	}
?>