<?php

abstract class ActiveRecord_Translation extends ActiveRecord
{
	static $current_lang;

	public
		$lang,
		$lang_fields,
		$translations,
		$populate_lang_versions = false;

	public function __construct()
	{
		parent::__construct();
		$this->lang = Lang::$default_lang;
		
		if (self::$current_lang)
			$this->lang = self::$current_lang;
	}

	public function __get($field_name)
	{
		if ($this->translatedField($field_name))
		{
			$translation = $this->getTranslation($field_name, $this->lang);
			if (!is_null($translation))
				return $translation;
		}
		return parent::__get($field_name);
	}

	public function __set($field_name, $value)
	{
		if ($this->translatedField($field_name))
		{
			if (!isset($this->translations))
				$this->loadTranslations();

			if (isset($this->translations[$this->lang][$field_name]))
			{
				$this->translations[$this->lang][$field_name]->value = $value;
			}
			else
			{
				$t = new Translation();
				$t->setAttributes(array(
					'table_name' => $this->getFullTableName(),
					'field_name' => $field_name,
					'lang' => $this->lang,
					'value' => $value
				));
				if (!$this->isNewRecord())
				{
					$t->tuple_id = $this->id;
				}

				$this->translations[$this->lang][$field_name] = $t;
			}

			return;
		}

		parent::__set($field_name, $value);
	}

	public function translatedField($field_name)
	{
		return isset($this->lang_fields) &&
			is_array($this->lang_fields) &&
			in_array($field_name, $this->lang_fields);
	}

	protected function afterSave()
	{
		if (isset($this->lang_fields) && is_array($this->lang_fields))
		{
			$Lang = Lang::instance();
			if (!$Lang->isValid($this->lang))
			{
				$this->lang = Lang::$default_lang;
			}

			if ($this->populate_lang_versions)
			{
				$this->populateLangVersions();
			}

			$this->saveLangFields();
		}
	}

	protected function populateLangVersions()
	{
		$stored_lang = $this->lang;

		$Lang = Lang::instance();
		foreach (array_keys($Lang->list) as $lang)
		{
			if ($lang != $stored_lang)
			{
				$this->lang = $lang;

				foreach ($this->lang_fields as $field_name)
				{
					$this->{$field_name} = $this->getTranslation($field_name, $lang);
				}

				$this->saveLangFields();
			}
		}

		$this->lang = $stored_lang;
	}

	protected function afterDrop()
	{
		$find_params = array(
			'where' => 'tuple_id = :id AND table_name = :table_name',
			'params' => array(
				'id' => $this->id,
				'table_name' => $this->getFullTableName()
			)
		);
		self::factory('Translation')->dropAll($find_params);
	}

	public function saveLangFields()
	{
		if (!isset($this->translations))
			$this->loadTranslations();

		foreach ($this->lang_fields as $field_name)
		{
			$t = isset($this->translations[$this->lang][$field_name])
				? $this->translations[$this->lang][$field_name]
				: NULL;

			if (is_null($t))
			{
				$t = new Translation();
				$t->setAttributes(array(
					'table_name' => $this->getFullTableName(),
					'field_name' => $field_name,
					'lang' => $this->lang,
				));

				$this->translations[$this->lang][$field_name] = $t;
			}

			$t->value = $this->{$field_name};
			if (is_null($t->tuple_id))
				$t->tuple_id = $this->id;

			$t->saveWithoutValidation();
		}
	}

	public function loadTranslations()
	{
		$this->translations = array();

		if ($this->isNewRecord())
			return;

		$find_params = array(
			'table_name' => $this->getFullTableName(),
			'where' => 'tuple_id = :id AND table_name = :table_name',
			'params' => array(
				'id' => $this->id,
				'table_name' => $this->getFullTableName()
			),
		);
		
		foreach (self::factory('Translation')->findAll($find_params) as $t)
		{
			if (!isset($this->translations[$t->lang]))
				$this->translations[$t->lang] = array();

			$this->translations[$t->lang][$t->field_name] = $t;						
		}
	}

	public function getTranslation($field_name, $lang)
	{
		if (!isset($this->translations))
			$this->loadTranslations();

		if (isset($this->translations[$lang][$field_name]))
		{
			return $this->translations[$lang][$field_name]->value;
		}
		else
		{
			return NULL;
		}
	}

	protected function afterCreate($obj)
	{
		$obj->lang = Lang::$default_lang;
		$obj->loadTranslations();
	}

	public function hasLangVersion($lang)
	{
		$Lang = Lang::instance();
		if (!$Lang->isValid($lang))
			return false;

		if (!isset($this->translations))
			$this->loadTranslations();

		return isset($this->translations[$lang]);
	}
}