<?php

class Parser
{
	public 
		$parser,
		$article_open,
		$row_open,
		$attrs,		
		$data,
		$counter,
		
		$id,
		$params,
		$data_count,
		$objects,
		$obj,
		$row_count,
		$Worker,
		$Proposal,
		$Group,
		$groups,
		$protocols,
		$certificates,
		$evidences,
		$result,
		$swap_options,
		$drop,
		$file_name,
		$xml,
		$drop_worker,
		$drop_company,
		$drop_user,
		$stop,
		$tag_name;
		
	function parser()
	{
		$this->parser = xml_parser_create();
		
		$this->counter = 0;
		
		xml_set_object($this->parser, &$this);
		xml_set_element_handler($this->parser, "tag_open", "tag_close");
		xml_set_character_data_handler($this->parser, "set_data");		
	}
	
	function tag_open($parser, $tagName, $attrs)
	{
		if ($tagName == 'ARTICLE' && $this->counter == 0)
		{
			$this->counter++;
			$this->article_open = true;			
		}
		
		$this->tag_name = $tagName;
	}
	
	function tag_close($parser, $tagName)
	{
		if ($tagName == 'ARTICLE')
		{			
			$this->article_open = false;
		}
	}
	
	function set_data($parser, $data)
	{
		if ($this->article_open && $this->counter == 1 && preg_replace("~\s+~", " ", $data) != ' '){
			var_dump($this->tag_name . ' ' . preg_replace("~\s+~", " ", $data)); echo '<br/ >';}
	}
	
	function parse($data)
	{
		xml_parse($this->parser, $data); 		
	}
	
}

?>