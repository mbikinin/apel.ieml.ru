<?php

abstract class Logon
{
	static public $Session;
	static protected $logged_user;

	static public function login($login, $password, $is_ipad = false)
	{
		$login = is_string($login) ? trim($login) : '';
		$password = is_string($password) ? trim($password) : '';

		$user = User::getUser($login, $password);
		if ($user instanceof User)
		{	
			self::$logged_user = $user;
			self::$Session->Set('user_id', self::$logged_user->id);
			
			return $user;
		}
		else
		{
			self::logout();
			return false;
		}
	}

	static public function logout()
	{
		self::$logged_user = NULL;

		if (isset(self::$ipad))
		{
			self::$ipad->logout();
			self::$ipad = NULL;
		}

		self::$Session->Wipe('user_id');		
	}

	static public function hashed($string)
	{
		return User::hashed($string);
	}

	static public function checkLoggedIn()
	{
		if (!self::$logged_user instanceof User)
		{
			$user_id = self::$Session->Get('user_id');
			if ($user_id !== false)
			{
				$user = ActiveRecord::factory('User')->find($user_id);
				if ($user instanceof User)
				{
					self::$logged_user = $user;
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}

	static public function getLoggedUser()
	{
		return ActiveRecord::factory('User')->find(self::$Session->get('user_id'));
	}
}