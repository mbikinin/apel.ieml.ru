<style>
.content_main {
    padding: 0 10% 0px 0;
}
</style>

<?php
$Form = $this->createFormHelper(new Proposal());

if ($notice = self::$Session->get('proposals', 'notice'))
{
	self::$Session->wipe('proposals', 'notice');
	echo $notice;
}	
elseif ($error = self::$Session->get('proposals', 'error'))
{	
	$Form->object->setAttributes(self::$Session->get('proposals', 'attrs'));
	self::$Session->wipe('proposals', 'error');
	self::$Session->wipe('proposals', 'attrs');
	echo $error;
}
	
?>

<div class="register_page">
	<div class="register">
		<p class="header"><?= $this->mic->form_subscriber ?></p>
		<?= $Form->startFormTag(array('multipart' => 1), array('controller' => '/proposals', 'action' => 'send', 'type' => ProposalType::subscription, 'lang' => $this->lang)) ?>
			<p><label><?= $this->mic->form_fio ?></label><?= $Form->textField('fio'); ?></p>
			<p><label><?= $this->mic->form_email ?></label><?= $Form->textField('email'); ?></p>
			<p><label><?= $this->mic->form_phone ?></label><?= $Form->textField('phone'); ?></p>
			<p><label><?= $this->mic->form_address ?></label><?= $Form->textField('address'); ?></p>
			<p class="header"><?= $this->mic->form_number ?></p>
			<p><label><i><?= $this->mic->form_issues ?></i></label><?= $Form->textArea('issues'); ?></p>
			<p class="header"><?= $this->mic->form_document ?></p>
			<p><label><i><?= $this->mic->form_copy ?></i></label><?= $Form->fileField('file'); ?></p>
			
			<?= $Form->submitTag($this->mic->form_send, array('class' => 'register_button')) ?>			
		<?= $Form->startFormTag() ?>
	</div>
</div>