<div class="auth">
	<?php if ($error) : ?>
		<div class="error">
			<b>ОШИБКА АВТОРИЗАЦИИ</b><br>
			Неправильное имя пользователя или пароль.
		</div>
	<?php endif ?>
	<?php if (!$this->user) : ?>
		<div class="userLogon">
			<form method="post" action="/agni/login.htm" id="login" name="login">
			<fieldset>
			<legend>Личный кабинет</legend>
				<p><label for="login">Логин</label>
				<input type="text" style="width:160px;" name="login" value=""></p>

				<p><label for="login">Пароль</label>
				<input type="password" style="width:160px;" name="password" value=""></p>

				<p><input type="image" class="submit" src="/design/images/submit.gif" name="submit" value=""></p>
			</fieldset>
			</form>
		</div>
	<?php else : ?>
		<p>Вы успешно авторизовались в системе!</p>
		<a href="/logout.htm">выйти</a>
	<?php endif ?>
</div>