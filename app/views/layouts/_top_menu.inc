<?php
$direct_children = MysqlTreeActiveRecord_Translation::factory('Page')->findAllByParent_id(1);
?>

<div class="main_menu">
	<ul>
		<?php
		foreach(/*$this->current_page->root->*/$direct_children as $c)
		{
		?>
			<li>
				<?php
				if ($c->id == $this->current_page->id)
				{
				?>
				<div class="current"><span><?= $c->name ?></span></div>
				<?php
				}
				elseif ($c->isParent($this->current_page))
				{
				?>
				<div class="current"><span><a href="<?= $c->url ?>"><?= $c->name ?></a></span></div>
				<?php
				}
				else
				{
				?>
				<a href="<?= $c->url . ($this->lang == Lang::$default_lang ? '' : '?lang=' . $this->lang) ?>"><span><?= $c->name ?></span></a>
				<?php
				}
				?>
			</li>
		<?php
		}
		?>
	</ul>
</div>