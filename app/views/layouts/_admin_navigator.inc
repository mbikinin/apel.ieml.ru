<ul id="navigation">
    <li><a href="/admin/page">Меню сайта</a></li>
	<li><a href="/admin/achievements">Достижения</a></li>
	<li><a href="/admin/news">Новости</a></li>
	<li><a href="/admin/proposals?filter[type]=<?= ProposalType::publication ?>">Заявки на публикацию</a></li>
	<li><a href="/admin/proposals?filter[type]=<?= ProposalType::subscription ?>">Заявки на подписку</a></li>
	<li><a href="/admin/archive/issues">Архив - номера</a></li>
	<li><a href="/admin/archive/years">Архив - годы</a></li>
    <li><a href="/admin/modules">Модули</a></li>
	<li><a href="/admin/mics/edit">Редактирование элементов сайта</a></li>
    <li><a href="/admin/mics/change_password">Изменить пароль</a></li>
    <li><a href="/auth/logout">Выйти</a></li>
</ul>