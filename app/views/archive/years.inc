<div class="content_main">

    <h1><?= $this->page_title ?></h1>

    <?php
    if (count($year->issues) > 0) {
        ?>
        <dl class="tabs">
            <?php
            foreach ($year->issues as $i) {
                ?>
                <dt <?= $this->params['issue_id'] == $i->id ? 'class="selected"' : '' ?>>№ <?= $i->id ?></dt>
                <dd <?= $this->params['issue_id'] == $i->id ? 'class="selected"' : '' ?>>
                    <div class="tab-content">

                        <?php
                        $archive_section_id = 0;
                        if (count($i->articles) > 0) {
                            ?>

                            <div class="journal_about">
                                <h3>
                                    <?php
                                        echo $this->lang == Lang::$default_lang ? $i->title . " (". $i->jabbrev . ")"
                                            : $i->getTranslation('jtitle', Lang::rus) . " (". $i->getTranslation('jabbrev', Lang::rus) . ")";

                                    ?>
                                </h3>
                                <div class="item">
                                    <?php
                                    echo $this->lang == Lang::$default_lang ?
                                        "ISSN печатной версии журнала : " . $i->jpissn :
                                        "Printed Version ISSN : " . $i->getTranslation('jpissn', Lang::rus);
                                    ?>
                                </div>
                                <div class="item">
                                    <?php
                                    echo $this->lang == Lang::$default_lang ?
                                        "ISSN электронной версии : " . $i->jeissn :
                                        "Electronic Version ISSN : " . $i->getTranslation('jeissn', Lang::rus);
                                    ?>
                                </div>
                                <div class="item">
                                    <?php
                                    echo $this->lang == Lang::$default_lang ?
                                        "DOI : " . $i->idoi :
                                        "DOI : " . $i->getTranslation('idoi', Lang::rus);
                                    ?>
                                </div>
                                <div class="item">
                                    <?php
                                    echo $this->lang == Lang::$default_lang ?
                                        "Дата издания : " . $i->jpubdayp ."/". $i->jpubmop ."/". $i->jpubyearp :
                                        "Publishing date : " . $i->getTranslation('jpubdayp', Lang::rus) ."/". $i->getTranslation('jpubmop', Lang::rus) ."/". $i->getTranslation('jpubyearp', Lang::rus);
                                    ?>
                                </div>

                            </div>

                            <table class="archive_list"><tbody>
                            <?php
                            foreach ($i->articles as $a) {
                                /*if ($a->archive_section_id != $archive_section_id) {
                                    $archive_section_id = $a->archive_section_id;
                                    ?>
                                    <h2 class="archive_section"><?= $a->section->name; ?></h2>
                                    <?php
                                }
                                <!-- <p class="archive_title">
                                    <?= $this->Hlp->linkTo($a->title, array('action' => 'article', 'id' => $a->id, 'lang' => ($this->lang == Lang::$default_lang ? null : $this->lang))) ?>
                                    <br>
                                    <span class="archive_author"><?= join(', ', $a->authors_hash) ?></span>
                                </p>-->*/
                                ?>
                                <?php if($this->lang == Lang::$default_lang ){?>
                                    <tr class="cat-list-row0">
                                        <td class="list-title">
                                            <?php
                                                echo $this->Hlp->linkTo($a->aOrigLangTitle,
                                                array('action' => 'show', 'id' => $a->id, 'lang' => ($this->lang == Lang::$default_lang ? null : $this->lang)));
                                            ?>
                                        </td>
                                    </tr>
                                <?php }
                                else{?>
                                    <tr class="cat-list-row0">
                                        <td class="list-title">
                                            <?php
                                            echo $this->Hlp->linkTo($a->getTranslation('atitle', Lang::rus),
                                                array('action' => 'show', 'id' => $a->id, 'lang' => ($this->lang == Lang::$default_lang ? null : $this->lang)));
                                            ?>
                                        </td>
                                    </tr>
                                <?php }?>
                                <?php
                            }
                            ?> </tbody></table>
                                <?php
                        } else {
                            echo "<br/><br/><p>Список статей пуст</p>";
                        }
                        ?>

                    </div>
                </dd>
                <?php
            }
            ?>

        </dl>
        <?php
    } else {
        ?>
        Нет номеров
        <?php
    }
    ?>

</div>

<script type="text/javascript">
    $(function () {
        $('dl.tabs dt').click(function () {
            $(this)
                .siblings().removeClass('selected').end()
                .next('dd').andSelf().addClass('selected');
        });
    });
</script>