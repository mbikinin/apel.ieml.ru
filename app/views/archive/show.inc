﻿<div class="article_page">
	<?php
		?>
		<div class="tab-content">
			<h3>DOI: <?php echo $this->lang == Lang::$default_lang ? $article->adoi
					: $article->getTranslation('adoi', Lang::rus); ?>
			</h3>
			<hr/><br/>
			<h1>
				<?php echo $this->lang == Lang::$default_lang ? $article->aOrigLangTitle
					: $article->getTranslation('atitle', Lang::rus); ?></h1>
			<hr/>
			<?php echo $this->lang == Lang::$default_lang ? $article->aOrigLangText
				: $article->getTranslation('aOrigLangTextEng', Lang::rus) ?>
			<br/>
			<strong><?php echo $this->lang == Lang::$default_lang ? "Keywords : " : "Ключевые слова : " ?></strong>
			<?php echo $this->lang == Lang::$default_lang ? $article->keywords
				: $article->getTranslation('keywords', Lang::rus) ?>
			<br/>
			<br/>
			<strong><?php echo $this->lang == Lang::$default_lang ? "Citation : " : "Цитирование : " ?></strong>

			<?php  echo $this->lang == Lang::$default_lang ? $article->citation
				: $article->getTranslation('citation', Lang::rus) ?>
		</div>
</div>