<div class="content_main">
	<h1><?= $this->Hlp->linkTo($article->issue->title, array('action' => 'years', 'id' => $article->issue->year->name, 'issue_id' => $article->archive_issue_id, 'lang' => ($this->lang == Lang::$default_lang ? null : $this->lang))); ?> <?= $this->lang == Lang::rus ? 'за' : 'by' ?> <?= $article->issue->year->name ?> <?= $this->lang == Lang::rus ? 'год' : 'year' ?></h1>
	
	<div class="article_page">
	
		<p class="article_udk"><?= $this->lang == Lang::rus ? 'УДК' : 'UDK'?> <?= strlen($article->udk) == 0 ? $article->getTranslation('udk', 'eng') : $article->udk ?></p>        
		<p class="article_section"><?= $article->section->name ?></p>
		<h2 class="article_title"><?= $article->title ?></h2>
		<?php
		foreach($article->authors as $a)
		{
		?>
		<p class="article_author"><?= $a->fio ?></p>
		<p class="article_author_position"><?= $a->post ?></p>
		<p class="article_adress"><?= $a->work ?></p>
		<?php
		}
		?>		
		<p class="article_keywords"><span class="article_subheader"><?= $this->mic->keyword ?></span><br /><?= join(', ', $article->keywords_hash) ?></p>
		<p class="article_subheader"><?= $this->mic->abstract ?></p>
		<p class="article_abstract"><?= $article->abstract ?></p>
		<p class="article_subheader"><?= $this->mic->literature ?></p>
		<ul>
			<?php
			foreach($article->literatures as $l)
			{
			?>
			<li><?= $l->processName() ?></li>
			<?php
			}
			?>
		</ul>
	</div>		
</div>