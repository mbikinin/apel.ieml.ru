<div class="content_main">

<h1>Результаты поиска</h1>

<?php 
if (count($articles))
{
?>
<dl class="tabs">
	<?php
	foreach($articles as $a)
	{
	?>
	<p class="archive_title">
		<?= $this->Hlp->linkTo($a->title, array('action' => 'article', 'id' => $a->id, 'lang' => ($this->lang == Lang::$default_lang ? null : $this->lang))) ?>
		<br>
		<span class="archive_author"><?= join(', ', $a->authors_hash) ?></span>
	</p>
	<?php
	}
	?>
</dl>
<?php
}
else
{
?>
	Не найдено ни одной записи
<?php
}
?>		
</div>