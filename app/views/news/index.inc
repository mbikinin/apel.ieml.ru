<h1><?= $this->page_title ?></h1>
<?php
foreach($list as $n)
{
?>
<table class="text">
	<tr>
		<td class="news">
			<p class="date"><?= $n->f_date ?></p>
			<h3><?= $this->Hlp->linkTo($n->title, array('action' => 'view', 'id' => $n->id, 'lang' => $this->lang)) ?></h3>
			<p><?= $n->preview ?></p>
		</td>
	</tr>
</table>
<?php
}
?>

<?= $pages ?>