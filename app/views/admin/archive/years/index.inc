<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php
if (count($years))
{
?>
<table class="table">
    <tr>
        <td>№</td>
        <td>Год</td>
        <td>Действия</td>
    </tr>
    <?php
    foreach($years as $n)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= $n->name ?></td>
        <td>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit', 'id' => $n->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $n->id), array('onclick' => $this->confirm)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    На данный момент нет ни одной записи
<?php
}
?>

<?= $this->Hlp->linkTo('добавить', array('action' => 'edit')); ?>
<br/><br/>
<?= $this->Hlp->linkTo('назад', $this->back); ?>


