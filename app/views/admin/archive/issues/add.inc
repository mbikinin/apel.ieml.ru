﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿<?php
/**
 * @var FormHelper $Form
 */
$Form;
?>

<h1>Добавить/Редактировать статью в Номер</h1>
<?php echo $Form->startFormTag(array('multipart' => true)) ?>

<div class="form">
    <div class="error"><?php echo $Form->errorMessages() ?></div>
    <table width="800" border="0">
        <tbody>
        <tr>
            <td colspan="4">
                <table width="100%" border="0">
                    <tbody>
                    <tr>
                        <td>Название статьи (на английском языке) *</td>
                        <td colspan="4">
                            <?php echo $Form->textField('atitle', array('size' => 55)) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Название статьи (на оригинальном языке)</td>
                        <td colspan="3">
                            <?php echo $Form->textField('aOrigLangTitle', array('size' => 55)) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Аннотация статьи (на оригинальном языке)</td>
                        <td colspan="3"><?php echo $Form->ckeditor('aOrigLangText') ?></td>
                    </tr>
                    <tr>
                        <td>Аннотация статьи (на английском языке)</td>
                        <td colspan="3"><?php echo $Form->ckeditor('aOrigLangTextEng') ?></td>
                    </tr>
                    <tr id="contributors" name="contributors">
                        <td colspan="6">
                            <table style="width:90%" id="conttab" name="conttab" style="margin-left: 15pt;">
                                <tbody>
                                <tr>
                                    <td nowrap="" colspan="6"><b>Информация об авторах статьи (на английском языке)</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Автор</td>
                                    <td><select name="arole" id="arole" size="1" tabindex="3">
                                            <option value="author">author</option>
                                            <option value="chair">chair</option>
                                            <option value="editor">editor</option>
                                            <option value="translator">translator</option>
                                        </select></td>
                                    <td>Имя&nbsp; <?php echo $Form->textField('authors[afname]', array('size' => 15)) ?><br/>
                                        Фамилия&nbsp;<?php echo $Form->textField('archive_authors[0][autor][alname]', array('size' => 15)) ?></td>
                                    <td>Тел.&nbsp;<input type="text" name="phone" size="15" tabindex="6"><br/>
                                        Email&nbsp;<input type="text" name="email" size="15" tabindex="7"></td>
                                    <td>Регалии&nbsp;<input type="text" name="regals" size="15" tabindex="5"></td>

                                    <td><input type="button" name="add contributors"
                                               onclick="javascript:addNextCont('conttab','person');"
                                               value="+ Автор" tabindex="7"></td>
                                </tr>
                                <tr>
                                    <td>Организация автора</td>
                                    <td><select name="aorgrole" id="aorgrole" size="1" tabindex="8">
                                            <option value="author">author</option>
                                            <option value="chair">chair</option>
                                            <option value="editor">editor</option>
                                            <option value="translator">translator</option>
                                        </select></td>
                                    <td colspan="3"><input type="text" name="aorg" size="55" tabindex="9"></td>
                                    <td><input type="button" name="add contributors"
                                               onclick="javascript:addNextCont('conttab','organization');"
                                               value="+ Организация" tabindex="10"></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>DOI*</td>
                        <td colspan="3">
                            <?php echo $Form->textField('adoi', array('size' => 55)) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Ключевые слова</td>
                        <td colspan="3">
                            <?php echo $Form->ckeditor('keywords') ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Цитирование</td>
                        <td colspan="3">
                        <?php echo $Form->ckeditor('citation') ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="">
                            Загрузить полный текст статьи
                        </td>
                        <td>
                            <?php echo $Form->fileField('file') ?>
                            <? echo $Form->object->cfg['base_path'] . $Form->object->file ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="smp-bt-h">
        <p class="">
            <ins><?php echo $Form->submitTag('сохранить') ?></ins>
        </p>
    </div>
    <br/><br/>
    <ins><a href="/admin/archive/issues">Список журналов</a></ins>
</div>

<?php echo $Form->endFormTag() ?>