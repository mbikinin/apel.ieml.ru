<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php
if (count($list))
{
?>
<table class="table">
    <tr>
        <td>№</td>
		<td>Заголовок</td>
        <td>Год</td>
		<td>XML</td>
        <td>Действия</td>
    </tr>
    <?php
    foreach($list as $n)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= $n->title ?></td>
		<td><?= $n->year->name ?></td>
		<td><?= $n->parsed ? 'обработан' : $this->Hlp->linkTo('начать обработку', array('action' => 'parse', 'id' => $n->id)) ?></td>
        <td>
            <?= $this->Hlp->linkTo('добавить статью', array('action' => 'add', 'id' => $n->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit', 'id' => $n->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $n->id), array('onclick' => $this->confirm)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    На данный момент нет ни одной записи
<?php
}
?>

<?= $this->Hlp->linkTo('добавить', array('action' => 'edit')); ?>
<br/><br/>
<?= $this->Hlp->linkTo('назад', $this->back); ?>


