<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php $this->renderPartial('/admin/lang') ?>

<?php echo $Form->startFormTag(array('multipart' => true)) ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<!--<div class="line full-size">
			<label>Год</label>
			php echo $Form->select('archive_year_id', array('') + ActiveRecord::factory('Archive_Year')->getIdNameHash()) ?>
		</div>
		
		php
		if ($Form->object->isNewRecord())
		{
		?>
		<div class="line full-size">
			<label>Файл</label>
			php echo $Form->fileField('file') ?>
		</div>
		php
		}
		else	
		{
		?>
		1
		php
		}
		?>-->
		<br/><br/>
		<h1>Поля необходимые для формирования DOI</h1>
		<table width="100%" border="0">
			<tbody>
			<tr>
				<td align="right">Заголовок журнала (на русском языке)</td>
				<td colspan="3">
					<?php echo $Form->textField('title', array('size' => 55)) ?>
				</td>
			</tr>
			<tr>
				<td align="right">Заголовок журнала (на английском языке)</td>
				<td colspan="3">
					<?php echo $Form->textField('jtitle', array('size' => 55)) ?>
				</td>
			</tr>
			<tr>
				<td align="right">Сокращенное название журнала (на английском языке)</td>
				<td>
					<?php echo $Form->textField('jabbrev', array('size' => 55)) ?>
				</td>


			</tr>
			<tr>
				<td align="right">ISSN печатной версии журнала (не менее 8 символов)</td>
				<td colspan="4">
					<?php echo $Form->textField('jpissn', array('size' => 20)) ?>
					ISSN электронной версии журнала
					<?php echo $Form->textField('jeissn', array('size' => 20)) ?>
				</td>
			</tr>
			<tr>
				<td align="right">Volume</td>
				<td colspan="4">
					<?php echo $Form->textField('jvol', array('size' => 10)) ?>Issue
					<?php echo $Form->textField('jissue', array('size' => 10)) ?>
				</td>
			</tr>
			<tr>
				<td align="right">DOI Журнала</td>
				<td>
					<?php echo $Form->textField('idoi', array('size' => 35)) ?>
				</td>
			</tr>
			<tr>
				<td align="right" style="font-weight:bold;">Дата издания печатной версии журнала</td>
			</tr>
			<tr>
				<td align="right">*Год</td>
				<td colspan="4">
					<?php echo $Form->textField('jpubyearp', array('size' => 4)) ?>
					 Месяц: <?php echo $Form->textField('jpubmop', array('size' => 4)) ?>
					День: <?php echo $Form->textField('jpubdayp', array('size' => 4)) ?>
				</td>
			</tr>
			<tr>
				<td align="right" style="font-weight:bold;">Дата издания электронной версии журнала</td>
			</tr>
			<tr>
				<td align="right">*Год</td>
				<td colspan="4">
					<?php echo $Form->textField('jpubyearo', array('size' => 4)) ?>
					Месяц: <?php echo $Form->textField('jpubmoo', array('size' => 4)) ?>
					День: <?php echo $Form->textField('jpubdayo', array('size' => 4)) ?>
				</td>
			</tr>
			</tbody>
		</table>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>

<br/><br/>
<h1>
	Список статей в данном журнале
</h1>
<table>
	<?php

		foreach($Form->object->articles as $article){
			echo "<tr><td>";
			echo $this->Hlp->linkTo($article->atitle ." (" . $article->aOrigLangTitle . ")", array('action' => 'add', 'article_id' => $article->id));
			echo "</td><td>" . $this->Hlp->linkTo('удалить',
												array('action' => 'articleDrop', 'id' => $article->id), array('onclick' => $this->confirm));
			echo "</td></tr>";
		}
	?>
	</table>

