<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		Начать обработку файла номера <?= $Form->object->title ?> ?
		
		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('Да') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>