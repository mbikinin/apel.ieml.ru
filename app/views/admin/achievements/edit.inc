<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php $this->renderPartial('/admin/lang') ?>

<?php echo $Form->startFormTag(array('multipart' => true)) ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>
		
        <div class="line full-size">
			<label>Текст</label>
			<?php echo $Form->textArea('text') ?>
		</div>

        <div class="line full-size">
			<label>Картинка</label>
			<?php
			if ($Form->object->picture)
			{
			?>
			<img src="<?= $Form->object->url ?>" />
			<?php
			}
			?>			
			<?php echo $Form->fileField('picture') ?>
		</div>
		
        <!--<div class="line full-size">
			<label><?php echo $Form->checkBox('show') ?> опубликовано</label>
		</div>-->

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>