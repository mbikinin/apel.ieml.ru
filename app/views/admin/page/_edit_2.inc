<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<div class="line full-size">
    <label>Текст</label>
    <?php echo $Form->ckeditor('text') ?>
</div>

<script>

CKEDITOR.replace('page_text',
{
	filebrowserBrowseUrl : '/js/ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : '/js/ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : '/js/ckfinder/ckfinder.html?type=Flash',
	filebrowserUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/',
	filebrowserImageUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/',
	filebrowserFlashUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
});

</script>