<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php $this->renderPartial('/admin/lang') ?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Название</label>
			<?php echo $Form->textField('name', array('size' => 50)) ?>
		</div>

		<!--<div class="line full-size">
			<label><?php echo $Form->checkBox('show_name') ?> отображать название</label>
		</div>-->

        <div class="line full-size">
			<label>URL</label>
			<?php echo $Form->textField('url') ?>
		</div>

		<div class="line full-size" style="display:none">
			<label>Тип</label>
			<?php echo $Form->select('type', PageType::instance()->list) ?>
		</div>

        <!--<div class="line full-size">
			<div>Опубликовать <?php echo $Form->checkBox('show', array(), 1, 0); ?></div>
		</div>-->

        <?php
        if (Page::$item_classes[$Form->object->type])
            $this->renderPartial('edit_' . $Form->object->type, array('Form'));
        ?>

        <!--<div id="attributes">
            <ul>
            <?php
            $a_index = 0;
            foreach($Form->object->attributes_arr as $name => $value)
            {
            ?>
                <li id="file_<?php echo ++$a_index; ?>" class="file_row">
                    <input type="text" name="attributes[<?php echo $a_index; ?>][name]" size="30" value="<?php echo $name; ?>" />
                    <input type="text" name="attributes[<?php echo $a_index; ?>][value]" size="30" value="<?php echo $value; ?>" />&mdash;
                    [ <a href="javascript:" onclick="dropAttribute(<?php echo $a_index; ?>)">&times;</a> ]
                </li>
            <?php
            }
            ?>
            </ul>
            + <a href="javascript:" onclick="addAttribute()">Добавить аттрибут</a>
        </div>-->

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>

<script type="text/javascript">

var a_index = <?= $a_index + 1 ?>;

function addAttribute()
{
	var html = '<li id="attribute_' + a_index + '">';

	html += '<input type="text" name="attributes[' + a_index + '][name]" size="30" />';
	html += ' &mdash; ';
	html += '<input type="text" name="attributes[' + a_index + '][value]" size="30" />';

	html += '[ <a href="javascript:" onclick="dropAttribute(' + a_index + ')">&times;</a> ]';
	html += '</li>';

	a_index++;

	$("#attributes ul").append(html);
}

function dropAttribute(a_index)
{
    $('#attribute_' + a_index).remove();
}

</script>