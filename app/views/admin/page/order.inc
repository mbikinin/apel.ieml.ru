<h1>Упорядочение подкатегорий категории <?= $Frm->object->name ?></h1>

<?= $Frm->errorMessages() ?>
<?= $Frm->startFormTag() ?>

<?php
if ($count = count($children_data))
{
?>
<ul>
	<?php
	foreach($children_data as $attrs)
	{
		$i++;
	?>
	<li><?= $attrs['name'] ?> <?= $i != 1 ? $this->Hlp->linkTo('вверх', array('action' => 'change_order', 'up' => 1, 'id' => $attrs['id'])) : '' ?> <?= $i != $count ? $this->Hlp->linkTo('вниз', array('action' => 'change_order', 'up' => 0, 'id' => $attrs['id'])) : '' ?></li>
	<?php
	}
	?>
</ul>
<?php
}
else
{
?>
Нет потомков
<?php
}
?>

<div class="right">
	<?= $Frm->buttonTo('Назад', $this->back) ?>
</div>

<?= $Frm->endFormTag() ?>