<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>

		<div class="line full-size">
			<label>Заголовок</label>
			<?php echo $Form->textField('title', array('size' => 50)) ?>
		</div>

        <div class="line full-size">
			<label>Тип</label>
			<?php echo $Form->select('type', array('') + ModuleType::instance()->list) ?>
		</div>

        <div class="line full-size">
			<label>Область</label>
			<?php echo $Form->select('area', array('') + ModuleArea::instance()->list) ?>
		</div>
		
		<?php
		if (is_object($Form->object->item))
			$this->renderPartial('module_' . $Form->object->type);
		?>

        <div class="line full-size">
			<label><?php echo $Form->checkBox('show') ?> опубликовано</label>
		</div>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>

        <div>
            <label>Отображать</label>

            <?= $js_tree_helper->out() ?>
        </div>

		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>