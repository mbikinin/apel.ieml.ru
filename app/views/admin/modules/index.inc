<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag() ?>
<!--
	<div class="form">

        <ins><?php echo $Form->submitTag('сохранить') ?></ins>
    </div>-->

<?php echo $Form->endFormTag() ?>

<?php
if (count($modules))
{
?>
<table class="table">
    <tr>
        <td>№</td>
        <td>Название</td>
        <td>Область</td>
        <td>Тип</td>
        <!--<td>Опубликован</td>-->
        <td>Действия</td>
    </tr>
    <?php
    foreach($modules as $m)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= $m->title ?></td>
        <td><?= $m->r_area ?></td>
        <td><?= $m->r_type ?></td>
        <!--<td><?= $m->show == 1 ? 'да' : 'нет' ?></td>-->
        <td>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit', 'id' => $m->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $m->id), array('onclick' => $this->confirm)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    На данный момент нет ни одного модуля
<?php
}
?>

<!--<?= $this->Hlp->linkTo('добавить', array('action' => 'edit')); ?>-->


