<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag() ?>

	<div class="form">
		<div class="error"><?php echo $Form->errors ?></div>

		<div class="line full-size">
			<label>Старый пароль</label>
			<?php echo $Form->passwordField('password_old') ?>
		</div>

        <div class="line full-size">
			<label>Новый пароль</label>
			<?php echo $Form->passwordField('password_new') ?>
		</div>
		
		<div class="line full-size">
			<label>Новый пароль ещё раз</label>
			<?php echo $Form->passwordField('password_repeat') ?>
		</div>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>
		
		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>