<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php $this->renderPartial('/admin/lang') ?>

<?php echo $Form->startFormTag(array('multipart' => true)) ?>

	<div class="form">
		<div class="error"><?php echo $Form->errorMessages() ?></div>
		
		<div class="line full-size">
			<label>Заголовок</label>
			<?php echo $Form->textField('header') ?>
		</div>
		
		<div class="line full-size">
			<label>ISSN</label>
			<?php echo $Form->textField('issn') ?>
		</div>
		
		<div class="line full-size">
			<label>Картинка на главной странице</label>
			<?php echo $Form->fileField('main_picture') ?>
		</div>
		
		<h3>Форма заявки</h3>
		
		<ul>
			<li><?php echo $Form->textField('form_fio') ?></li>
			<li><?php echo $Form->textField('form_email') ?></li>
			<li><?php echo $Form->textField('form_phone') ?></li>
			<li><?php echo $Form->textField('form_address') ?></li>
			<li><?php echo $Form->textField('form_number') ?></li>
			<li><?php echo $Form->textField('form_issues') ?></li>
			<li><?php echo $Form->textField('form_document') ?></li>
			<li><?php echo $Form->textField('form_copy') ?></li>
			<li><?php echo $Form->textField('form_subscriber') ?></li>
			<li><?php echo $Form->textField('form_send') ?></li>
			<li><?php echo $Form->textField('form_pub') ?></li>
		</ul>
		
		
		<div class="line full-size">
			<label>Текст после отправки Заявки на публикацию</label>
			<?php echo $Form->ckeditor('notice_proposal_1') ?>
		</div>
		
		<div class="line full-size">
			<label>Текст после отправки Заявки на подписку</label>
			<?php echo $Form->ckeditor('notice_proposal_2') ?>
		</div>
		
		<div class="line full-size">
			<label>Email</label>
			<?php echo $Form->textField('email') ?>
		</div>
		
        <div class="line full-size">
			<label>Футер правая часть</label>
			<?php echo $Form->ckeditor('powered') ?>
		</div>

        <div class="line full-size">
			<label>Футер левая часть</label>
			<?php echo $Form->ckeditor('footer') ?>
		</div>
		
		<div class="line full-size">
			<label>Яндекс Директ</label>
			<?php echo $Form->textArea('yandex_direct') ?>
		</div>

		<div class="smp-bt-h">
			<p class="">
				<ins><?php echo $Form->submitTag('сохранить') ?></ins>
			</p>
		</div>
		
		<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
	</div>

<?php echo $Form->endFormTag() ?>