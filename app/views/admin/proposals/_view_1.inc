<div>
	<div class="line full-size">
		<label>ФИО</label><br/>
		<?= $proposal->fio ?>
	</div>
	<br/>
	<div class="line full-size">
		<label>Электронная почта</label><br/>
		<?= $proposal->email ?>
	</div>
	<br/>
	<div class="line full-size">
		<label>Контактный телефон</label><br/>
		<?= $proposal->phone ?>
	</div>
	<br/>
	<div class="line full-size">
		<label>Публикация в zip-архиве</label><br/>
		<?= $this->Hlp->linkTo('скачать', $proposal->file_url) ?>
	</div>
	<br/>
	<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>
</div>