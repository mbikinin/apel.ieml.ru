<?php
if (count($gallery->photos))
{
?>
<table class="table">
    <tr>
        <td>№</td>
        <td>Заголовок</td>
        <td>Фотография</td>
        <td>Действия</td>
    </tr>
    <?php
    foreach($gallery->photos as $p)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= $p->title ?></td>
        <td><img src="<?= $p->getUrl() ?>" height="100px" /></td>
        <td>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit_photo', 'id' => $p->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $p->id)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    На данный момент нет ни одной фотографии
<?php
}
?>

<?= $this->Hlp->linkTo('добавить', array('action' => 'edit_photo', 'gallery_id' => $gallery->id)); ?>
<br/><br/>
<ins><?php echo $this->Hlp->linkTo('назад', $this->back) ?></ins>


