<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php echo $Form->startFormTag() ?>
<!--
	<div class="form">

        <ins><?php echo $Form->submitTag('сохранить') ?></ins>
    </div>-->

<?php echo $Form->endFormTag() ?>

<?php
if (count($galleries))
{
?>
<table class="table">
    <tr>
        <td>№</td>
        <td>Название</td>
        <td>Опубликован</td>
        <td>Действия</td>
    </tr>
    <?php
    foreach($galleries as $g)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= $g->title ?></td>
        <td><?= $g->r_show ?></td>
        <td>
            <?= $this->Hlp->linkTo('фотографии', array('action' => 'photos', 'id' => $g->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit', 'id' => $g->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $g->id)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    На данный момент нет ни одной фотогаллереи
<?php
}
?>

<?= $this->Hlp->linkTo('добавить', array('action' => 'edit')); ?>


