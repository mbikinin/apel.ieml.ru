<?php
	/**
	* @var FormHelper $Form
	*/
	$Form;
?>

<?php
if (count($news))
{
?>
<table class="table">
    <tr>
        <td>№</td>
        <td>Заголовок</td>
        <td>Дата</td>
        <!--<td>Опубликован</td>-->
        <td>Действия</td>
    </tr>
    <?php
    foreach($news as $n)
    {
    ?>
    <tr>
        <td><?= ++$count ?></td>
        <td><?= $n->title ?></td>
        <td><?= $n->f_date ?></td>
        <!--<td><?= $n->show == 1 ? 'да' : 'нет' ?></td>-->
        <td>
            <?= $this->Hlp->linkTo('редактировать', array('action' => 'edit', 'id' => $n->id)) ?>
            <br/>
            <?= $this->Hlp->linkTo('удалить', array('action' => 'drop', 'id' => $n->id), array('onclick' => $this->confirm)) ?>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php
}
else
{
?>
    На данный момент нет ни одной новости
<?php
}
?>

<?= $this->Hlp->linkTo('добавить', array('action' => 'edit')); ?>


